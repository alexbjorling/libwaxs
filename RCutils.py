# -*- coding: utf-8 -*-
"""
This file contains consero-style data rejection and reduction functions.
Author Dr. Chris N. van Dijk.
"""
import collections
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from mpl_toolkits.axes_grid1 import AxesGrid
from matplotlib.ticker import Locator
import logging
import datetime

mad2std = 1.4826
Sn2std = 1.0
Qn2std = 2.21914446
now = datetime.datetime.now()

def sprint_array(arr, fmt="%s", delimiter=" "):
    """
    Convert a 1D array, list or tuple to a string.

    Args:
        arr (array): 1D array to be printed

    Kwargs:
        delimiter (str): character separating values in returned string
        fmt (str): format to print values with

    Returns:
        A string.
    """
    f = ((fmt + delimiter) * len(arr))[:-len(delimiter)]
    return f % tuple(arr)


def mean_with_error(data, data_err, mask=None, **kwargs):
    """
    Calculates the mean of data and propagation of error in data_err,
    optionally ignoring points where mask == False

    Args:
        data (array): data to be averaged
        data_err (array): error of data to be averaged

    Kwargs:
        mask (bool array): same shape as data and data_err, only values where
            mask == True will be used for computation
        Remaining kwargs are passed directly to np.sum

    Returns:
        A 2-tuple of averaged data and the propagated error
    """
    if mask is not None:
        if not data.shape == data_err.shape == mask.shape:
            raise ValueError("all arrays must have the same shape")
        mask_sum = mask.sum(**kwargs)
        return [masked_sum(data, mask, **kwargs) / mask_sum,
                np.sqrt(masked_sum(data_err**2, mask, **kwargs)) / mask_sum]
    else:
        if "axis" in kwargs:
            return [np.mean(data, **kwargs),
                    np.sqrt(np.sum(data_err**2, **kwargs)) / data_err.shape[kwargs['axis']]]
        else:
            return [np.mean(data, **kwargs),
                    np.sqrt(np.sum(data_err**2, **kwargs)) / data_err.size]


def mean_with_error_from_std(data, data_err, mask=None, **kwargs):
    """
    Calculates the mean of data the error determined from the standard deviation,
    optionally ignoring points where mask == False

    Args:
        data (array): data to be averaged
        data_err (array): IGNORED!

    Kwargs:
        mask (bool array): same shape as data and data_err, only values where
            mask == True will be used for computation
        Remaining kwargs are passed directly to np.sum

    Returns:
        A 2-tuple of averaged data and the error determined from the standard deviation
    """

    if mask is not None:
        if not data.shape == data_err.shape == mask.shape:
            raise ValueError("all arrays must have the same shape")
        mask_sum = mask.sum(**kwargs) #returns number of valid points
        return [masked_sum(data, mask, **kwargs) / mask_sum,
                masked_std(data, mask, **kwargs) / np.sqrt(mask_sum)]
    else:
        if "axis" in kwargs:
            return [np.mean(data, **kwargs),
                    np.std(data, **kwargs) / np.sqrt(data.shape[kwargs['axis']])]
        else:
            return [np.mean(data, **kwargs),
                    np.std(data, **kwargs) / np.sqrt(data.size)]


def median_with_error_from_mad(data, data_err, mask=None, **kwargs):
    """
    Calculates the mean of data the error determined from the median absolute deviation (MAD),
    optionally ignoring points where mask == False

    Args:
        data (array): data to be averaged
        data_err (array): IGNORED!

    Kwargs:
        mask (bool array): same shape as data and data_err, only values where
            mask == True will be used for computation
        Remaining kwargs are passed directly to np.sum

    Returns:
        A 2-tuple of averaged data and the error determined from the MAD
    """
    if 'madFactor' in kwargs:
        madFactor = kwargs['madFactor']
    else:
        # from https://en.wikipedia.org/wiki/Median_absolute_deviation
        madFactor = 1.4826
    
    if mask is not None:
        if not data.shape == data_err.shape == mask.shape:
            raise ValueError("all arrays must have the same shape")
        mask_sum = mask.sum(**kwargs) #returns number of valid points
        return [masked_median(data, mask, **kwargs),
                masked_mad(data, mask, **kwargs) * madFactor / np.sqrt(mask_sum)]
    else:
        if "axis" in kwargs:
            return [np.mean(data, **kwargs),
                    mad(data, **kwargs) * madFactor / np.sqrt(data.shape[kwargs['axis']])]
        else:
            return [np.mean(data, **kwargs),
                    mad(data, **kwargs) * madFactor/ np.sqrt(data.size)]


def masked_sum(data, mask, **kwargs):
    """
    Calculates the sum of data, ignoring points where mask == False

    Args:
        data (array): data to be summed
        mask (bool array): same shape as data, only values in data where
            mask == True will be used for summing

    Kwargs: passed directly to np.sum
    """
    mask = np.asarray(mask, dtype=bool)
    return (data * mask).sum(**kwargs)


def masked_mean(data, mask, **kwargs):
    """
    Calculates the mean of data, ignoring points where mask == False

    Args:
        data (array): data to be averaged
        mask (bool array): same shape as data, only values in data where
            mask == True will be used for averaging

    Kwargs: passed directly to np.sum
    """
    mask = np.asarray(mask, dtype=bool)
    return masked_sum(data, mask, **kwargs) / mask.sum(**kwargs)


def masked_std(data, mask, **kwargs):
    """
    Calculates the standard deviation of data, ignoring points where mask == False

    Args:
        data (array): data to calculate the standard deviation of
        mask (bool array): same shape as data, only values in data where
            mask == True will be used for calculations

    Kwargs: passed directly to np.sum
    """
    mask = np.asarray(mask, dtype=bool)
    return np.sqrt(masked_mean(
        (data - masked_mean(data, mask, **kwargs))**2, mask, **kwargs))


def masked_apply_along_axis(func1d, axis, arr, mask, *args, **kwargs):
    """
    Modified from np.apply_along_axis to only select values along each axis
    where mask == True.

    Args:
        func1d (function): this function is passed 1D slices of arr along axis,
            and should return a scalar.
        axis (int): axis along which arr is sliced.
        arr (array): input array.
        mask (bool array): same shape as data, only values in arr where
            mask == True will be passed to func1d.
        Remaining args and kwargs are passed on to func1d.

    Since fancy indexing is used along each axis for the mask, the input is
    guaranteed to not be modified.
    """
    arr = np.asarray(arr)
    nd = arr.ndim
    if axis < 0:
        axis += nd
    if (axis >= nd):
        raise ValueError("axis must be less than arr.ndim; axis=%d, rank=%d."
            % (axis,nd))
    ind = [0]*(nd-1)
    i = np.zeros(nd,'O')
    indlist = list(range(nd))
    indlist.remove(axis)
    i[axis] = slice(None,None)
    outshape = np.asarray(arr.shape).take(indlist)
    i.put(indlist, ind)
    res = func1d(arr[tuple(i.tolist())][mask[tuple(i.tolist())]], *args, **kwargs)
    #  if res is a number, then we have a smaller output array
    if not np.isscalar(res):
        raise ValueError("func1d must return a scalar")
    outarr = np.zeros(outshape,np.asarray(res).dtype)
    outarr[tuple(ind)] = res
    Ntot = np.product(outshape)
    k = 1
    while k < Ntot:
        # increment the index
        ind[-1] += 1
        n = -1
        while (ind[n] >= outshape[n]) and (n > (1-nd)):
            ind[n-1] += 1
            ind[n] = 0
            n -= 1
        i.put(indlist,ind)
        res = func1d(arr[tuple(i.tolist())][mask[tuple(i.tolist())]], *args, **kwargs)
        outarr[tuple(ind)] = res
        k += 1
    return outarr


def masked_median(data, mask, axis=None):
    """
    Calculates the median of data, ignoring points where mask == False.

    Args:
        data (array): data to compute the median of.
        mask (bool array): same shape as data, only values in data where
            mask == True will be used in computing the median.

    Kwargs:
        axis (int): axis to compute the median along. If None, median of all
            unmasked elements is calculated.
    """
    if axis is None:
        return np.median(data[mask], overwrite_input=True)
    else:
        return masked_apply_along_axis(np.median, axis, data, mask, overwrite_input=True)


def mad(data, axis=None, overwrite_input=False):
    """
    Calculates the median absolute deviation of data.

    Args:
        data (array): data to compute the MAD of.

    Kwargs:
        axis (int): axis to compute the MAD along. If None, MAD of all
            elements is calculated.
        overwrite_input (bool): modify input data, in order to save memory.
            Result will be fully or partially sorted.  Note: caution should be
            used when using overwrite_input=True with multidimensional arrays,
            especially in combination with axis>0.
    """
    if axis > 0:
        # ensure broadcasting works as expected
        data = np.rollaxis(data, axis, 0)
        axis = 0
    return np.median(
               np.abs(data - np.median(data, axis=axis, overwrite_input=overwrite_input)),
               axis=axis, overwrite_input=overwrite_input)


def masked_mad(data, mask, axis=None):
    """
    Calculates the median absolute deviation of data, ignoring points where
    mask == False.

    Args:
        data (array): data to compute the MAD of.
        mask (bool array): same shape as data, only values in data where
            mask == True will be used in computing the MAD.

    Kwargs:
        axis (int): axis to compute the MAD along. If None, MAD of all
            unmasked elements is calculated.
    """
    if axis is None:
        return mad(data[mask], overwrite_input=True)
    else:
        return masked_apply_along_axis(mad, axis, data, mask, overwrite_input=True)


def find_outliers_large_dy_mean(data_err, limit, mask=None, log=None, full_output=False):
    """
    Find points with an error (standard deviation) more than limit * the mean
    of the error, per pixel.

    Args:
        data_err (array): error of data, where multiple spectra are stacked
            row-wise.
        limit (float): points with an error more than limit * the mean of the
            error are considered outliers.

    Kwargs:
        log (logging.Logger instance): Logger instance to send messages to. If
            not specified, the default Logger for consero is used.
        mask (bool array): boolean array the same size as data_err, only points
            in data_err where mask is True will be used in computing the mean.
        full_output (bool): also return number of points removed

    Returns:
        A boolean array the same shape as data_err, where False represents
        outliers and True normal values. If full_output is True, a 2-tuple is
        returned where the second element is an array containing the number of
        points removed per pixel.

    The mean is calculated per pixel, and any points with an error more than
    limit * the mean of the error are considered outliers.

    If a mask is supplied the return array will already be masked and can be
    used directly, no further processing is required.
    """
    if mask is not None:
        mask = np.asarray(mask, dtype=bool)
    if not log:
        log = logging.getLogger(__prog__)

    # TODO: estimate minimum number of scans to do statistics on (consider statistical efficiency)
    if data_err.shape[0] < 20:
        log.warning("number of scans (%d) may be too low to reliably remove outliers",
                    data_err.shape[0])

    if mask is not None:
        data_err_mean = masked_mean(data_err, mask, axis=0)
        out = (data_err <= limit * data_err_mean) & mask
    else:
        data_err_mean = data_err.mean(axis=0)
        out = data_err <= limit * data_err_mean

    if not np.all(out):
        if mask is not None:
            diff = mask.sum(axis=0) - out.sum(axis=0)
        else:
            diff = out.shape[0] - out.sum(axis=0)
    else:
        diff = np.zeros(out.shape[1])
    if log.isEnabledFor(logging.WARNING):
        if np.any(diff):
            log.warning("npoints removed: [%s]", sprint_array(diff))
        if log.isEnabledFor(logging.DEBUG):
            for i in range(data_err.shape[1]):
                log.debug("pixel %2d: mean dy: %6.4g, OK/all: %d/%d",
                          i+1, data_err_mean[i], out[:,i].sum(), out.shape[0])

    if full_output:
        return [out, diff]
    else:
        return out


def find_outliers_large_dy_median(data_err, limit, mask=None, log=None, full_output=False):
    """
    Find points with an error (standard deviation) more than limit * the median
    of the error, per pixel.

    Args:
        data_err (array): error of data, where multiple spectra are stacked
            row-wise.
        limit (float): points with an error more than limit * the median of the
            error are considered outliers.

    Kwargs:
        log (logging.Logger instance): Logger instance to send messages to. If
            not specified, the default Logger for consero is used.
        mask (bool array): boolean array the same size as data_err, only points
            in data_err where mask is True will be used in computing the median.
        full_output (bool): also return number of points removed

    Returns:
        A boolean array the same shape as data_err, where False represents
        outliers and True normal values. If full_output is True, a 2-tuple is
        returned where the second element is an array containing the number of
        points removed per pixel.

    The median is calculated per pixel, and any points with an error more than
    limit * the median of the error are considered outliers.

    If a mask is supplied the return array will already be masked and can be
    used directly, no further processing is required.
    """
    if mask is not None:
        mask = np.asarray(mask, dtype=bool)
    if not log:
        log = logging.getLogger(__prog__)

    # TODO: estimate minimum number of scans to do statistics on (consider statistical efficiency)
    if data_err.shape[0] < 20:
        log.warning("number of scans (%d) may be too low to reliably remove outliers",
                    data_err.shape[0])

    if mask is not None:
        data_err_median = masked_median(data_err, mask, axis=0)
        out = (data_err <= limit * data_err_median) & mask
    else:
        data_err_median = np.median(data_err, axis=0)
        out = data_err <= limit * data_err_median

    if not np.all(out):
        if mask is not None:
            diff = mask.sum(axis=0) - out.sum(axis=0)
        else:
            diff = out.shape[0] - out.sum(axis=0)
    else:
        diff = np.zeros(out.shape[1])
    if log.isEnabledFor(logging.WARNING):
        if np.any(diff):
            log.warning("npoints removed: [%s]", sprint_array(diff))
        if log.isEnabledFor(logging.DEBUG):
            for i in range(data_err.shape[1]):
                log.debug("pixel %2d: median dy: %6.4g, OK/all: %d/%d",
                          i+1, data_err_median[i], out[:,i].sum(), out.shape[0])

    if full_output:
        return [out, diff]
    else:
        return out


def find_outliers_stdev(data, limit, mask=None, log=None, full_output=False):
    """
    Find points more than limit * stdev away from the mean of the data, per
    pixel.

    Args:
        data (array): input data, where multiple spectra are stacked row-wise.
        limit (float): points more than limit * stdev away from the mean are
            considered outliers.

    Kwargs:
        log (logging.Logger instance): Logger instance to send messages to. If
            not specified, the default Logger for consero is used.
        mask (bool array): boolean array the same size as data, only points in
            data where mask is True will be used in computing the median.
        full_output (bool): also return number of points removed

    Returns:
        A boolean array the same shape as data, where False represents outliers
        and True normal values. If full_output is True, a 2-tuple is returned
        where the second element is an array containing the number of points
        removed per pixel.

    The mean and stdev are calculated per pixel, and any values more than
    limit * stdev away from the pixel-wise mean are considered outliers.

    If a mask is supplied the return array will already be masked and can be
    used directly, no further processing is required.
    """
    if mask is not None:
        mask = np.asarray(mask, dtype=bool)
    if not log:
        log = logging.getLogger(__prog__)

    # TODO: estimate minimum number of scans to do statistics on (consider statistical efficiency)
    if data.shape[0] < 20:
        log.warning("number of scans (%d) may be too low to reliably remove outliers",
                    data.shape[0])

    if mask is not None:
        data_mean = masked_mean(data, mask, axis=0)
        data_stdev = masked_std(data, mask, axis=0)
        out = (np.abs(data - data_mean) / data_stdev <= limit) & mask
    else:
        data_mean = data.mean(axis=0)
        data_stdev = data.std(axis=0)
        out = np.abs(data - data_mean) / data_stdev <= limit

    if not np.all(out):
        if mask is not None:
            diff = mask.sum(axis=0) - out.sum(axis=0)
        else:
            diff = out.shape[0] - out.sum(axis=0)
    else:
        diff = np.zeros(out.shape[1])
    if log.isEnabledFor(logging.WARNING):
        if np.any(diff):
            log.warning("npoints removed: [%s]", sprint_array(diff))
        if log.isEnabledFor(logging.DEBUG):
            for i in range(data.shape[1]):
                log.debug("pixel %2d: mean: %6.4g, stdev: %6.4g, OK/all: %d/%d",
                          i+1, data_mean[i], data_stdev[i], out[:,i].sum(), out.shape[0])

    if full_output:
        return [out, diff]
    else:
        return out


def find_outliers_mad(data, limit, mask=None, log=None, full_output=False):
    """
    Find points more than limit * mad2std * MAD away from the median of the
    data, per pixel.

    Args:
        data (array): input data, where multiple spectra are stacked row-wise.
        limit (float): points more than limit * mad2std * MAD away from the
            median are considered outliers.

    Kwargs:
        log (logging.Logger instance): Logger instance to send messages to. If
            not specified, the default Logger for consero is used.
        mask (bool array): boolean array the same size as data, only points in
            data where mask is True will be used in computing the median.
        full_output (bool): also return number of points removed

    Returns:
        A boolean array the same shape as data, where False represents outliers
        and True normal values. If full_output is True, a 2-tuple is returned
        where the second element is an array containing the number of points
        removed per pixel.

    The median and MAD are calculated per pixel, and any values more than
    limit * mad2std * MAD away from the pixel-wise median are considered
    outliers.

    If a mask is supplied the return array will already be masked and can be
    used directly, no further processing is required.
    """
    __prog__ = "libwaxs"
    if mask is not None:
        mask = np.asarray(mask, dtype=bool)
    if not log:
        log = logging.getLogger(__prog__)
        

    # TODO: estimate minimum number of scans to do statistics on (consider statistical efficiency)
    if data.shape[0] < 20:
        log.warning("number of scans (%d) may be too low to reliably remove outliers",
                    data.shape[0])

    if mask is not None:
        data_median = masked_median(data, mask, axis=0)
        data_mad = mad2std * masked_mad(data, mask, axis=0)
        out = (np.abs(data - data_median) / data_mad <= limit) & mask
    else:
        data_median = np.median(data, axis=0)
        data_mad = mad2std * mad(data, axis=0)
        out = np.abs(data - data_median) / data_mad <= limit

    if not np.all(out):
        if mask is not None:
            diff = mask.sum(axis=0) - out.sum(axis=0)
        else:
            diff = out.shape[0] - out.sum(axis=0)
    else:
        diff = np.zeros(out.shape[1])
    if log.isEnabledFor(logging.WARNING):
        if np.any(diff):
            log.warning("npoints removed: [%s]", sprint_array(diff))
        data_mean = data.mean(axis=0)
        data_stdev = data.std(axis=0)
        if log.isEnabledFor(logging.DEBUG):
            for i in range(data.shape[1]):
                log.debug("pixel %2d: median/mean: %6.4g / %6.4g (%.2f), "
                          "MAD/stdev: %6.4g / %6.4g (%.2f), OK/all: %d/%d",
                          i+1, data_median[i], data_mean[i], data_median[i] / data_mean[i],
                          data_mad[i], data_stdev[i], data_mad[i] / data_stdev[i],
                          out[:,i].sum(), out.shape[0])

        medmean = np.abs(data_median - data_mean) > 1e-3 + 0.1 * np.abs(data_mean)
        if np.any(medmean):
            log.warning("median and mean do not agree well (10%%) for %d pixel(s), "
                        "data may not be normally distributed", medmean.sum())
            log.info("median / mean anomalous pixel(s): %s)", medmean.nonzero()[0]+1)
        madstd = np.abs(data_mad - data_stdev) > 1e-3 + 0.1 * np.abs(data_stdev)
        if np.any(madstd):
            log.warning("MAD and stdev do not agree well (10%%) for %d pixel(s), "
                        "data may not be normally distributed", madstd.sum())
            log.info("MAD/stdev anomalous pixel(s): %s)", madstd.nonzero()[0]+1)

    if full_output:
        return [out, diff]
    else:
        return out


def merge_reg(dat, ax, reg_ax, axis=0):

    import sp.interpolate.interp1d as interp

    f = interp(dat, ax, axis=axis, copy=False, fill_value=np.nan, assume_sorted=True, bounds_error=False)

    return f(reg_ax)


def outlier_rejection(dataset, func, limit, mask=None):

    mask, junked = func(dataset, limit, mask=mask, log=None, full_output=True)

    return mask

