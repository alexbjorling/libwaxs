
import re, os
from collections import Iterable
import matplotlib
###
def read_all_dS(fnAlldS):
    filenamesLines = tryOpen(fnAlldS).readlines()
    qRef    = []
    allS    = []
    allSerr = []
    for f1 in filenamesLines:
        f = f1.rstrip('\n')
        q1, S1, Serr1 = read_dat(f)
        if (qRef != []):
            if (q1[0] != qRef[0] or q1[-1] != qRef[-1] or len(q1) != len(qRef)):
                error('q range inconsistent in input S(q) files')
        else:
            qRef = [q1[i] for i in range(len(q1))]
        allS.append(   np.array(S1   ))
        allSerr.append(np.array(Serr1))
        
    return np.array(q1), allS, allSerr

def openInAllSubdirs(dirs, fn):
    fps = []
    for dir in dirs:
        fps.append(open(dir + '/' + fn, 'w'))
        
    return fps
###

def pickleData(data, path='./', addTime=True):
    if addTime:
        f = "{0}_{1}{2}_{3}".format(datetime.date.today().isoformat(), time.localtime()[3], time.localtime()[4], os.path.basename(path))
    else:
        f = "{0}".format(os.path.basename(path))
    fn = os.path.join(os.path.dirname(path), f)
    with open(f, 'wb') as handle:
        print("Pickling {0}".format(f))
        pickle.dump(data, handle)
        print("Pickled {0}".format(f))

def unpickleData(path='./'):
    if os.path.isfile(path):
        f = path
    else:
        raise IOError("{0} does not exist.".format(path))
    
    with open(glob.glob(f)[0], 'rb') as handle:
        print("Un-pickling {0}".format(f))
        data = pickle.load(handle)
    print("Loaded {0}".format(f))
    
    return data

def printFig(Dict, path='./fig.png', **kwargs):
    for key in Dict:
        head, tail = os.path.splitext(path)
        fn = '{0}{1}{2}'.format(head,str(key).replace('.','-'),tail)
        print('saving {0}'.format(fn))
        Dict[key].savefig(fn,**kwargs)

def _std2lim(A, lim):

    if not isinstance(A, Iterable) or type(A)==str:
        raise TypeError("A has to be an array-like")
    else:
        A = np.asarray(A)

    if type(lim)==type(None):
        lim = np.asarray([A.min(), A.max()])
    elif not isinstance(lim, Iterable) or type(lim)==str:
        raise TypeError("lim has to be an array-like")
    elif len(lim)!=2:
        raise ValueError("lim must have two elements")
    else:
        lim = np.asarray(lim)

    return lim

def stdIdxLim(A, lim, slice=False, limOnly=False, debug=False):
    """
    Helper function returns indices and values of points in an array.
    A (sorted 1darray-like)            : 
    lim (1darray-like with 2 elements) : 

    slice (bool)                       : If True, any index retreived that comes up as
                                            -1, is set to None.
    """

    A = np.asarray(A).squeeze()

    lim = _std2lim(A, lim)

    if type(lim)==type(None):
        lim = np.asarray([A.min(), A.max()])
    elif not isinstance(lim, Iterable) or type(lim)==str:
        raise TypeError("lim has to be an 1darray-like.")

    if type(lim[1])==type(None):
        lim[1]=A[-1]
    if type(lim[0])==type(None):
        lim[0]=A[0]

    lim = np.asarray(lim).squeeze()
    if lim.size>2:
        raise ValueError("lim has to contain only 2 elements.")

    if limOnly:
        return lim

    #Original
    #idx = get_lim(A, lim)
    
    idx = find_closest(A, lim)
    
    Lim = A[idx]
    
    if slice:
        IDX = idx.astype(object)
        if IDX[1]!=-1:
            IDX[1] += 1
        IDX[IDX==-1] = None
        IDX[IDX>=len(A)] = None
        if A[0]>A[-1]:
            IDX = np.hstack((IDX, -1))
    else:
        IDX = idx
    if debug:
        raise ValueError
    else:
        return (IDX, Lim)


def find_closest(A, target):
    """
    From: https://stackoverflow.com/questions/8914491/finding-the-nearest-value-and-return-the-index-of-array-in-python
    by:   Bi Rico
    A must be sorted
    """
    A = A.squeeze()
    A = np.atleast_1d(A)

    if A.size>1:
        if type(target)!=type(None):
            idx = A.searchsorted(target)
            idx = np.clip(idx, 1, len(A)-1)
            left = A[idx-1]
            right = A[idx]
            idx -= target - left < right - target
        else:
            idx = np.asarray([0, len(A)-1])
    else:
        idx = np.array([0])

    return idx


def get_lim(A,x):
    '''
    Helper function for idxpt().
    If values of x are beyond the range of A, they will be defined as the appropriate extremities of A.
    '''
    if type(x)==type(float()):
        x = [x]
    elif type(x)!=type(np.array([])) and type(x)!=type(list()) and type(x)!=type(tuple()):
        x = np.asarray(x)

    if type(x[0])==type(None):
        x[0]=A[0]

    if type(x[-1])==type(None):
        x[-1]=A[-1]


    A = np.asarray(A)

    idx = find_closest(A,x)

    if A[0]>A[-1]:
        for i,id in enumerate(idx):
            if id == len(A) and float(x[i])>A.min():
                idx[i] = 0
            elif id == len(A) and float(x[i])<A.max():
                idx[i] = -1
            else:
                continue
    else:
        for i,id in enumerate(idx):
            if id == len(A) and float(x[i])>A.min():
                idx[i] = -1
            elif id == len(A) and float(x[i])<A.max():
                idx[i] = 0
            else:
                continue
    return idx

def get_lim_legacy(A,x):
    '''
    Helper function for idxpt().
    If values of x are beyond the range of A, they will be defined as the appropriate extremities of A.
    '''
    if type(x)==type(float()):
        x = [x]
    elif type(x)!=type(np.array([])) and type(x)!=type(list()) and type(x)!=type(tuple()):
        x = np.asarray(x)

    A = np.asarray(A)

    idx = idxpt(A,x)

    if A[0]>A[-1]:
        for i,id in enumerate(idx):
            if id == len(A) and float(x[i])>A.min():
                idx[i] = 0
            elif id == len(A) and float(x[i])<A.max():
                idx[i] = -1
            else:
                continue
    else:
        for i,id in enumerate(idx):
            if id == len(A) and float(x[i])>A.min():
                idx[i] = -1
            elif id == len(A) and float(x[i])<A.max():
                idx[i] = 0
            else:
                continue
    return idx


def idxpt(A,x):
    '''
    Searches for indices of the values in a given array-like and returns the indices of the elements.
    Args:
            A (1D array-like): Array of sequential values. The array can be increasing or decreasing in value.
            x (1D array-like): Array of values of which the indices will be searched for in A.

    Returns:
            idx (list): indices of the given values.
    '''
    A = np.ravel(np.asarray(A))
    x = np.ravel(np.asarray(x))
    if A[0]>A[-1]:
        x=A[::-1].searchsorted(x, side='left')
        #x = find_closest(A[::-1], x)
        idx=(A.shape[0]-x)
    else:
        idx=A.searchsorted(x, side='left')
        #idx=find_closest(A, x)
    return idx

def det_sci(A):
    A = np.asarray(A)
    mask = A != 0
    E = np.zeros_like(A)
    C = np.zeros_like(A)
    E[mask] = np.floor(np.log10(np.abs(A[mask])))
    C = A/10**E
    return (C,E)


def resample(y, x, x_new, axis=0):
    from scipy import interpolate
    f = interpolate.interp1d(x, y, axis=axis, fill_value=0., bounds_error=False)
    return f(x_new)


def getRdf(q, dat, rExt=None, alpha=0.07, xLim=None, **kwargs):
    """
    Method that calculates radial distribution function from difference scattering
    by Fourier sine transformation as described in Kim et al., Nature 2015
    Input: scattering angle q, difference scattering dS and distance vector r
    """
    x = q.squeeze()

    dxFT = np.mean(np.diff(x))

    xIdx,xLim = stdIdxLim(x, xLim, slice=True)
    xSlice = slice(*xIdx)

    dat = np.atleast_2d(dat)

    if dat.shape[0] != x.shape[0]:
        if dat.T.shape[0]!=x.shape[0]:
            raise ValueError("Input arrays do not belong to one another.")
        else:
            dat = dat.T

    xFT = np.linspace(np.min(x[xSlice]),np.max(x[xSlice]),num=int(np.max(x[xSlice])/dxFT),endpoint=True)
    dFT = resample(dat[xSlice,:], x[xSlice], xFT, axis=0)
    dFT[np.isnan(dFT)] = 0.

    r = np.linspace(2*np.pi/xFT.min(),2*np.pi/xFT.max(),num=xFT.shape[0]).reshape(xFT.shape)

    expFac = np.exp(-(np.square(xFT))*alpha)
    rdf    = np.zeros(r.shape + (dat.shape[1],))
    for i in range(r.size):
        rdf[i,:] = (r[i]/(2*(np.pi**2))) * np.sum(dFT * (xFT[:,None] * np.sin(xFT[:,None]*r[i]) * expFac[:,None] * dxFT), axis=0, keepdims=True)

    r   = r[::-1]
    rdf = rdf[::-1,:]

    if type(rExt) != type(None):
        r = np.asarray(r)
        rdf = resample(rdf, r, rExt, axis=0)

    return (r, rdf)


def quick_cont(x,y,z, ax=None, cbax=None, xLim=None, yLim=None, zLim=None, my_cmap = 'RdBu_r',
               yLab=None, xLab=None, cLab='', title='', cntlvl=20, clrlvl=20, no_contLine = False, log=None,
               set_contlines=None,
               linthreshX=1., linthreshY=1., colorbar_location='right', colorbar_on=True,
               fig_kws={}, cont_kws={}):

    import matplotlib
    #matplotlib.rcParams['text.latex.preamble']=['\usepackage{mathptmx}']
    #matplotlib.rcParams['text.usetex'] = True
    import matplotlib.pyplot as plt

    my_cmap = getattr(matplotlib.cm, my_cmap)

    xLim = _std2lim(x, xLim)
    yLim = _std2lim(y, yLim)
    zLim = _std2lim(z, zLim)

    xIdx,xLim = fc.stdIdxLim(x, xLim, slice=True)
    xSlice=slice(*xIdx)
    yIdx,yLim = fc.stdIdxLim(y, yLim, slice=True)
    ySlice=slice(*yIdx)

    zLim = fc.stdIdxLim(z[xSlice,ySlice], zLim, limOnly=True)

    if type(set_contlines)!=type(None):
        set_contlines = np.asarray(set_contlines)
        contours = zeroRange(np.asarray([set_contlines.min(),set_contlines.max()]), set_contlines.size)
        colorlvl = np.linspace(zLim.min(),zLim.max(),num=clrlvl,endpoint=True)
        
    else:
        contours = zeroRange(zLim, cntlvl)
        colorlvl = np.linspace(zLim.min(),zLim.max(),num=clrlvl,endpoint=True)

    cbartick = zeroRange(contours, contours.size)

    newcmap = shiftedColorMap(my_cmap, midpoint=(1 - zLim.max()/(zLim.max() + abs(zLim.min()))), name='shifted')

    grid_x, grid_y = np.meshgrid(x[xSlice], y[ySlice], copy=False, indexing='ij')

    if type(ax)==type(None):
        fig, ax = plt.subplots(**fig_kws)
    else:
        fig = None

    if type(log)==type(None):
        pass
    elif log=='x':
        ax.set_xscale('symlog', linthreshx=linthreshX)
    elif log=='y':
        ax.set_yscale('symlog', linthreshy=linthreshY)
    else:
        raise ValueError("log kwarg can be \"None\", \"x\", or \"y\"")

    if log=='y':
        ax.set_yscale('symlog',linthreshy=linthreshY)
        ax.yaxis.set_minor_locator(MinorSymLogLocator(linthreshY))
    else:
        ax.yaxis.set_major_formatter(matplotlib.ticker.ScalarFormatter(useOffset=False, useMathText=True))
        ax.yaxis.set_minor_locator(matplotlib.ticker.AutoMinorLocator())
        ax.yaxis.set_major_locator(matplotlib.ticker.MaxNLocator(steps=[1,2,10], prune='lower'))
        
    if log=='x':
        ax.set_xscale('symlog',linthreshx=linthreshX)
        ax.xaxis.set_minor_locator(MinorSymLogLocator(linthreshX))
    else:
        ax.xaxis.set_major_formatter(matplotlib.ticker.ScalarFormatter(useOffset=False, useMathText=True))
        ax.xaxis.set_minor_locator(matplotlib.ticker.AutoMinorLocator())
        ax.xaxis.set_major_locator(matplotlib.ticker.MaxNLocator(steps=[1,2,10], prune='upper'))


    CK = {'hold':'on','colors':'0', 'linestyles':'solid', 'linewidths':0.5,}
    for key in list(CK.keys()):
        if key not in cont_kws:
            cont_kws[key] = CK[key]

    CP=ax.contour(grid_x,grid_y,z[xSlice,ySlice], levels=contours, vmin=zLim.min(), vmax=zLim.max(),
                  **cont_kws) if not no_contLine else None

    CP1=ax.contourf(grid_x,grid_y,z[xSlice,ySlice], levels=colorlvl, vmin=zLim.min(), vmax=zLim.max(),
                    cmap=newcmap)

    if colorbar_on:
        if type(cbax)!=type(None):
            cAx = cbax
        else:
            cAx,kw = matplotlib.colorbar.make_axes_gridspec(ax, pad=0, fraction=0.1)

        if colorbar_location == ('top' or 'bottom'):
            orientation = 'horizontal'
        else:
            orientation = 'vertical'
        #fmt = matplotlib.ticker.ScalarFormatter(useOffset=True, useMathText=True)
        #fmt.set_scientific(True)
        #fmt.set_powerlimits([-1e-5,1e5])

        #def fmt(x, pos):
            #a, b = '{:.2e}'.format(x).split('e')
            #b = int(b)
            #return r'${} \times 10^{{{}}}$'.format(a, b)
        
        cb = matplotlib.colorbar.Colorbar(cAx,CP1, drawedges=False, orientation=orientation,ticks=cbartick)

        #cb.ax.tick_params(axis=u'both', which=u'both',length=0)

        #cb.formatter.set_powerlimits((0,0))
        #cb.update_dict['array']=True
        #bork
        if colorbar_location=='top':
            #tick_labs,exp = makeExp(cbartick, fmt='%.1f', extremities=True) #[float(rex.match(el.get_text()).groups()[0]) for el in cb.ax.xaxis.get_ticklabels()]
            #cb.set_ticklabels([tick for tick in tick_labs])
            #cb.ax.set_xlabel(cLab+r'$/10^{%s}$'%(int(exp)))
            #cAx.xaxis.tick_top()
            cb.ax.xaxis.set_tick_params(which='major', reset=True, labeltop='on')
            cb.ax.xaxis.tick_top()
            cb.ax.xaxis.set_label_position('top')
            #cb.ax.xaxis.set_tick_params(which=u'major', reset=False, labelbottom='off')
            #bork
            cb.ax.set_xlabel(cLab)
            for tick in cb.ax.xaxis.get_ticklabels(): tick.set_verticalalignment('bottom')
            ##bork
        elif colorbar_location=='left':
            cb.ax.yaxis.set_label_position('left')
            cb.ax.yaxis.tick_left()
            #_,exp = makeExp(cb.ax.xaxis)
            #cb.ax.yaxis.set_label(cLab+r'$/10^{%s}$'%(int(exp)))
            #cb.ax.xaxis.tick_left()
            cb.ax.yaxis.set_label(cLab)
            for tick in cb.ax.yaxis.get_ticklabels(): tick.set_horizontalalignment('right')

        #cb.locator = matplotlib.ticker.MaxNLocator(nbins=5, offset=False)
        cb.update_ticks()
        #[tick.get_text() for tick in cb.ax.xaxis.get_ticklabels()]
    else:
        cb = None

    ax.set_ylabel(yLab)
    ax.set_xlabel(xLab)
    ax.title.set_text(title)

    return (fig,ax,cbax,cb)


def multi_plot(dataL, title=None, ncols=1, nrows=1, sharex=False, sharey=False,
               figsize=(6,6), **kwargs):

    import matplotlib
    matplotlib.rcParams['text.latex.preamble']=['\\usepackage{mathptmx}']
    matplotlib.rcParams['text.usetex'] = True
    import matplotlib.pyplot as plt

    if len(dataL)>ncols*nrows:
        nrows=len(dataL)

    fig, ax = plt.subplots(ncols=ncols, nrows=nrows, squeeze=True, 
                           sharex=sharex, sharey=sharey, figsize=figsize,
                           gridspec_kw={'wspace': 0.0,'hspace':0.0})

    if type(ax)!=np.ndarray:
        ax = np.asarray([ax])

    for i in range(len(dataL)):
        dat = dataL[i]
        
        fc.quick_plot(dat, ax=ax[i], title=None, **kwargs)

    if type(title)!=type(None):
        fig.suptitle(title)

    return (fig,ax)


def quick_plot(dataL, Ax=None, style=None, xLim=None, yLim=None, autoscaleY=False, autoscaleX=True, 
               log=None, linthreshX=1., linthreshY=1., 
               increment=False, inc_scale=1., increment_labels={}, increment_major_ticks=True, inc_token=None, 
               yLab=None, xLab=None, title=None,
               vertical_lines=None,zeroY=True, 
                gradBand=None, gradRes=10, base=10., 
               legend_ax=None, 
               legend_kws={'frameon':False,
                           'loc':0},
               fig_kws={},
               ):

    """
    data is a list of tuples: [(x1,y1,lab1),
                               (x2,y2,lab2),
                                ...
                               (xn,yn,labn),]
    increment_labels dict structure: 'x', 'y', 'text', **kwargs passed on to matplotlib.text
    """

    import matplotlib as mpl
    #matplotlib.rcParams['text.latex.preamble']=['\usepackage{mathptmx}']
    #matplotlib.rcParams['text.usetex'] = True
    import matplotlib.pyplot as plt

    def _update_kwarg_std(i, j, D, key):
        """
        returns a Dict
        """
        if key in D:
            if isinstance(D[key], collections.Iterable) and type(D[key])!=str:
                if len(D[key])>1:
                    return {key:D[key][j]}
                else:
                    return {key:D[key][0]}
            else:
                if j==0:
                    return {key:D[key]}
                else:
                    return {}
        else:
            return {}

    def _update_kwarg_stack(i, j, D, key):
        """
        returns a Dict
        """
        if key in D:
            if isinstance(D[key], collections.Iterable) and type(D[key])!=str:
                if key=='label':
                    if i==0:
                        return {key:D[key][j]}
                    else:
                        return {}
                else:
                    return {key:D[key][j]}

            else:
                if i==0:
                    return {key:D[key]}
                else:
                    return {}
        else:
            return {}

    xL    = []
    yL    = []
    kwsL  = []
    xLimL = []
    yLimL = []

    for i in range(len(dataL)):

        x,y,lab = dataL[i]
        x.squeeze()
        y.squeeze()
        tF=False
        rF=False
        if y.ndim==2:
            if y.shape[1]== x.shape[0]:
                y = y.T
                tF=True
        elif y.ndim==1:
            y = y.reshape(-1,1)
            rF=True
        elif y.ndim==0:
            x = x.reshape(-1,1)
            y = y.reshape(-1,1)
            rF=True
        else:
            raise ValueError("ndimensions of y has to be either 0, 1 or 2, not {0}".format(y.ndim))

        if x.ndim==1:
            x = np.atleast_2d(x)
        if y.ndim<2:
            y = y[None,:,:]
            #y = 

        if not isinstance(lab, Iterable) or type(lab)==str:
            lab = (lab,)

        xL.append(x)
        yL.append(y)
        kwsL.append(lab)

        [xLimL.append(_std2lim(x[i], xLim)) for i in range(len(x))]
        [[yLimL.append(_std2lim(y[i][j], yLim)) for j in range(len(y[i]))] for i in range(len(y))]

    xLimL = np.asarray(xLimL)
    yLimL = np.asarray(yLimL)

    xLim = np.asarray([xLimL.min(), xLimL.max()])
    yLim = np.asarray([yLimL.min(), yLimL.max()])

    if type(Ax)==type(None):
        fig, ax = plt.subplots(**fig_kws)
    else:
        ax = Ax

    if type(log)==type(None) or log=='x' or log=='y':
        pass
    else:
        raise ValueError("log kwarg can be \"None\", \"x\", or \"y\"")

    if log=='y':
        ax.set_yscale('symlog',linthreshy=linthreshY, basey=base)
        ax.yaxis.set_minor_locator(MinorSymLogLocator(linthreshY))
    else:
        ax.yaxis.set_major_locator(mpl.ticker.MaxNLocator(nbins=9, prune='lower'))
        ax.yaxis.set_major_formatter(mpl.ticker.ScalarFormatter(useOffset=False, useMathText=True))
        ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator(n=5))
    if log=='x':
        ax.set_xscale('symlog',linthreshx=linthreshX, basex=base)
        ax.xaxis.set_minor_locator(MinorSymLogLocator(linthreshX))
    else:
        ax.xaxis.set_major_locator(mpl.ticker.MaxNLocator(nbins=9, prune='upper'))
        ax.xaxis.set_major_formatter(mpl.ticker.ScalarFormatter(useOffset=False, useMathText=True))
        ax.xaxis.set_minor_locator(mpl.ticker.AutoMinorLocator(n=5))

    if increment:
        inc = (yLim.max()-yLim.min())*inc_scale
    else:
        inc = 0

    ax.set_xlim(*xLim)


    locs = []
    for i in range(len(dataL)):
        x,y,kws = xL[i],yL[i],kwsL[i]

        if 'error' in kws:
            err = kws.pop('error')
            if tF:
                err = err.T
            if rF:
                err = err.reshape(-1,1)
        else:
            err = None

        for j in range(y.shape[1]):
            _kws={}

            if j==0 and zeroY:
                ax.axhline(y=i*inc,ls=':',color='black', lw=1.)

            #if j==0 and type(style)==type(None):
                ##
            if type(style)==type(None):
                [_kws.update(_update_kwarg_std(i, j, kws, key)) for key in list(kws.keys())]
            elif style=='WAXSstack':
                [_kws.update(_update_kwarg_stack(i, j, kws, key)) for key in list(kws.keys())]

            #if type(fill_coords[i,j])!=type(None):
                #for k,coord in enumerate(fill_coords):
                    #ax.

            for k in range(len(y)):
                #print(i,j,k)
                kkk
                ax.errorbar(x[k],y[k,j]+i*inc,yerr=err[k,j]+i*inc if type(err)!=type(None) else None, capsize=3., **_kws)

        if len(list(increment_labels.keys()))!=0:
            xpos = increment_labels['x']*np.diff(xLim)[0]
            ypos = increment_labels['y']*np.diff(yLim)[0]
            ax.text(xpos,i*inc+ypos,r'{}'.format(increment_labels['text'][i]), **increment_labels['kwargs'])

        locs.append(i*inc)
    locs = np.asarray(locs)

    if type(inc_token)!=type(None) and inc!=0:
        ikw={   'text':r'{0} = {1}'.format(yLab, r'%.0f'),
                'text_offset':(1.,1.),
                'decimal_mod':0,
                'text_kws':{'va':'center','ha':'left'},
                'position':'right',
                'txt_fmt':r'${0} {1}$',
                'unit':r'',
                'offset':(0.75,0.),
                #'half_on_baseline':True,
                'half_scale':True,
                'arrowstyle':'|-|',
                'connectionstyle':'arc3,rad=0',
                'mutation_scale':2.,}
        ikw.update(inc_token)

        mag  = np.round(inc/2., decimals=-int(np.log10(inc/2.))+ikw.pop('decimal_mod')) if ikw['half_scale'] else np.round(inc, decimals=-int(np.log10(inc))+ikw.pop('decimal_mod'))
        dx   = np.diff(ax.get_xlim())
        px   = np.mean(ax.get_xlim())
        xt   = float(px+((dx/2.))*ikw['offset'][0]) if ikw['position']=='right' else float(px-((dx/2.))*ikw['offset'][0])
        toff = ikw.pop('text_offset')
        yt   = ikw['offset'][1] -inc
        yt  += (inc-mag)/2.
        #bork
        txt_fmt = ikw.pop('txt_fmt')
        unit = ikw.pop('unit').replace('$','')
        text = ikw.pop('text')
        lab = txt_fmt.format((text%(mag)).replace('$',''),unit)
        ax.text(xt*(toff[0]),(yt+mag/2.)*toff[1], lab, **ikw.pop('text_kws'))

        [ikw.pop(key) for key in ('position', 'offset','half_scale')]
        ax.add_patch(mpl.patches.FancyArrowPatch(posA=(xt,yt), posB=(xt,yt+mag), **ikw))

    elif type(inc_token)!=type(None) and inc==0:
        print("Cannot implement an \'inc_token\' because increment size is 0.")

    if not increment and not autoscaleY:
        ax.set_ylim(*yLim)
    elif autoscaleY and not increment:
        ax.autoscale(enable=True, axis='y')
    else:
        ma = np.abs((locs.min()-inc, locs.max()+inc)).max()
        ax.set_ylim(-inc,ma)

    if gradBand:
        import scipy as sp
        import scipy.interpolate
        yLim = ax.get_ylim()
        col = ['red', 'green', 'blue']
        for i in list(gradBand.keys()):
            band, amp = gradBand[i]['args'][:,:3], gradBand[i]['args'][:,3:].squeeze()
            if len(locs)!=len(band[:,0]):
                raise ValueError
            cmap = mpl.cm.Reds
            #normalize = mpl.colors.Normalize(vmin=amp.min(), vmax=amp.max())
            old  = np.hstack((locs[0]-inc, locs, locs[-1]+inc))
            oamp = np.hstack((amp[0], amp, amp[-1]))
            olow = np.hstack((band[0,1], band[:,1], band[-1,1]))
            ohig = np.hstack((band[0,2], band[:,2], band[-1,2]))
            
            f = sp.interpolate.interp1d(old, oamp, kind=gradBand[i]['kwargs']['kind'], assume_sorted=True)
            h = sp.interpolate.interp1d(old, olow, kind=gradBand[i]['kwargs']['kind'], assume_sorted=True)
            g = sp.interpolate.interp1d(old, ohig, kind=gradBand[i]['kwargs']['kind'], assume_sorted=True)

            new  = np.linspace(locs[0]-inc, locs[-1]+inc, num=int(gradRes*(len(locs)+2)), endpoint=True)
            dx   = np.diff(new).mean()
            namp = f(new)
            low  = h(new)
            high = g(new)

            namp[namp<0] = 0.
            namp[namp>1] = 1.
            #if i==2:
                #bork

            #[locs[:]-inc, locs[:]+inc],[band[:,2], band[:,2]], [band[:,1], band[:,1]]
            for j in range(len(new)):
                ax.fill_betweenx([new[j]-dx, new[j]+dx],[low[j], low[j]], [high[j], high[j]], 
                                 facecolor = gradBand[i]['kwargs']['color'],
                                 edgecolor = gradBand[i]['kwargs']['color'],
                                 linewidth= 0.,
                                 alpha = namp[j],
                                )

    if (increment and increment_major_ticks):
        ax.yaxis.set_major_locator(mpl.ticker.FixedLocator(locs, nbins=len(dataL)))
        ax.yaxis.set_major_formatter(mpl.ticker.ScalarFormatter(useOffset=False, useMathText=True))
        ax.yaxis.set_minor_locator(mpl.ticker.AutoMinorLocator())
    
    if type(vertical_lines)!=type(None):
        for el in vertical_lines:
            ax.axvline(x=el[0], **el[1])

    if type(legend_kws)!=type(None):

        handles, labels = ax.get_legend_handles_labels()

        if 'reverse' in legend_kws:
            if legend_kws['reverse']:
                handles, labels = handles[::-1], labels[::-1]
            legend_kws.pop('reverse')

        _leg_kws={}
        for key in ['ha', 'horizontal alignment', 'horizontal_alignment']:
            if legend_kws.has_key(key):
                _leg_kws['ha'] = legend_kws.pop(key)


        if not legend_ax is None:
            legend_ax.legend(handles, labels, **legend_kws)
        else:
            ax.legend(handles, labels, **legend_kws)

    ax.set_ylabel(yLab)
    ax.set_xlabel(xLab)

    if type(title)!=type(None):
        ax.title.set_text(title)

    if type(Ax)==type(None):
        return (fig,ax)
    elif type(Ax)!=type(None):
        return (None,ax)
    elif type(legend_ax)==type(None):
        return (ax, legend_ax)
    else:
        return (None,ax)


def zeroRange(lim, num):

    ra  = np.linspace(lim.min(),lim.max(),num=num,endpoint=True)

    if (0>=lim.min())*(0<=lim.max()).any():
        zero = [0.]
    else:
        zero = lim.min()

    raZ = ra - ra[get_lim(ra, zero)]

    upS = raZ[-1]+np.diff(raZ).mean()
    doS = raZ[0]-np.diff(raZ).mean()
    raC = raZ[(raZ>=lim.min())*(raZ<=lim.max())]

    if len(raC)!=len(ra):
        if upS<lim.max():
            np.insert(raC, -1, ipS, axis=0)
        elif doS>lim.min():
            np.insert(raC, 0, doS, axis=0)

    return raC

def shiftedColorMap(cmap, start=0, midpoint=0.5, stop=1.0, name='shiftedcmap'):
    '''
    Ripped from http://stackoverflow.com/questions/7404116/defining-the-midpoint-of-a-colormap-in-matplotlib
    By Paul H.
    
    Function to offset the "center" of a colormap. Useful for
    data with a negative min and positive max and you want the
    middle of the colormap's dynamic range to be at zero

    Input
    -----
      cmap : The matplotlib colormap to be altered
      start : Offset from lowest point in the colormap's range.
          Defaults to 0.0 (no lower ofset). Should be between
          0.0 and `midpoint`.
      midpoint : The new center of the colormap. Defaults to 
          0.5 (no shift). Should be between 0.0 and 1.0. In
          general, this should be  1 - vmax/(vmax + abs(vmin))
          For example if your data range from -15.0 to +5.0 and
          you want the center of the colormap at 0.0, `midpoint`
          should be set to  1 - 5/(5 + 15)) or 0.75
      stop : Offset from highets point in the colormap's range.
          Defaults to 1.0 (no upper ofset). Should be between
          `midpoint` and 1.0.
    '''
    cdict = {
        'red': [],
        'green': [],
        'blue': [],
        'alpha': []
    }

    # regular index to compute the colors
    reg_index = np.linspace(start, stop, 257)

    # shifted index to match the data
    shift_index = np.hstack([
        np.linspace(0.0, midpoint, 128, endpoint=False), 
        np.linspace(midpoint, 1.0, 129, endpoint=True)
    ])

    for ri, si in zip(reg_index, shift_index):
        r, g, b, a = cmap(ri)

        cdict['red'].append((si, r, r))
        cdict['green'].append((si, g, g))
        cdict['blue'].append((si, b, b))
        cdict['alpha'].append((si, a, a))

    newcmap = matplotlib.colors.LinearSegmentedColormap(name, cdict)
    plt.register_cmap(cmap=newcmap)

    return newcmap

class MinorSymLogLocator(matplotlib.ticker.Locator):
    """
    Dynamically find minor tick positions based on the positions of
    major ticks for a symlog scaling.
    Ripped from http://stackoverflow.com/questions/20470892/how-to-place-minor-ticks-on-symlog-scale
    By David Zwicker
    """
    def __init__(self, linthresh):
        """
        Ticks will be placed between the major ticks.
        The placement is linear for x between -linthresh and linthresh,
        otherwise its logarithmically
        """
        self.linthresh = linthresh

    def __call__(self):
        'Return the locations of the ticks'
        majorlocs = self.axis.get_majorticklocs()

        # iterate through minor locs
        minorlocs = []

        # handle the lowest part
        for i in range(1, len(majorlocs)):
            majorstep = majorlocs[i] - majorlocs[i-1]
            if abs(majorlocs[i-1] + majorstep/2) < self.linthresh:
                ndivs = 10
            else:
                ndivs = 9
            minorstep = majorstep / ndivs
            locs = np.arange(majorlocs[i-1], majorlocs[i], minorstep)[1:]
            minorlocs.extend(locs)

        return self.raise_if_exceeds(np.array(minorlocs))

    def tick_values(self, vmin, vmax):
        raise NotImplementedError('Cannot get tick locations for a '
                                  '%s type.' % type(self))

def set_panel(ax, panel_location='left', legend_ax=False, legend_kws={}):
    import matplotlib as mpl
    import inspect
    def left(ax):
        ax.yaxis.set_label_position('left')
        ax.yaxis.tick_left()
        return ax

    def right(ax):
        ax.yaxis.set_label_position('right')
        ax.yaxis.tick_right()
        #for tick in ax.yaxis.get_ticklabels(): tick.set_horizontalalignment('right')
        return ax

    def bottom(ax):
        ax.xaxis.set_label_position('bottom')
        ax.xaxis.tick_bottom()
        return ax

    def top(ax):
        ax.xaxis.set_label_position('top')
        ax.xaxis.tick_top()
        return ax

    if legend_ax and panel_location==('top' or 'bottom'):
        leg        = ax.legend_
        leg_kwargs = get_legend_kwargs(leg)
        leg_prop   = leg.properties()
        handles    = leg_prop['lines']
        labels     = [el.get_text() for el in leg_prop['texts']]
        ncol       = len(leg_prop['lines'])
        leg.set_visible = False
        leg_kwargs.update({'ncol':ncol, 'mode':'extend',
                           'loc':'upper left',
                           })
        leg_kwargs.update(legend_kws)
        #inspect.getargspec(mpl.axes.Axes.legend)
        ax.legend(handles, labels, **leg_kwargs)
    else:
        ax = locals()[panel_location](ax)

    return ax

def makeExp(temp, fmt='%1.0f', limit=False, extremities=False):
    lab = r"${0}$".format(fmt)
    float_re = r"[-+]?\d+(?:\.\d*)?"
    rex = re.compile(r'\$(%s)\$'%(float_re))
    C,E = det_sci(temp)

    if limit:
        exp = 0
    elif limit==False and len(E[E!=0])!=0:
        exp = E[E!=0].max() 
    else:
        #return (temp,0)
        exp = 0

    tick_labs = []
    for i,x in enumerate(temp):
        if not extremities:
            tick_labs.append(lab % (float(x)/10**exp))
        elif i==0 or i==len(temp)-1 or x==0.:
            tick_labs.append(lab % (float(x)/10**exp))
        else:
            tick_labs.append(r"")
    return (tick_labs, exp)

def CBniceTickLabels(ax, axis='x', **kwargs):
    import matplotlib as mpl
    if hasattr(ax, 'ax'):
        ax = getattr(ax, 'ax')
    elif hasattr(ax, '{0}axis'.format(axis)):
        ax = ax
    else:
        raise AttributeError("ax needs to be {0} or {1}".format(mpl.colorbar.Colorbar, mpl.axes._subplots.Axes))
    A = getattr(ax, '{0}axis'.format(axis))
    #bork
    tick_labs = [float(el.get_text().replace('$','')) for el in getattr(A, 'get_ticklabels'.format(axis))()]
    #bork
    tick_labs, exp = makeExp(tick_labs, **kwargs)
    if exp!=0:
        lab = getattr(ax, 'get_{0}label'.format(axis))()
        new_lab = r'${0}\ /10^{{{1}}}$'.format(lab.replace('$',''), int(exp)) if int(exp)!=0 else r'${0}$'.format(lab.replace('$',''))
        set_call = getattr(ax, 'set_{0}label'.format(axis))
        set_call(new_lab)

    A.set_ticklabels(tick_labs)

    #zoink

    return ax

def niceTickLabels(ax, axis='x', locator_kws={'locator':'MaxNLocator', 'kwargs':{}},
                   formatter_kws={'formatter':None, 'kwargs':{}},
                   ):
    import matplotlib as mpl
    import matplotlib.pyplot as plt
    A = getattr(ax, axis+'axis')
    scale = A.get_scale()
    if scale =='log':
        A.set_minor_locator(MinorLogLocator())
    elif scale == 'symlog':
        A.set_minor_locator(MinorSymLogLocator(**{'linthresh':A.get_minor_locator().linthresh}))
    elif scale == 'linear':

        if list(locator_kws.keys()):
            if locator_kws['locator']!='same':
                A.set_major_locator(getattr(mpl.ticker, locator_kws['locator'])(**locator_kws['kwargs']))
            else:
                A.set_major_locator(getattr(mpl.ticker, A.get_major_locator().__class__.__name__)(**locator_kws['kwargs']))

        if type(formatter_kws['formatter'])!=type(None):
            if formatter_kws['formatter']=='exp' and type(formatter_kws['formatter'])!=type(None):
                _kws = {'fmt':"%.1f"}
                _kws.update(formatter_kws['kwargs'])
                tick_labs = [el for el in getattr(ax, 'get_{0}ticks'.format(axis))()]
                tick_labs, exp = makeExp(tick_labs, **_kws)
                lab = getattr(ax, 'get_{0}label'.format(axis))()
                new_lab = r'${0}\ /10^{{{1}}}$'.format(lab.replace('$',''), int(exp)) if int(exp)!=0 else r'${0}$'.format(lab.replace('$',''))
                set_call = getattr(ax, 'set_{0}label'.format(axis))
                set_call(new_lab)
                A.set_ticklabels(tick_labs)
            elif formatter_kws['formatter']!='same' and type(formatter_kws['formatter'])!=type(None):
                A.set_major_formatter(getattr(mpl.ticker, formatter_kws['formatter'])(**formatter_kws['kwargs']))
            else:
                fmt = getattr(mpl.ticker, A.get_major_formatter().__class__.__name__)(**formatter_kws['kwargs'])
                A.set_major_formatter(fmt)

        A.set_minor_locator(mpl.ticker.AutoMinorLocator())
    #lol
    return ax

def removeTickLab(ax, axis='x', total=False, is_cbar=False):

    A = getattr(ax, axis+'axis')
    A.set_ticklabels([])

    if not is_cbar:
        ax.tick_params(axis='{0}'.format(axis), which='both',length=0)
    else:
        #pad = matplotlib.rcParams[u'{0}tick.major.pad'.format(axis)]/2.
        pad = 0.
        ax.tick_params(axis='{0}'.format(axis), which='major', pad=pad)
        ax.tick_params(axis='{0}'.format(axis), which='both',length=2.5)
    if total:
        A.set_visible(False)
    
    return ax

def removeLines(ax, is_cbar=False):
    [ax.spines[key].set_visible(False) for key in list(ax.spines.keys())]
    [removeTickLab(ax, axis=axis, is_cbar=is_cbar) for axis in ['x','y']]
    ax.set_facecolor("None")
    return ax

def get_legend_kwargs(l):
    import matplotlib as mpl

    defaults = dict(
        loc = l._loc,
        numpoints = l.numpoints,
        markerscale = l.markerscale,
        scatterpoints = l.scatterpoints,
        scatteryoffsets = l._scatteryoffsets,
        prop = l.prop,
        # fontsize = None,
        borderpad = l.borderpad,
        labelspacing = l.labelspacing,
        handlelength = l.handlelength,
        handleheight = l.handleheight,
        handletextpad = l.handletextpad,
        borderaxespad = l.borderaxespad,
        columnspacing = l.columnspacing,
        ncol = l._ncol,
        mode = l._mode,
        fancybox = type(l.legendPatch.get_boxstyle())==mpl.patches.BoxStyle.Round,
        shadow = l.shadow,
        title = l.get_title().get_text() if l._legend_title_box.get_visible() else None,
        framealpha = l.get_frame().get_alpha(),
        bbox_to_anchor = l.get_bbox_to_anchor()._bbox,
        bbox_transform = l.get_bbox_to_anchor()._transform,
        frameon = l._drawFrame,
        handler_map = l._custom_handler_map,
    )

    #if "fontsize" in kwargs and "prop" not in kwargs:
        #defaults["prop"].set_size(kwargs["fontsize"])

    return defaults


def move_legend(ax, x, y):
    """
    Manual legend mover.
    """

    # Get legend
    leg = ax.get_legend()

    # Get original bounding box
    bb  = leg.get_bbox_to_anchor().inverse_transformed(ax.transAxes)

    bb.x0 += x
    bb.x1 += x
    bb.y0 += y
    bb.y1 += y
    #bork

    leg.set_bbox_to_anchor(bb, transform = ax.transAxes)

    return ax


def addGradBand(bandList):
    if isinstance(bandList, Iterable) and type(bandList)!=str:
        bandList = np.asarray(bandList)
    else:
        raise TypeError

    gradBand = {}

    keyL = []
    c = 0
    for D in bandList:
        for i in list(D.keys()):
            gradBand[c]={}
            gradBand[c].update(D[i])
            c += 1

    return gradBand


def mergeGradBand(bandList, kwargN=0):
    """
    Merges the values of the the bands per key
    Merging goes in order of elements provided in bandList.
    """

    if isinstance(bandList, Iterable) and type(bandList)!=str:
        bandList = np.asarray(bandList)
    else:
        raise TypeError

    if np.unique([len(list(band.keys())) for band in bandList]).size!=1:
        raise ValueError("Number of keys in all bandList dictionaries must be the same. Not: {0}".format(' '.join([len(list(band.keys())) for band in bandList])))

    if np.unique(np.asarray([sorted(band.keys()) for band in bandList],dtype=str)).size!=len(list(band.keys())):
        raise ValueError("")

    keyL = list(band.keys())
    gradBand = {}
    for key in keyL:
        argL     = []
        kwargL   = []
        for band in bandList:
            argL.append(band[key]['args'])
            kwargL.append(band[key]['kwargs'])
        gradBand.update({key: {'args':np.vstack(argL), 'kwargs':kwargL[kwargN]}})

    return gradBand


def makeGradBandFromXY(coords_list, gradation_list=None, interpolate='cubic', color=None):

    if isinstance(coords_list, Iterable) and type(coords_list)!=str:
        coords_list = np.asarray(coords_list)
        if np.ndim(coords_list)==2:
            coords_list = coords_list[None,:,:]
    else:
        raise TypeError

    if isinstance(gradation_list, Iterable) and type(gradation_list)!=str:
        gradation_list = np.atleast_2d(gradation_list)
    elif type(gradation_list)==type(None):
        gradation_list = np.ones((coords_list.shape[0],))
    else:
        raise TypeError

    if not coords_list.shape[0]==gradation_list.shape[0]:
        raise ValueError

    gradBand = {}
    temp = np.zeros(((coords_list.shape[1],) + (4,)))
    for i,el in enumerate(coords_list):
        C = {i: {'kwargs':{}, 'args':temp}}

        C[i]['args'][:, 0] = el[:,0]
        C[i]['args'][:, 1] = el[:,1]
        C[i]['args'][:, 2] = el[:,2]
        C[i]['args'][:, 3] = gradation_list[i]

        C[i]['kwargs'].update({'color':color if type(color)!=type(None) else 'grey'})
        C[i]['kwargs'].update({'kind':interpolate})

        gradBand.update(C)

    return gradBand


def makeGradBandFromParam(ParamsDict, y, param_names_and_gradation=[], selectYidx=None, width=0.1, 
                          interpolate='cubic', colors=None):
    P = ParamsDict['time_dependent']
    y = np.asarray(y.squeeze())
    for key in param_names_and_gradation:
        if key[0] not in P:
            raise KeyError("{0} is not a valid parameter!".format(key[0]))
        if key[1] not in P and type(key[1])!=type(None):
            raise KeyError("{0} is not a valid parameter!".format(key[1]))

    if type(selectYidx)==type(None):
        idx = np.arange(len(y))
    else:
        idx = selectYidx

    gradBand = {}

    for i,key in enumerate(param_names_and_gradation):

        if y.shape!=P[key[0]][:,1].shape:
            raise ValueError

        temp = np.zeros(y[idx].shape + (4,))
        C = {i: {'kwargs':{}, 'args':temp}}
        inc = width
        if type(P[key[1]])!=type(None):
            alpha = P[key[1]][idx,1]
        else:
            alpha = np.ones_like(P[key[0]][idx,1])
        #bork
        C[i]['args'][:, 0] = y[idx]
        C[i]['args'][:, 1] = P[key[0]][idx,1]-inc
        C[i]['args'][:, 2] = P[key[0]][idx,1]+inc
        C[i]['args'][:, 3] = alpha

        C[i]['kwargs'].update({'color':colors[i] if type(colors)!=type(None) else None})
        C[i]['kwargs'].update({'kind':interpolate})
        gradBand.update(C)
    return gradBand

import numpy as np
import pylab as plt

#Plot a rectangle
def rect(ax, x, y, w, h, c, **kwargs):

    #Varying only in x
    if len(c.shape) is 1:
        rect = plt.Rectangle((x, y), w, h, color=c, ec=c,**kwargs)
        ax.add_patch(rect)
    #Varying in x and y
    else:
        #Split into a number of bins
        N = c.shape[0]
        hb = h/float(N); yl = y
        for i in range(N):
            yl += hb
            rect = plt.Rectangle((x, yl), w, hb, 
                                 color=c[i,:], ec=c[i,:],**kwargs)
            ax.add_patch(rect)

#Fill a contour between two lines
def rainbow_fill_between(ax, X, Y1, Y2, colors=None, 
                         cmap=plt.get_cmap("Reds"),**kwargs):
    """
    Mega hack from Ed Smith.
    http://stackoverflow.com/questions/11564273/matplotlib-continuous-colormap-fill-between-two-lines
    """

    ax.plot(X,Y1,lw=0)  # Plot so the axes scale correctly

    dx = X[1]-X[0]
    N  = X.size

    #Pad a float or int to same size as x
    if (type(Y2) is float or type(Y2) is int):
        Y2 = np.array([Y2]*N)

    #No colors -- specify linear
    if colors is None:
        colors = []
        for n in range(N):
            colors.append(cmap(n/float(N)))
    #Varying only in x
    elif len(colors.shape) is 1:
        colors = cmap((colors-colors.min())
                      /(colors.max()-colors.min()))
    #Varying only in x and y
    else:
        cnp = np.array(colors)
        colors = np.empty([colors.shape[0],colors.shape[1],4])
        for i in range(colors.shape[0]):
            for j in range(colors.shape[1]):
                colors[i,j,:] = cmap((cnp[i,j]-cnp[:,:].min())
                                    /(cnp[:,:].max()-cnp[:,:].min()))

    colors = np.array(colors)

    #Create the patch objects
    for (color,x,y1,y2) in zip(colors,X,Y1,Y2):
        rect(ax,x,y2,dx,y1-y2,color,**kwargs)


def rgb2gray(rgb):
    if np.ndim(rgb)!=3:
        raise ValueError("Not an rgb or rgba image!")
    if rgb.shape[-1]==4:
        return np.dot(rgb[...,:3], [0.299, 0.587, 0.114])
    else:
        return np.dot(rgb[...,:3], [0.299, 0.587, 0.114])


def get_N_HexCol(N):
    """
    Generate N number of colors in hex format
    """
    import colorsys
    HSV_tuples = [(x*1.0/N, 1., .75) for x in range(N)]
    hex_out = []
    for rgb in HSV_tuples:
        rgb = [int(x*255) for x in colorsys.hsv_to_rgb(*rgb)]
        hex_out.append("#"+"".join([chr(x).encode('hex') for x in rgb]))
    return hex_out


def gen_cb_from_im(im, ax=None, ext=None, sort=0, force_cmap='', cmap_name='custom',**kwargs):
    import collections


    if type(ext)!=type(None):
        if isinstance(ext, collections.Iterable):
            image = ext
        else:
            image   = np.asarray(ext.get_array())
    else:
        image   = np.asarray(im.get_array())

    if np.ndim(image)!=3:
        raise ValueError("")

    if type(force_cmap)!=type('') or force_cmap=='':
        #bork
        colors  = np.asarray([y for y in set(tuple(x) for x in image[:,:].reshape(-1,image.shape[-1]))])
        colors  = colors[np.argsort(colors[:,int(sort)])]
        im.cmap = matplotlib.colors.ListedColormap(colors.astype(float), name=cmap_name)
        cmap = im.cmap
    else:
        cmap    = getattr(matplotlib.cm, force_cmap)
        im.cmap = cmap

    if type(ax)!=type(None):
        cb      = matplotlib.colorbar.Colorbar(ax, im, cmap=im.cmap)
        

    return im,cb


def plot_colors(colors):
    import matplotlib as mpl
    #import 

    fig,ax = plt.subplots()
    inc = 1./len(colors)
    p = []
    for i,col in enumerate(colors):
        p.append(mpl.patches.Rectangle((i*inc,0.), inc, 1., angle=0.0,
                                        color     = mpl.colors.to_rgba(col),
                                        facecolor = mpl.colors.to_rgba(col),
                                        edgecolor = mpl.colors.to_rgba(col),))
        ax.add_patch(mpl.patches.Rectangle((i*inc,0.), inc, 1., angle=0.0,
                                        color     = mpl.colors.to_rgba(col),
                                        facecolor = mpl.colors.to_rgba(col),
                                        edgecolor = mpl.colors.to_rgba(col),))

    #P = mpl.collections.PatchCollection(p)
    #P.set_array(colors)
    #ax.add_collection(P)
    ax.set_ylim(0,1)
    ax.set_xlim(0,1)
    ax.plot()
    #bork
    #plt.show()
    return fig,ax


def minMax(A):
    return np.array([np.min(A),np.max(A)])
