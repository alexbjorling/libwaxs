# -*- coding: utf-8 -*-
"""
This module holds two classes, which can each be used to represent single 
experimental runs. The classes represent rapid-readout and pump-probe runs, and
are kept separate because these runs are so different. But both classes are 
conveniently used for creating Dataset objects, which hold both types equally
well.

For each new experiment, small additions are needed for these classes. New 
info must be supplied in two places
    1. In the constructor "__init__()", add info on how folders and log files are named
    2. In the function load(), add the experiment ID to one of the existing loading routines, or write a new routine if necessary (ie if the beamline has changed its file format).
"""

import numpy as np
import scipy.io
import matplotlib.pyplot as plt
import libwaxs
from .helpers import *
from .RCutils import *
import collections
import sys,os,re
import glob
import scipy.signal as ssi

class Run_PP:
    """
    Class that holds one Pump-Probe (PP) run, like from id09b, BioCARS or SACLA.
    """

    def __init__(self, expID=''):
        """
        Constructs a Run_PP object, with initialized hardware parameters such as detector distance, X-ray energy, and pixel size.
        @param expID: The experiment ID, such as 'aps2012' or 'esrf2013'
        @type expID: str
        """

        self._expID = expID
        if (expID == 'esrf2013_reintegrated') | (expID == 'esrf2013') | (expID == 'aps2013') | (expID == 'esrf2015'):
            self._logfilename   = 'run.log'
            self._runfoldername = 'run%03u/'
        elif (expID == 'maxlab2014c'):
            self._logfilename   = ''
            self._runfoldername = 'scan%03u/'
        elif (expID == 'sacla2015A8033'):
            self._matfilename = '%s_hvXDS.mat'
            self._runfoldername = '%03u'
            self._logfilename   = ''
            self._unit  = {}
            self._quant = {}
            self._unit['S']     = r'q'
            self._unit['Sh']    = r'q'
            self._unit['Sv']    = r'q'
            self._unit['t']     = r'ps'
            self._unit['q']     = r'\mathring{\mathrm{A}}^{-1}'
            self._quant['S']    = r'\mathrm{S}'
            self._quant['Sh']   = r'\mathrm{S}'
            self._quant['Sv']   = r'\mathrm{S}'
            self._quant['t']    = r'delay'
            self._quant['q']    = r'q'
            
            raise NotImplementedError("Processing of a sacla2015A8033 experiment is not yet implemented.")

        elif (expID == 'sacla2016A8037'):
            self._matfilename   = '%s_hvXDS.mat'
            self._runfoldername = '%03u'
            self._logfilename   = ''
            self._MPscale       = 6.67
            self._TTscale       = 2.463
            self._TTpixLim      = [700.,1300.]
            self._unit          = {}
            self._quant         = {}
            self._unit['S']     = r'q'
            self._unit['Sh']    = r'q'
            self._unit['Sv']    = r'q'
            self._unit['t']     = r'ps'
            self._unit['q']     = r'\mathring{\mathrm{A}}^{-1}'
            self._quant['S']    = r'\mathrm{S}'
            self._quant['Sh']   = r'\mathrm{S}'
            self._quant['Sv']   = r'\mathrm{S}'
            self._quant['t']    = r'delay'
            self._quant['q']    = r'q'
            self._unit['dS']    = r'q'
            self._unit['dSh']   = r'q'
            self._unit['dSv']   = r'q'
            self._quant['dS']   = r'\Delta\mathrm{S}'
            self._quant['dSh']  = r'\Delta\mathrm{S}'
            self._quant['dSv']  = r'\Delta\mathrm{S}'
            
        elif (expID == 'aps2014'):
            self._matfilename = 'scan%03u.mat'
            self._runfoldername = 'scan%03u/'
        elif expID == 'p12_2017':
            # We read in the 1D files directly
            self._matfile_names      = 'img_%04u_*.dat'
        elif (expID == 'esrf2017'):
            self._logfilename   = 'run01.log'
            self._runfoldername = 'run%03u/'   
        else:
            raise ValueError('INVALID EXPERIMENT ID')

    def load(self, folder, runnr=1, logfilename='run01.log', laser_off=False):
        """
        Loads integrated data into the current dataset.
        @param folder: Base folder where all the runs are kept
        @type folder: str
        @param nr: The scan number to load
        @type nr: int
        """

        if folder[-1] != '/': folder += '/'
        folder_ = folder
        self.runnr = runnr
        #folder += self._runfoldername%self.runnr

        t, S, dS, diode = [], [], [], []

        ### For each experiment ID, here are if statements to load the data files properly.
        ### The cases are VERY similar and could be handled with if-statements, but keeping
        ### them as separate blocks to maintain clarity and freedom in the future.

        if self._expID == 'p12_2017':
            # Initialize lists
            S, sigma, t, diode1, diode2, T, q = [], [], [], [], [], [], []
            # Set some parameters
            self.runnr = runnr
            self.folder = folder
            self.fns = glob.glob(self.folder + '/' + self._matfile_names%self.runnr)
            self.fns.sort()
            #print(self.fns)
            #print(self.folder)
            print("Found %i scans for run %s" % (len(self.fns), self.runnr))
            # Loop through single files and load data into object
            for fn in self.fns:
                # Add this!!!!!!!!!
                # Extract offset for calculating delay time from file name 
                data, temp_params = load_p12(fn)
                # define q if not done already, otherwise check for consistency
                # Add q to list, check inconsistencies later
                q.append(data[:,0])
                #elif (self.q != data[:,0]).any():
                #    raise ValueError('q-VALUE MISMATCH BETWEEN REPEATS')
                # Save t, S and sigma locally:
                S     += [data[:,1]]
                sigma += [data[:,2]]
                t     += [temp_params['Exposure Delay [s]']]
                T     += [temp_params['Transmitted Beam (mean)']]
                diode1 += [temp_params['Diode 1']]
                diode2 += [temp_params['Diode 2']]
                # Todo: read in diode data!!!!!!
                # Will do that later
            # Check for inconsistencies in q and correct S and sigma
            lens = np.array([len(temp) for temp in q])
            min_len = np.min(np.array(lens))
            S = [oneS[:min_len] for oneS in S]
            sigma = [oneSigma[:min_len] for oneSigma in sigma]
            self.q = np.array(q[0][:min_len])
            self.S = np.array(S)
            self.sigma = np.array(sigma)
            # Dummy dS. this is just to be able to use the further scripts
            # We don't use difference scattering on a single shot basis but after averaging here
            self.dS = np.array(dS) * np.nan
            self.t = np.array(t)
            self._Nrepeats = len(t)
            self.diode = np.array(diode)
            self.diode2 = np.array(diode2)
            self.T = np.array(T)
            self.keep = np.ones(self._Nrepeats, dtype=bool) #np.arange(self._Nrepeats)
            self.out = np.zeros(self._Nrepeats, dtype=bool)

            ### store original sequence of repeats and info on what repeats (exposures) are blanks
            original_order    = []
            blanks            = []
            for i in range(len(S)):
                blanks            += [t[i] < 0] ## negative time means blank
                original_order    += [i]
            self._blanks            = np.array(blanks)
            self._original_order    = np.array(original_order)
        
        # ESRF 2017
        elif   (self._expID == 'esrf2017'):
            self._logfilename = logfilename
            logfile = open(folder + self._logfilename, 'r')
#            # Determine times
#            ts = []
#            for row in logfile:
#                if row.strip()[0] == '#': continue
#                ts.append(row.split()[3])
#            # Take unique values
#            ts = np.unique(ts)
#            # Load data from diffs
#            for t in ts:
#                diffs = np.genfromtxt('diffs_%s.dat' % t, comments='#')
            # Now go through each line of the log file, loading ascii files accordingly:
            for row in logfile:
                if row.strip()[0] == '#': continue
                print(row.split()[2])
                #fn = row.split()[2].replace('_', '_%s_'%row.split()[3]) + '.chi'
                fn = row.split()[2].replace('_', '*') + '.chi'
                print(fn)
                filename = glob.glob(folder + '/' + fn)
                print(filename)
                if len(filename) == 1:
                    filename = filename[0]
                else:
                    #print('There should be exactly one file with the name %s but there are %i!!!' % (filename, len(filename)))
                    print('File not found. Skipping this one')
                    continue    
                # Load chi file
                data  = np.loadtxt(filename, comments='#')              

                # define q if not done already, otherwise check for consistency
                if  (not hasattr(self, 'q')):
                    self.q = data[:,0]
                elif (self.q != data[:,0]).any():
                    raise ValueError('q-VALUE MISMATCH BETWEEN REPEATS')

                # Save t and S locally:
                S += [data[:,1]]
                # Dummy dS will later be set to nans (see below)
                dS +=  [data[:,1]]
                #print('WARNING: loading raw CCD data from .chi file without correction: make sure this is correct')
                if row.split()[3] == 'off':
                    t += [-1e10]
                    print('time is nan (off)')
                else:
                    t += [stringToNs(row.split()[3])]
                    print('time is %f' %stringToNs(row.split()[3]))
                # Read in diode data   
                # Read diode signal intensity pd2ic from log file
                # That's the 9th column
                diode += [float(row.split()[8])]
                #print(row.split()[8])

            logfile.close()
            self.S = np.array(S)
            # Dummy dS. this is just to be able to use the further scripts
            # We don't use difference scattering on a single shot basis but after averaging here
            self.dS = np.array(dS) * np.nan
            print(t)
            self.t = np.array(t)
            self._Nrepeats = len(t)
            self.diode = np.array(diode)
            self.keep = np.ones(self._Nrepeats, dtype=bool) #np.arange(self._Nrepeats)
            self.out = np.zeros(self._Nrepeats, dtype=bool)

            ### store original sequence of repeats and info on what repeats (exposures) are blanks
            original_order    = []
            blanks            = []
            for i in range(len(S)):
                blanks            += [t[i] < 0] ## negative time means blank
                original_order    += [i]
            self._blanks            = np.array(blanks)
            self._original_order    = np.array(original_order)
        ### ID09b, integrated data stored in text (.chi) files
        elif (self._expID == 'esrf2013'):
            logfile = open(folder + self._logfilename, 'r')
            # Now go through each line of the log file, loading ascii files accordingly:
            for row in logfile:
                if row.strip()[0] == '#': continue
                filename = row.split()[2][:-6] + 'chi'
                data  = np.loadtxt(folder + filename, comments='#')

                # define q if not done already, otherwise check for consistency
                if  (not hasattr(self, 'q')):
                    self.q = data[:,0]
                elif (self.q != data[:,0]).any():
                    raise ValueError('q-VALUE MISMATCH BETWEEN REPEATS')

                # Save t and S locally:
                S += [data[:,1]]
                print('WARNING: loading raw CCD data from .chi file without correction: make sure this is correct')
                t += [stringToNs(row.split()[3])]

            logfile.close()
            self.S = np.array(S)
            self.t = np.array(t)
            self._Nrepeats = len(t)

            ### store original sequence of repeats and info on what repeats (exposures) are blanks
            original_order    = []
            blanks            = []
            for i in range(len(S)):
                blanks            += [t[i] < 0] ## negative time means blank
                original_order    += [i]
            self._blanks            = np.array(blanks)
            self._original_order    = np.array(original_order)

        ### ID09b, integrated data stored in mat-files
        elif (self._expID == 'esrf2013_reintegrated') | (self._expID == 'esrf2015'):
            import scipy.io
            logfile = open(folder + self._logfilename, 'r')
            # Now go through each line of the log file, loading mat files accordingly:
            for row in logfile:
                if row.strip()[0] == '#': continue
                filename = row.split()[2][:-6] + 'mat'
                # instead of text files, load the corresponding mat file:
                # there's NaN in the intensities, so discard first 12 points.
                data = scipy.io.loadmat(folder + filename)
                q = data['q'].squeeze()[12:]
                S_ = data['I'].squeeze()[12:]

                # define q if not done already, otherwise check for consistency
                if  (not hasattr(self, 'q')):
                    self.q = q
                elif (self.q != q).any():
                    raise ValueError('q-VALUE MISMATCH BETWEEN REPEATS')

                # Save t and S locally:
                S += [S_]
                t += [stringToNs(row.split()[3])]

            logfile.close()
            self.S = np.array(S)
            self.t = np.array(t)
            self._Nrepeats = len(t)

            ### store original sequence of repeats and info on what repeats (exposures) are blanks
            original_order    = []
            blanks            = []
            for i in range(len(S)):
                blanks            += [t[i] < 0] ## negative time means blank
                original_order    += [i]
            self._blanks            = np.array(blanks)
            self._original_order    = np.array(original_order)

        ### BioCARS, very similar to the above, but keeping separate anyway
        elif self._expID == 'aps2013':
            logfile = open(folder + self._logfilename, 'r')
            # Now go through each line of the log file, loading ascii files accordingly:
            for row in logfile:
                if row.strip()[0] == '#': continue
                filename = row.split()[2] + '_C.dat'
                data  = np.loadtxt(folder + filename, comments='#')

                # define q if not done already, otherwise check for consistency
                if  (not hasattr(self, 'q')):
                    self.q = data[:,0]
                elif (self.q != data[:,0]).any():
                    raise ValueError('q-VALUE MISMATCH BETWEEN REPEATS')

                # Save t and S locally:
                S += [data[:,1]]
                print('WARNING: loading raw CCD data from .dat file without correction: make sure this is correct')
                t += [stringToNs(row.split()[3])]

            logfile.close()
            self.S = np.array(S)
            self.t = np.array(t)
            self._Nrepeats = len(t)

            ### store original sequence of repeats and info on what repeats (exposures) are blanks
            original_order    = []
            blanks            = []
            for i in range(len(S)):
                blanks            += [t[i] < 0] ## negative time means blank
                original_order    += [i]
            self._blanks            = np.array(blanks)
            self._original_order    = np.array(original_order)

        ### MAX-Lab I911-2, integrated data (LED2-LED1) stored as mat files
        elif (self._expID == 'maxlab2014c'):
            import scipy.io
            # Now go through and load the files in the right order: LED1__001, LED2__001, LED1__002 ...
            i = 0
            done = False
            while not done:
                i += 1
                for LED in (1,2):
                    filename = 'phy_LED%u__%03u.mat'%(LED, i)
                    try:
                        data = scipy.io.loadmat(folder + filename)
                    except:
                        done = True
                        break
                    q = data['q'].squeeze()
                    S_ = data['I'].squeeze()

                    # define q if not done already, otherwise check for consistency
                    if  (not hasattr(self, 'q')):
                        self.q = q
                    elif (self.q != q).any():
                        raise ValueError('q-VALUE MISMATCH BETWEEN REPEATS')

                    # Save t and S locally:
                    S += [S_]
                    if LED == 1:
                        t += [-1]
                    else:
                        t += [1]

            self.S = np.array(S)
            self.t = np.array(t)
            self._Nrepeats = len(t)

            ### store original sequence of repeats and info on what repeats (exposures) are blanks
            original_order    = []
            blanks            = []
            for i in range(len(S)):
                blanks            += [t[i] < 0] ## negative time means blank
                original_order    += [i]
            self._blanks            = np.array(blanks)
            self._original_order    = np.array(original_order)

        # SACLA pump-probe data stored in Tim's reduced mat files
        elif (self._expID == 'sacla2015A8033'):
            print("NOT IMPLEMENTED")
        
        # SACLA pump-probe data stored in DTU's reduced mat files
        elif (self._expID == 'sacla2016A8037'):
            import scipy as sp
            import scipy.io
            import scipy.stats
            data = scipy.io.loadmat(self._matfilename%folder)
            
            self._hdr = {}
            self._hdr[self.runnr] = {
                'motor_positions'       : None,
                'motor_positions_scale' : self._MPscale,
                'time_tool_pix2fs'      : self._TTscale,
                'time_tool_pix_lim'     : self._TTpixLim,
                'Xray_params'           : {},
                'detector_params'       : {},
                '_SdV'                  : [], # Shot dependent variables
                '_SdK'                  : [], # Shot dependent keys
                '_DdV'                  : [], # Difference dependent variables
                '_DdK'                  : [], # Difference dependent keys
                '_QdV'                  : [], # Qvector dependent variables
                '_QdK'                  : [], # Qvector dependent variables
                '_TdV'                  : [], # Time dependent variables
                '_AsS'                  : [], # Absolute Scattering signals
                '_DsS'                  : [], # Difference Scattering signals
                                    }
            hdr = self._hdr[self.runnr]
            ## The following dict entries are not used 
            # Dummy channels:
            # data['DiodeU1']
            # data['DiodeU2']
            # data['DiodeU3']
            
            # Photodiodes? All pretty much 0 anyway
            #Pd = []
            #for pd in data['Pd'].squeeze():
            #Pd += [pd.squeeze()]
            #Pd = np.asarray(Pd)
            
            # just a counter for the number of shots starting at 1, very matlab
            # data['Index']

            # dunno what this is
            # data['Group']
            # data['HV']

            # SACLA HPC metadata
            # data['CsvPath']
            # data['dataset']
            # data['LocalH5Dir']
            # data['LocalRootDir']
            # data['fina'] # translated h5 file and path
            # data['ActualFileNumber']
            # data['ReducedDir']
            # data['HostName'] # hpc compute node
            # data['hdf5dir']
            # data['Matdir']
            # data['NodeGroup']
            # data['Suffix']
            # data['IsRemote']
            # data['__header__']
            # data['__globals__']
            # data['RemoteRootDir']
            # data['MLdir']

            # Junk
            # data['__version__']
            # data['PlotStuff']
            # data['MakeMean']
            ########################

            # metadata, raw motor positions etc
            if self.runnr != int(data['ScanNumber'][0]):
                raise ValueError("Supplied run number (%s) does not correspond with the one found in the file (%s)!!!"%(self.runnr,int(data['ScanNumber'][0])))
            self.tags       = data['Tags'].squeeze() # same as: data['ImgTags']
            self._Nrepeats  = int(data['ii']) #check this is actually the number of shots!
            self._oNrepeats = self._Nrepeats
            self._tNom      = data['tNom'].squeeze()[0]*self._MPscale/1000. # Check whether all nominal times are equal! (I think so)
            hdr['_SdV'].extend((('tags',),('_Nrepeats',)))

            # photon set energy & shot-to-shot energy distribution
            #hdr['Xray_params']['photon_energy_nominal'] = float(data['NominalE'])
            #hdr['Xray_params']['photon_energy_dist']    = data['Energy'].squeeze()
            #hdr['Xray_params']['intensity_diode_0']     = data['I0'].squeeze()# X-ray Intensity on diode 0
            #hdr['Xray_params']['intensity_diode_1']     = data['I1'].squeeze()# X-ray Intensity on diode 1
            self.Enom   = float(data['NominalE'])
            self.Eshot  = data['Energy'].squeeze()
            self.diode1 = data['I0'].squeeze()# X-ray Intensity on diode 0
            self.diode2 = data['I1'].squeeze()# X-ray Intensity on diode 1


            # motor positions
            if keep_meta:
                eH2_mopo    = []
                for mopo in data['Mopo'].squeeze():
                    if mopo.squeeze().size<self._Nrepeats :
                        eH2_mopo += [np.zeros((self._Nrepeats )).squeeze()]
                    else:
                        eH2_mopo += [mopo.squeeze()]
                hdr['motor_positions'] = np.asarray(eH2_mopo)
                # Dunno what these are, shape is (300, 4), diode channels
                # I0C         = data['I0C']
                # I1C         = data['I1C']
                hdr['_SdK'].extend((('motor_positions',None),('Xray_params','photon_energy_dist'),('Xray_params','intensity_diode_0'),('Xray_params','intensity_diode_1')))

                # No of pixels in the h, v, and azimuthal scattering
                nPix = int(data['AngleBins'])
                hdr['detector_params']['pixels_number']  = nPix
                hdr['detector_params']['pixels_dist']    = np.zeros((3,nPix), dtype=int)
                hdr['detector_params']['pixels_dist'][0] = data['nPixels'].squeeze().astype(int)
                hdr['detector_params']['pixels_dist'][1] = data['nPixelsh'].squeeze().astype(int)
                hdr['detector_params']['pixels_dist'][2] = data['nPixelsv'].squeeze().astype(int)
                hdr['detector_params']['frac']           = data['frac'].squeeze() # I do not know what this is
                if (hdr['detector_params']['pixels_dist'][2].size != hdr['detector_params']['pixels_dist'][0].size) or (hdr['detector_params']['pixels_dist'][1].size != hdr['detector_params']['pixels_dist'][0].size) or np.all(((hdr['detector_params']['pixels_dist'][1] + hdr['detector_params']['pixels_dist'][2]) != hdr['detector_params']['pixels_dist'][0])):
                    raise ValueError("Detector pixel inconsistency!")
                hdr['_QdK'].extend((('detector_params','pixels_number'),('detector_params','pixels_dist'),('detector_params','frac')))
                
            # synchronization metadata
            self._Lon         = data['IsL'].squeeze().astype(bool)
            self._Loff        = data['IsOff'].squeeze().astype(bool)
            self._Xon         = data['IsX'].squeeze().astype(bool)
            self._Xoff        = data['IsDark'].squeeze().astype(bool)
            hdr['_SdV'].extend((('_Lon',),('_Loff',),('_Xon',),('_Xoff',)))
            
            # data
            self.t      = data['t'].squeeze()
            self.q      = data['q'].squeeze()
            self.S      = data['I'].squeeze()
            self.Sh     = data['Ih'].squeeze()
            self.Sv     = data['Iv'].squeeze()
            
            # diagnostic stuff
            self.Isum   = data['ISum'].squeeze()
            
            # Timing Tool stuff
            #TTtags              = data['TT']['tags'][0,0].squeeze() #same as data['Tags'].squeeze()
            self._TTpos_d       = data['TT']['fltpos_d'][0,0].squeeze() # 
            self._TTpos_f       = data['TT']['fltpos_f'][0,0].squeeze() #
            self._TTpos         = (self._TTpos_d+self._TTpos_f)/(2.) # two different methods fo determining the rise on the time tool.
            self._TTtagMask     = ~np.isnan(self._TTpos)
            
            if stdTTtagging:
                self.TTtagging()
            
            hdr['_SdV'].extend((('t',),('Isum',),('_TTpos_d',),('_TTpos_f',),('_TTpos',),('_TTtagMask',)))
            hdr['_QdV'].extend((('q',),))
            hdr['_AsS'].extend((('S',),('Sh',),('Sv',)))
            
        # BioCARS 2014: integrated data stored in mat files
        elif  (self._expID == 'aps2014'):
            import scipy.io

            data = scipy.io.loadmat(folder_ + self._matfilename%nr)

            # get data and skip first NaN q-bins
            self.q = data['q'].squeeze()[10:]
            self.S = data['S_'][:,10:]
            self.t = data['delays_'].squeeze()
            self._Nrepeats = len(self.t)

            ### store original sequence of repeats and info on what repeats (exposures) are blanks
            original_order    = []
            blanks            = []
            for i in range(len(self.t)):
                blanks            += [self.t[i] < 0] ## negative time means blank
                original_order    += [i]
            self._blanks            = np.array(blanks)
            self._original_order    = np.array(original_order)

        else:
            raise ValueError('INVALID EXPERIMENT ID')


    
    def calcDsig(self, method='average', useAzi=False):
        """
        Generates the change in scattering intensity from the absolute scattering.
        Will fail if no suitable unpumped shots are present in the run. 
        Nearest-neighbor search not implemented.
        
        Kwargs:
            method           (bool) : Specify method of generating the difference signal.
            useAzi           (bool) : If True, use the azimuthally averaged off shots for all signals. 
                                      If False use the respective signal's off shots.
            ovrWrt           (bool) : 
        
        TODO
        Implement Nearest-neighbor and interpolate (MatLab script style).
        
        """
        hdr = self._hdr[self.runnr]
        
        p_mask = self._Xon*self._Lon
        u_mask = self._Xon*~self._Lon
        
        #self._hdr[self.runnr]['_DsS'] = []
        for sig in self._hdr[self.runnr]['_AsS']:
            
            sig_p = getattr(self,'{!s}'.format(*sig))[:,p_mask]
            
            if not useAzi:
                sig_u = getattr(self,'{!s}'.format(*sig))[:,u_mask]
            else:
                sig_u = getattr(self,'{!s}'.format('S'))[:,u_mask]
            
            if method=='interpolate':
                raise NotImplementedError("Interpolated unpumped-shot method is not yet implemented.")
            elif method=='average':
                sig_u = np.average(sig_u, axis=-1)
                setattr(self, 'd{!s}'.format(*sig), sig_p - sig_u.reshape(-1,1))
                
            elif method=='nearest neighbor':
                # sort indices of Lon & Loff shots on tag number, these should be measured consecutively!
                tagList = self.tags.astype(np.int)
                
                p_mask = self._Xon*self._Lon
                u_mask = self._Xon*~self._Lon
                
                LonIdx  = np.argsort(tagList[p_mask])
                LoffIdx = np.argsort(tagList[u_mask])
                
                tagOn  = tagList[p_mask][LonIdx]
                tagOff = tagList[u_mask][LoffIdx]
                
                tag_bin_lims = (tagOff[:-1] + np.diff(tagOff)/2.).astype(np.int)
                binnedtags   = np.digitize(tagOn, tag_bin_lims, right=False)
                
                sig   = getattr(self,'{!s}'.format('S'))
                setattr(self, 'd{!s}'.format(*sig), np.zeros_like(sig_p))
                if not np.all(tagList==np.sort(tagList)):
                    for i,tag in enumerate(tagOn):
                        idxOn  = np.where(tagList==tag)[0]
                        idxOff = np.where(tagList==tagOff[binnedtags[i]])[0]
                        getattr(self, 'd{!s}'.format(*sig))[:,idxOn] = sig[:,idxOn]-sig[:,idxOff].reshape(-1,1)
                else:
                    for tagOffIdx in np.unique(binnedtags):
                        idxOn  = np.searchsorted(tagList, tagOn[binnedtags==tagOffIdx])
                        idxOff = np.searchsorted(tagList, tagOff[tagOffIdx])
                        getattr(self, 'd{!s}'.format(*sig))[:,idxOn] = sig[:,idxOn]-sig[:,idxOff].reshape(-1,1)
                
                raise NotImplementedError("Nearest neighbor unpumped-shot method is not yet implemented.")
            
            if 'd{!s}'.format(*sig) not in ['{0}'.format(*el) for el in hdr['_DsS']]:
                hdr['_DsS'] += [(('d{!s}'.format(*sig)),)]
        
        self._dt         = self.t[p_mask]
        self._dTTtagMask = self._TTtagMask[p_mask]
        self._dNrepeats  = self._dTTtagMask.size
        for elem in [('_dt',),('_dTTtagMask',),('_dNrepeats',)]:
            if '{}'.format(*elem) not in ['{0}'.format(*el) for el in hdr['_DdV']]:
                hdr['_DdV'] += [elem]
        #bork
        print("Run {!s}: Generated the following signals: {!s}. ".format(self.runnr,', '.join(map(str, hdr['_DsS']))))
        return
    
    def TTtagging(self, tol=None, plot=None):
        
        # get rid of nans
        
        # hard Time Tool pixel limits rejection
        self._TTtagMask[self._TTtagMask] *= (self._TTpos[self._TTtagMask]>min(self._TTpixLim))*(self._TTpos[self._TTtagMask]<max(self._TTpixLim))
        # rejection based on median etc.
        if self._TTtagMask.any() and type(tol)!=type(None):
            TTt0, tolerance, isValid = self._filter1D(self._TTpos, isValid=self._TTtagMask, tol=tol, plot=plot, out=True)
            self._TTtagMask *= isValid
            
        elif type(tol)==type(None):
            TTt0 = np.median(self._TTpos[self._TTtagMask])
            #raise ValueError("tol cannot be None!")
        
        self.t[self._TTtagMask] -= ((self._TTpos[self._TTtagMask]-TTt0)* self._TTscale/1000.)
        print("Run {0}: TTtagging() tagged {1} out of {2} repeats ({3:.0%})".format(self.runnr, len(self._TTtagMask[self._TTtagMask]), self._Nrepeats, float(len(self._TTtagMask[self._TTtagMask]))/self._Nrepeats))
        
        return
    
    def printStats(self):
        
        
        
        return
    
    def rejectS(self, qrange=[1.5,2.0], tol='3percent', plot=False):
        """
        Rejects individual exposures based on the deviation of their q-averaged absolute scattering intensities, compared to the median of the whole run.
        @param qrange: q-range over which to average. Defaults to 1.5/A < q < 2.0/A.
        @type qrange: list
        @param tol: The tolerance cutoff. Can be 'XXpercent' or 'XXunits', where XX is a number in any format. With 'percent', the cutoff is defined as a percentage of the median, with 'units' it is absolute. All exposures for which the q-averaged scattering falls further than one cutoff distance from the median are rejected.
        @type tol: str
        @param plot: Whether or not to plot a histogram of the run, which helps in setting reasonable ranges.
        @type plot: bool
        """

        # the statistic for each repeat (exposure) is the q-averaged signal
        # over a particular q-range.
        statistics = np.mean( self.S[:, (self.q>qrange[0])&(self.q<qrange[1])], axis=1)

        # calculate median and tolerances
        median = np.median( statistics )
        if 'percent' in tol:
            tolerance = float(tol.split('percent')[0]) * .01 * median
        elif 'units' in tol:
            tolerance = float(tol.split('units')[0])
        elif 'std' in tol: 
            tolerance = float(tol.split('std')[0]) * np.std(statistics-median)
        else:
            raise Exception('Bad input')

        # work out which repeats to keep:
        # keep = np.arange(len(statistics))[np.abs(statistics-median) < tolerance]
        keep = np.abs(statistics-median) < tolerance
        self.keep = keep
        self.out = np.invert(keep)
        print('Run %u: rejectS() rejected %u out of %u repeats (%.0f%%)'%(self.runnr, self._Nrepeats-np.sum(keep), self._Nrepeats, float(self._Nrepeats-np.sum(keep))/self._Nrepeats*100))

        # it's importatnt to modify _blanks, _original_order and Nrepeats as well as S and t
        #self.S                  = self.S[keep, :]
        #self._blanks            = self._blanks[keep]
        #self._original_order    = self._original_order[keep]
        #self._Nrepeats          = len(keep)
        #self.t                  = self.t[keep]
        #self.keep = keep
        #mask = np.ones(len(self.S), np.bool)
        #mask[keep] = 0
        #self.out = np.where(mask)[0]
        
        # optionally plot:
        if plot:
            fig, axs = plt.subplots(2,1)
            # Plot histogram
            ax = axs[1]
            ax.hist(statistics, bins = 50)
            ax.axvline(median-tolerance, color='r')
            ax.axvline(median+tolerance, color='r')
            #ax.set_title('Histogram for run %u t=%.3f ns' % (self.runnr, np.average(self.t))
            ax.axes.set_xlabel('Sum in q-range used for outlier rejection (%s)' % tol)
            ax.axes.set_ylabel('Counts')            
            # Plot scattering
            ax = axs[0]
            if np.sum(self.keep) > 0:
                hkeep = ax.plot(self.q, np.log10(self.S[self.keep].T), color='blue', alpha=.5, linewidth=.5)
            print(np.sum(self.out))
            if np.sum(self.out) > 0:
                hout = ax.plot(self.q, np.log10(self.S[self.out].T), color='red', alpha=.5, linewidth=.5)
            ax.axvline(qrange[0], color='r')
            ax.axvline(qrange[1], color='r')
            if np.sum(self.out)*np.sum(self.keep) != 0:
                ax.legend((hkeep[0], hout[0]), ['Keep(%s)'%np.sum(self.keep), 'Out(%s)'%np.sum(self.out)]) 
            ax.axes.set_xlabel('q / $\AA^{-1}$')
            ax.axes.set_ylabel('log10(S)')
            #if len(np.unique(t)) == 1:
            #    
            ax.set_title('Spectra with q-range for outlier rejection')
            #fig.suptitle('Run %u'%self.runnr)
            plt.tight_layout()
            fig.show()
    
    def _filter1D(self, vec1D, isValid=None, tol='1percent', plot=False, out=False):
        """
        Shot rejection workhorse. Should be nan proof!
        
        Args:
            
        Kwargs:
            
        """
        
        if type(isValid)==type(None):
            isValid = ~np.isnan(vec1D)
        elif isValid.shape == vec1D.shape:
            isValid = isValid
        else:
            raise ValueError("Shape mismatch between last dimension of vec1D (%s) and isValid mask (%s)."%(isValid.shape,vec1D.shape))
            
        # calculate median and tolerances
        if 'percent' in tol:
            median = np.median(vec1D[isValid])
            tolerance = float(tol.split('percent')[0]) * .01 * median
        elif 'units' in tol:
            median = np.median(vec1D[isValid])
            tolerance = float(tol.split('units')[0])
        elif 'sigma' in tol:
            median = np.median(vec1D[isValid])
            tolerance = float(tol.split('sigma')[0])*np.std(vec1D[isValid])
        elif 'norm' in tol:
            norm = sp.stats.norm
            median, std = norm.fit(vec1D[isValid]) # I know this function gives the mean, not the median, but I want to use this in the following.
            tolerance = float(tol.split('norm')[0])*std
        else:
            raise Exception('Bad input')
        
        # Modify isValid mask accordingly
        isValid[isValid] *= (np.abs(vec1D[isValid]-median) < tolerance)
        
        if plot!=False and type(plot)!=type(None):
            if not plt.isinteractive():
                plt.ion()
                plt.draw()
            plt.cla()
            if 'norm' in tol:
                plt.hist(vec1D[~np.isnan(vec1D)], bins = 50, histtype='stepfilled', alpha=0.5, color='blue', normed=True)
                x = np.linspace(*plt.xlim(), num=100)
                plt.plot(x, norm.pdf(x), loc=median, scale=std)
            else:
                plt.hist(vec1D[~np.isnan(vec1D)], bins = 50, histtype='stepfilled', alpha=0.5, color='blue')
            plt.axvline(median-tolerance, color='r')
            plt.axvline(median+tolerance, color='r')
            plt.title('Run %u'%self.runnr)
            plt.ylabel('no shots.')
            #plt.xlabel(''.format(xlab)) "average absolute intensity over {}-{}x10-1 nm-1.'.format(*qrange)"
            plt.draw()
            plt.pause(1e-15)
        
        if not out:
            return isValid
        else:
            return (median, tolerance, isValid)
    
    def _checkNumShot(self, data, isValid=None):
        """
        Internal check whether the number of shots in self._Nrepeats is the same as the last dimension of the scattering signals.
        """
        if type(isValid)!=type(None):
            if isValid.shape[-1] != data.shape[-1]:
                raise ValueError("The number of shots does not correspond to the size of the last dimension.")
            else:
                return
        else:
            raise ValueError("isValid needs to be something.")
        
    def removeShots(self, isValid=None, VarOrKey=None):
        """
        Removes the dark shots via a boolean mask in a manner depending on the experiment:
            self._expID:
                'sacla2016A8037': uses the self._Xon as a mask to keep the "X-ray on" shots.
                                  Modifies the shot dependent variables.
                the rest        : not implemented
        """
        
        if self._expID=='sacla2016A8037':
            
            hdr = self._hdr[self.runnr]
            
            if type(isValid)!=type(None):
                isValid = isValid
            else:
                isValid = self._Xon
            
            print('Run {0}: removeShots() removed {1} out of {2} repeats ({3}%)'.format(self.runnr, self._Nrepeats-len(isValid[isValid]), self._Nrepeats, float(self._Nrepeats-len(isValid[isValid]))/self._Nrepeats*100))
            
            if type(VarOrKey)!=type(None):
                self._applyIsValid(isValid=isValid, VarOrKey=VarOrKey)
            else:
                self._applyIsValid(isValid=isValid, VarOrKey='_AsS')
                self._applyIsValid(isValid=isValid, VarOrKey='_SdK')
                self._applyIsValid(isValid=isValid, VarOrKey='_SdV')
        else:
            raise NotImplementedError
        return
            
    def _applyIsValid(self, isValid=None, VarOrKey=None, report=False):
        """
        Applies supplied mask to supplied variables & keys.
        """
        rex = re.compile(r'_(\S)(\S)(\S)')
        
        hdr = self._hdr[self.runnr]
        
        if not rex.match(VarOrKey):
            raise ValueError("Supply VarOrKey.")
        else:
            typ = rex.match(VarOrKey).groups()
        vList, kList, sList = [],[],[]
        for keys in hdr[VarOrKey]:
            if typ[0]=='S' and typ[2]=='K':
                if keys[0]!='motor_positions':
                    self._checkNumShot(hdr[keys[0]][keys[1]], isValid=isValid)
                    hdr[keys[0]][keys[1]] = hdr[keys[0]][keys[1]][isValid]
                    kList.append('[\'%s\'][\'%s\']'%(keys))
                else:
                    self._checkNumShot(hdr[keys[0]], isValid=isValid)
                    hdr[keys[0]] = hdr[keys[0]][:,isValid]
                    kList.append('[\'%s\'][\'%s\']'%(keys))
                
            elif typ[0]=='S' and typ[2]=='V':
                if keys[0]!='_Nrepeats':
                    self._checkNumShot(getattr(self, keys[0]), isValid=isValid)
                    setattr(self, keys[0], getattr(self, keys[0])[isValid])
                    vList.append(keys[0])
                else:
                    setattr(self, keys[0], len(isValid[isValid]))
                    vList.append(keys[0])
                    
            elif typ[0]=='D' and typ[2]=='K':
                if keys[0]!='motor_positions':
                    self._checkNumShot(hdr[keys[0]][keys[1]], isValid=isValid)
                    hdr[keys[0]][keys[1]] = hdr[keys[0]][keys[1]][isValid]
                    kList.append('[\'%s\'][\'%s\']'%(keys))
                else:
                    self._checkNumShot(hdr[keys[0]], isValid=isValid)
                    hdr[keys[0]] = hdr[keys[0]][:,isValid]
                    kList.append('[\'%s\'][\'%s\']'%(keys))
                
            elif typ[0]=='D' and typ[2]=='V':
                if keys[0]!='_dNrepeats':
                    self._checkNumShot(getattr(self, keys[0]), isValid=isValid)
                    setattr(self, keys[0], getattr(self, keys[0])[isValid])
                    vList.append(keys[0])
                else:
                    setattr(self, keys[0], len(isValid[isValid]))
                    vList.append(keys[0])
                    
            elif typ[0]=='A' and typ[1]=='s':
                if typ[2]=='S':
                    self._checkNumShot(getattr(self, keys[0]), isValid=isValid)
                    setattr(self, keys[0], getattr(self, keys[0])[:,isValid])
                    sList.append(keys[0])
                elif typ[2]=='Q':
                    self._checkNumShot(getattr(self, keys[0]), isValid=isValid)
                    setattr(self, keys[0], getattr(self, keys[0])[isValid,:])
                    sList.append(keys[0])
                    
            elif typ[0]=='D' and typ[1]=='s':
                if typ[2]=='S':
                    self._checkNumShot(getattr(self, keys[0]), isValid=isValid)
                    setattr(self, keys[0], getattr(self, keys[0])[:,isValid])
                    sList.append(keys[0])
                elif typ[2]=='Q':
                    self._checkNumShot(getattr(self, keys[0]), isValid=isValid)
                    setattr(self, keys[0], getattr(self, keys[0])[isValid,:])
                    sList.append(keys[0])
            
            else:
                raise NotImplementedError
        
        if report:
            print("Run {0}: Adjusted the number of shots of the following variables: {1!s}; the following variables in _hdr[\'{0}\']: {1!s}; and the following signals: {1!s}..".format(self.runnr,', '.join(map(str, vList)),', '.join(map(str, kList)),', '.join(map(str, sList))))
        
        return
    

    def normalize(self, qrange=[2.1, 2.2], use_diode=False, use_T=False):
        """
        Normalize the absolute scattering intensities so that its average over a certain q-range is 1.
        @param qrange: q-range to normalize over
        @type qrange: list
        """
        if use_diode:
            if  (not hasattr(self, 'diode')):
                print("No diode readout available! Use with use_diode=False")
            else:
                for i in range(self._Nrepeats):
                    self.S[i, :] /= self.diode[i]             
#                # Scale with diode readout divided by median of diode readouts
#                scale = self.diode / np.median(self.diode)
#                for i in range(self._Nrepeats):
#                    self.S[i, i] /= scale[i]
#                self.scale = scale
        elif use_T:
            if  (not hasattr(self, 'T')):
                print("No transmittance readout available! Use with use_T=False")
            else:
                for i in range(self._Nrepeats):
                    self.S[i, :] /= self.T[i]            
        else:
            # Simple
            for i in range(self._Nrepeats):
                self.S[i, :] /= np.mean(self.S[i, (self.q>qrange[0]) & (self.q<qrange[1])])
#            # Determine scaling factors
#            scale = np.zeros(self._Nrepeats)
#            for i in range(self._Nrepeats):
#                scale[i] = np.mean(self.S[i, (self.q>qrange[0]) & (self.q<qrange[1])])
#            # Normalize to median, so that outlier rejection does not need to be modified
#            scale /= np.median(scale)
#            # Now normalize
#            for i in range(self._Nrepeats):
#                self.S[i, :] /= scale[i]
#            self.scale = scale    
    
    def subtract(self):
        """
        Subtracts blanks from each exposure in the run, by removing the interpolation of the blanks before and after. Blanks are identified for each run when loading it, based on experiment-specific criteria.
        """

        self.dS = []
        # go through all repeats, for each one find blanks before and
        # after, then take the appropriate intepolated difference.
        for i in range(self._Nrepeats):

            # only proceed for non-blank repeats: for PP you want the blank dS too
            #if (self._blanks[i]): continue

            # find blank before:
            j = i - 1
            beforeblank = -1
            while j > -1:
                if self._blanks[j]:
                    beforeblank = j
                    break
                j -= 1

            # find blank after:
            j = i + 1
            afterblank = -1
            while j < self._Nrepeats:
                if self._blanks[j]:
                    afterblank = j
                    break
                j += 1

            if (beforeblank<0) or (afterblank<0):
                print('    no blank for repeat %u, kicking out.'%i)
                self.dS += [self.S[i,:] * np.nan]
                continue

            afterspace  =   self._original_order[afterblank]  - self._original_order[i]
            beforespace = -(self._original_order[beforeblank] - self._original_order[i])
            blankspace = float(afterspace + beforespace)
            # don't interpolate too far:
            if (beforespace>3) or (afterspace>3):
                self.dS += [self.S[i,:] * np.nan]
                continue

            self.dS += [self.S[i,:] - afterspace/blankspace*self.S[beforeblank,:] - beforespace/blankspace*self.S[afterblank,:] ]

        self.dS     = np.array(self.dS)

    def rejectSteps(self, qrange=[1.5,2.0], tol='.5percent', plot=False):
        """
        Rejects shots which lie between a negative timepoint and a jump in the signal. This is an ad-hoc correction method which was necessary when a syringe pump was used at BioCARS and id09b.
        """

        # the statistic for each repeat (exposure) is the q-averaged signal
        # over a particular q-range.
        statistics = np.mean( self.S[:, (self.q>qrange[0])&(self.q<qrange[1])], axis=1)

        # calculate the tolerances
        if 'percent' in tol:
            median = np.median( statistics )
            tolerance = float(tol.split('percent')[0]) * .01 * median
        elif 'units' in tol:
            tolerance = float(tol.split('units')[0])
        else:
            raise Exception('Bad input')

        keep = np.ones(self.S.shape[0], dtype=bool)
        for i in range(1, len(statistics)):
            if np.abs(statistics[i] - statistics[i-1]) > tolerance:
                if plot:
                    plt.plot(2*[self._original_order[i]], [np.min(statistics), np.max(statistics)], 'k')
                j = i-1;
                while (self.t[j]>0):
                    keep[j] = False
                    j -= 1
                    if (j <= 0):
                        break
                j = i;
                while (self.t[j]>0):
                    keep[j] = False
                    j += 1
                    if (j >= len(keep)):
                        break

        print('Run %u: rejectSteps() rejected %u out of %u repeats (%.0f%%)'%(self.runnr, self._Nrepeats-len(keep[(keep==True)]), self._Nrepeats, float(self._Nrepeats-len(keep[(keep==True)]))/self._Nrepeats*100))

        # optionally plot:
        if plot:
            #plt.figure()
            plt.plot(self._original_order[keep], statistics[keep], 'o-')
            plt.plot(self._original_order[(self.t<0)], statistics[(self.t<0)], 'rs')
            plt.show()

        # As above, it's important to modify _blanks, _original_order and Nrepeats as well as S and t
        self.S                  = self.S[keep, :]
        self._blanks            = self._blanks[keep]
        self._original_order    = self._original_order[keep]
        self._Nrepeats          = len(keep[(keep==True)])
        self.t                  = self.t[keep]

    def average_run(self):
        '''
        This is a simple average run
        Use it after outliers have been rejected
        with run.rejectS(plot=True)
        '''
        self.S_av = np.average(self.S[self.keep], axis=0)
        print("Used %i out of %i spectra for averaging" % (np.sum(self.keep), len(self.S)))
        
    def sn_ratio(self, sg_pol=2, sg_window=11, plot=False):
        '''
        This is an attempt to estimate signal to noise for the X-ray power titration
        ''' 
        if  (not hasattr(self, 'S_av')):
            print("There is no average scattering in this dataset!")
            return None
        smoothed = ssi.savgol_filter(self.S_av, sg_window, sg_pol)
        # Measure for signal is sum of abs scattering
        # measurement for noise is rmsd of deviation from smoothed
        self.signal = np.sum(self.S_av)
        self.noise = np.sum((self.S_av - smoothed)**2)
        self.sn = self.signal / self.noise / np.sqrt(len(self.keep))
        if plot:
            fig, axs = plt.subplots(1,1)
            axs.plot(self.q, self.S_av, self.q, smoothed)
            fig.show()
            
    def drift(self, qrange=[.1, .5], plot=False):
        '''
        Look for drifts
        '''
        sums = []
        for S in self.S:
            sums.append(np.sum(S[(self.q>qrange[0]) & (self.q<qrange[1])]))
        self.sums = np.array(sums)
        if plot==True:
            fig, axs = plt.subplots(1, 1)
            for i in range(len(self.t)):
                if self.keep[i]:
                    color='green'
                else:
                    color='red'
                axs.plot(i, self.sums[i], 'o',color=color)
            axs.plot(range(len(self.t)), self.sums, color='gray')
            # Create label with times
            tlabel = []
            for t in np.unique(self.t):
                tlabel.append('%.1E ms ' % (t*1e-6))
            axs.set_title('Sum of signal in qrange = %.1E - %.1E \n Times: %s' % (qrange[0], qrange[1], tlabel))
            axs.axes.set_xlabel('q / $\AA^{-1}$')
            axs.axes.set_ylabel('sum(S)')
            fig.show()

            
class Run_RR:
    """
    Class that holds one Rapid-Readout (RR) run, like from Pilatus or Eiger detectors
    @param expID: The experiment ID, such as 'sls2011' or 'sls2015'
    @type expID: str
    """

    def __init__(self, expID=''):
        """
        Constructs a Run_RR object, with initialized hardware parameters such as detector distance, X-ray energy, and pixel size.
        """
        self._expID = expID;
        if expID == 'sls2011':
            self._wavelength         = 12.39752/13.6 # Å
            self._pixel_size         = .172          # mm
            self._detector_distance  = 257.7         # mm
            self._matfile_names      = 'e13436_2_%05u_00000_00000_integ.mat'
        elif expID == 'sls2012':
            self._wavelength         = 12.39752/11   # Å
            self._pixel_size         = .172          # mm
            self._detector_distance  = 190.5         # mm
            self._matfile_names      = 'e13727_2_%05u_00000_00000_integ.mat'
        elif expID == 'sls2014':
            self._wavelength         = 12.39752/11.2 # Å
            self._pixel_size         = .075          # mm
            self._detector_distance  = 148.96        # mm
            self._matfile_names      = 'e14609_1_%05u_00000_00000_integ.mat'
        elif expID == 'sls2015a':
            # A dual-Pilatus experiment, April 2015, first measurements (see logbook for which)
            self._wavelength         = 12.39752/11.2 # Å
            self._pixel_size         = .172          # mm
            self._detector_distance  = 2140          # mm
            self._matfile_names      = 'e15509_1_%05u_00000_00000_integ.mat'
            self._matfile2_names     = 'e15509_2_%05u_integ.mat'
            self._scale_waxs         = 1 # not used!
        elif expID == 'sls2015b':
            # A dual-Pilatus experiment, April 2015, most measurements (see logbook for which)
            self._wavelength         = 12.39752/11.2 # Å
            self._pixel_size         = .172          # mm
            self._detector_distance  = 2140          # mm
            self._matfile_names      = 'e15509_1_%05u_00000_00000_integ.mat'
            self._matfile2_names     = 'e15509_2_%05u_integ.mat'
            self._scale_waxs         = .075
        else:
            raise ValueError('INVALID EXPERIMENT ID')


    def loadmat(self, folder, runnr, freq=100, folder2='', scale_waxs=1, detector_q_border=.5, offset=0.0):
        """
        Loads integrated data into the current dataset.
        @param folder: Base folder where all the runs are kept.
        @type folder: str
        @param runnr: The run number to load.
        @type runnr: int
        @param freq: The frequency of the detector exposures, in Hz.
        @type freq: float, int
        @param folder2: The second base folder, for dual-detector experiments.
        @type folder2: str
        @param scale_waxs: The factor by which to scale the second detector intensities before appending the numbers.
        @type scale_waxs: float
        @param detector_q_border: The cutoff q value between the two detectors, the data from 'folder' is used for lower q, the data from 'folder2' is used for higher q.
        @type detector_q_border: float
        @param offset: Time offset to apply to the first exposure.
        @type offset: float
        """

        if ((self._expID == 'sls2014')):
            ### load mat file
            matfile = self._matfile_names%runnr
            if folder[-1] != '/':
                folder += '/'
            # Here we need to check scipy.__version__: the variable_names kwarg doesn't exist in scipy 0.7.2 (cSAXS)
            #data = scipy.io.loadmat(folder + matfile, variable_names=['I_all', 'radius', 'filenames_all'])
            data = scipy.io.loadmat(folder + matfile)
            self.runnr = runnr

            ### calculate q
            q       = 4*np.pi/self._wavelength*np.sin(np.arctan(data['radius']*self._pixel_size/self._detector_distance)/2);
            Nq      = np.max(q.shape)
            self.q  = np.reshape(q, Nq)

            ### reshape the data matrix to (repeat, q, time)
            Nrepeats  = len(data['filenames_all'])
            Ntimes  = data['I_all'].shape[2]/Nrepeats
            print(Nrepeats, Nq, Ntimes)
            self.S  = np.zeros((Nrepeats, Nq, Ntimes))
            for i in range(Nrepeats):
                self.S[i,:,:] = data['I_all'][:, 0, i*Ntimes:(i+1)*Ntimes]
            self.Nrepeats = Nrepeats

            ### calculate t
            self.t = np.arange(Ntimes)*1.0/freq + offset
            self.Ntimes = len(self.t)

            ### store original sequence of repeats and info on what repeats are blanks
            original_order    = []
            blanks            = []
            for i in range(Nrepeats):
                blanks            += [not bool(i%2)] ## every other repeat is a blank
                original_order    += [i]
            self._blanks            = np.array(blanks)
            self._original_order    = np.array(original_order)

        ### In 2015, I (Alex) found they'd changed the format of their
        ### mat-files. Specifically, the length of filenames_all can no
        ### longer be used to determine the number of repeats, and also
        ### now the mat-files can contain incomplete repeats.
        ### The following code is adapted for that.
        ### It later turned out that the format matched that of 2012.
        if (self._expID == 'sls2015a' or self._expID == 'sls2015b' or self._expID == 'sls2012' or self._expID == 'sls2011'):

            ### SAXS
            ### load mat file
            matfile = self._matfile_names%runnr
            if folder[-1] != '/':
                folder += '/'
            # Here we need to check scipy.__version__: the variable_names kwarg doesn't exist in scipy 0.7.2 (cSAXS)
            #data = scipy.io.loadmat(folder + matfile, variable_names=['I_all', 'radius', 'filenames_all'])
            data = scipy.io.loadmat(folder + matfile)
            self.runnr = runnr

            ### calculate q
            q       = 4*np.pi/self._wavelength*np.sin(np.arctan(data['radius']*self._pixel_size/self._detector_distance)/2);
            Nq      = np.max(q.shape)
            self.q  = np.reshape(q, Nq)

            ### reshape the data matrix to (repeat, q, time)
            filenames_all = data['filenames_all'].squeeze()
            # In order to support incomplete integrated mat files, we find
            # Ntimes by going down the list of files and looking for the end
            # of the first repeat
            Nfiles = data['I_all'].shape[2]
            for i in range(Nfiles):
                if int(filenames_all[i][0][15:20]) == 1:
                    Ntimes = i
                    break
            # Nrepeats is then found by integer division, so it's the
            # number of complete repeats
            Nrepeats = Nfiles / Ntimes
            self.S  = np.zeros((Nrepeats, Nq, Ntimes))
            for i in range(Nrepeats):
                self.S[i,:,:] = data['I_all'][:, 0, i*Ntimes:(i+1)*Ntimes]
            self.Nrepeats = Nrepeats

            ### calculate t
            self.t = np.arange(Ntimes)*1.0/freq + offset
            self.Ntimes = len(self.t)

            ### store original sequence of repeats and info on what repeats are blanks
            original_order    = []
            blanks            = []
            for i in range(Nrepeats):
                blanks            += [not bool(i%2)] ## every other repeat is a blank
                original_order    += [i]
            self._blanks            = np.array(blanks)
            self._original_order    = np.array(original_order)

            ### WAXS

            try:
                ### load the mat file
                matfile = self._matfile2_names%runnr
                if folder2[-1] != '/':
                    folder2 += '/'
                # Check scipy.__version__ here too!
                #data = scipy.io.loadmat(folder + matfile, variable_names=['I_all', 'radius', 'filenames_all'])
                data = scipy.io.loadmat(folder2 + matfile)

                ### calculate q
                # here, the cSAXS mat file already contains calculated q:s, called 'radius'...
                q2 = data['radius']
                Nq2 = np.max(q2.shape)
                self._q2 = np.reshape(q2, Nq2)

                ### reshape the data matrix to (repeat, q, time)
                self._S2  = np.zeros((Nrepeats, Nq2, Ntimes))
                for i in range(Nrepeats):
                    self._S2[i,:,:] = self._scale_waxs*data['I_all'][:, 0, i*Ntimes:(i+1)*Ntimes]

                ### crop the two detector readings at detector_q_border and put them together
                self._S1 = self.S
                self._q1 = self.q
                self.S = np.hstack(( self._S1[:, (self._q1<=detector_q_border), :], self._S2[:, (self._q2>detector_q_border), :]))
                self.q = np.concatenate(( self._q1[(self._q1<=detector_q_border)], self._q2[(self._q2>detector_q_border)]))

            except ValueError:
                print('    WAXS data doesn\'t match SAXS data, disregarding the former.')

            except:
                if self._expID != 'sls2012':
                    print('    no integrated WAXS files found, disregarding this.')

            ### output message
            print('    %d repeats loaded, %d timepoints in each.'%(Nrepeats, Ntimes))

    def reject(self, repeats=[]):
        """
        Rejects a certain set of repeats, known to be bad.
        @param repeats: Repeat numbers to throw out, the first one being number 0.
        @type repeats: int, list
        """

        if type(repeats) == int:
            repeats = [repeats]
        keep = [i for i in range(self.S.shape[0]) if (i not in repeats)]

        # it's importatnt to modify _blanks, _original_order and Nrepeats as well as S
        self.S                  = self.S[keep, :, :]
        self._blanks            = self._blanks[keep]
        self._original_order    = self._original_order[keep]
        self.Nrepeats             = len(keep)


    def rejectS(self, qrange=[1.5,2.0], tol='3prcnt', plot=False):
        """
        Rejects repeats based on their absolute intensity in some q-range. This number is found by averaging over all the exposures in the repeat, and over the q-range of intereset.
        @param qrange: q-range over which to average. Defaults to 1.5/A < q < 2.0/A.
        @type qrange: list
        @param tol: Tolerance, in the same format as Run_PP.rejectS().
        @type tol: str
        @param plot: Whether or not to plot the histogram of average intensities, useful for setting appropriate ranges.
        @type plot: bool
        """

        # the statistic for each repeat is the time- and q-averaged signal
        # over a particular q-range.
        statistics = np.mean( np.mean( self.S[:, (self.q>qrange[0])&(self.q<qrange[1]), :], axis=2), axis=1)

        # calculate median and tolerances
        median = np.median( statistics )
        if 'percent' in tol:
            tolerance = float(tol.split('percent')[0]) * .01 * median
        elif 'units' in tol:
            tolerance = float(tol.split('units')[0])
        else:
            raise Exception('Bad input')

        # work out which repeats to keep:
        keep = np.arange(len(statistics))[np.abs(statistics-median) < tolerance]
        print('Run %u: rejected %u out of %u repeats (%.0f%%)'%(self.runnr, self.Nrepeats-len(keep), self.Nrepeats, float(self.Nrepeats-len(keep))/self.Nrepeats*100))

        # optionally plot:
        if plot:
            plt.hist(statistics, bins = 50)
            plt.axvline(median-tolerance, color='r')
            plt.axvline(median+tolerance, color='r')
            plt.title('Run %u'%self.runnr)
            plt.show()

        # it's importatnt to modify _blanks, _original_order and Nrepeats as well as S
        self.S                  = self.S[keep, :, :]
        self._blanks            = self._blanks[keep]
        self._original_order    = self._original_order[keep]
        self.Nrepeats             = len(keep)


    def normalize(self, qrange):
        """
        Normalize the absolute scattering intensities so that its average over a certain q-range is 1.
        @param qrange: q-range to normalize over
        @type qrange: list
        """
        if np.max(self.q) < qrange[1]:
            print('    cannot normalize, insufficient q-range.')
            return
        for i in range(self.Nrepeats):
            for j in range(self.Ntimes):
                self.S[i, :, j] /= np.mean(self.S[i, (self.q>qrange[0]) & (self.q<qrange[1]) , j])


    def subtract(self):
        """
        Subtracts blanks from each repeat in the run, by removing, for each timepoint, the interpolation of corresponding timepoint in the the blanks before and after. Blanks are identified for each run when loading it, based on experiment-specific criteria.
        """

        self.dS = []
        # go through all repeats, for each one find blanks before and
        # after, then take the appropriate intepolated difference.
        for i in range(self.Nrepeats):

            # only proceed for non-blank repeats:
            if (self._blanks[i]): continue

            # find blank before:
            j = i - 1
            beforeblank = -1
            while j > -1:
                if self._blanks[j]:
                    beforeblank = j
                    break
                j -= 1

            # find blank after:
            j = i + 1
            afterblank = -1
            while j < self.Nrepeats:
                if self._blanks[j]:
                    afterblank = j
                    break
                j += 1

            if (beforeblank<0) or (afterblank<0):
                print('    no blank for repeat %u, kicking out.'%i)
                continue

            afterspace  =   self._original_order[afterblank]  - self._original_order[i]
            beforespace = -(self._original_order[beforeblank] - self._original_order[i])
            blankspace = float(afterspace + beforespace)

            # don't interpolate too far:
            if (beforespace>3) or (afterspace>3): continue

            self.dS += [self.S[i,:,:] - afterspace/blankspace*self.S[beforeblank,:,:] - beforespace/blankspace*self.S[afterblank,:,:] ]

        else:
            raise Exception('ILLEGAL SUBTRACTION MODE')

        self.dS     = np.array(self.dS)
        self.dSav   = np.mean(np.mean(self.dS, axis=2), axis=0)

class Proc_PP:
    """Class that holds one fully processed matlab/ascii file of a pump-probe experiment, like that from SACLA.

    "private" variables:
            _expID                  ; unique experiment ID, allows variations in setup, data file format, etc.
            _runfoldername          ; format string such that _runfoldername%nr is the run subfolder for run nr.
            _logfilename            ; the name of the logfile within that subfolder
            _blanks                 ; bool array, says if a repeat is a blank or not.
            _original_order         ; original sequence of repeats, needed for interpolation after outlier rejection.
            _Nrepeats               ; total number of exposures in this run, not used for accounting just for convenience

    "public" variables:
            q                       ; q-array
            t                       ; array of delay times, one for each exposure, in nanoseconds
            S[exposure, q]          ; absolute
            dS[exposure, q]         ; difference
            sigma[exposure, q]      ; error

    methods:
            loadmat()               ; load from integrated and processed matlab file
            reject()
            normalize()
            subtract()
            rejectSteps()           ; reject based on shot-to-shot jumps in the data
    """

    def __init__(self, expID=''):
        self._expID = expID
        self._unit = {}
        self._quant = {}
        if self._expID == 'sacla2015mat':
            self._logfilename   = ''
            self._runfoldername = ''
            self._matfile_names = '%s%s%s'
        elif self._expID in ('sacla2016AziMat','sacla2016mat','lcls2016AziMat','lcls2016mat'):
            self._logfilename   = ''
            self._runfoldername = ''
            self._matfile_names = '%s%s%s'
        elif self._expID in ('2016A8037', 'LN34'):
            self._logfilename   = ''
            self._runfoldername = ''
            self._matfile_names = '%s%s%s'
        elif (self._expID == 'DTUtxt' or self._expID == 'ascii'):
            self._logfilename   = ''
            self._runfoldername = ''
            self._txtfile_names = '%s%s%s'

        elif self._expID == 'modPack' or self._expID == 'modpack':
            self._logfilename   = ''
            self._runfoldername = ''
            self._txtfile_names = '%s%s%s'

        elif (self._expID == 'rdSrdpt'):
            self._logfilename     = ''
            self._runfoldername   = ''
            self._asciifile_names = '%s%s%s'
        elif (self._expID == 'qdSdpt'):
            self._logfilename     = ''
            self._runfoldername   = ''
            self._asciifile_names = '%s%s%s'
        else:
            raise ValueError('INVALID EXPERIMENT ID')

        self._unit['S']       = r'q'
        self._unit['dS']      = r'q'
        self._unit['dSdT']    = r'q'
        self._unit['dSdRho']  = r'q'
        self._unit['qdS']     = r'q'
        self._unit['rdSr']    = r'r'
        self._unit['q']       = r'\mathring{\mathrm{A}}^{-1}'
        self._unit['r']       = r'\mathring{\mathrm{A}}'
        self._unit['t']       = r'ps'
        self._unit['sigma']   = r'q'

        self._quant['S']      = r'\mathrm{S}'
        self._quant['dS']     = r'\Delta\mathrm{S}'
        self._quant['dSdT']   = r'\Delta\mathrm{S}\delta T'
        self._quant['dSdRho'] = r'\Delta\mathrm{S}\delta\rho'
        self._quant['qdS']    = r'q\Delta\mathrm{S}'
        self._quant['rdSr']   = r'r\Delta\mathrm{S}'
        self._quant['q']      = r'q'
        self._quant['r']      = r'r'
        self._quant['t']      = r'delay'
        self._quant['sigma']  = r'error'

    def load(self, folder, name, nr):
        """
        Loads fully reduced and processed file containing data from a pump-probe
        experiment into self.S[repeat, q, time].
        Expects the q-range (q), time points (t), change in scattering (dI), error (sigma)
        variables nomenclature from the matlab file.

        When loading a 'sacla2015mat' type experiment, load() attempts to load the matlab file
        as an HDF5 format with h5py and numpy. Failing that, it will load the proprietary
        matlab engine for python (matlab required) and attempt to load the file. The matlab
        engine will remain loaded and available until explicitly closed (it takes a
        considerable time to load, and close).

        When loading a 'DTUtxt' type experiment, load() will assume the following structure:
        1st two rows are header, q:column 1, dS:column 2. load will also attempt to read
        the error file in order to extract sigma. The DTU error file has the same format as
        the normal file.

        Args:
                folder  (str)  : folder path containing the matlab file.
                name    (str)  : matlab file prefix.
                nr      (int) : run number, just using the same convention as above.

        """
        import glob

        if folder[-1] != '/':
            folder += '/'

        #
        # Reduced and outlier-rejected dataset from a SACLA experiment performed
        # in June 2015.
        #
        if (self._expID == 'sacla2015mat'):
            #logfile = open(folder + self._logfilename, 'r')
            matfile = self._matfile_names%(os.path.splitext(name)[0],nr,os.path.splitext(name)[1])
            fn = os.path.join(folder, matfile)
            if os.path.isfile(fn):
                try:
                    #print 'hi h5py'
                    import h5py
                    f = h5py.File(fn,'r') #will work for matlab files v7.3
                    if len(list(f.keys()))==1:
                        data = {}
                        keys = list(f[list(f.keys())[0]].keys())
                        data.fromkeys(keys)
                        for elem in keys:
                            #print elem
                            data[elem] = np.asarray(f[list(f.keys())[0]][elem])
                    else:
                        data = {}
                        keys = list(f.keys())
                        data.fromkeys(keys)
                        for elem in keys:
                            data[elem] = np.asarray(f[elem])
                    f.close()
                except IOError:
                    try:
                        #print 'hi scipy'
                        import scipy.io as sio
                        data = sio.loadmat(fn)
                        #print 'bye scipy'
                    except IOError:
                        try:
                            eng.isprime(1) #tests if the matlab engine is loaded
                        except:
                            eng = start_mat_engine() #function in helpers.py
                        data = eng.load(fn,'-mat')[name] #should work for all matlab files

                q = np.asarray(data['q'], dtype=np.float).squeeze()
                t = np.asarray(data['t'], dtype=np.float).squeeze()
                
                if 'I' in data:
                    S = np.asarray(data['I']).squeeze()
                    self._unit['S']  = r'q'
                    self._quant['S'] = r'\mathrm{S}'
                    print('Absolute scattering data found in: %s'%(matfile))
                else:
                    self._unit['S']  = r'q'
                    self._quant['S'] = r'\mathrm{S}'
                    #print 'No absolute scattering data found in: %s'%(matfile)

                if 'dIh' in data and 'dIv' in data:
                    dSh    = np.asarray(data['dIh']).squeeze()
                    sigma_h = np.asarray(data['sigma_h']).squeeze()
                    dSv    = np.asarray(data['dIv']).squeeze()
                    sigma_v = np.asarray(data['sigma_v']).squeeze()
                    self._unit['dSh']    = r'q'
                    self._unit['dSv']    = r'q'
                    self._quant['dSh']   = r'\Delta\mathrm{S}'
                    self._quant['dSv']   = r'\Delta\mathrm{S}'
                    #print 'Anisotropic data found in: %s'%(matfile)
                    if 'dI' not in data:
                        dS = dSh + dSv
                        #print('Isotropic data generated for: %s'%(matfile))
                        sigma = np.sqrt(np.power(sigma_h, 2) + np.power(sigma_v, 2))
                        #print('Sigma for isotropic data generated for: %s'%(matfile))
                    
                if 'dI' in data:
                    dS = np.asarray(data['dI']).squeeze()
                    self._unit['dS']  = r'q'
                    self._quant['dS'] = r'\Delta\mathrm{S}'
                    #print 'Isotropic data found in: %s'%(matfile)
                
                if 'sigma' in data:
                    sigma = np.asarray(data['sigma']).squeeze()
                    self._unit['sigma']  = r'q'
                    self._quant['sigma'] = r'\Delta\mathrm{S}'
                    #print 'Sigma found in: %s'%(matfile)
                elif 'sigma' not in data and not ('dIh' in data and 'dIv' in data):
                    print('No sigma found in: %s'%(matfile))
                        
            else:
                print("%s does not exist in %s."%(matfile, folder))
                return None

            self.q = np.asarray(q)
            self.t = np.asarray(t)
            self.Nrepeats = int(1)

            
            #libwaxs convention is dS(q,t)

            try:
                self.dS = np.asarray(dS)
                if self.dS.shape == (self.t.shape[0], self.q.shape[0]):
                    self.dS = np.transpose(self.dS)
            except NameError:
                self.dS=None

            try:
                self.sigma = np.asarray(sigma)
                if self.sigma.shape == (self.t.shape[0], self.q.shape[0]):
                    self.sigma = np.transpose(self.sigma)
            except NameError:
                pass

            try:
                #print 'hi dSh'
                self.dSh = np.asarray(dSh)
                if self.dSh.shape == (self.t.shape[0], self.q.shape[0]):
                    self.dSh = np.transpose(self.dSh)
                self.sigma_h = np.asarray(sigma_h)
                if self.sigma_h.shape == (self.t.shape[0], self.q.shape[0]):
                    self.sigma_h = np.transpose(self.sigma_h)
                if type(self.dS)==type(None) and type(self.dSv)!=type(None) and type(self.dSh)!=type(None):
                    self.dS = np.asarray(self.dSh)
                #print 'bye dSh'
            except NameError:
                
                pass

            try:
                self.dSv = np.asarray(dSv)
                if self.dSv.shape == (self.t.shape[0], self.q.shape[0]):
                    self.dSv = np.transpose(self.dSv)
                self.sigma_v = np.asarray(sigma_v)
                if self.sigma_v.shape == (self.t.shape[0], self.q.shape[0]):
                    self.sigma_v = np.transpose(self.sigma_v)
            except NameError:
                pass

            try:
                self.S = np.asarray(S)
            except:
                self.S = np.zeros_like(self.dS)

            if self.S.shape == (self.t.shape[0], self.q.shape[0]):
                self.S = np.transpose(self.S)
            
            if type(self.dS)==type(None) and type(self.dSv)==type(None) and type(self.dSh)==type(None) and not self.S.any():
                raise LookupError('Nothing useful loaded for %s'%(fn))

            print("Sucessfully loaded a \'{0}\' type experiment.".format(self._expID))

            return

        elif self._expID in ('sacla2016AziMat','sacla2016mat','lcls2016AziMat','lcls2016mat'):
            #logfile = open(folder + self._logfilename, 'r')
            matfile = self._matfile_names%(os.path.splitext(name)[0],nr,os.path.splitext(name)[1])
            fn = os.path.join(folder, matfile)
            if os.path.isfile(fn):
                try:
                    #print 'hi h5py'
                    import h5py
                    f = h5py.File(fn,'r') #will work for matlab files v7.3
                    if len(list(f.keys()))==1:
                        data = {}
                        keys = list(f[list(f.keys())[0]].keys())
                        data.fromkeys(keys)
                        for elem in keys:
                            #print elem
                            data[elem] = np.asarray(f[list(f.keys())[0]][elem])
                    else:
                        data = {}
                        keys = list(f.keys())
                        data.fromkeys(keys)
                        for elem in keys:
                            data[elem] = np.asarray(f[elem])
                    f.close()
                except IOError:
                    try:
                        #print 'hi scipy'
                        import scipy.io as sio
                        data = sio.loadmat(fn)
                        #print 'bye scipy'
                    except IOError:
                        try:
                            eng.isprime(1) #tests if the matlab engine is loaded
                        except:
                            eng = start_mat_engine() #function in helpers.py
                        data = eng.load(fn,'-mat')[name] #should work for all matlab files
                
                q = np.asarray(data['q'], dtype=np.float).squeeze()
                try:
                    t = np.asarray(data['timeBins'], dtype=np.float).squeeze()
                except:
                    try:
                        t = np.asarray(data['t'], dtype=np.float).squeeze()
                    except:
                        None
                
                if 'I' in data:
                    S = np.asarray(data['I']).squeeze()
                    self._unit['S']  = r'q'
                    self._quant['S'] = r'\mathrm{S}'
                    print('Absolute scattering data found in: %s'%(matfile))
                else:
                    #S = None
                    self._unit['S']  = r'q'
                    self._quant['S'] = r'\mathrm{S}'
                    #print 'No absolute scattering data found in: %s'%(matfile)

                if 'dIh' in data and 'dIv' in data:
                    ## v & h seem to be inverted since sacla2015mat!
                    dSh    = np.asarray(data['dIv']).squeeze()
                    sigma_h = np.asarray(data['sigma_v']).squeeze()
                    dSv    = np.asarray(data['dIh']).squeeze()
                    sigma_v = np.asarray(data['sigma_h']).squeeze()
                    self._unit['dSh']    = r'q'
                    self._unit['dSv']    = r'q'
                    self._quant['dSh']   = r'\Delta\mathrm{S}'
                    self._quant['dSv']   = r'\Delta\mathrm{S}'
                    self._unit['sigma_h']    = r'q'
                    self._unit['sigma_v']    = r'q'
                    self._quant['sigma_h']   = r'\Delta\mathrm{S}'
                    self._quant['sigma_v']   = r'\Delta\mathrm{S}'
                    print('Anisotropic data found in: %s'%(matfile))
                    if 'dI' not in data:
                        dS = dSh + dSv
                        #print 'Isotropic data generated for: %s'%(matfile)
                        sigma = np.sqrt(np.power(sigma_h, 2) + np.power(sigma_v, 2))
                        #print 'Sigma for isotropic data generated for: %s'%(matfile)
                    
                if 'dI' in data:
                    dS = np.asarray(data['dI']).squeeze()
                    self._unit['dS']  = r'q'
                    self._quant['dS'] = r'\Delta\mathrm{S}'
                    #print 'Isotropic data found in: %s'%(matfile)
                
                if 'sigma' in data:
                    sigma = np.asarray(data['sigma']).squeeze()
                    self._unit['sigma']  = r'q'
                    self._quant['sigma'] = r'\Delta\mathrm{S}'
                    #print 'Sigma found in: %s'%(matfile)
                elif 'sigma' not in data and not ('dIh' in data and 'dIv' in data):
                    print('No sigma found in: %s'%(matfile))
                        
                if 'scanNumbers' in data:
                    self.nr = data['scanNumbers'].squeeze()
            else:
                print("%s does not exist in %s."%(matfile, folder))
                return None

            self.q = np.asarray(q)
            self.t = np.asarray(t)
            
            if self._expID == 'sacla2016AziMat':
                self.t *= 1e-3
                #print("Yes\n")
            else:
                self.t = self.t
                #print("No\n")
            self.Nrepeats = int(1)

            
            #libwaxs convention is dS(q,t)

            try:
                self.dS = np.asarray(dS)
                if self.dS.shape == (self.t.shape[0], self.q.shape[0]):
                    self.dS = np.transpose(self.dS)
            except NameError:
                self.dS=None

            try:
                self.sigma = np.asarray(sigma)
                if self.sigma.shape == (self.t.shape[0], self.q.shape[0]):
                    self.sigma = np.transpose(self.sigma)
            except NameError:
                pass

            try:
                #print 'hi dSh'
                self.dSh = np.asarray(dSh)
                if self.dSh.shape == (self.t.shape[0], self.q.shape[0]):
                    self.dSh = np.transpose(self.dSh)
                self.sigma_h = np.asarray(sigma_h)
                if self.sigma_h.shape == (self.t.shape[0], self.q.shape[0]):
                    self.sigma_h = np.transpose(self.sigma_h)
                if type(self.dS)==type(None) and type(self.dSv)!=type(None) and type(self.dSh)!=type(None):
                    self.dS = np.asarray(self.dSh)
                #print 'bye dSh'
            except NameError:
                
                pass

            try:
                self.dSv = np.asarray(dSv)
                if self.dSv.shape == (self.t.shape[0], self.q.shape[0]):
                    self.dSv = np.transpose(self.dSv)
                self.sigma_v = np.asarray(sigma_v)
                if self.sigma_v.shape == (self.t.shape[0], self.q.shape[0]):
                    self.sigma_v = np.transpose(self.sigma_v)
            except NameError:
                pass

            try:
                self.S = np.asarray(S)
            except:
                self.S = np.zeros_like(self.dS)

            if self.S.shape == (self.t.shape[0], self.q.shape[0]):
                self.S = np.transpose(self.S)
            
            if type(self.dS)==type(None) and type(self.dSv)==type(None) and type(self.dSh)==type(None) and not self.S.any():
                raise LookupError('Nothing useful loaded for %s'%(fn))

            print("Sucessfully loaded a \'{0}\' type experiment.".format(self._expID))

            return

        elif self._expID in ('2016A8037','LN34', '2018A8055'):
            #logfile = open(folder + self._logfilename, 'r')
            matfile = self._matfile_names%(os.path.splitext(name)[0],nr,os.path.splitext(name)[1])
            fn = os.path.join(folder, matfile)
            if os.path.isfile(fn):
                try:
                    #print 'hi h5py'
                    import h5py
                    f = h5py.File(fn,'r') #will work for matlab files v7.3
                    if len(list(f.keys()))==1:
                        data = {}
                        keys = list(f[list(f.keys())[0]].keys())
                        data.fromkeys(keys)
                        for elem in keys:
                            #print elem
                            data[elem] = np.asarray(f[list(f.keys())[0]][elem])
                    else:
                        data = {}
                        keys = list(f.keys())
                        data.fromkeys(keys)
                        for elem in keys:
                            data[elem] = np.asarray(f[elem])
                    f.close()
                except IOError:
                    try:
                        #print 'hi scipy'
                        import scipy.io as sio
                        data = sio.loadmat(fn)
                        #print 'bye scipy'
                    except IOError:
                        try:
                            eng.isprime(1) #tests if the matlab engine is loaded
                        except:
                            eng = start_mat_engine() #function in helpers.py
                        data = eng.load(fn,'-mat')[name] #should work for all matlab files

                timeKeyList = ['TimeBins', 'Delays', 't','timeBins']
                timeKey = np.asarray([key if key in timeKeyList else False for key in list(data.keys())], dtype=object)
                timeKey = (timeKey[timeKey!=False]).tolist()

                #bork

                if len(timeKey)!=1:
                    raise KeyError("Data has multiple variables denoting time: {0}".format(' '.join(timeKey)))

                q = np.asarray(data['q'], dtype=np.float).squeeze()
                t = np.asarray(data[timeKey[0]], dtype=np.float).squeeze()

                ## v & h seem to be inverted since sacla2015mat!

                self._signalList = ['dS','dS0','dS2',
                                'dS_JC','dS0_JC','dS2_JC',
                                'dI','dI0','dI2',
                                'dI_JC','dI0_JC','dI2_JC',
                                'AllTTDelay','AllTTDelay0','AllTTDelay2',
                                'AllTTDelay','AllTTDelayS0','AllTTDelayS2'] # don't put numbers other than those denoting the signal type in these variables!!!!

                DscatKey = np.asarray([key if key in self._signalList else False for key in list(data.keys())], dtype=object)
                DscatKey = (DscatKey[DscatKey!=False]).tolist()
                #kill

                if 'dS_JC' in DscatKey:
                    try:
                        _a = DscatKey.pop('dS')
                        print("{0} popped".format(_a))
                    except:
                        print("")

                if len(DscatKey)>3:
                    raise KeyError("Data has too many variables denoting difference scattering: {0}".format(' '.join(DscatKey)))

                for key in DscatKey:
                    if '0' in key:
                        dS0   = np.asarray(data[key]).squeeze()
                    elif '2' in key:
                        dS2   = np.asarray(data[key]).squeeze()
                    else:
                        dS    = np.asarray(data[key]).squeeze()

                DsigmaKeyList = ['sigma','sigma0','sigma2'] # don't put numbers other than those denoting the signal type in these variables!!!!
                DsigmaKey = np.asarray([key if key in self._signalList else False for key in list(data.keys())], dtype=object)
                DsigmaKey = (DsigmaKey[DsigmaKey!=False]).tolist()

                if len(DsigmaKey)>3:
                    raise KeyError("Data has too many variables denoting the error on the difference scattering: {0}".format(' '.join(DscatKey)))

                for key in DsigmaKey:
                    if '0' in key:
                        sigma0   = np.asarray(data[key]).squeeze()
                    elif '2' in key:
                        sigma2   = np.asarray(data[key]).squeeze()
                    else:
                        sigma    = np.asarray(data[key]).squeeze()

                AscatKeyList = ['I','S']
                AscatKey = np.asarray([key if key in AscatKeyList else False for key in list(data.keys())], dtype=object)
                AscatKey = (AscatKey[AscatKey!=False]).tolist()

                if len(AscatKey)>1:
                    raise KeyError("Data has too many variables denoting absolute scattering: {0}".format(' '.join(AscatKey)))
                elif len(AscatKey)==1:
                    S = np.asarray(data[AscatKey[0]], dtype=np.float).squeeze()
                else:
                    print("Warning: No absolute scattering signal found.")

                self._unit['dS']     = r'q'
                self._unit['dS0']    = r'q'
                self._unit['dS2']    = r'q'
                self._quant['dS']    = r'\Delta\mathrm{S}'
                self._quant['dS0']   = r'\Delta\mathrm{S}_{\rm iso}'
                self._quant['dS2']   = r'\Delta\mathrm{S}_{\rm ani}'

                self._unit['sigma_dS']     = r'q'
                self._unit['sigma_dS0']    = r'q'
                self._unit['sigma_dS2']    = r'q'
                self._quant['sigma_dS']    = r'\Delta\mathrm{S}'
                self._quant['sigma_dS0']   = r'\Delta\mathrm{S}_{\rm iso}'
                self._quant['sigma_dS2']   = r'\Delta\mathrm{S}_{\rm ani}'

            else:
                print("%s does not exist in %s."%(matfile, folder))
                return None

            self.q = np.asarray(q)
            self.t = np.asarray(t)

            self.Nrepeats = int(1)

            try:
                self.dS = np.asarray(dS)
                if self.dS.shape == (self.t.shape[0], self.q.shape[0]):
                    self.dS = np.transpose(self.dS)
            except NameError:
                pass

            try:
                self.dS0 = np.asarray(dS0)
                if self.dS0.shape == (self.t.shape[0], self.q.shape[0]):
                    self.dS0 = np.transpose(self.dS0)
            except NameError:
                pass

            try:
                self.dS2 = np.asarray(dS2)
                if self.dS2.shape == (self.t.shape[0], self.q.shape[0]):
                    self.dS2 = np.transpose(self.dS2)
            except NameError:
                pass

            assert (hasattr(self, 'dS') or hasattr(self, 'dS0') or hasattr(self, 'dS2')), 'No difference signals found!'

            try:
                self.S = np.asarray(S)
            except NameError:
                for el in list(self.__dict__.keys()):
                    if 'dS' in el:
                        shape = getattr(self, el).shape
                self.S = np.zeros(shape)

            if self.S.shape == (self.t.shape[0], self.q.shape[0]):
                self.S = np.transpose(self.S)

            try:
                self.sigma = np.asarray(sigma)
            except NameError:
                for el in list(self.__dict__.keys()):
                    if 'sigma' in el:
                        shape = getattr(self, el).shape
                self.sigma = np.zeros(shape)

            if self.sigma.shape == (self.t.shape[0], self.q.shape[0]):
                self.sigma = np.transpose(self.sigma)

            print("Sucessfully loaded a \'{0}\' type experiment.".format(self._expID))
            return

        elif self._expID == 'modPack' or self._expID == 'modpack':
            if not os.path.isdir(folder):
                raise IOError("Path not found: {0}".format(folder))

            fnL   = glob.glob(os.path.join(folder,'*'))
            fileS = set([os.path.split(el)[1] for el in fnL])

            dfn    = 'q-vs-I.dat'
            sfn    = 'q-vs-sigma.dat'
            tfn    = 't.dat'
            f_re   = r'[-+]?[0-9]*\.?[0-9]+'
            rex_mp = re.compile(r'q-dI-sigma_t_(%s).dat'%(f_re))

            fnS    = set([dfn,sfn,tfn])
            if len(fnS.intersection(fileS))!=0:
                _rawdat    = np.loadtxt(os.path.join(folder,dfn))
                t          = np.loadtxt(os.path.join(folder,tfn))
                rawdat     = _rawdat[:,1:].T
                q          = _rawdat[:,0]
                try:
                    rawerr = np.loadtxt(os.path.join(folder,sfn))
                except IOError:
                    rawerr = np.ones_like(rawdat)
            else:
                _t  = []
                _fn = []
                for fn in fnL:
                    if rex_mp.match(os.path.split(fn)[1]):
                        _t  += [rex_mp.match(os.path.split(fn)[1]).groups()[0]]
                        _fn += [fn]
                t    = np.asarray(_t, dtype=float)
                datL = []
                for i,fn in enumerate(_fn):
                    #print(fn)
                    rawdata = np.loadtxt(fn)
                    if i==0:
                        q = rawdata[:,0]
                    datL.append(rawdata[:,1:])
                datL  = np.asarray(datL, dtype=float)
                rawdat = datL[:,:,0]
                rawerr = datL[:,:,1]

            tIdx = np.argsort(t)
            self.t     = t[tIdx]
            self.q     = q
            self.dS    = rawdat[tIdx,:].T
            self.sigma = rawerr[tIdx,:].T
            #bork

            if self.dS.shape == (self.t.shape[0], self.q.shape[0]):
                self.dS = np.transpose(self.dS)

            if self.sigma.shape == (self.t.shape[0], self.q.shape[0]):
                self.sigma = np.transpose(self.sigma)

            self.S = np.zeros_like(self.dS)

            if self.S.shape == (self.t.shape[0], self.q.shape[0]):
                self.S = np.transpose(self.S)
            print("Sucessfully loaded a \'{0}\' type experiment.".format(self._expID))
            return

        #
        # DTU single dS curve format.
        #
        elif (self._expID == 'DTUtxt' or self._expID == 'ascii'):
            txtfile = self._txtfile_names%(os.path.splitext(name)[0],nr,os.path.splitext(name)[1])
            fn = os.path.join(folder + txtfile)
            if os.path.isfile(fn):
                with open(fn,'U') as f:
                    if self._expID == 'DTUtxt':
                        rawdata = np.loadtxt(f,skiprows=2)
                    elif self._expID == 'ascii':
                        rawdata = np.loadtxt(f)
                print('Loaded %s file from %s.'%(self._expID, fn))
                q      = rawdata[:,0].squeeze()
                dSdT   = rawdata[:,1].squeeze()
                dSdRho = rawdata[:,2].squeeze()
                t = np.array([0.])
                S = None
            else:
                raise NameError("{} does not exist.".format(fn))

            self.q        = np.asarray(q)
            self.dSdT     = np.asarray(dSdT)
            self.dSdRho   = np.asarray(dSdRho)
            self.t        = np.asarray(t)
            self.Nrepeats = int(1)
            sigma         = None

            errfile = self._txtfile_names%(os.path.splitext(name)[0],nr,'_error'+os.path.splitext(name)[1])
            fn = os.path.join(folder,errfile)
            if os.path.isfile(fn):
                with open(fn,'U') as f:
                    if self._expID == 'DTUtxt':
                        rawerror = np.loadtxt(f,skiprows=2)
                    elif self._expID == 'ascii':
                        rawerror = np.loadtxt(f)
                print('Loaded %s error file from %s.'%(self._expID, fn))
                try:
                    rawerror = rawdata[:,::2].squeeze()
                except:
                    rawerror = None
                    print("No sigma found for %s."%(fn))
                q = rawerror[:,0].squeeze()
                sigma = rawerror[:,1].squeeze()

            if (self.q != q).all():
                raise ValueError('q-VALUE MISMATCH BETWEEN FILES')

            if type(sigma)!=type(None):

                self.sigma = np.asarray(sigma)

                if np.all(self.sigma==0):
                    self.sigma = np.ones_like(self.sigma)
                else:
                    self.sigma[self.sigma==0] = 1.
            else:
                self.sigma = np.ones_like(self.dSdT)

            if type(S)!=type(None):
                self.S = S
            else:
                self.S = np.zeros_like(self.dSdT)

            #libwaxs convention is dS(q,t)
            if self.dSdT.shape == (self.t.shape[0], self.q.shape[0]):
                self.dSdT = np.transpose(self.dSdT)
            if self.dSdRho.shape == (self.t.shape[0], self.q.shape[0]):
                self.dSdRho = np.transpose(self.dSdRho)
            if self.S.shape == (self.t.shape[0], self.q.shape[0]):
                self.S = np.transpose(self.S)
            if self.sigma.shape == (self.t.shape[0], self.q.shape[0]):
                self.sigma = np.transpose(self.sigma)

        #
        # Extracted rdSr curves
        #
        elif (self._expID == 'rdSrdpt'):
            txtfile = self._asciifile_names%(os.path.splitext(name)[0],nr,os.path.splitext(name)[1])
            fn = os.path.join(folder, txtfile)
            if os.path.isfile(fn):
                with open(fn,'U') as f:
                    rawdata = np.loadtxt(f)
                r    = rawdata[:,0].squeeze()
                rdSr = rawdata[:,1].squeeze()
                t    = np.array([0.])
                print('Loaded %s Pair-wise distribution data from %s.'%(self._expID, fn))
                #S    = None
            self.r = np.asarray(r)
            self.rdSr = np.asarray(rdSr)
            self.t = np.asarray(t)
            self.Nrepeats = int(1)

            if (self.r != r).all():
                raise ValueError('r-VALUE MISMATCH BETWEEN FILES')

            #libwaxs convention is dS(q,t)
            if self.rdSr.shape == (self.t.shape[0], self.r.shape[0]):
                self.rdSr = np.transpose(self.rdSr)
            #if self.S.shape == (self.t.shape[0], self.q.shape[0]):
                #self.S = np.transpose(self.S)
            #if self.sigma.shape == (self.t.shape[0], self.q.shape[0]):
                #self.sigma = np.transpose(self.sigma)
        
        elif (self._expID == 'qdSdpt'):
            txtfile = self._asciifile_names%(os.path.splitext(name)[0],nr,os.path.splitext(name)[1])
            fn = os.path.join(folder, txtfile)
            if os.path.isfile(fn):
                with open(fn,'U') as f:
                    rawdata = np.loadtxt(f)
                q    = rawdata[:,0].squeeze()
                qdS = rawdata[:,1].squeeze()
                t    = np.array([0.])
                print( 'Loaded %s Pair-wise distribution data from %s.'%(self._expID, fn))
                #S    = None
            self.q = np.asarray(q)
            self.qdS = np.asarray(qdS)
            self.t = np.asarray(t)
            self.Nrepeats = int(1)

            if (self.q != q).all():
                raise ValueError('q-VALUE MISMATCH BETWEEN FILES')

            #libwaxs convention is dS(q,t)
            if self.qdS.shape == (self.t.shape[0], self.q.shape[0]):
                self.qdS = np.transpose(self.qdS)

        else:
            raise ValueError('INVALID EXPERIMENT ID')

class Spec_Run_PP:
    
    def __init__(self, expID=''):
        
        self._expID             = expID
        self._unit              = {}
        self._quant             = {}
        self._hdr               = None
        self._Nrepeats          = None
        self._logfilename       = ''
        self._runfoldername     = ''
        
        self._unit['PrbU1']     = r'V'
        self._unit['PrbP1']     = r'V'
        self._unit['RefU1']     = r'V'
        self._unit['RefP1']     = r'V'
        
        self._unit['TP1']       = r'\%'
        self._unit['TU1']       = r'\%'
        self._unit['dA1']       = r'w'
        self._unit['dT1']       = r''
        self._unit['t']         = r'ps'
        self._unit['chrp']      = r'ps'
        self._unit['_Nrepeats'] = None
        
        self._quant['PrbU1']    = r'I'
        self._quant['PrbP1']    = r'I'
        self._quant['RefU1']    = r'I'
        self._quant['RefP1']    = r'I'
        self._quant['TP1']      = r'\%'
        self._quant['TU1']      = r'\%'
        self._quant['dA1']      = r'\Delta\mathrm{A}'
        self._quant['dT1']      = r'\frac\mathrm{T}\mathrm{T_0}\%'
        self._quant['chrp']     = r'delay'   
        self._quant['t']        = r'delay'
        self._quant['_Nrepeats']= r'N\textsuperscript{o}'

        self._hdr   = {
            'header'        : "",
            'filetype'      : "",
            'proc_exist'    : False,
            'chirp_exist'   : False,
            'twochop'       : False,
            'fabry'         : False,
            'inhibit'       : False,
            'processed'     : False,
            'delayunits'    : 'ps',
            'probeunits'    : 'cm',
            'N_pix'         : None,
            'N_ext_chan'    : None,
            'chop_chan'     : None,
            }

        
        if (self._expID == 'ir2015tdms'):
            self._tdms_name             = '%s%s.tdms'
            self._strmScale             = 13107
            self._unit['w']             = r'\mathrm{cm}^{-1}'
            self._unit['prb_w']         = r'\mathrm{cm}^{-1}'
            self._unit['ref_w']         = r'\mathrm{cm}^{-1}'
            self._unit['pmp_w']         = r'\mathrm{cm}^{-1}'
            self._quant['w']            = r'\tilde{\nu}'
            self._quant['prb_w']        = r'\tilde{\nu}'
            self._quant['ref_w']        = r'\tilde{\nu}'
            self._quant['pmp_w']        = r'\tilde{\nu}'
            
        else:
            raise ValueError('INVALID EXPERIMENT ID')
        
    def load(self, folder, nr, keep_meta=False):
        
        self.runnr = nr
        
        if (self._expID == 'ir2015tdms'):
            
            
            import nptdms #available through pip
            
            self._hdr = {}
            #self._hdr[self.runnr] = {
                #'detector_params'       : {},
                #'_SdV'                  : [], # Shot dependent variables
                #'_SdK'                  : [], # Shot dependent keys
                #'_DdV'                  : [], # Difference dependent variables
                #'_DdK'                  : [], # Difference dependent keys
                #'_WdV'                  : [], # Wvector dependent variables
                #'_WdK'                  : [], # Wvector dependent variables
                #'_TdV'                  : [], # Time dependent variables
                #'_AsS'                  : [], # Absolute signals
                #'_DsS'                  : [], # Difference signals
                                    #}
            #hdr = self._hdr[self.runnr]
            
            if type(nr)==(int or float):
                nr = np.asarray([nr])
            elif isinstance(nr, collections.Iterable) and type(nr)!=str:
                nr = np.asarray(nr)
            else:
                raise TypeError("nr must be an integer or list-like, not {}.".format(type(nr).__name))
            
            for i,el in enumerate(nr):
                infile  = os.path.join(folder,self._tdms_name%(os.path.split(folder)[1], el))
                if not os.path.isfile(infile):
                    raise NameError("{} is not a valid file.".format(infile))
                
                with open(infile, 'U') as f:
                    tdms_file = nptdms.TdmsFile(f)
                
                rawdata     = tdms_file.channel_data('alldata','data+ext').astype(float)/self._strmScale
                t           = tdms_file.channel_data('alldata','tpvector')
                Nshots      = tdms_file.channel_data('alldata','tp')[1]
                rawdrk      = tdms_file.channel_data('alldata','background').astype(float)/self._strmScale
                w_nm        = tdms_file.channel_data('alldata','wavevector')
                w_cm        = 1./(w_nm*1e-7)
                Npix        = w_nm.size
                ### TODO: Get this from tdms!!!
                NextChan    = 16
                chopChan    = 9
                ###
                totchan     = Npix*2+NextChan
                
                if i==0:
                    d_shape         = (t.size,totchan,Nshots)
                    drk_array_shape = (1,Npix*2,1)
                    
                    data            = np.zeros((len(nr),)+d_shape)
                    drk             = np.zeros((len(nr),)+drk_array_shape)
                    self.t          = np.asarray(t)
                    self.w          = np.asarray(w_cm)
                    
                else:
                    if (self.t != t).all():
                        raise ValueError("Subsequent scans do not have the same time point values!")
                    
                    if (self.w != w_cm).all():
                        raise ValueError("Subsequent scans do not have the same wavevector values!")
                    
                    if Nshots != data[i].shape[-1]:
                        raise ValueError("TDMS given number of shots (%s) does not match the data inferred number of shots (%s)!"%(Nshots,data[i].shape[-1]))
                
                data[i] = np.copy(rawdata.reshape(d_shape))
                drk[i]  = np.copy(rawdrk.reshape(drk_array_shape))
                print( "Found a {} style experiment containing {} pixels {} delays and {} repeats ({}).".format(self._expID, Npix, t.size, Nshots, i))
                
            del(rawdata)
            del(rawdrk)
            
            
            #data.reshape(d_shape[:-1] + (-1,))
            #drk.reshape(drk_array_shape[:-1] + (-1,))
            #Nshots = data.shape[-1]
            
            #pu1_mask_shape  = (len(nr),t.size,Npix,Nshots)
            pix_array_shape = (len(nr),t.size,Npix,-1)
            extchan_shape   = (len(nr),t.size,NextChan,-1)
            drk_shape       = (len(nr),1,Npix*2,1)
            
            #self.pu1_on     = np.zeros(pu1_mask_shape)
            #self.RefU1      = np.zeros(pix_array_shape)
            #self.RefP1      = np.zeros(pix_array_shape)
            #self.PrbU1      = np.zeros(pix_array_shape)
            #self.PrbP1      = np.zeros(pix_array_shape)
            #self.drkRef     = np.zeros(drk_shape)
            #self.drkPrb     = np.zeros(drk_shape)
            #self._extchan   = np.zeros(extchan_shape)
            
            self._hdr['det_arrays'] = {'Prb' : np.arange(64),'Ref' : np.arange(64,128)}
            self._hdr['N_pix']      = Npix
            self._hdr['ext_chan']   = {'ext': np.arange(128,144)}
            self._hdr['chop_chan']  = chopChan
            self._Npump             = [1]
            self._Nrepeats          = (np.ones(pix_array_shape[:-1])*Nshots).astype(int)
            
            self._extchan    = data[:,:,Npix*2:,:]
            
            #TODO
            # Sort out non-equal pump on and pump off shots(separate function?)
            #self._pumpSort()
            pu1_on      = np.tile((self._extchan[:,:,self._hdr['chop_chan'],:]>self._extchan[:,:,self._hdr['chop_chan'],:].mean())[:,:,np.newaxis,:], (1,1,Npix,1)).astype(bool)
            
            self.RefU1       = data[:,:,:Npix,:][~pu1_on].reshape(pix_array_shape)
            self.RefP1       = data[:,:,:Npix,:][pu1_on].reshape(pix_array_shape)
            
            self.PrbU1       = data[:,:,Npix:Npix*2,:][~pu1_on].reshape(pix_array_shape)
            self.PrbP1       = data[:,:,Npix:Npix*2,:][pu1_on].reshape(pix_array_shape)
            
            self.drkRef      = drk[:,:,:Npix,:]
            self.drkPrb      = drk[:,:,Npix:Npix*2,:]
            
            self._hdr['ref_sig']  = ['RefU1','RefP1']
            self._hdr['prb_sig']  = ['PrbU1','PrbP1']
            self._hdr['raw_sig']  = ['RefU1','RefP1','PrbU1','PrbP1', 'drkPrb', 'drkRef']
            self._hdr['proc_sig'] = []
            self._hdr['corr_sig'] = []
            self._hdr['diff_sig'] = []
            
            for attr in self._hdr['raw_sig']:
                if attr == '_extchan':
                    if getattr(self, attr).shape[-3:-1] == (1, self._hdr['N_ext_chan']):
                        setattr(self, attr, np.transpose(getattr(self, attr), axes=(0,2,1,3)))
                else:
                    if getattr(self, attr).shape[-3:-1] == (self.t.shape[0], self.w.shape[0]):
                        setattr(self, attr, np.transpose(getattr(self, attr), axes=(0,2,1,3)))
                        if not hasattr(self, '_valid1'):
                            setattr(self, '_valid1', np.ones(getattr(self, attr).shape, dtype=bool))
                    elif getattr(self, attr).shape[-3:-1] == (1, self.w.shape[0]):
                        setattr(self, attr, np.transpose(getattr(self, attr), axes=(0,2,1,3)))
            
            print( "Loaded {} {} style experiment(s) containing {} pixels {} delays and {} repeats (each).".format(self._valid1.shape[0],self._expID, Npix, self.t.size, Nshots))
            # dunno if this helps, but it is meant to clean up some memory.
            #bork
            del(data)
            del(drk)
            
            return
    
    def _pumpSort(self):
        """
        Method with which to sort out poorly defined pump on/off shots.
        """
        
        #if 
        
        return
    
    def subtractDark(self):
        """
        Subtracts the dark current from the reference and probe channels.
        """
        for attr in self._hdr['ref_sig']:
            if hasattr(self, attr) and hasattr(self, 'drkRef') and self._hdr['proc_sig'].count(attr)==0:
                setattr(self, attr, getattr(self, attr) - self.drkRef)
                self._hdr['proc_sig'] += [attr]
        
        for attr in self._hdr['prb_sig']:
            if hasattr(self, attr) and hasattr(self, 'drkPrb') and self._hdr['proc_sig'].count(attr)==0:
                setattr(self, attr, getattr(self, attr) - self.drkPrb)
                self._hdr['proc_sig'] += [attr]
        #bork
        
        return
    
    def calcDsig(self):
        """
        """
        
        for sig in self._hdr['corr_sig']:
            p     = getattr(self, '{}P{}'.format(sig[0], sig[1]))
            u     = getattr(self, '{}U{}'.format(sig[0], sig[1]))
            p_err = getattr(self, '{}P{}_err'.format(sig[0], sig[1]))
            u_err = getattr(self, '{}U{}_err'.format(sig[0], sig[1]))
            
            if sig[0]=='T':
                dSig  = p/u
                dSerr = np.abs(dSig)*np.sqrt(np.power(p_err/p,2)+np.power(u_err/u,2))
            elif sig[0]=='A':
                dSig = p-u
                dSerr = np.sqrt(np.power(p_err,2)+np.power(u_err,2))
            
            setattr(self, 'd{}{}'.format(sig[0], sig[1]), dSig)
            setattr(self, 'd{}{}_err'.format(sig[0], sig[1]), dSerr)
            self._hdr['diff_sig'] += ['d{}{}'.format(sig[0], sig[1])]
        return

    def convert(self, sig='T', ovrWrt=False):
        """
        """
        
        if sig not in ['T','A',None]:
            sig = None
        
        for diffS in self._hdr['diff_sig']:
            if diffS[1]=='T' and sig==('T' or None):
                SIG = list(diffS)
                SIG[1] = 'A'
                con = ''.join(SIG)
                if hasattr(self, con) and not ovrWrt:
                    print("{} already exists, not overwriting.".format(con))
                dSig  = getattr(self, diffS)
                dSerr = getattr(self, '{}_err'.format(diffS))
                
                dCon  = -np.log10(dSig)
                dCerr = np.abs(np.true_divide(dSerr, dSig*np.log(10)))
            
            elif diffS[1]=='A' and sig==('A' or None):
                SIG = list(diffS)
                SIG[1] = 'T'
                con = ''.join(SIG)
                if hasattr(self, con) and not ovrWrt:
                    print("{} already exists, not overwriting.".format(con))
                dSig  = getattr(self, diffS)
                dSerr = getattr(self, '{}_err'.format(diffS))
                
                dCon  = np.exp(-sDig/np.log10(np.e))
                dCerr = np.abs(np.exp(np.log10(np.e)*dSerr))
                
            setattr(self, '{}'.format(con), dCon)
            setattr(self, '{}_err'.format(con), dCerr)
            
            print("convert(): Converted {}(_err) to {}(_err).".format(diffS, con))
        
        return
    
    #def _mergeScans(self, axis=0, keepdims=False):
        #"""
        #Reshapes data so that all shots with the same time point (axis 0)
        #kwargs:
            #axis      (int) = axis which will be lost.
            #keepdims (bool) = same functionality as numpy keepdims.
        #"""
        #for pN in self._Npump:
            #for arr in self._hdr['det_arrays'].keys():
                #bork
        #return
    
    def diagnoseCorr(self, pump=None, scan=None, pix=None, wrange=None, trange=None,
                     isValid=None, allValid=False, mergeScans=False, mergeAxis=0):
        import matplotlib
        import matplotlib.gridspec as gridspec
        
        if type(self._hdr['corr_sig'])==type(None):
            raise TypeError("Run correlateSig() before using diagnoseCorr().")
        
        if type(wrange)==type(None) and type(pix)==type(None):
            raise TypeError("You need to specify wrange or pix!")
        elif type(wrange)!=type(None):
            if len(wrange)==1:
                wSlice = get_lim(self.w, wrange)[0]
            elif len(wrange)<=3:
                wSlice = slice(*StdIdxLim(self.w, wrange, slice=True)[0])
        
        if type(pix)!=type(None):
            if type(pix)==int:
                pix = [int(x) for x in str(pix)]
            if len(list(pix))==1:
                wSlice = pix
            elif len(pix)<=3:
                wSlice = slice(*pix)
        
        if type(trange)!=type(None):
            if type(trange)==int:
                tr = [trange]
                trange = [int(x) for x in tr]
            if len(trange)==1:
                tSlice = get_lim(self.t, trange)[0]
            elif len(trange)<=3:
                tSlice = slice(*StdIdxLim(self.t, trange, slice=True)[0])
        
        if type(scan)==list and self._valid1.ndim>3:
            sSlice = slice(*scan)
            slices = (sSlice,wSlice,tSlice)
        elif type(scan)==type(None) and self._valid1.ndim>3:
            sSlice = slice(scan)
            slices = (sSlice,wSlice,tSlice)
        else:
            slices = (None,wSlice,tSlice)
            print("Data only has one scan.")
        
        if type(pump)==list and len(self._Npump)>3:
            pList = pump
        else:
            pList = self._Npump
            print("Data only has one pump.")
        
        for pN in pList:
            
            if type(isValid)==type(None):
                isValid = getattr(self, '_valid{}'.format(pN))
            elif isValid.shape == getattr(self, '_valid{}'.format(pN)):
                isValid = isValid*getattr(self, '_valid{}'.format(pN))
            elif allValid:
                isValid = np.ones(getattr(self, '_valid{}'.format(pN)).shape, dtype=bool)
            else:
                raise ValueError("Shape mismatch between last dimension of data (%s) and supplied isValid mask (%s)."%(isValid.shape,getattr(self, '_valid{}'.format(pN))))
            
            if mergeScans:
                mShape = list(isValid.shape)
                mShape.pop(mergeAxis)
                mShape[-1] = -1
                mShape = tuple(mShape)
                mask = isValid.reshape(mShape)
                slicesArr = slices[1:]
                
            mask = isValid[slices]
            #bork
            
            if not mergeScans:
                xU,xP = getattr(self, 'RefU{}'.format(pN))[slices],getattr(self, 'RefP{}'.format(pN))[slices]
                yU,yP = getattr(self, 'PrbU{}'.format(pN))[slices],getattr(self, 'PrbP{}'.format(pN))[slices]
            else:
                xU,xP = getattr(self, 'RefU{}'.format(pN)).reshape(mShape)[slices],getattr(self, 'RefP{}'.format(pN)).reshape(mShape)[slices]
                yU,yP = getattr(self, 'PrbU{}'.format(pN)).reshape(mShape)[slices],getattr(self, 'PrbP{}'.format(pN)).reshape(mShape)[slices]
            
            U = np.asarray(getattr(self, 'TU{}'.format(pN))[slices[1:]]).reshape(mask.shape[:-1])
            P = np.asarray(getattr(self, 'TP{}'.format(pN))[slices[1:]]).reshape(mask.shape[:-1])
            D = P/U
            
            #if mask.ndim==1:
                #iterator = (1,)
            #else:
                #iterator = mask.shape[:-1]
            ##bork
            matplotlib.rc('text', usetex=True)
            matplotlib.rc('font', family='serif')
            matplotlib.rcParams[r'text.latex.preamble'] = [r'\usepackage{mathptmx}']
            
            fig     = plt.figure(figsize=(10,5))
            gs      = gridspec.GridSpec(3, 2, wspace=0.0, hspace=0.0)
            ax      = np.empty((4), dtype=object)
            ax[0]   = plt.subplot(gs[:, 0])
            ax[1]   = plt.subplot(gs[0, 1])
            ax[2]   = plt.subplot(gs[1, 1], sharey=ax[1])
            ax[3]   = plt.subplot(gs[2, 1])
            
            ax[1].yaxis.tick_right()
            ax[1].xaxis.tick_top()
            ax[1].set_xlabel(r'T ($\%$)')
            ax[1].xaxis.set_label_position("top")
            ax[1].set_ylabel(r'$\rm T_0$ shots')
            ax[1].yaxis.set_label_position("right")
            
            ax[2].set_xticklabels([])
            ax[2].set_ylabel(r'$\rm T$ shots')
            ax[2].yaxis.set_label_position("right")
            
            ax[3].set_ylabel(r'$\frac{\rm T}{\rm T_0}$ shots')
            ax[3].yaxis.set_label_position("right")
            ax[3].set_xlabel(r'$\Delta$T ($\%$)')
            
            for i in range(1,4):
                ax[i].yaxis.set_ticks_position('right')
            
            for index in np.ndindex(mask.shape[:-1]):
                ax[0].plot(xU[index], yU[index], 'x', color='purple')
                ax[0].plot(xP[index], yP[index], 'x', color='orange')
                x_ = np.linspace(*ax[0].get_xlim(), num=len(xU[index].T))
                
                ax[0].plot(x_, x_*U[index].T, '--', linewidth=2, color='blue')
                ax[0].plot(x_, x_*P[index].T, '-.', linewidth=2, color='red')
                ax[0].set_xlabel(r'Reference I (V)')
                ax[0].set_ylabel(r'Probe I (V)')
                
                ax[1].hist((yU/xU)[index], bins=round((yU/xU)[index].shape[-1]/6.),histtype='stepfilled', alpha=0.5, color='blue', normed=True)
                ax[2].hist((yP/xP)[index], bins=round((yP/xP)[index].shape[-1]/6.),histtype='stepfilled', alpha=0.5, color='orange', normed=True)
                ax[3].hist(((yP/xP)/(yU/xU))[index], bins=round(((yP/xP)/(yU/xU))[index].shape[-1]/6.),histtype='stepfilled', alpha=0.5, color='green', normed=True)
        
        return fig
    
    def correlateSig(self, method='lsqfit', sig='T', isValid=None, mergeScans=True, mergeAxis=0):
        """
        
        """
        
        if sig not in ['A','T']:
            raise ValueError("sig needs to be either T for transmission, or A for absorption, not {}.".format(sig))
        for pN in self._Npump:
            mask = getattr(self, '_valid{}'.format(pN))
            if mergeScans:
                mShape = list(mask.shape)
                mShape.pop(mergeAxis)
                mShape[-1] = -1
                mShape = tuple(mShape)
                mask = mask.reshape(mShape)
            
            for F in ['P','U']:
                if not mergeScans:
                    x    = np.copy(getattr(self, 'Ref{}{}'.format(F,pN)))
                    y    = np.copy(getattr(self, 'Prb{}{}'.format(F,pN)))
                else:
                    x    = np.copy(getattr(self, 'Ref{}{}'.format(F,pN)).reshape(mShape))
                    y    = np.copy(getattr(self, 'Prb{}{}'.format(F,pN)).reshape(mShape))
                    self._Nrepeats = np.sum(self._Nrepeats.reshape(mShape), axis=-1)
                if method == 'lsqfit' and x.shape==y.shape:
                    Sig    = np.zeros(x.shape[:-1])
                    stderr = np.zeros(x.shape[:-1])
                    import statsmodels.api as sm
                    for index in np.ndindex(x.shape[:-1]):
                        if sig=='T':
                            #Sig[index],res[index] = np.linalg.lstsq(x[index],y[index])
                            X = x[index][mask[index]]
                            Y = y[index][mask[index]]
                            model   = sm.OLS(Y,X, missing='drop', hasconst=False)
                            results = model.fit()
                            #plt.plot(X, Y, 'o')
                            #plt.plot(X, results.params*X, '--')
                        elif sig=='A':
                            raise NotImplementedError
                            ##Sig[index],res[index] = np.linalg.lstsq(np.log10(x[index]),np.log10(y[index]))
                            #X = sm.add_constant(-np.log10(x[index][mask[index]][:,None]))
                            ##X = -np.log10(x[index][mask[index]])
                            #Y = -np.log10(y[index][mask[index]])
                            #model   = sm.OLS(Y,X, missing='drop', hasconst=True)
                            #results = model.fit()
                            #plt.plot(X.T[1], Y, 'o')
                            #plt.plot(X.T[1], np.dot(X[:,None],results.params), '--')
                            #bork
                        Sig[index]    = results.params
                        stderr[index] = results.bse
                        print("correlateSig(): fitted pixel {2:2d} delay {3:3d} for {0}{1} signals.\r".format(F,pN,*index)),
                        sys.stdout.flush()
                    #r2 = 1-np.true_divide(res, y.shape[-1]*np.nanvar(y, axis=-1, keepdims=True))
                elif method == 'average':
                    #from collections import Counter
                    #for index in np.ndindex(x.shape[:-1]):
                    x[~mask] = np.nan
                    y[~mask] = np.nan
                    
                    #fix the _Nrepeats!
                    tile = np.empty((mask.ndim), dtype=object)
                    tile.fill(slice(None))
                    tile[-3] = 0
                    count  = np.asarray(np.nonzero(~mask[tile.tolist()]))
                    unique = np.unique(count[-2])
                    Nshots = np.bincount(count[-2])
                    Rshots = np.zeros(mask.shape[-2], dtype=np.int64)
                    Rshots[unique] = Nshots
                    self._Nrepeats -= Rshots
                    if sig=='T':
                        stderr = np.nanstd(y/x)/np.sqrt(self._Nrepeats)
                        Sig    = np.nanmean(y/x, axis=-1)
                    elif sig=='A':
                        stderr = np.nanstd(-np.log10(y) + np.log10(x))/np.sqrt(self._Nrepeats)
                        Sig    = np.nanmean(-np.log10(y) + np.log10(x), axis=-1)
                    print("correlateSig(): averaged {0}{1} signals.\r".format(F,pN)),
                    sys.stdout.flush()
                setattr(self, '{}{}{}'.format(sig,F,pN), Sig)
                setattr(self, '{}{}{}_err'.format(sig,F,pN), stderr)
                print("")
            self._hdr['corr_sig'] += ['{}{}'.format(sig,pN)]
        return
    
    #def _fitCorr(self, A):
        
        #return
    
    def resetIsValid(self):
        """
        Resets self._valid<Npump>.
        """
        for pN in self._Npump:
            setattr(self, '_valid{}'.format(pN), np.ones_like(getattr(self, '_valid{}'.format(pN))))
            print("Reset self._valid{}\r".format(pN)),
            sys.stdout.flush()
        print("")
        return
    
    def rejectS(self, pump=None, sig='T', wrange=None, tol='3sigma', isValid=None, plot=False):
        """
        Rejects individual exposures based on the deviation of their q-averaged absolute scattering
        intensities, compared to the median of the whole run.
        @param sig:     Options are 'absolute', 'difference', 'all'. Specifies which signal to
                        use for the rejections of points. If 'all', the cumulative result is used.
        @type  sig:     string
        @param wrange:  q-range over which to average. Defaults to 1.5/A < q < 2.0/A.
        @type  wrange:  1darray-like
        @param tol:     The tolerance cutoff. See _filter1D for option details.
        @type  tol:     str
        @param plot:    Whether or not to plot a histogram of the run, which helps in setting 
                        reasonable ranges.
        @type  plot:    bool
        """

        # the statistic for each repeat (exposure) is the q-averaged signal
        # over a particular q-range.
        
        if sig not in ['A','T']:
            raise ValueError("sig needs to be either T for transmission, or A for absorption, not {}.".format(sig))
        
        if type(pump)!=type(None):
            if pump in self._Npump:
                pList = np.asarray(pump, dtype=int)
        elif type(pump)==type(None):
            pList = self._Npump
        else:
            raise TypeError("pump needs to be None or a list of existant pump numbers.")
        
        if type(wrange)!=type(None):
            wrange = np.asarray(wrange)
        else:
            wrange = np.asarray([self.w[0],self.w[-1]])
        
        wSlice = slice(*StdIdxLim(self.w, wrange, slice=True)[0])
        
        for pN in pList:
            
            #data = getattr(self, pN)
            ref_u,ref_p  = getattr(self, 'RefU{}'.format(pN)), getattr(self, 'RefP{}'.format(pN))
            prb_u,prb_p  = getattr(self, 'PrbU{}'.format(pN)), getattr(self, 'PrbP{}'.format(pN))
            
            if sig=='T':
                dSig = (prb_p/ref_p)/(prb_u/ref_u)
            elif sig=='A':
                dSig = -np.log10((prb_p/ref_p)/(prb_u/ref_u))
            
            # setup valid-shot mask
            # qualifier for a valid shot.
            # update valid-shot mask
            
            if type(isValid)==type(None):
                sigIsValid = ~np.isnan(dSig)
            elif isValid.shape == dSig.shape:
                sigIsValid = isValid*(~np.isnan(dSig))
            else:
                raise ValueError("Shape mismatch between last dimension of data (%s) and isValid mask (%s)."%(isValid.shape[-1],data.shape[-1]))
            
            sigIsValid *= getattr(self, '_valid{}'.format(pN))
            
            statistics = np.nanmean(dSig[sigIsValid[wSlice]].reshape(dSig[wSlice].shape[:-1]+(-1,)), axis=-3, keepdims=True)
            
            tile = np.ones((dSig.ndim))
            tile[~(np.array(statistics.shape)==np.array(dSig.shape))] = dSig.shape[-3]
            
            sigIsValid *= np.tile(self._filter1D_Ndim(statistics, isValid=None, tol=tol, plot=plot, out=False), (tile))
            
            setattr(self, '_valid{}'.format(pN), sigIsValid)
        return

    def _filter1D_Ndim(self, vecND, isValid=None, tol='1percent', plot=False, out=False):
        """
        Shot rejection workhorse. Should be nan proof!
        
        Args:
            
        Kwargs:
            
        """
        vec_shape = vecND.shape[:-1]+(-1,)
        
        if type(isValid)==type(None):
            isValid = ~np.isnan(vecND)
        elif isValid.shape == vecND.shape:
            isValid = isValid*(~np.isnan(vecND))
        else:
            raise ValueError("Shape mismatch between last dimension of vecND (%s) and isValid mask (%s)."%(isValid.shape,vecND.shape))
        
        #vecValid = vecND[isValid].reshape(vec_shape)
        
        # calculate median and tolerances
        if 'percent' in tol:
            median = np.nanmedian(vecND, axis=-1, keepdims=True)
            tolerance = float(tol.split('percent')[0]) * .01 * median
        elif 'units' in tol:
            median = np.nanmedian(vecND, axis=-1, keepdims=True)
            tolerance = float(tol.split('units')[0])
        elif 'sigma' in tol:
            median = np.nanmedian(vecND, axis=-1, keepdims=True)
            tolerance = float(tol.split('sigma')[0])*np.nanstd(vecND, axis=-1, keepdims=True)
        else:
            raise Exception('Bad input')
        
        # Modify isValid mask accordingly
        isValid *= (np.abs(vecND-median) < tolerance)
        
        if plot!=False and type(plot)!=type(None):
            if not plt.isinteractive():
                plt.ion()
                plt.draw()
            plt.cla()
            for tpt in range(vecND.shape[-2]):
                if 'norm' in tol:
                    plt.hist(vecND[~np.isnan(vecND)].reshape(vec_shape)[tpt,:], bins = 50, histtype='stepfilled', alpha=0.5, color='blue', normed=True)
                    x = np.linspace(*plt.xlim(), num=100)
                    plt.plot(x, norm.pdf(x), loc=median, scale=std)
                else:
                    plt.hist(vecND[~np.isnan(vecND)].reshape(vec_shape)[tpt,:], bins = 50, histtype='stepfilled', alpha=0.5, color='blue')
                plt.axvline(median-tolerance, color='r')
                plt.axvline(median+tolerance, color='r')
                plt.title('Run %u'%self.runnr)
                plt.ylabel('no shots.')
                #plt.xlabel(''.format(xlab)) "average absolute intensity over {}-{}x10-1 nm-1.'.format(*qrange)"
                plt.draw()
                plt.pause(1e-15)
        
        if not out:
            return isValid
        else:
            return (median, tolerance, isValid)
    
    
    def cleanUp(self):
        """
        Removes the large datasets (probe, and reference) from the instance in preparation for .
        """
        rmL = '|'.join(['Prb','Ref','_extchan','_valid\d+'])
        rex = re.compile(r'\S*({})\S*'.format(rmL))
        for attr in list(self.__dict__.keys()):
            if rex.match(attr):
                delattr(self,attr)
                print("Deleted {} from instance {}.".format(attr, self))
        return

class Spec_Proc_PP:
    """
    Class that holds one fully processed ascii file of a spectroscopic(!) pump-probe experiment.
    Can currently read file formats from the following laboratories:
        Uppsala University & Lund University (FS)
        Gothenburg University ascii
    For future implementation:
        University of Amsterdam / AMOLF (consero & procul)
        Jyväskylä University
    """
    
    
    
    def __init__(self, expID='',**kwargs):
        
        self._expID         = expID
        self._unit          = {}
        self._quant         = {}
        self._hdr           = None
        self.Nrepeats       = None
        self._logfilename       = ''
        self._runfoldername     = ''
        self._unit['dA']        = r'w'
        self._unit['dT']        = r''
        self._unit['pT']        = r'\%'
        self._unit['pT0']       = r'\%'
        self._unit['rT']        = r'\%'
        self._unit['rT0']       = r'\%'
        self._unit['t']         = r'ps'
        self._unit['chrp']      = r'ps'
        self._unit['Nrepeats']  = None
        self._quant['dA']       = r'\Delta\mathrm{A}'
        self._quant['dT']       = r'\frac\mathrm{T}\mathrm{T_0}\%'
        self._quant['pT']       = r'\%'
        self._quant['pT0']      = r'\%'
        self._quant['rT']       = r'\%'
        self._quant['rT0']      = r'\%'   
        self._quant['chrp']     = r'delay'   
        self._quant['t']        = r'delay'
        self._quant['Nrepeats'] = r'N\textsuperscript{o}'
        
        self._hdr           = {
                        'header'        : "",
                        'filetype'      : "",
                        'proc_exist'    : False,
                        'chirp_exist'   : False,
                        'twochop'       : False,
                        'fabry'         : False,
                        'inhibit'       : False,
                        'processed'     : False,
                        'delayunits'    : 'ps',
                        'probeunits'    : 'nm',
                        }
        
        if (self._expID == 'FS_uv'):
            self._file_name             = '%s%s'
            self._head_name             = '%s'
            self._unit['Ipump']         = r'V'
            #self._unit['dA']            = r'OD'
            self._unit['w']             = r'nm'
            self._unit['pr_w']          = r'nm'
            self._unit['pu_w']          = r'nm'
            self._quant['Ipump']        = r'I'
            #self._quant['dA']           = r'\lambda'
            self._quant['w']            = r'w'
            self._quant['pr_w']         = r'\lambda'
            self._quant['pu_w']         = r'\lambda'
            self._hdr['filetype']       = 'FS_uv'

            self._asoc                  = {'Ipump' : {'x':None,'y':'t'},
                                           'pr_w'  : {'x':'w','y':'t'},
                                           'pu_w'  : {'x':'w','y':'t'},
                                           'dA'    : {'x':'w','y':'t'},
                                           'sigma' : {'x':'w','y':'t'},}

        elif (self._expID == 'FS_uv_no_sigma_no_pump'):
            self._file_name             = '%s%s'
            self._head_name             = '%s'
            self._unit['Ipump']         = r'V'
            self._unit['w']             = r'nm'
            self._unit['pr_w']          = r'nm'
            self._unit['pu_w']          = r'nm'
            self._quant['Ipump']        = r'I'
            self._quant['w']            = r'w'
            self._quant['pr_w']         = r'\lambda'
            self._quant['pu_w']         = r'\lambda'
            self._hdr['filetype']       = 'FS_uv_no-sigma_no-pump'

            self._asoc                  = {'Ipump' : {'x':None,'y':'t'},
                                           'pr_w'  : {'x':'w','y':'t'},
                                           'pu_w'  : {'x':'w','y':'t'},
                                           'dA'    : {'x':'w','y':'t'},
                                           'sigma' : {'x':'w','y':'t'},}

        elif (self._expID == 'procul_ir'):
            self._file_name = '%s%s%s'
            self._unit['w']             = r'\mathrm{cm}^{-1}'
            self._unit['prb_w']         = r'\mathrm{cm}^{-1}'
            self._unit['ref_w']         = r'\mathrm{cm}^{-1}'
            self._unit['pmp_w']         = r'\mathrm{cm}^{-1}'
            self._quant['w']            = r'\tilde{\nu}'
            self._quant['prb_w']        = r'\tilde{\nu}'
            self._quant['ref_w']        = r'\tilde{\nu}'
            self._quant['pmp_w']        = r'\tilde{\nu}'
            self._hdr['filetype']       = 'procul_ir'
            raise NotImplementedError('%s format is not implemented yet.'%(expID))

        elif (self._expID == 'consero_ir'):
            self._file_name             = '%s%s%s'
            self._hdr['filetype']       = 'consero_ir'
            raise NotImplementedError('%s format is not implemented yet.'%(expID))

        elif (self._expID == 'ir2015ascii'):
            import nptdms #available through pip
            self._asci_name             = '%s%s'
            self._unit['w']             = r'\mathrm{cm}^{-1}'
            self._unit['prb_w']         = r'\mathrm{cm}^{-1}'
            self._unit['ref_w']         = r'\mathrm{cm}^{-1}'
            self._unit['pmp_w']         = r'\mathrm{cm}^{-1}'
            self._quant['w']            = r'\tilde{\nu}'
            self._quant['prb_w']        = r'\tilde{\nu}'
            self._quant['ref_w']        = r'\tilde{\nu}'
            self._quant['pmp_w']        = r'\tilde{\nu}'
            self._hdr['filetype']       = 'ir2015ascii'

        elif (self._expID == 'b23_single' or self._expID == 'B23_single'):
            self._unit['dCD']           = r'w'
            self._quant['dCD']          = r'\Delta CD'
            self._unit['w']             = r'nm'
            self._quant['w']            = r'\lambda'
            self._unit['t']             = r'ms'
            self._quant['t']            = r'delay'
            self._unit['scan']          = r'N'
            self._quant['scan']         = r'scan'

            self._asoc                  = {'dCD':{'x':'w','y':'t'}}

        else:
            raise ValueError('INVALID EXPERIMENT ID')


    def load(self, folder, name, merge_scans=False, merge_axis=-1):
        if (self._expID == 'FS_uv' or self._expID == 'FS_uv_no_sigma_no_pump'):
            
            float_re = r"[-+]?\d+(?:\.\d*)?"
            
            infile  = os.path.join(folder,self._file_name%(os.path.splitext(name)[0],os.path.splitext(name)[1]))
            inhead  = os.path.join(folder,self._head_name%(os.path.splitext(name)[0]))
            #fn_chrp = os.path.join(folder, infile)
            #bork
            if os.path.isfile(infile):
                
                range_rex=re.compile(r"^Observation Wavelength Range :\s*(%s)\s*-\s*(%s)"%(float_re,float_re))
                temp_rex=re.compile(r"^Temperature :\s*(\S*)\s*$")
                erg_rex=re.compile(r"^Energy :\s*(%s)\s*([a-zA-Z]*)\s*$"%(float_re))
                lpump_rex=re.compile(r"Exitation Wavelength :\s*(%s)\s*([a-zA-Z]*)\s*$"%(float_re))
                
                if os.path.isfile(inhead) and os.path.splitext(infile)[-1]!='.csv':
                    with open(inhead,'U') as f:
                        for line in f:
                                self._hdr['header'] += line.replace("\r", "")
                                if range_rex.match(line):
                                        self._hdr['lmax']=float(range_rex.match(line).groups()[0])
                                        self._hdr['lmin']=float(range_rex.match(line).groups()[1])
                                elif temp_rex.match(line):
                                        if temp_rex.match(line).groups()[0]=='RT':
                                                self._hdr['temperature']=temp_rex.match(line).groups()[0]
                                        else:
                                                self._hdr['temperature']=float(temp_rex.match(line).groups()[0])
                                elif erg_rex.match(line):
                                        self._hdr['energy']=float(erg_rex.match(line).groups()[0])
                                        self._hdr['energy_unit']=erg_rex.match(line).groups()[1]
                                elif lpump_rex.match(line):
                                        self._hdr['l_pump']=float(lpump_rex.match(line).groups()[0])
                                        self._hdr['l_pump_unit']=lpump_rex.match(line).groups()[1]
                                else:
                                        continue
                else:
                    print("Warning: No header file found for %s."%(infile))

                with open(infile,'U') as f:
                    
                    if os.path.splitext(infile)[-1]=='.csv':
                        rawdata = np.genfromtxt(f, delimiter=',')
                    else:
                        rawdata = np.genfromtxt(f)


                self.t     = rawdata[1:,0]
                
                if self._expID == 'FS_uv_no_sigma_no_pump':
                    #self.Ipump = rawdata[1:,-1]
                    self.w     = np.unique(rawdata[0,1:])
                    idx = len(np.unique(self.w,return_index=True)[0])

                    self.dA  = rawdata[1:,1:(idx+1)]
                    self.sigma = np.ones_like(self.dA)
                    #bork
                else:
                    #self.Ipump = rawdata[1:,-1]
                    self.w     = np.unique(rawdata[0,1:-1])
                    idx = len(np.unique(self.w,return_index=True)[0])

                    self.dA  = rawdata[1:,1:(idx+1)]
                    self.sigma = rawdata[1:,(idx+1):(idx*2+1)]
                
                if self.dA.shape == (self.t.shape[0], self.w.shape[0]):
                    self.dA = np.transpose(self.dA)
                #if self.Ipump.shape == (self.t.shape[0], self.w.shape[0]):
                    #self.Ipump = np.transpose(self.Ipump)
                if self.sigma.shape == (self.t.shape[0], self.w.shape[0]):
                    self.sigma = np.transpose(self.sigma)                    
                
                self.Nrepeats = int(1)
            else:
                raise NameError("%s does not exist!"%infile)
        
        elif (self._expID == 'ir2015ascii'):
            
            infile  = os.path.join(folder,self._file_name%())
            
            with open(infile, 'U') as f:
                rawdata = np.loadtxt(os.path.join(f))
            
            self.t  = rawdata[:,0]
            self.w  = rawdata[0,:]
            self.dA = rawdata[1:,1:]
            
            if type(sigma)!=type(None):
                self.sigma = np.asarray(sigma)
            else:
                self.sigma = np.zeros_like(self.dS)
            
            if self.dA.shape == (self.t.shape[0], self.w.shape[0]):
                self.dA = np.transpose(self.dA)
            if self.sigma.shape == (self.t.shape[0], self.w.shape[0]):
                self.sigma = np.transpose(self.sigma)
            return

        elif (self._expID == 'b23_single' or self._expID == 'B23_single'):

            import os.path as op

            if type(folder)!=str:
                raise TypeError("\'folder\' needs to be a string.")
            if isinstance(name, collections.Iterable) and type(name)!=str:
                name = name
            elif type(name)==str:
                name = [name]

            _fnL = np.asarray([fn if op.isfile(fn) else 'None' for fn in [op.abspath(op.join(folder,fn)) for fn in name]])
            fnL  = _fnL[_fnL!='None']

            datL = []

            for i,fn in enumerate(fnL):
                rawinput = np.genfromtxt(fn)
                _hdr     = rawinput[0,1:]
                t        = rawinput[1:,0]
                scan     = rawinput[0,1:]
                w        = np.asarray([0])
                rawdat   = rawinput[1:,1:][None,:,:]
                #shape    = (fnL.shape + w.shape + t.shape + scan.shape)

                if i==0:
                    #Shape       = shape
                    self.t      = t
                    self.scan   = scan.astype(np.int)
                    self.w      = w
                else:
                    if np.all(self.w!=w) and merge_axis:
                        rawdat = merge_reg(rawdat, w, self.w, axis=0)
                    elif np.all(self.w!=w) and not merge_axis:
                        raise ValueError("\'w\' axis of different measurements do not match.")
                    if np.all(self.t!=t) and merge_axis:
                        rawdat = merge_reg(rawdat, t, self.t, axis=1)
                    elif np.all(self.w!=w) and not merge_axis:
                        raise ValueError("\'t\' axis of different measurements do not match.")
                datL.append(rawdat)
            self.dat_nm  = np.concatenate(datL, axis=merge_axis)[None,:] if merge_scans else np.asarray(datL)
            self.mask_nm = np.ones_like(self.dat_nm)
            print("Sucessfully loaded a B23 single-wavelength style experiment")
            return
        else:
            raise ValueError('INVALID EXPERIMENT ID')


    def mergeScans(self, repeat_axis=0):

        shape  = list(self.dat_nm.shape)
        axes   = list(range(len(shape)))
        axes.pop(repeat_axis)
        axes.append(repeat_axis)

        dat_nm       = np.transpose(self.dat_nm, axes)
        mask_nm      = np.transpose(self.mask_nm, axes)
        self.dat_nm  = dat_nm.reshape((1,) + dat_nm.shape[:2] + (-1,))
        self.mask_nm = mask_nm.reshape((1,) + mask_nm.shape[:2] + (-1,))
        self.scan   = np.arange(dat_nm.shape[-1], dtype=np.int)+1

        return


    def reduce(self, plot=False, removeOutliers=False, remove_shot_to_shot=False,
               reduction_method='stdev', **kwargs):
        """
        outlier_removal (list of tuples)  : list of tuples. 
                                            tuples have the structure (method, limit).
        """

        reduction_method_fns = {'prop'  : mean_with_error,
                                'stdev' : mean_with_error_from_std,
                                'mad'   : median_with_error_from_mad}

        self.dat_nm_err = np.ones_like(self.dat_nm)
        shape           = self.dat_nm.shape[:-1]

        self.dCD        = np.empty(shape)
        self.sigma      = np.empty(shape)

        if removeOutliers:
            self.removeOutliers(**kwargs)

        for index in np.ndindex(shape):
            self.dCD[index], self.sigma[index] = reduction_method_fns[reduction_method](
                self.dat_nm[index], self.dat_nm_err[index],
                mask=self.mask_nm[index], axis=0)

        if remove_shot_to_shot:
            delattr(self, 'dat_nm')
            delattr(self, 'mask_nm')
            delattr(self, 'dat_nm_err')
            delattr(self, 'scan')

        return


    def removeOutliers(self, outlier_removal=None):

        outlier_removal_fns = {
            "large_dy_mean"     : find_outliers_large_dy_mean,
            "large_dy_median"   : find_outliers_large_dy_median,
            "stdev"             : find_outliers_stdev,
            "mad"               : find_outliers_mad,
            }

        if not hasattr(self, mask_nm):
            self.mask_nm = np.ones_like(self.dat_nm, dtype=bool)

        for fn,limit in outlier_removal:
            func = outlier_removal_fns[fn]
            for i in range(shape[0]):
                for j in range(shape[2]):
                    self.mask_nm[i,:,j,:] &= outlier_rejection(self.dat_nm[i,:,j,:], func, limit, mask=self.mask_nm[i,:,j,:])

        return

