# -*- coding: utf-8 -*-
"""
This module contains helpful functions used by the libwaxs Classes. See the code for detailed documentation.
"""

import collections
import os,sys
import os.path as osp
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from mpl_toolkits.axes_grid1 import AxesGrid
from matplotlib.ticker import Locator
#import libwaxs as lw

import sys
lw_path = os.path.realpath(__file__).replace('helpers.py','')
sys.path.append(lw_path)
import libwaxs

# A function which takes a number of lists/arrays, and returns a sorted unique list as well as a dict of offsets 0<=offset<1 and a dict of plotting colors.
def delays(*args):
    import colorsys
    # unique list of delays dt:
    dt = uniqueList(*args)
    # offsets from 0 to ~1, in a dict with dt keys:
    offsets = dict(list(zip(dt, np.arange(0, len(dt)))))
    # color spectrum in a similar dict:
    cols = [colorsys.hls_to_rgb(h*1.0/len(dt), .45, .6) for h in range(len(dt))]
    colors = dict(list(zip(dt, cols)))
    #
    return dt, offsets, colors

# A function which takes an integer, assumes it's nanoseconds, and produces a nice string with units.
def nsToString(ns):
    if   abs(ns * 1e-6) >= 1:
        return "%.1f ms"%(ns * 1e-6)
    elif abs(ns * 1e-3) >= 1:
        return "%.1f us"%(ns * 1e-3)
    elif abs(ns * 1e-0) >= 1:
        return "%.1f ns"%(ns * 1e-0)
    elif abs(ns * 1e+3) >= 1:
        return "%.1f ps"%(ns * 1e3)
    elif ns == 0:
        return ""
    else:
        raise ValueError

# A function which takes a string with units, and converts it to (float) nanoseconds:
def stringToNs(string):
    string = string.strip()
    if 'ps' in string:
        return float(string.strip('ps'))*1e-3
    if 'ns' in string:
        return float(string.strip('ns'))
    if 'us' in string:
        return float(string.strip('us'))*1e3
    if 'ms' in string:
        return float(string.strip('ms'))*1e6
    if 's' in string:
        return float(string.strip('s'))*1e9
    if string == '-':
        # off images at id9 have this
        return -10.0
    else:
        raise ValueError('No valid unit found')

# A function which takes any number of lists or np-arrays as arguments, and returns a composite list with only unique entries, sorted by value, converted to floats.
def uniqueList(*args):
    biglist = []
    for i in range(len(args)):
        #biglist += list(args[i].squeeze())
        biglist += list(args[i])
    return list(map(float, sorted(list(set(biglist))))) # conversion to a set, then back to list, gets rid of duplicates

# A function for resampling (x,y) data over a new x grid:
def resample(y, x, x_new, axis=0):
    from scipy import interpolate
    f = interpolate.interp1d(x, y, axis=axis, fill_value=0., bounds_error=False)
    return f(x_new)

# An auxiliary function which labels gca
def labelAxes(label, x=.05, y=.9, fontsize=14):
    plt.text(x, y, label, transform=plt.gca().transAxes, va='top', fontsize=fontsize)

# An auxiliary function which adds a new axes in the old axes' coordinates
# The keyword axlines can be "bottom", or else defaults to None, which gives a box.
# The keyword fontsize defaults to 8 and decides about labels and ticklabels.
# The keyword xlabelpad does what it sounds like and defaults to 0.
# Any other keyword arguments are passed on as axes properties.
def addSubAxes(newpos, axlines=None, fontsize=8, xlabelpad=0, **kwargs):
    pos = plt.gca().get_position()
    x0      = newpos[0] * (pos.x1-pos.x0) + pos.x0
    width   = newpos[2] * (pos.x1-pos.x0)
    y0      = newpos[1] * (pos.y1-pos.y0) + pos.y0
    height  = newpos[3] * (pos.y1-pos.y0)
    ax = plt.axes([x0, y0, width, height])
    ax.set(yticks=[])
    ax.set(**kwargs)
    if axlines == 'bottom':
        ax.get_yaxis().set_visible(False)
        ax.get_xaxis().tick_bottom()
        ax.set(frame_on=False)
        xmin, xmax = ax.get_xticks()[0], ax.get_xticks()[-1]
        ymin, ymax = ax.get_ylim()
        ax.add_artist(plt.Line2D((xmin, xmax), (ymin, ymin), color='black', linewidth=2))
    for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] + ax.get_xticklabels() + ax.get_yticklabels()):
        item.set_fontsize(fontsize)
    ax.xaxis.labelpad = xlabelpad
    return ax

# A function lifted from stackoverflow
def error(data, confidence=0.95):
    import numpy as np
    import scipy as sp
    import scipy.stats
    a = 1.0*np.array(data)
    n = len(a)
    m, se = np.mean(a), scipy.stats.sem(a)
    h = se * sp.stats.t._ppf((1+confidence)/2., n-1)
    return m, h

# convert matlab notation to a list:
def mat2list(mat):
    lst = []
    mat = mat.replace('[','')
    mat = mat.replace(']','')
    mat = mat.replace(',',' ')
    for item in mat.split():
        if not ':' in item:
            lst += [int(item)]
        else:
            beg, end = list(map(int, item.split(':')))
            lst += list(range(beg, end+1))
    return lst

# Function to smooth dS matrices (or timeslice matrices) across the first dimension (q)
def running_mean(x,N):
    # Copy array so that original is not changed (pointer!)
    temp=np.copy(x)
    # The values at the beginning and end cannot be averaged
    # Therefore fill up beginning and end with NaNs
    # This way the normal q values can be used when plotting
    temp[:N/2,:]=np.nan
    temp[-N/2:,:]=np.nan
    # Go through columns and do running average
    for i in range(np.shape(x)[1]):
        cumsum=np.cumsum(np.insert(x[:,i], 0, 0))
        temp[N/2:-N/2+1,i]=(cumsum[N:] - cumsum[:-N]) / N
    return temp

# Calculates radial distribution function from difference scattering dS
# by Fourier sine transformation as described in Kim et al., Nature 2015
# Input: scattering angle q, difference scattering dS and distance vector r
# Requires equally spaced q values!!
def get_rdf(q,dS,r=np.arange(.001,20.01,.01)):
    print("Calculate sine fourier transform")
    # Numerical integration
    alpha = 0.03 # damping factor as used by Kim et al.
    print("Damping factor %.2f"%alpha)
    exp_term=np.exp(-(q**2)*alpha)
    dq = np.average(np.diff(q))
    # Loop
    sinefour=[]
    for ri in r:
        whole_term=q*dS*np.sin(q*ri)*exp_term*dq
        sinefour.append(ri/2.0/(np.pi**2)*np.sum(whole_term))
    return np.array(sinefour)

# Starts the proprietary matlab engine for python.
def start_mat_engine():
    global eng    # Needed to modify global copy of globvar, allows the matlab engine to be used outside this function
    print("Starting matlab engine, please be patient...")
    import matlab.engine
    eng = matlab.engine.start_matlab()
    return eng

# Writes an entry in a log file
def write_log(params,sig,func,logfile):
    raise NotImplemented('Writing to a log file is not yet implemented.')

# Definition of residuals for lsq fit of xAdjust.
def xAdjust_residuals(params,q,dS,dSRef,qRef,sigmaRef):
    """
    leastsq reqires a flat array.
    """
    dSfit = xAdjust_fitfunc(params,q,dS,dSRef,qRef)
    #mask = np.invert(np.isnan(dSfit))
    mask = np.nonzero(sigmaRef)
    return np.ravel((dSRef[mask]-dSfit[mask])/sigmaRef[mask])

# Lsq fit function of xAdjust.
def xAdjust_fitfunc(params,q,dS,dSRef,qRef):
    #import scipy as sp
    from scipy.interpolate import interp1d

    A, stretch = params
    qFit = q*stretch

    f = interp1d(qFit,dS.squeeze(),kind='linear',copy=False, assume_sorted=True, bounds_error=False, fill_value=0.)

    return A*f(qRef)

#
def simple_residuals(pfree,data,pfixed,flags,calc=None,func=None,sigma=None):

    if type(sigma)==type(None):
        sigma = np.ones_like(data)
    
    #print pfree
    
    if type(calc)!=type(None) and type(func)==type(None):
        func = scale_data
    
    p_arg = np.zeros_like(flags,dtype=float) #creates a numpy array of zeros
    p_arg[flags]=pfree #uses boolean indexing to stick in the values of the free parameters in the p_arg
    p_arg[~flags]=pfixed #uses boolean indexing to stick in the values of the fixed parameters in the p_arg, ~ means it sticks them in the FALSE bits.

    mask = np.nonzero(sigma)

    return np.ravel((data[mask]-func(p_arg,data)[mask])/sigma[mask])

def scale_data(scale,data):
    
    return data*scale
#
def expSubtractHeat_residuals(pfree,pfixed,flags,t,dS,sigma,dSheat,eval=None):

    if eval != None:
        p_arg = eval
    else:
        p_arg = np.zeros_like(flags,dtype=float) #creates a numpy array of zeros
        p_arg[flags]=pfree #uses boolean indexing to stick in the values of the free parameters in the p_arg
        p_arg[~flags]=pfixed #uses boolean indexing to stick in the values of the fixed parameters in the p_arg, ~ means it sticks them in the FALSE bits.

    mask = np.nonzero(sigma)
    return np.ravel((dS[mask]-eval_exp_2Dltd(p_arg,t,dSheat)[mask])/sigma[mask])

# Evaluates the an exponential over a given timerange. Gives 0 for x<0.
def eval_exp_2Dltd(p, x, y):
    """
    Args:
            p (1D array-like) : Parameters, k, A, x0.
            x (1D array-like) : Values at which the exponential needs to be evaluated.

    Returns:
            1D-array with length of x
    """
    p = np.asarray(p)
    condlist = [(x-p[2]) < 0, (x-p[2]) >= 0]
    funclist = [lambda x: 0.*(x-p[2]) , lambda x : p[1]*(1-np.exp(-p[0]*(x-p[2])))]

    cnstrct = np.dot(y.reshape(-1,1),np.piecewise(x, condlist, funclist).reshape(1,-1))

    return cnstrct


# convert covariance matrix from scipy leastsq fit to 1 sigma errors.
def cov2sigma(out,redChi2):
    p, cov = out[0],out[1]
    #bork
    #print cov.shape
    sigma = np.sqrt(np.diag(cov)*redChi2)
    if len(p)!=len(sigma):
        print("Parameters do not belong to the covariance matrix given!")

    return sigma

# perform an svd on a dataset and returns a tuple with the results
def svd_array(data):
    """
    Wrapper function to structure the result of an SVD in a tuple (U,s,V).
    """
    return np.linalg.svd(data, full_matrices=0, compute_uv=1)

# perform an svd on a dataset and returns the results in dictionary format
def svd_dict(data):
    """
    Wrapper function to structure the result of an SVD in a dictionary format.
    """

    D = {}

    (U,s,V)=np.linalg.svd(data, full_matrices=0, compute_uv=1)

    D['U'] = U
    D['s'] = s
    D['V'] = V

    return D

# reconstruct data from truncated projection, target and projection vectors
def reconstruct_from_svd(U,s,V, components=None):
    """
    Reconstruct the data from singular values, target and projection vectors.
    """
    U = np.matrix(U)
    s = np.asarray(s)
    V = np.matrix(V)

    if components>len(s):
        print("Given number of components exceeds number of actual components!\n setting number of components to %s"%(len(s)))
        idx = len(s)
    else:
        idx = components

    if components != None:
        Sr = np.diag(s[0:idx])
        Ur = U[:,0:idx]
        Vr = V[0:idx]
    else:
        Sr = np.diag(s)
        Ur = U
        Vr = V

    recstrct = np.asarray(np.dot(Ur, np.dot(Sr, Vr)))

    return recstrct

# using an FFT to smooth a curve, why not?
def smooth_fft(x,y,filter=None,fromZero=False):
    """
    Based off of stackexchange: http://stackoverflow.com/questions/20618804/how-to-smooth-a-curve-in-the-right-way


    Args:
            x,y (1D array-like)     : 1D data, x values, y values.

    Kwargs:
            filter (tuple)          : tuple (str,float) of which the first element contains the filter to be used, and the second element the value to be applied.
                                            Possible filters:
                                            cutoff          :
                                            lowpass         :
                                            highpass        :


    Returns:

    """
    import scipy.fftpack as sfft

    # Spacing of the samples
    dxFT = np.mean(np.diff(x))
    #create equally spaced grid for fft via linear interpolation that starts at 0 and ends at max of given range.
    if fromZero:
        xFT = np.linspace(0,x[-1],num=(x[-1]/dxFT),endpoint=True)
        yFT = resample(y, x, xFT)
        yFT[np.isnan(yFT)] = 0.
    # create equally spaced grid for fft via linear interpolation between max and min of given range
    elif not len(np.unique(np.diff(x))):
        xFT = np.linspace(x[0],x[-1],num=(x[-1]/dxFT),endpoint=True)
        yFT = resample(y, x, xFT)
        yFT[np.isnan(yFT)] = 0.
    # if grid is already equally spaced
    else:
        xFT = x
        yFT = y


    N = len(xFT)
    w = sfft.rfft(yFT)


    f = sfft.rfftfreq(N, dxFT)
    curve = w**2
    w2 = w.copy()

    xlev=[0,0]
    ylev=0
    level=[xlev,0]
    if filter != None:
        for fil in filter:
            if fil[0]=='cutoff':
                lev = curve.max()/fil[1]
                cutoff_idx = curve < lev #returns boolean array where values above curve.max()/cutoff are True
                w2[cutoff_idx] = 0
                ylev=lev
            elif fil[0]=='lowpass':
                lev = (fil[1]/100.)*f.max()
                lowpass_idx = abs(f) > lev #returns boolean array where values above f.max()/lowPass are True
                w2[lowpass_idx] = 0
                xlev[1]=lev
            elif fil[0]=='highpass':
                lev = (fil[1]/100.)*f.max()
                highpass_idx = abs(f) < lev #returns boolean array where values above f.max()/lowPass are True
                w2[highpass_idx] = 0
                xlev[0]=lev
            elif fil[0]=='damping':
                lev = fil[1]
                w2 = w*np.exp(-(np.power(f,2))*lev)
                xlev = np.exp(-(np.power(f,2))*lev)
            else:
                raise NotImplementedError('%s not an implemented filter!'%(fil[0]))
                continue
        level[0]=xlev
        level[1]=ylev



    y2 = sfft.irfft(w2)

    # Back to the original spacing, for compatibility with the rest of the data
    ySmooth = resample(y2, xFT, x)

    return (ySmooth,(N,dxFT,xFT,yFT,f,w,w2,level))

## using an FFT to smooth a curve, why not?
#def smooth_fft_N1D(x,y,filter=None,fromZero=False):
    #"""
    #Based off of stackexchange: http://stackoverflow.com/questions/20618804/how-to-smooth-a-curve-in-the-right-way
    #Takes the FFT along the x axis for each y-array (2D array).
    #Args:
        #x,y (1D array-like)    : 1D data, x values, y values.

    #Kwargs:
        #filter (tuple)         : tuple (str,float) of which the first element contains the filter to be used, and the second element the value to be applied.
                                            #Possible filters:
                                            #cutoff         :
                                            #lowpass                :
                                            #highpass       :

    #Returns:

    #"""
    #import scipy.fftpack as sfft

    #x = np.asarray(x)
    #y = np.asarray(y)

    ## Spacing of the samples
    #dxFT = np.mean(np.diff(x))
    ##create equally spaced grid for fft via linear interpolation that starts at 0 and ends at max of given range.
    #if fromZero:
        #xFT = np.linspace(0,x[-1],num=(x[-1]/dxFT),endpoint=True)
        #yFT = resample(y, x, xFT)
        #yFT[np.isnan(yFT)] = 0.
    ## create equally spaced grid for fft via linear interpolation between max and min of given range
    #elif not len(np.unique(np.diff(x))):
        #xFT = np.linspace(x[0],x[-1],num=(x[-1]/dxFT),endpoint=True)
        #yFT = resample(y, x, xFT)
        #yFT[np.isnan(yFT)] = 0.
    ## if grid is already equally spaced
    #else:
        #xFT = x
        #yFT = y


    #N = len(xFT)

    #w = sfft.rfft(yFT,axis=0)


    #f = sfft.rfftfreq(N, dxFT)
    #curve = w**2
    #w2 = w.copy()

    #xlev=[0,0]
    #ylev=0
    #level=[xlev,0]
    #if filter != None:
        #for fil in filter:
            #if fil[0]=='cutoff':
                #lev = np.max(curve, axis=0)/fil[1]
                #cutoff_idx = curve < lev #returns boolean array where values above curve.max()/cutoff are True
                #w2[cutoff_idx] = 0
                #ylev=lev
            #elif fil[0]=='lowpass':
                #lev = (fil[1]/100.)*np.max(f, axis=0)
                #lowpass_idx = np.abs(f) > lev #returns boolean array where values above f.max()/lowPass are True
                #w2[lowpass_idx] = 0
                #xlev[1]=lev
            #elif fil[0]=='highpass':
                #lev = (fil[1]/100.)*np.max(f, axis=0)
                #highpass_idx = np.abs(f) < lev #returns boolean array where values above f.max()/lowPass are True
                #w2[highpass_idx] = 0
                #xlev[0]=lev
            #elif fil[0]=='damping':
                #lev = fil[1]
                #w2 = w*np.exp(-(np.power(f,2))*lev)
                #xlev = np.exp(-(np.power(f,2))*lev)
            #else:
                #raise NotImplementedError('%s not an implemented filter!'%(fil[0]))
                #continue
        #level[0]=xlev
        #level[1]=ylev

    #y2 = sfft.irfft(w2)

    ## Back to the original spacing, for compatibility with the rest of the data
    #ySmooth = resample(y2, xFT, x)

    #return (ySmooth,(N,dxFT,xFT,yFT,f,w,w2,level))

def fft_filter(dataFFT, f, filter=None):
    '''
    Helper function for the various FFT functions. Contains various filters for
    the inverse-space of the original data.
    Args:
            dataFFT (ndarray) :
            f       (ndarray) :

    Kwargs:
            filter  : list of tuples in the form (str,float) of which the first element contains the
                      filter to be used, and the second element the value to be applied.
                      Possible filters:
                            'cutoff'        : Sets all dataFFT(f) values to 0, that fall under
                                              the specified threshold.
                            'lowpass'       : Sets all dataFFT(f) values to 0 above a specified
                                              value of f.
                            'highpass'      : Sets all dataFFT(f) values to 0 below a specified
                                              value of f.
                            'exp2_damp'     : Multiplies all values of dataFFT(f) with a decaying
                                              exponential function of the form: exp(-(f**2))*alpha).
                                              Where alpha is the specified damping factor.
    '''
    
    if type(filter)==tuple:
        filter = [filter]
    elif type(filter)!=list or type(filter)!=tuple:
        raise TypeError("filter has to be a tuple or list, not a {}.".format(type(filter).__name__))
    
    curve = dataFFT**2
    xlev=[]
    ylev=[]
    #f_grid,_ = np.meshgrid(f, np.zeros_like(dataFFT[0]), copy=False, indexing='ij')
    for fil in filter:
        if fil[0]=='cutoff':
            lev = np.max(curve, axis=0)/fil[1]
            cutoff_idx = curve < lev #returns boolean array where values above curve.max()/cutoff are True
            dataFFT[cutoff_idx] = 0
            ylev.append(lev)
        elif fil[0]=='lowpass':
            lev = (fil[1]/100.)*np.max(f, axis=0)
            lowpass_idx = np.abs(f) > lev #returns boolean array where values above f.max()/lowPass are True
            dataFFT[lowpass_idx] = 0
            xlev.append(lev)
        elif fil[0]=='highpass':
            lev = (fil[1]/100.)*np.max(f, axis=0)
            highpass_idx = np.abs(f) < lev #returns boolean array where values above f.max()/lowPass are True
            dataFFT[highpass_idx] = 0
            xlev.append(lev)
        elif fil[0]=='exp2_damp' or fil[0]=='exp2damp':
            lev = fil[1]
            dataFFT = dataFFT*np.exp(-(np.power(f,2))*lev)
            ylev.append(np.exp(-(np.power(f,2))*lev))
        else:
            raise NotImplementedError('%s not an implemented filter!'%(fil[0]))
            continue

    return (dataFFT, (xlev,ylev))

# Searches for indices of the values in a given array-like and returns the indices of the elements.
# Will give nearest match, useful when you do not necessarily know the timepoint or q point available.
def idxpt(A,x):
    '''
    Searches for indices of the values in a given array-like and returns the indices of the elements.
    Args:
            A (1D array-like): Array of sequential values. The array can be increasing or decreasing in value.
            x (1D array-like): Array of values of which the indices will be searched for in A.

    Returns:
            idx (list): indices of the given values.
    '''
    A = np.ravel(np.asarray(A))
    x = np.ravel(np.asarray(x))
    if A[0]>A[-1]:
        x=A[::-1].searchsorted(x, side='left')
        idx=(A.shape[0]-x)
    else:
        idx=A.searchsorted(x, side='left')
    return idx

def find_closest(A, target):
    """
    From: https://stackoverflow.com/questions/8914491/finding-the-nearest-value-and-return-the-index-of-array-in-python
    by:   Bi Rico
    A must be sorted
    """

    idx = A.searchsorted(target)
    idx = np.clip(idx, 1, len(A)-1)
    left = A[idx-1]
    right = A[idx]
    idx -= target - left < right - target
    return idx


def get_lim(A,x):
    '''
    Helper function for idxpt().
    If values of x are beyond the range of A, they will be defined as the appropriate extremities of A.
    '''
    if type(x)==type(float()):
        x = [x]
    elif type(x)!=type(np.array([])) and type(x)!=type(list()) and type(x)!=type(tuple()):
        x = np.asarray(x)

    A = np.asarray(A)

    idx = idxpt(A,x)

    if A[0]>A[-1]:
        for i,id in enumerate(idx):
            if id == len(A) and float(x[i])>A.min():
                idx[i] = 0
            elif id == len(A) and float(x[i])<A.max():
                idx[i] = -1
            else:
                continue
    else:
        for i,id in enumerate(idx):
            if id == len(A) and float(x[i])>A.min():
                idx[i] = -1
            elif id == len(A) and float(x[i])<A.max():
                idx[i] = 0
            else:
                continue
    return idx

def StdIdxLim(A, lim, slice=False, limOnly=False):
    """
    Helper function returns indices and values of points in an array.
    A (sorted 1darray-like)            : 
    lim (1darray-like with 2 elements) : 
    
    slice (bool)                       : If True, any index retreived that comes up as
                                            -1, is set to None.
    """
    A = np.asarray(A).squeeze()
    
    if type(lim)==type(None):
        lim = np.asarray([A.min(), A.max()])
    elif not isinstance(lim, collections.Iterable) or type(lim)==str:
        raise TypeError("lim has to be an 1darray-like.")
    
    lim = np.asarray(lim).squeeze()
    if lim.size>2:
        raise ValueError("lim has to contain only 2 elements.")
    
    if limOnly:
        return lim
    
    
    idx = get_lim(A, lim)
    
    Lim = A[idx]
    
    if slice:
        idx = idx.astype(object)
        if idx[1]!=-1:
            idx[1] += 1
        idx[idx==-1] = None
        idx[idx==len(A)-1] = None
        if A[0]>A[-1]:
            idx = np.hstack((idx, -1))
        
    return (idx, Lim)

def list_indices(seq):
    """
    Returns tuples of unique items of a given list (seq) and a list of the indices corresponding to their occurrence in the the givenlist.
    Ripped off of: http://stackoverflow.com/questions/5419204/index-of-duplicates-items-in-a-python-list
    By Paul McGuire
    """
    from collections import defaultdict
    tally = defaultdict(list)
    for i,item in enumerate(seq):
        tally[item].append(i)
    return ((key,locs) for key,locs in list(tally.items()))

def det_sci(A):
    A = np.asarray(A)
    mask = A != 0
    E = np.zeros_like(A)
    C = np.zeros_like(A)
    E[mask] = np.floor(np.log10(np.abs(A[mask])))
    C = A/10**E
    return (C,E)

def sci_notation(num, decimal_digits=1, precision=None, exponent=None, lim=None):
    """
    From: http://stackoverflow.com/questions/18311909/how-do-i-annotate-with-power-of-ten-formatting
    Returns a string representation of the scientific
    notation of the given number formatted for use with
    LaTeX or Mathtext, with specified number of significant
    decimal digits and precision (number of decimal digits
    to show). The exponent to be used can also be specified
    explicitly.
    lim (int): limit on exponent not to represent number in exponential format.
    """
    from math import floor, log10
    
    if type(lim)!=int and type(lim)!=float:
        raise TypeError('lim has to be either int or float, not %s')%(type(lim))

    if num==0:
        exponent = 0.
    elif not exponent:
        exponent = int(floor(log10(abs(num))))
    
    if not lim or exponent>lim:
        coeff = round(num / float(10**exponent), decimal_digits)
    elif lim and exponent<=lim:
        coeff = num

    if not precision:
        precision = decimal_digits
    
    if num==0:
        return r"${0:.{1}f}$".format(coeff, precision)
    else:
        if not lim or exponent>lim:
            return r"${0:.{2}f}\times10^{{{1:d}}}$".format(coeff, exponent, precision)
        elif lim and exponent<=lim:
            return r"${0:.{1}f}$".format(coeff, int(floor(precision/lim)))

def round_to(n, precision):
    correction = 0.5 if n >= 0 else -0.5
    return int( n/precision+correction ) * precision

def getsublist(thelist, item, adef=None):
    '''
    From http://stackoverflow.com/questions/1658505/searching-within-nested-list-in-python.
    Function to get a sublist from a nested list structure.
    adef is the item the next function stops on, None is default (when list finishes).
    '''
    return next((sublist for sublist in thelist if item in sublist), adef)

def QonveRt_func(x):
    """
    Convert between q-space (reciprocal space (Å-1)) and r-space (real space (Å)).
    r = 2.pi/q
    """
    if isinstance(x, collections.Iterable) and type(x)!=str:
        x = np.asarray(x)
    else:
        raise TypeError('Variable type (%s) passed cannot be converted.'%(type(x)))
    return np.asarray(np.linspace(2*np.pi/x.min(),2*np.pi/x.max(),num=len(x)).reshape(x.shape))


def genLogScale(AX, num=None, exp_lim=None):

    if type(num)!=type(None):
        num = int(num)
    else:
        num = 10

    base,exp = det_sci(AX)
    if type(exp_lim)!=type(None):
        exp_lim = int(exp_lim)
    else:
        exp_lim = exp.min()

    Uexp = np.unique(exp)
    sign = np.unique(np.sign(base))

    new_base = np.linspace(0, 9, num=num, endpoint=True)
    new_AX = []
    for sig in sign:
        for ex in Uexp[exp_lim<=Uexp]:
            new_AX.append(sig*new_base*10**(ex))
    new_AX = np.unique(new_AX)
    new_AX = new_AX[(new_AX>=AX.min()) * (new_AX<=AX.max())]
    #kneut

    return new_AX



def contPlotParams():
    plotParams = {
            "image.cmap"                    : 'RdBu_r',
            "contour.negative_linestyle"    : 'solid',
            "xtick.major.size"              : 8,      # major tick size in points
            "xtick.minor.size"              : 4,      # minor tick size in points
            "xtick.major.width"             : 1.,    # major tick width in points
            "xtick.minor.width"             : 1.,
            "ytick.major.size"              : 8,      # major tick size in points
            "ytick.minor.size"              : 4,      # minor tick size in points
            "ytick.major.width"             : 1.,    # major tick width in points
            "ytick.minor.width"             : 1.,
            'axes.titlesize'                : 'large',
            'grid.linewidth'                : 0.0,
            'lines.linewidth'               : 0.5,
            'axes.linewidth'                : 1.5,
            "font.size"                     : 18,
            'axes.formatter.use_mathtext'   : True,
            #Text settings
            "text.usetex"                   : True,
            #Font settings
            "font.family"                   : 'sanserif',
            #Preamble settings
            "text.latex.preamble"           : [r'\usepackage[version=3]{mhchem}'],
            # display plot settings
            # plot settings
            "xtick.direction"               : "in",
            "ytick.direction"               : "in",
            "savefig.transparent"           : True,
            "legend.fontsize"               : 12,
            "legend.handletextpad"          : 0.5,
    }
    return plotParams

def zeroRange(lim, num):

    ra  = np.linspace(lim.min(),lim.max(),num=int(num),endpoint=True)

    if (0>=lim.min())*(0<=lim.max()).any():
        zero = 0
    else:
        zero = lim.min()
    raZ = ra - ra[get_lim(ra, zero)]

    upS = raZ[-1]+np.diff(raZ).mean()
    doS = raZ[0]-np.diff(raZ).mean()
    raC = raZ[(raZ>=lim.min())*(raZ<=lim.max())]
    #bork
    if len(raC)!=len(ra):
        if upS<lim.max():
            np.insert(raC, -1, ipS, axis=0)
        elif doS>lim.min():
            np.insert(raC, 0, doS, axis=0)
    return raC

def shiftedColorMap(cmap, start=0, midpoint=0.5, stop=1.0, name='shiftedcmap'):
    '''
    Ripped from http://stackoverflow.com/questions/7404116/defining-the-midpoint-of-a-colormap-in-matplotlib
    By Paul H.
    
    Function to offset the "center" of a colormap. Useful for
    data with a negative min and positive max and you want the
    middle of the colormap's dynamic range to be at zero

    Input
    -----
      cmap : The matplotlib colormap to be altered
      start : Offset from lowest point in the colormap's range.
          Defaults to 0.0 (no lower ofset). Should be between
          0.0 and `midpoint`.
      midpoint : The new center of the colormap. Defaults to 
          0.5 (no shift). Should be between 0.0 and 1.0. In
          general, this should be  1 - vmax/(vmax + abs(vmin))
          For example if your data range from -15.0 to +5.0 and
          you want the center of the colormap at 0.0, `midpoint`
          should be set to  1 - 5/(5 + 15)) or 0.75
      stop : Offset from highets point in the colormap's range.
          Defaults to 1.0 (no upper ofset). Should be between
          `midpoint` and 1.0.
    '''
    cdict = {
        'red': [],
        'green': [],
        'blue': [],
        'alpha': []
    }

    # regular index to compute the colors
    reg_index = np.linspace(start, stop, 257)

    # shifted index to match the data
    shift_index = np.hstack([
        np.linspace(0.0, midpoint, 128, endpoint=False), 
        np.linspace(midpoint, 1.0, 129, endpoint=True)
    ])

    for ri, si in zip(reg_index, shift_index):
        r, g, b, a = cmap(ri)

        cdict['red'].append((si, r, r))
        cdict['green'].append((si, g, g))
        cdict['blue'].append((si, b, b))
        cdict['alpha'].append((si, a, a))

    newcmap = matplotlib.colors.LinearSegmentedColormap(name, cdict)
    plt.register_cmap(cmap=newcmap)

    return newcmap

def byte_offset(a):
    """
    Diagnostic function,
    Returns a 1-d array of the byte offset of every element in `a`.
    Note that these will not in general be in order.
    Taken from: http://stackoverflow.com/questions/11286864/is-there-a-way-to-check-if-numpy-arrays-share-the-same-data
    """
    stride_offset = np.ix_(*list(map(range,a.shape)))
    element_offset = sum(i*s for i, s in zip(stride_offset,a.strides))
    element_offset = np.asarray(element_offset).ravel()
    return np.concatenate([element_offset + x for x in range(a.itemsize)])

def share_memory(a, b):
    """
    Diagnostic function,
    Returns the number of shared bytes between arrays `a` and `b`.
    Taken from: http://stackoverflow.com/questions/11286864/is-there-a-way-to-check-if-numpy-arrays-share-the-same-data
    """
    a_low, a_high = np.byte_bounds(a)
    b_low, b_high = np.byte_bounds(b)

    beg, end = max(a_low,b_low), min(a_high,b_high)

    if end - beg > 0:
        # memory overlaps
        amem = a_low + byte_offset(a)
        bmem = b_low + byte_offset(b)

        return np.intersect1d(amem,bmem).size
    else:
        return 0

class MinorSymLogLocator(Locator):
    """
    Dynamically find minor tick positions based on the positions of
    major ticks for a symlog scaling.
    Ripped from http://stackoverflow.com/questions/20470892/how-to-place-minor-ticks-on-symlog-scale
    By David Zwicker
    """
    def __init__(self, linthresh):
        """
        Ticks will be placed between the major ticks.
        The placement is linear for x between -linthresh and linthresh,
        otherwise its logarithmically
        """
        self.linthresh = linthresh

    def __call__(self):
        'Return the locations of the ticks'
        majorlocs = self.axis.get_majorticklocs()

        # iterate through minor locs
        minorlocs = []

        # handle the lowest part
        for i in range(1, len(majorlocs)):
            majorstep = majorlocs[i] - majorlocs[i-1]
            if abs(majorlocs[i-1] + majorstep/2) < self.linthresh:
                ndivs = 10
            else:
                ndivs = 9
            minorstep = majorstep / ndivs
            locs = np.arange(majorlocs[i-1], majorlocs[i], minorstep)[1:]
            minorlocs.extend(locs)

        return self.raise_if_exceeds(np.array(minorlocs))

    def tick_values(self, vmin, vmax):
        raise NotImplementedError('Cannot get tick locations for a '
                                  '%s type.' % type(self))

def quickLoad(path, expID = None):
    """
    
    """
    from .datasetClass import Dataset
    from .runClasses import Proc_PP
    if type(expID)==type(None):
        raise TypeError("expID needs to be a experiment ID, not Nonetype.")

    if type(path)==str:
        path = [path]

    MyRun = []
    for pth in path:
        pth, fn = osp.split(osp.abspath(pth))
        run = Proc_PP(expID=expID)
        run.load(pth,fn,'') if expID!='modPack' else run.load(osp.join(pth,fn),'','')
        MyRun.append(run)
    dataset = Dataset(procPpList=MyRun)

    for opt in ['dS','dSdT','dSdRho']:
        if not hasattr(dataset, 'dep'):
            try:
                dataset.dep = np.arange(getattr(dataset, opt).shape[0])
            except(AttributeError):
                pass
        else:
            break

    return dataset

def initDataset(x,y,z, dep=None, kw={}):
    """
    """
    from .datasetClass import Dataset
    from .runClasses import Proc_PP
    _kw={'x':'q','y':'t','z':'dS',
                 'xUnit':r'\mathring{\mathrm{A}}^{-1}','xQuant':r'q',
                 'yUnit':r'ps','yQuant':r'delay',
                 'zUnit':r'q','zQuant':r'\Delta S',}
    _kw.update(kw)

    data = Dataset(dummy=True)

    x,y,z = data.std_dTp(x,y,z)

    data.gen_dTp(_kw['x'], x, axis=True,  unit=_kw['xUnit'], quant=_kw['xQuant'])
    data.gen_dTp(_kw['y'], y, axis=True,  unit=_kw['yUnit'], quant=_kw['yQuant'])
    data.gen_dTp(_kw['z'], z, axis=False, unit=_kw['zUnit'], quant=_kw['zQuant'],
                 xAxis = _kw['x'], yAxis=_kw['y'])
    data.dep = np.arange(z.shape[0])  if type(dep)==type(None) else dep

    return data

def load_p12(fn):
    ''' 
    This reads the radially integrated scattering data
    from P12
    Inputs:
      fn:     File name (full path)
    Outputs:
      data:   Scattering (q, I, sigma)  (array)
      params: Acquisition parameters    (dictionary)
    '''
    # Create dictionary for parameters
    params = {}
    # Create list for scattering data (later tranlated to array)
    scatt = []
    # Open file and read data
    with open(fn, 'r') as f:
        for s in f:
            if s[0] == " ":
                temp = s.strip().split()
                # Convert to float
                temp = list(map(float,temp))
                scatt.append(temp)
            else:
                temp = s.strip().split(':')
                # Convert parameters to numbers if possible
                try:
                    temp[1] = int(temp[1])
                except:
                    try:
                        temp[1] = float(temp[1])
                    except:
                        pass
                    params[temp[0]] = temp[1]
    # Convert to array
    data = np.array(scatt) #.astype(np.float)
    # Divide x axis by 10 since it's given in nm^-1
    # and we want to have A^-1
    data[:,0] = data[:,0] / 10
    return data, params

def process_run(run, q_norm = [2.1, 2.2],q_drift = [0.03, 0.05], q_outlier = [1, 2], tol='1std', plot=False, use_diode=False, use_T=False):
    '''
    Standard processing
    '''
    ## Normalize
    run.normalize(use_diode=use_diode, use_T=use_T, qrange=q_norm)
    ## Detect outliers
    run.rejectS(plot=plot, qrange=q_outlier,tol=tol)
    ## Average
    run.average_run()
    ## Determine signal to noise
    run.sn_ratio() 
    # Look for drifts
    run.drift(plot=plot, qrange=q_drift)

def split_run_t(run, q_norm = [2.1, 2.2],q_drift = [0.03, 0.05], q_outlier = [1, 2], tol='1std', plot=False, use_diode=False, use_T=False):
    print(os.getcwd())
    # Split up in single time points, this is how data will be acquired in our case
    ts = np.unique(run.t)
    run_list = []
    print(run.dS)
    for t in ts:
        temp = libwaxs.Run_PP(expID='esrf2017')
        inds = (run.t == t)
        temp.q = run.q
        temp.S = run.S[inds]
        temp.dS = run.dS[inds]
        temp.t = run.t[inds]
        temp.T = run.T[inds]
        temp.diode = run.diode[inds]
        if hasattr(temp, 'diode2'):
            temp.diode2 = run.diode2[inds]
        if hasattr(temp, 'T'):
            temp.T = run.T[inds]
        temp._Nrepeats = np.sum(inds)
        temp.runnr = run.runnr
        temp.keep = run.keep[inds]
        temp.out = run.out[inds]
        run_list.append(copy.deepcopy(temp))    
    for single in run_list:
        ## Normalize
        single.normalize(use_diode=use_diode, use_T=use_T, qrange=q_norm)
        ## Detect outliers
        single.rejectS(plot=plot, qrange=q_outlier,tol=tol)
        ## Average
        single.average_run()
        ## Determine signal to noise
        single.sn_ratio() 
        # Look for drifts
        single.drift(plot=plot, qrange=q_drift)
    return run_list
   
def merge_run(run_list):
    '''
    Merge single shot runs into one class instance
    '''
    big_run = libwaxs.Run_PP(expID='esrf2017')
    for run in run_list:
        if  (not hasattr(big_run, 'q')):
            big_run.q = copy.deepcopy(run.q)
            big_run.dS = copy.deepcopy(run.dS)
            big_run.S = copy.deepcopy(run.S)
            big_run.t = copy.deepcopy(run.t)
            big_run.T = copy.deepcopy(run.T)
            big_run.diode = copy.deepcopy(run.diode)
            if hasattr(temp, 'diode2'):
                big_run.diode2 = copy.deepcopy(run.diode2)
            if hasattr(temp, 'T'):
                big_run.T = copy.deepcopy(run.T)
            big_run._Nrepeats = copy.deepcopy(run._Nrepeats)
            big_run.runnr = copy.deepcopy(run.runnr)
            big_run.keep = copy.deepcopy(run.keep)
            big_run.out = copy.deepcopy(run.out)
        elif (big_run.q != run.q).any():
            raise ValueError('q-VALUE MISMATCH BETWEEN REPEATS')
            return None
        else:
            big_run.S = np.vstack((big_run.S, run.S))
            big_run.S = np.vstack((big_run.dS, run.dS))
            big_run.t = np.hstack((big_run.t, run.t))
            big_run.diode = np.hstack((big_run.diode, run.diode))
            if hasattr(run, 'diode2'):
                big_run.diode2 = np.hstack((big_run.diode2, run.diode2))
            if hasattr(run, 'T'):
                big_run.T = np.hstack((big_run.T, run.T))
            big_run._Nrepeats += run._Nrepeats
            big_run.keep = np.hstack((big_run.keep, run.keep))
            big_run.out = np.hstack((big_run.out, run.out))
            #big_run.runnr = np.hstack((big_run.runnr, run.runnr))
        # Set keep to all
        big_run.keep = np.arange(len(big_run.S))
    return big_run 
    
def sub_run(run, inds):
    '''
    This inputs indices and outputs 
    a subset of this run
    '''
    # Initialize
    sub = libwaxs.Run_PP(expID='esrf2017')
    sub.q = run.q
    sub.S = copy.deepcopy(run.S[inds])
    sub.dS = copy.deepcopy(run.dS[inds])
    sub.t = copy.deepcopy(run.t[inds])
    sub.diode = copy.deepcopy(run.diode[inds])
    if hasattr(run, 'diode2'):
        sub.diode2 = copy.deepcopy(run.diode2[inds])
    if hasattr(run, 'T'):
        sub.T = copy.deepcopy(run.T[inds])
    sub._Nrepeats = np.sum(inds)
    sub.runnr = run.runnr
    sub.keep = run.keep[inds]
    sub.out = run.out[inds]
    return sub
    
def on_detect(run, blank):
    '''
    This detects the on shot among the off shots for
    the offset experiment by subtracting a blank from another 
    run
    '''
    # Take differences
    diffs = run.S - blank
    # Take sum as a measure
    np.sum(np.abs(diffs))
