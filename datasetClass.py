# -*- coding: utf-8 -*-

import sys,os,re
import collections
import numpy as np
import matplotlib.pyplot as plt
import matplotlib,pylab
import pickle
import libwaxs
from .helpers import *
from .utils import *
###
import os.path as osp

class Dataset:
    """
    The Dataset class is the basic building block of this library. The class holds
    a scattering dataset, which consists of vectors q and t, and matrices S and/or
    dS, indexed as (q,time). It carries plotting methods and methods for correcting,
    analyzing and decomposing the data.
    """
    def __init__(self, runRrList=None, runPpList=None, runPpTtList=None, procPpList=None, datasetList=None, filename=None, dummy=False,
                 t=None, q=None, dS_h=None, dS=None, dS_v=None, S=None, sigma=None, sigma_h=None, sigma_v=None, r=None, dSr=None,
                 scale=None, average=True, TTbinRange=None, load=None, dep=None, inconsistent_data=False):
        """
        A constructor which can be called in different ways, to create or load a Dataset object from different sources.

        @param filename: Load an existing Dataset object from the specified binary file.
        @type filename: str
        @param runRrList: Create object from a list of rapid-readout runs (Run_RR objects).
        @type runRrList: list
        @param runPpList: Create object from a list of pump-probe runs (Run_PP objects).
        @type runPpList: list
        @param datasetList: Create object from a list of existing dataset-objects, which are concatenated with no averaging.
        @type datasetList: list
        @param q, t, S, dS: Create object from explicitly supplied arrays corresponding to q-values, timepoints, absolute and (optionally) difference intensities.
        @type q: numpy.ndarray
        @type t: numpy.ndarray
        @type S: numpy.ndarray
        @type dS: numpy.ndarray
        @param average: Whether or not to average together identical timepoints for pump-probe data. For rapid readout data, this is always done.
        @type average: bool
        """
        # Dataset object constructed explicitly from q- and t-vectors and dS and/or S matrices

        if (q!=None and (S!=None or dS!=None)) or (r!=None and dSr!=None):
            self._unit  = {}
            self._quant = {}
            self._fitLogName = 'fitlog.txt'

            if t   != None:
                self.t = t.squeeze().reshape(-1,1)
                self._unit['t']     = r'fs'
                self._quant['t']    = r'delay'
            else:
                self.t = np.array([0.0]).reshape(-1,1)
                self._unit['t']     = r's'
                self._quant['t']    = r'delay'

            if q   != None:
                self.q  = q.squeeze()
                self._unit['q']     = r'\mathring{\mathrm{A}}^{-1}'
                self._quant['q']    = r'q'

            if r   != None:
                self.r  = r.squeeze()
                self._unit['r']     = r'\mathring{\mathrm{A}}'
                self._quant['r']    = r'r'

            if S   != None:
                self.S  = S
                self._unit['S']    = r'q'
                self._quant['S']   = r'\mathrm{S}'
                if self.S.shape == (self.t.shape[0], self.q.shape[0]):
                    self.S = np.transpose(self.S)

            if dS  != None:
                self.dS = dS
                self._unit['dS']    = r'q'
                self._quant['dS']   = r'\Delta\mathrm{S}'
                if self.dS.shape == (self.t.shape[0], self.q.shape[0]):
                    self.dS = np.transpose(self.dS)

            if dS_h  != None:
                self.dS_h = dS_h
                self._unit['dS_h']    = r'q'
                self._quant['dS_h']   = r'\Delta\mathrm{S}'
                if self.dS_h.shape == (self.t.shape[0], self.q.shape[0]):
                    self.dS_h = np.transpose(self.dS_h)

            if dS_h  != None:
                self.dS_v = dS_v
                self._unit['dS_v']    = r'q'
                self._quant['dS_v']   = r'\Delta\mathrm{S}'
                if self.dS_v.shape == (self.t.shape[0], self.q.shape[0]):
                    self.dS_v = np.transpose(self.dS_v)

            if dSr != None:
                self.dSr = dSr
                self._unit['dSr']   = r'r'
                self._quant['dSr']  = r'\Delta\mathrm{S}'
                if self.dSr.shape == (self.t.shape[0], self.r.shape[0]):
                    self.dSr = np.transpose(self.dSr)

        # Dataset object constructed from a list of Run_RR objects, which are averaged together.
        elif (runRrList != None):
            # S and dS are now 2D matrices (q, time)
            self.q  = runRrList[0].q
            self.t  = runRrList[0].t
            self.S  = np.zeros(runRrList[0].S.shape[1:])
            if hasattr(runRrList[0], 'dS'):
                self.dS = np.zeros(runRrList[0].dS.shape[1:])
            else:
                self.dS = self.S

            Nrepeats = 0
            i = 0
            for run in runRrList:
                i += 1
                try:
                    if (run.q != self.q).any():
                        print('     q-range mismatch, discarding this run (#%d in the list)'%i)
                        continue
                except:
                    print('     q-range mismatch, discarding this run (#%d in the list)'%i)
                    continue
                if hasattr(run, 'dS'):
                    self.dS += np.mean(run.dS, axis=0) * run.Nrepeats
                self.S  += np.mean(run.S,  axis=0) * run.Nrepeats
                Nrepeats  += run.Nrepeats
            self.S      /= Nrepeats
            self.dS     /= Nrepeats
            self.dSav    = np.mean(self.dS, axis=1)
            self.Nrepeats = np.ones(len(self.t), dtype=int) * Nrepeats

        # Dataset object constructed from a list of Run_PP objects, which are averaged together or not.
        elif (runPpList != None):

            self.q  = runPpList[0].q

            if (not average):
                # put all of the run's exposures into it's own column in S and dS

                # Finding the time vector now requires counting the number of total exposures
                lst = []
                for run in runPpList:
                    lst += list(run.t)
                self.t = np.array(lst)
                Ntimes = self.t.shape[0]

                # S and dS are now 2D matrices (q, exposure/time)
                self.S  = np.zeros((runPpList[0].S.shape[1], Ntimes))
                self.dS = np.copy(self.S)

                # exposure accounting:
                self.Nrepeats = np.zeros(len(self.t), dtype=int)

                # Fill in S and dS:
                I = 0
                for run in runPpList:
                    for i in range(run._Nrepeats):
                        self.S[:, i+I] = run.S[i, :]
                        self.dS[:, i+I] = run.dS[i, :]
                        self.Nrepeats[i+I] += 1
                    I += i + 1

            elif (average):
                # weigh the run's exposures at each delay into the appropriate column of S and dS

                # Finding the time vector now requires looking for the unique delays
                lst = []
                for run in runPpList:
                    lst += list(run.t)
                self.t = np.array(uniqueList(lst))
                Ntimes = self.t.shape[0]

                # S and dS are now 2D matrices (q, time)
                self.S  = np.zeros((runPpList[0].S.shape[1], Ntimes))
                self.dS = np.copy(self.S)

                # exposure accounting:
                self.Nrepeats = np.zeros(len(self.t), dtype=int)

                # for each run in the list...
                for run in runPpList:
                    # Fill in S and dS: loop over the data and add it to the correct column ("timeindex")
                    for i in range(run._Nrepeats):
                        # first check for NaN (which can happen when an image doesn't have blanks:
                        if (np.isnan(np.sum(run.dS[i]))): continue
                        timeindex = np.where(run.t[i] == self.t)[0][0]
                        self. S[:, timeindex] += run. S[i]
                        self.dS[:, timeindex] += run.dS[i]
                        self.Nrepeats[timeindex] += 1

                for i in range(len(self.t)):
                    self. S[:, i] /= self.Nrepeats[i]
                    self.dS[:, i] /= self.Nrepeats[i]
        
        # dataset constructed from a list of Run_PP objects.
        elif (runPpTtList != None):
            """
            Sort runs with Time Tool data into a common time base.
            The average option calls the TTaverage() function.
            """

            tList       = []
            tagList     = []
            rnrList     = []
            counterList = []
            rnnrList    = []
            tNomList    = []
            q           = None
            tot_rep = 0
            for i,run in enumerate(runPpTtList):
                tNomList.append(run._tNom)
                rnnrList.append(run.runnr)
                if type(q)==type(None):
                    q = run.q
                else:
                    if not np.all(q==run.q):
                        raise ValueError("q-scale mismatch between runs!")
            
            self._unit  = run._unit
            self._quant = run._quant
            
            self._runs  = np.asarray(runPpTtList)
            self.q = q
            
            #nominal meta data
            NTmd    = np.empty((2,len(tNomList)), dtype=int)
            NTmd[0] = np.arange(len(runPpTtList), dtype=int)
            NTmd[1] = np.asarray(rnnrList,dtype=int)
            NTtd    = np.asarray(tNomList).astype(np.float64)
            self._Nt    = NTtd
            self._NTmd  = NTmd
        
        # dataset constructed from a list of Proc_PP objects, which are averaged together or not.
        elif (procPpList != None) and len(procPpList)!=0: #if average also sorts the dependencies, if given dep can be a list of labels or values.
            
            dTpL  = []
            inCon = []
            for i,item in enumerate(procPpList): #Runs through the provided list of runClass.procPp instances.
                for quant,data in list(item.__dict__.items()): #makes a list from a dictionary of the individual runs.
                    #count = []
                    if type(data)==np.ndarray: #selects for numpy arrays, i.e. data
                        if not hasattr(self, quant): #if the attribute does not exist yet, i.e. the 1st occurrence of the attribute
                            if np.ndim(data)<2: # if the attribute is one of the axes, e.g. x values, y values. Will not work if multidimensional x and/or y axis is given, on the TDOD list!
                                shape = (len(procPpList),) + data.shape # the shape of the data array is determined by the number of items in the procPpList.
                                setattr(self, quant, np.zeros(shape))
                                getattr(self, quant)[i] = np.asarray(data) # sets the attibute
                            elif np.ndim(data)==2: # if the dictionary entry is 2 dimensional, then it is considered data.
                                shape = (len(procPpList),) + data.shape # the shape of the data array is determined by the number of items in the procPpList.
                                setattr(self, quant, np.zeros(shape))
                                getattr(self, quant)[i] = data
                                dTpL.append(quant) # useful for later in the averaging
                            else: # if the dictionary entry is 2 dimensional, then it is considered data.
                                shape = data.shape # the shape of the data array is determined by the number of items in the procPpList.
                                setattr(self, quant, np.zeros(shape))
                                getattr(self, quant)[i] = data[i]
                                dTpL.append(quant) # useful for later in the averaging
                                #count.append(i) # useful for later with the averaging
                        else:
                            #if np.ndim(data)<2:
                                #if (getattr(self, quant)!=data).all() and not inconsistent_data: # a bit superfluous because the next conditional also checks for this in a different manner
                                    #raise ValueError("You are trying to add datasets with differing axes!")
                                #else:
                                    #continue
                            if getattr(self, quant)[i].shape!=data.shape: # if subsequent datasets do not have the same shape as the 1st one, they are considered inconsistent.
                                if inconsistent_data:
                                    inCon.append(i) # useful for later when chucking out inconsistent data
                                    continue
                                else:
                                    raise ValueError("Shape mismatch between dependencies (existing: %s, next: %s)!"%(getattr(self, quant)[i].shape,data.shape))
                            else:
                                getattr(self, quant)[i] = data # adds the next dataset
                                #count.append(i) # useful for later with the averaging
                    else: # simply adds the remaining attributes such as Nrepeats and _unit, _quant, etc.
                        if not hasattr(self, quant):
                            setattr(self, quant, data)
                        elif type(getattr(self, quant))==dict:
                            setattr(self, quant, data)
                        else:
                            continue
            inCon = list(set(inCon)) # get rid of the double counting due to sigma, and S etc.
            if len(inCon)!=0:
                if not inconsistent_data:
                    raise ValueError("The number of items in dep does not match the number of items in procPpList.")
                else:
                    inConRemove  = []
                    procPpRemove = []
                    inCon.reverse() #need to reverse the inconsistencies list to remove inconsistent data using list operations.
                    for idx in inCon:
                        inConRemove.append(dep[idx])
                        procPpRemove.append(procPpList.pop(idx)) # pops the item in place, which is why you need to reverse the index list beforehand
                    print("Removed the following inconsistent data from procPpList: instance: %s, no: %s, label: %s"%(procPpRemove,inCon,inConRemove))
                    setattr(self, '_inconsistent_data', procPpRemove)
            
            if type(dep)==type(None): # if no dep list is provided, a generic dep list is generated
                dep = list(range(len(procPpList)))
            dep = np.asarray(dep)
            #bork
            if average: # currently only averages full datasets
                for dTp in dTpL:
                    data = getattr(self, dTp)
                    if type(data)==np.ndarray:
                        if np.ndim(data)>=2:
                            avg  = np.empty(((len(set(dep)),)+data.shape[1:]))
                            depL = np.empty(len(set(dep)))
                            for i,reps in enumerate(sorted(list_indices(dep))):
                                el,idx = reps
                                avg[i]  = np.mean(data[idx], axis=0)
                                depL[i] = np.mean(dep[idx])
                            
                            setattr(self, dTp, avg)
                            setattr(self, 'dep', depL)
                        else:
                            continue
                    else:
                        continue

        elif dummy:
            self._unit  = {}
            self._quant = {}
            self._fitLogName = 'fitlog.txt'

        # Dataset object constructed from a list of dataset-objects, which will be concatenated.
        # Have to have same q vector as no resampling is done (which should be implemented of course)
        elif (datasetList != None):

            self.q        = datasetList[0].q
            self.S        = datasetList[0].S
            self.t        = datasetList[0].t
            self.Nrepeats = datasetList[0].Nrepeats
            if hasattr(datasetList[0], 'dS'):
                self.dS = datasetList[0].dS
            else:
                self.dS = datasetList[0].S * np.nan

            for i in range(1, len(datasetList)):
                self.S          = np.hstack((self.S,  datasetList[i].S))
                self.t          = np.hstack((self.t,datasetList[i].t))
                self.Nrepeats   = np.hstack((self.Nrepeats, datasetList[i].Nrepeats))
                if hasattr(datasetList[i], 'dS'):
                    self.dS = np.hstack((self.dS, datasetList[i].dS))
                else:
                    self.dS = datasetList[i].S * np.nan

            self.dSav    = np.mean(self.dS, axis=1)

        elif (filename != None):
            self.load(filename)

        else:
            return None
            #raise ValueError
        

        self.fixNan()
        self.buildAsoc() if not hasattr(self, '_asoc') else None

        if hasattr(self, 'dep'):
            self.dep = np.asarray(self.dep)

        return


    def fixNan(self):
        """
        """

        attr = list(self.__dict__.keys())

        for key in attr:
            try:
                data       = getattr(self, key)
                mask       = np.isnan(data)
                data[mask] = 0.
            except TypeError:
                continue

        return


    #def removeZeroData(self):

        #dTpS = list(self._asoc.keys())
        #xL,yL,zL,xQuant,yQuant,zQuant,xUnit,yUnit,zUnit,dTp_list=self.loaddTpList(dTpS, _nolab=True)

        #for i in len(zL):
            #x,y,z = self.std_dTp(xL[i],yL[i],zL[i])

        #return


    def orderAxes(self):

        dTpS = list(self._asoc.keys())
        xL,yL,zL,xQuant,yQuant,zQuant,xUnit,yUnit,zUnit,dTp_list=self.loaddTpList(dTpS, _nolab=True)
        
        for i in range(len(zL)):
            x,y,z = self.std_dTp(xL[i],yL[i],zL[i])
            for j in range(self.dep.size):
                argx = np.argsort(x[j], axis=0)
                argy = np.argsort(y[j], axis=1)

                for k in range(x[j].shape[-1]):
                    x[j,:,k] = x[j,argx[:,k],k]
                    z[j,:,k] = z[j,argx[:,k],k]
                for k in range(y[j].shape[-2]):
                    y[j,k,:] = y[j,k,argy[k,:]]
                    z[j,k,:] = z[j,k,argy[k,:]]
            x,y,z = self.std_dTp(x,y,z)
        return


    def applyDt(self, scan=None, dt=None):
        """
        """

        scanList = self.getScan(scan=scan)

        if type(dt)==(float or int):
            dt = np.ones(len(scanList), dtype=np.float)*dt
        elif type(dt)==tuple or type(dt)==list:
            dt = np.asarray(dt)
        elif type(dt)==np.ndarray:
            dt = dt
        else:
            raise TypeError("dt cannot be a {}.".format(type(dt).__name__))

        if dt.shape[0]!=len(scanList):
            raise ValueError("If dt is an array-like, then its length ({}) must match that of the number of specified scans ({})!".format(len(dt), len(scanList)))

        for i,scn in enumerate(scanList):

            runIdx  = scn[0]
            runList = self._runs[runIdx]

            for run in runList:
                print("Run {!s}: applyDt() applying a {!s} offset.".format(run.runnr, dt[i]))
                run.t         -= dt[i]
                run._dt       -= dt[i]
                run._tNom     -= dt[i]
        #self._scan_Nt -= dt[i] don't really know how to deal with this just now.
        return

    def getRunIdx(self,nr, scan=None):

        scanList = self.getScan(scan=scan)
        runIdxL = []
        for scn in scanList:
            sortIdx = np.argsort(scn[1])
            runIdxL.append(scn[0][sortIdx][self._idxpt(scn[1][sortIdx],nr)])

        return runIdxL

    def maskRun(self, nr=None, ylims=None, scan=None, valid=False):

        if type(nr)==type(None) and type(ylims)==type(None) and type(scan)==type(None):
            raise ValueError("You need to specify a value for either nr or ylims.")

        if type(ylims)!=type(None):
            if not hasattr(ylims, '__iter__') or type(ylims)==str:
                ylims = np.asarray([ylims], dtype=float)
            else:
                ylims = np.asarray(ylims)
        elif type(nr)==type(None) and type(ylims)==type(None):
            ylims = np.asarray([self._scan_Nt.min(),self._scan_Nt.max()])

        if type(nr)!=type(None):
            if not hasattr(nr, '__iter__') or type(nr)==str:
                nr = np.asarray([nr], dtype=int)
            else:
                nr = np.asarray(nr, dtype=int)

        scan = self.getScan(scan=scan)
        #bork
        if type(valid)!=type(None) and type(valid)==bool:
            for scn in scan:
                #sortIdx = np.argsort(scn[1])
                if type(nr)!=type(None):
                    idx = []
                    for rn in nr:
                        idx.append(np.where(scn[1]==rn)[0])
                    idx = np.asarray(idx, dtype=int).ravel()
                    scn[3][idx]   = valid
                elif type(ylims)!=type(None):
                    ymask         = (self._scan_Nt[scn[2]]>=ylims.min())*(self._scan_Nt[scn[2]]<=ylims.max())
                    scn[3][ymask] = valid
        else:
            raise TypeError("valid needs to be either True or False, not {}.".format(type(valid).__name__))
        return

    def _idxpt(self, A, x):
        """
        Returns indices via numpy.searchsorted. Elements not in the array are not returned, differs from normal np.searchsorted behaviour.
        """
        IdxL = np.searchsorted(A, x, side='left')
        IdxR = np.searchsorted(A, x, side='right')
        return (IdxL[~(((IdxL==0)|(IdxL==len(A)))&((IdxR==0)|(IdxR==len(A))))])

    def _binTTkeepScans(self, sig=None, scan=None, tlims=None, TTlims=None, resolution=None, dec=None, ignoreTTvalid=False):
        """
        Helper function
        """

        scanList = self.getScan(scan=scan)

        t_new = self.gen_t_from_TT(tlims=tlims, TTlims=TTlims, resolution=resolution, dec=dec)

        scnDs       = []
        scnDsErr    = []
        scnT        = []
        binShot     = []
        dScollected = []
        t           = []
        for i,scn in enumerate(scanList):

            if not scn[3].astype(bool).any():
                scnDs.append(None)
                scnDsErr.append(None)
                continue
            else:

                runIdx  = scn[0][scn[3].astype(bool)]
                runList = self._runs[runIdx]

                tList   = []
                dSList  = []

                for run in runList:
                    sig = self._getDsig(run=run, sig=sig)

                    _t        = getattr(run, '_dt')
                    isInTTbin = (_t>min(TTlims))*(_t<max(TTlims))
                    TTvalid   = getattr(run, '_dTTtagMask')
                    if not ignoreTTvalid:
                        isValid   = (TTvalid | ~isInTTbin)
                    else:
                        isValid   = np.ones_like(_t, dtype=bool)
                    
                    tList.append(_t[isValid])
                    dsigList = []
                    for sg in sig:
                        dSig      = getattr(run, '{!s}'.format(sg))
                        if isValid.shape[0]!=dSig.shape[1]:
                            raise ValueError
                        else:
                            dsigList.append(dSig[:,isValid])
                    
                    dSList.append(np.transpose(np.asarray(dsigList), (2,1,0)))
                
                dScollected = np.transpose(np.vstack(dSList), (1,0,2))
                t = np.hstack(tList)
                binShot = self._genBinLims(t, t_new)
                dsig_binned  = np.ones((self.q.shape[0],len(t_new), len(sig)))*np.nan
                dsig_error   = np.ones_like(dsig_binned)*np.nan
                ptC = 0
                for j in range(len(t_new)):
                    binmask = (binShot==j)
                    Npoints = np.nonzero(binmask)[0].size
                    #print("Scan {}: _binTTkeepScans() sorting {} points into {} bins: DONE\r".format(i+1, Npoints, len(t_new))),
                    dsig_binned[:,j,:] = np.nanmean(dScollected[:,binmask,:], axis=1)
                    dsig_error[:,j,:]  = np.nanstd(dScollected[:,binmask,:], axis=1)/np.sqrt(Npoints)
                    ptC += Npoints
                    print("Scan {}: _binTTkeepScans() sorted {} points into {} bins: DONE\r".format(i+1, ptC, len(t_new))),
                print("")
                sys.stdout.flush()
                #if i == 3:
                    #bork
                scnDs.append(dsig_binned)
                scnDsErr.append(dsig_binned)
        
        shape = None
        fail  = []
        
        for i,el in enumerate(scnDs):
            if type(el)==np.ndarray:
                if type(shape)!=type(None):
                    if shape == el.shape:
                        continue
                    else:
                        raise ValueError("This is quite impossible.")
                else:
                    shape = el.shape
            else:
                fail.append(i)
        
        if len(fail)!=0:
            fix = np.ones(shape)*np.nan
            for idx in fail:
                scnDs[idx]    = fix
                scnDsErr[idx] = fix

        binned    = np.asarray(scnDs)
        binnedErr = np.asarray(scnDsErr)

        return (t_new,binned)


    def _binTT(self, sig=None, scan=None, tlims=None, TTlims=None, resolution=None, dec=None, ignoreTTvalid=False):
        """
        Helper function
        """

        scanList = self.getScan(scan=scan)

        t_new = self.gen_t_from_TT(tlims=tlims, TTlims=TTlims, resolution=resolution, dec=dec)

        scnT        = []
        binShot     = []
        dScollected = []
        t           = []
        for i,scn in enumerate(scanList):

            if not scn[3].astype(bool).any():
                #dScollected.append(None)
                continue
            else:
                
                runIdx  = scn[0][scn[3].astype(bool)]
                runList = self._runs[runIdx]
                
                tList   = []
                dSList  = []
                
                for run in runList:
                    sig = self._getDsig(run=run, sig=sig)
                    
                    _t        = getattr(run, '_dt')
                    isInTTbin = (_t>min(TTlims))*(_t<max(TTlims))
                    TTvalid   = getattr(run, '_dTTtagMask')
                    if not ignoreTTvalid:
                        isValid   = (TTvalid | ~isInTTbin)
                    else:
                        isValid   = np.ones_like(_t, dtype=bool)
                    
                    tList.append(_t[isValid])
                    dsigList = []
                    for sg in sig:
                        dSig      = getattr(run, '{!s}'.format(sg))
                        if isValid.shape!=dSig[0].shape:
                            raise ValueError
                        else:
                            dsigList.append(dSig[:,isValid])
                    
                    dSList.append(np.transpose(np.asarray(dsigList), (2,1,0)))
                
                dScollected.append(np.transpose(np.vstack(dSList), (1,0,2)))
                t.append(np.hstack(tList))
        
        t = np.hstack(t)
        binShot = self._genBinLims(t, t_new)
        dScollected = np.hstack(dScollected)
        
        dsig_binned  = np.ones((self.q.shape[0],len(t_new), len(sig)))*np.nan
        dsig_error   = np.ones_like(dsig_binned)*np.nan
        for j in range(len(t_new)):
            binmask = (binShot==j)
            Npoints = np.nonzero(binmask)[0].size
            print("binTT() sorting {} points into bin {}: BUSY\r".format(Npoints, t_new[j])),
            dsig_binned[:,j,:] = np.nanmean(dScollected[:,binmask,:], axis=1)
            dsig_error[:,j]  = np.nanstd(dScollected[:,binmask,:], axis=1)/np.sqrt(Npoints)
        print("binTT() sorted {} points into {} bins: DONE\r".format(dScollected.shape[1],len(t_new)))
        print("")
        sys.stdout.flush()
        binned    = dsig_binned
        binnedErr = dsig_error
        
        return (t_new,binned)

    def _getDsig(self, run=None, sig=None):
        """
        Helper function to standardize the retreival of the signals from runs.
        Also maintains the order in which they are returned. The order of sig
        is the one when self._dSig is first created. This also checks whether
        run has all elements of sig. The function adds the extra elements to 
        self._dSig if these elements are not present in self._dSig.
        """
        
        if type(run)==type(None) and not hasattr(self, '_dSig'):
            raise ValueError("run needs to be specified.")
        elif type(run)!=type(None) and not isinstance(run, libwaxs.Run_PP):
            raise TypeError("run is not a {1} instance from the {0} module.".format(libwaxs.Run_PP.__module__, libwaxs.Run_PP.__name__))
        
        if type(sig)!=type(None):
            sig = np.asarray(sig, dtype=np.str).ravel()
        else:
            if hasattr(self, '_dSig'):
                return self._dSig
            else:
                sig = np.asarray(run._hdr[run.runnr]['_DsS'], dtype=np.str).ravel()
        
        if hasattr(self, '_dSig'):
            if type(getattr(self, '_dSig'))!=type(None):
                xtr = []
                idx = []
                for sg in sig:
                    if (self._dSig==sg).any():
                        idx.append(np.where(self._dSig==sg)[0][0])
                    else:
                        xtr.append(sg)
                idx.sort()
                sig = self._dSig[idx].tolist()
                if len(xtr)!=0:
                    for xt in xtr:
                        if hasattr(run, xt):
                            continue
                        else:
                            raise AttributeError("{0} is not an attribute of run.".format(xt))
                    xtr.sort()
                    sig.extend(xtr)
                    sig = np.asarray(sig, dtype=np.str)
                    self._dSig = sig
            else:
                self._dSig = sig
        else:
            self._dSig = sig
        
        return sig
    
    def getScan(self, scan=None):
        
        if not hasattr(self, '_runDict'):
            raise AttributeError("No scans defined. Define scans via the define_scan() method.")
        
        if type(scan)==type(None):
            scan = list(self._runDict.keys())
        elif type(scan)==float or type(scan)==int:
            scan = [scan]
        elif type(scan)==np.ndarray:
            scan = scan.astype(np.int).tolist()
        elif type(scan)==list or type(scan)==tuple:
            scan = list(scan)
        else:
            raise TypeError("scan has to be a int, float, numpy.ndarray, tuple, or list. Not {!s}".format(type(scan)))
        
        scanList = []
        
        for scn in scan:
            scanList.append(self._runDict[scn])
            
        return scanList
    
    def gen_t_from_TT(self, tlims=None, TTlims=None, resolution=None, dec=None):
        """
        Generates a time vector based on the nominal times. A Time Tool range is specified with a 
        given resolution.
        """
        
        if type(tlims)!=type(None):
            tlims = np.asarray(tlims)
        else:
            tlims = np.asarray(self._scan_Nt.min(),self._scan_Nt.max())
        
        if type(TTlims)!=type(None):
            TTlims = np.asarray(TTlims)
        else:
            TTlims = np.asarray([-2,5])
        
        if type(resolution)!=type(None):
            resolution = np.float(resolution)
        else:
            resolution = np.float(0.100)
        
        if type(dec)!=type(None):
            dec = np.int(dec)
        else:
            dec = np.int(2)
        
        sortedNt = np.sort(self._scan_Nt)
        
        scaleTT = np.linspace(*TTlims, num = ((TTlims.max())-(TTlims.min()))/resolution, endpoint=True)
        t       = np.round(np.unique(np.hstack((sortedNt[(sortedNt<TTlims.min())], scaleTT, sortedNt[(sortedNt>TTlims.max())]))), decimals=dec)
        
        return t
    
    def _genBinLims(self, full_x, new_x):
        
        bin_lims = np.zeros_like(new_x)
        
        bin_lims[:-1] = new_x[:-1] + np.diff(new_x)/2.
        bin_lims[-1]  = full_x.max()
        
        return np.digitize(full_x, bin_lims, right=True)
    
    def define_scan(self, runs=None):
        """
        Function used to sort runs into scans (one pass over a certain motor etc).
        
        Kwargs:
            runs (list of 1darray-like) : List of 1darray-like of the run numbers which belong to 
                                          1 scan. The scan numbers should correspond to those
                                          in the provided runlist! 
                                          If scan is None, then it is inferred from the number of 
                                          counts of each unique Nominal time point (approved, 
                                          preffered and easiest method).
        """
        
        if type(runs)!=type(None):
            NrIdx = [] # list containing np.arrays of the runs that define a "scan".
            tL = []
            for run in runs:
                if len(run)==2:
                    run = np.arange(*run, dtype=int)
                    idx = libwaxs.get_lim(self._NTmd[1], run)
                    NrIdx.append(np.asarray((idx, run), dtype=int))
                elif len(run)>2:
                    run = np.asarray(run)
                    idx = libwaxs.get_lim(self._NTmd[1], run)
                    NrIdx.append(np.asarray((idx, run), dtype=int))
                else:
                    raise ValueError("{!s} is an invalid input for scan. Valid options are: None, array-like min & max value a range, array-like containing the individual run numbers to be used.".format(scan))
                
                tL.append(self._Nt[idx])
            nominal_t = np.hstack(tL)
            scan_Nt,_Nscan = np.unique(nominal_t,return_counts=True)
        else:
            NrIdx = self._NTmd
            nominal_t = self._Nt
            scan_Nt,_Nscan = np.unique(nominal_t,return_counts=True)
        
        # determine what runNumbers belong to 1 scan and stick them in a dict
        SortNTidx = np.argsort(NrIdx[1], axis=-1)
        SnO = np.zeros_like(scan_Nt, dtype=int)
        runDict = {}
        for key in range(_Nscan.max()):
            runDict[int(key+1)] = []
        
        for i in SortNTidx:
            idx       = np.int(*np.where(scan_Nt==nominal_t[i]))
            SnO[idx] += 1
            runDict[int(SnO[idx])] += [[NrIdx[0,SortNTidx][i] , NrIdx[1,SortNTidx][i], idx, 1]]
        for scn in list(runDict.keys()):
            runDict[scn] = np.asarray(runDict[scn], dtype=int).T
        print("Found {0!s} scan(s).".format(len(runDict)))
        
        self._runDict = runDict
        self._scan_Nt = scan_Nt

    def binTT(self, sig=None, scan=None, tlims=None, TTlims=None, resolution=None, dec=None, ignoreTTvalid=False, averageOnly=True, out=False, ovrWrt=True):
        """
        Wrapper function which employs _binTT() or _binTTkeepScans() to bin and average timepoints into a single dataset 
        or in scans, respectively, with the averageOnly kwarg.
        """
        
        if not averageOnly:
            t, dS_binned = self._binTTkeepScans(sig=sig, scan=scan, tlims=tlims, TTlims=TTlims, resolution=resolution, dec=dec, ignoreTTvalid=ignoreTTvalid)
        else:
            t, dS_binned = self._binTT(sig=sig, scan=scan, tlims=tlims, TTlims=TTlims, resolution=resolution, dec=dec, ignoreTTvalid=ignoreTTvalid)
        
        sig = self._getDsig(sig=sig)
        
        if not out:
            self.gen_dTp('t', t, like_dTp='t', axis=True, ovrWrt=ovrWrt)
            for i,sg in enumerate(sig):
                self.gen_dTp('{0}'.format(sg), dS_binned[:,:,i], like_dTp='{0}'.format(sg),
                             xAxis=self.getUnit('{0}'.format(sg)),
                             yAxis='t',
                             ovrWrt=ovrWrt)
        else:
            return (t, dS_binned)
    
    def averageScans(self):
        """
        Function which averages the scans generated with _binTTkeepScans().
        """
        
        return
    
    def diagnoseTT(self, scan=None, tlims=None, TTlims=None, resolution=None, dec=None, ignoreTTvalid=False, 
                   averageOnly=False, ovrWrt=False, trange=None, qrange=None, tlim=None, apply_dT = None, plot=True):
        """
        This method is meant to sort the Time Tool tagged shots and correct the t0 slip over scans.
        The main assumption is that run numbers are measured consecutively.
        If not, a more complex implementation would involve sorting on a timestamp.
        Kwargs:
            resolution          (float) : the bin width in which the Time Tool tagged points will
                                          be placed.
            tbin_range   (1darray-like) : 1darray-like of time points around the suspected value 
                                          of t0 which will be used to determine the "actual" 
                                          value of t0.
            apply_dT    (bool or float) : If float, bypases the automatic t0 determination and applies the 
                                          supplied value as the t0 correction.
            scan (list of 1darray-like) : List of 1darray-like of the run numbers which belong to 
                                          1 scan. The scan numbers should correspond to those
                                          in the provided runlist! 
                                          If scan is None, then it is inferred from the number of 
                                          counts of each unique Nominal time point (approved, 
                                          preffered and easiest method).
            ovrWrt   (bool)             : 
        """
        
        # create "scans".
        if not hasattr(self, '_runDict'):
            raise AttributeError("No scans defined. Define scans via the define_scan() method.")
        
        if type(trange)!=type(None):
            trange=trange
        else:
            trange = [-2.000, .100, .500, 10.000, 1e3]
        
        tList = []
        dS_binnedList = []
        
            #NT_run_mask   = (self._scan_Nt[self._runDict[scn][2]]<=max(tbin_range))*(self._scan_Nt[self._runDict[scn][2]]>=min(tbin_range))
        if not averageOnly:
            t, dS_binned = self._binTTkeepScans(sig=[('dS',)] ,scan=scan,tlims=tlims, TTlims=TTlims, resolution=resolution, dec=dec, ignoreTTvalid=ignoreTTvalid)
            
        else:
            t, dS_binned = self._binTT(sig=[('dS',)] ,scan=scan,tlims=tlims, TTlims=TTlims, resolution=resolution, dec=dec, ignoreTTvalid=ignoreTTvalid)

        if type(qrange)!=type(None):
            qIdx = get_lim(self.q, qrange)
        else:
            qrange = [0.5,5.5]
            qIdx = [0,len(self.q)-1]
        
        if type(tlim)==type(None):
            tlim = np.asarray([t.min(), t.max()])
        
        if dS_binned.ndim<=3:
            dS_binned = dS_binned[np.newaxis]
        
        qSlice = slice(*qIdx)
        #mask = ~np.isnan(dS_binned[:,qSlice])
        dS_summed = np.sum(np.absolute(dS_binned[:,qSlice]),axis=1)
        
        if plot:
            cycle=['DarkRed','orange','green','blue','violet','purple','black']
            fig,ax = plt.subplots(ncols=2,nrows=1,figsize=(10,15))
            tIdx = get_lim(t, trange)
            tSlice = slice(*get_lim(t, [tIdx.min(),tIdx.max()]))
            
            mx = dS_binned[:,qSlice,tSlice][(~np.isnan(dS_binned[:,qSlice,tSlice]))].max()
            mn = dS_binned[:,qSlice,tSlice][(~np.isnan(dS_binned[:,qSlice,tSlice]))].min()
            inc = (mx - mn)/2.
            
            for i in range(dS_summed.shape[0]):
                ax[0].set_ylim(auto=True)
                ax[0].plot(t, dS_summed[i], '-x', color=cycle[i], label='scan {!s}'.format(i+1))
            ax[0].plot(t, np.nanmean(dS_summed,axis=0), '-x', color='red', linewidth=2, label='average')
            ax[0].set_xlim(*tlim)
            ax[0].legend(frameon=False, loc=0)
            
            #bork
            for j,tpt in enumerate(tIdx):
                for i in range(dS_binned.shape[0]):
                    if i==0 and (~np.isnan(dS_binned[i])).any():
                        ax[1].text(max(qrange),j*inc,'{!s}'.format(t[tpt]))
                        ax[1].plot(self.q[qSlice], dS_binned[i,qSlice,tpt]+j*inc, '-', color=cycle[i])
                    elif (~np.isnan(dS_binned[i])).any():
                        ax[1].plot(self.q[qSlice], dS_binned[i,qSlice,tpt]+j*inc, '-', color=cycle[i])
                #bork
                ax[1].axhline(y=inc*j, linestyle=':', color='black')
                ax[1].plot(self.q[qSlice], np.nanmean(dS_binned[:,qSlice,tpt], axis=0)+j*inc, '-', linewidth=2, color='red')
            #ax[1].legend(frameon=False, loc=0)
            ax[1].set_xlim(*qrange)
            ax[1].set_ylim(mn, inc*len(tIdx))
            
            #plt.tight_layout()
            plt.show()
        return (t, dS_binned)

    def calcDsig(self, ovrWrt=False, *args, **kwargs):
        """
        Wrapper for the libwaxs.Run_PP.calcDsig(). Requires libwaxs.Run_PP instances!
        ovrWrt (bool) : overwrites the variables even if difference signal already exists.
        """
                
        for scn in list(self._runDict.keys()):
            
            for i,run in enumerate(self._runs[self._runDict[scn][0]]):
                
                if run.runnr!=self._runDict[scn][1,i]:
                    raise ValueError("Run number fail!")
                
                if not isinstance(run, libwaxs.Run_PP):
                    raise NameError("{0} is not an instance of {1}".format(type(run).__name__, libwaxs.Run_PP.__module__))
                
                if ovrWrt or np.asarray(['{}'.format(*el) if not hasattr(run, *el) else False for el in run._hdr[run.runnr]['_DsS']]).any():
                    run.calcDsig(*args,**kwargs)
                else:
                    print("Difference signal already exists for run {0}".format(run.runnr))
                    continue
        return
    
    # Output method to dump data to a textfile,
    def dTpTXToutput(self, dTp, filename,ovrWrt=False):
        """
        Dumps difference data to a textfile.
        Allows you to select the datatype being dumped.
        @param dTp: datatype.
        @type dTp: str
        @param filename: output file.
        @type filename: str
        @return: The dumped data.
        @rtype: numpy.ndarray
        """
        import os
        x,y,z,xQuant,yQuant,zQuant,xUnit,yUnit,zUnit,dTp_list=self.loaddTpList(dTp, _nolab=False)
        for i in range(len(z)):
            if type(y[i])==type(None) or y[i].shape[0]==1:
                output_array = np.hstack((x[i].reshape(-1,1), z[i].reshape(x[i].reshape(-1,1).shape[0], -1)))
            else:
                output_array = np.vstack((np.reshape(np.hstack((0,y[i])),(-1,len(y[i])+1)),np.hstack((np.reshape(x[i],(len(x[i]),-1)),z[i]))))

            preout,ext = os.path.splitext(filename)
            if ext=='':
                ext = '.txt'
            outfile = os.path.join(preout,dTp_list[i]+ext)
            
            if os.path.exists(outfile) and not ovrWrt:
                raise FileExistsError('%s exists, not overwriting!'%(outfile))
            elif not os.path.exists(os.path.dirname(outfile)):
                os.makedirs(os.path.abspath(os.path.dirname(outfile)))
            
            np.savetxt(outfile, output_array,fmt='%.3e')
            print('Saved %s.'%(outfile))
        return None

    def py2modPackOld(self, dTp=None, dep=0, path='./', fmt='%.18e'):
        """
        Outputs the data in the format used by Dr. Hub's modelling package.
        """
        xL,yL,zL,xQuantL,yQuantL,zQuantL,xUnitL,yUnitL,zUnitL,dTp_list=self.loaddTpList(dTp, _nolab=True)

        if type(dep)!=int:
            raise TypeError("" )

        for i in range(len(zL)):

            dTpE = dTp_list[i]

            x, y, z  = self.std_dTp(xL[i],yL[i],zL[i])

            L = ['Ih', 'Iv', 'I', 'sigma_h', 'sigma_v', 'sigma', 'sigma_dS0', 'sigma_dS2']
            q = x[dep].squeeze()
            t = y[dep].squeeze()

            if yUnitL[i]==r'ps':
                t *= 1.e3
            if yUnitL[i]==r'ns':
                t *= 1.e6

            D = {'t': t, 'dS': None, 'sigma':None}
            dat=[]
            for el in L:
                if 'I' in el:
                    if 'v' in el and not 'sigma' in el:
                        d = dTpE+'v'
                    elif 'h' in el and not 'sigma' in el:
                        d = dTpE+'h'
                    else:
                        d = dTpE
                elif 'sigma' in el:
                    if 'dS' not in dTpE:
                        d = el+'_'+dTpE
                    else:
                        d = el
                else:
                    d = el
                #base = os.path.split(path)[0]
                #_fn  = os.path.basename(base)

                outDir = os.path.abspath(path)
                if not os.path.isdir(outDir):
                    os.mkdir(outDir)
                if not os.path.isdir(outDir+'_azimutav'):
                    os.mkdir(outDir+'_azimutav')

                try:
                    dat = getattr(self, d)[dep].squeeze()
                    if 'sigma' in d:
                        D['sigma']=dat
                    elif 'dS' in d:
                        D['dS']=dat
                    else:
                        raise NameError("")
                except AttributeError:
                    continue

                fn = 'q-vs-{0}{1}dat'.format(el, os.path.extsep)
                #bork
                print("Datatype {0:7} written in: {1}".format(d, os.path.join(outDir,fn)))
                qdat = np.vstack((x.squeeze(), dat.squeeze().T)).T
                np.savetxt(os.path.join(outDir,fn), qdat, fmt=fmt, delimiter="\t")

            print("Datatype {0:7} written in: {1}".format('t', os.path.join(outDir,'t{0}dat'.format( os.path.extsep))))
            np.savetxt(os.path.join(outDir,'t{0}dat'.format(os.path.extsep)), t, fmt=fmt, delimiter="\t")

            for i,tpt in enumerate(D['t']):
                pt  = '{0}'.format(int(tpt))
                sig = D['sigma'][:,i]
                sig[np.isnan(sig)] = 0
                np.savetxt(os.path.join(path,'q-dI-sigma_t_{0}.dat'.format(pt)), np.vstack((q, D['dS'][:,i], sig)).T)

                print('q-dI-sigma style file written: {0}'.format(os.path.join(path,'q-dI-sigma_t_{0}.dat'.format(pt))))
        return


    def py2modPack(self, dTp=None, dTpSigma = None, dep=0, path='./', fmt='%.18e',
                      var_dict={}):
        """
        version can take the following values:  'hdf5': data is stored in the hdf5 format
                                                '7.3' : data is stored in the matlab 7.3 filetype
                                                '5'   : data is stored in the matlab 5 filetype
                                                '4'   : data is stored in the matlab 4 filetype
        var_dict (dict)                      :  Maps the variables from self to the experimental ones.
        """
        if type(dTp)!=list:
            dTp = [dTp]
        if type(dTpSigma)!=list:
            dTpSigma = [dTpSigma]
        if type(path)!=list:
            path = [path]

        xL,yL,zL,xQuantL,yQuantL,zQuantL,xUnitL,yUnitL,zUnitL,dTp_list=self.loaddTpList(dTp, _nolab=True)

        if type(dTpSigma)!=type(None):
            xLS,yLS,zLS,xQuantLS,yQuantLS,zQuantLS,xUnitLS,yUnitLS,zUnitLS,dTp_listS=self.loaddTpList(dTpSigma, _nolab=True)

        if type(dep)!=int:
            raise TypeError("")

        _var_dict = {'dS'       :{'quant':'dS',    'type':'azimutav'},
                     'dS0'      :{'quant':'dS0',   'type':'isotropic'},
                     'dS2'      :{'quant':'dS2',   'type':'anisotropic'},
                     'sigma'    :{'quant':'sigma', 'type':'azimutav'},
                     'sigma_dS0':{'quant':'sigma', 'type':'isotropic'},
                     'sigma_dS2':{'quant':'sigma', 'type':'anisotropic'},
                     }

        _var_dict.update(var_dict)

        if type(_var_dict)==dict:
            if len(list(_var_dict.keys()))<len(dTp_list):
                raise TypeError("The number of keys of var_dict must match the number of signals (dTp) to be saved.")
        else:
            raise TypeError("var_dict must be a dictionary and the number of keys must match the signals (number of dTp) to be saved.")

        D = {}
        for i in range(len(zL)):

            x, y, z  = self.std_dTp(xL[i],yL[i],zL[i])
            if type(dTpSigma)!=type(None):
                xS, yS, zS  = self.std_dTp(xLS[i],yLS[i],zLS[i])
            else:
                xS, yS, zS = x, y, np.zeros_like(z)

            if not np.all(x==xS) or not np.all(y==yS):
                raise ValueError

            q   = x[dep].squeeze()
            t   = y[dep].squeeze()
            dat = z[dep].squeeze()
            sig = zS[dep].squeeze()

            datLab = _var_dict[dTp_list[i]]['quant']
            typLab = _var_dict[dTp_list[i]]['type']

            directory,fn = os.path.split(path[i])
            base,ext  = os.path.splitext(fn)

            path_q_vs_I   = os.path.join(directory,base)
            path_q_dI_s_t = os.path.join(directory,base+'_'+typLab)

            #print(path_q_dI_s_t)

            D[i]={'dat'  : dat,
                         'sig'  : sig,
                         'x'    : q.squeeze(),
                         'y'    : t,
                         'yU'   : yUnitL[i],
                         #'path_q_vs_I' : path_q_vs_I,
                         'path_q_dI_s_t' : path_q_dI_s_t,
                         }
        #bork
        for j in D:

            C   = D[j]
            dat = C['dat']
            sig = C['sig']
            q   = C['x']
            t   = C['y']
            tU  = C['yU']

            #if not os.path.isdir(C['path_q_vs_I']):
                #os.mkdir(C['path_q_vs_I'])

            if not os.path.isdir(C['path_q_dI_s_t']):
                os.mkdir(C['path_q_dI_s_t'])
            #print(C['path_q_dI_s_t'])

            for k,tpt in enumerate(t):
                fn  = os.path.join(C['path_q_dI_s_t'],'q-dI-sigma_t_{0:.0f}.dat'.format(tpt))
                out = np.vstack((q, dat[:,k], sig[:,k])).T
                np.savetxt(fn, out, fmt=fmt, delimiter="\t")

                print('q-dI-sigma file for time point {0:.0f} {1} written: {2}'.format(tpt, tU, fn))

        return


    def py2DTUmat(self, dTp=None, dep=0, path='./out.mat', fmt='%.18e',
                  var_dict={}, version='7.3', ovrWrt=False):
        """
        version can take the following values:  'hdf5': data is stored in the hdf5 format
                                                '7.3' : data is stored in the matlab 7.3 filetype
                                                '5'   : data is stored in the matlab 5 filetype
                                                '4'   : data is stored in the matlab 4 filetype
        var_dict (dict)                      :  Maps the variables from self to the experimental ones.
        """

        import scipy.io as sio

        xL,yL,zL,xQuantL,yQuantL,zQuantL,xUnitL,yUnitL,zUnitL,dTp_list=self.loaddTpList(dTp, _nolab=True)

        if type(dep)!=int:
            raise TypeError("" )

        _var_dict = {'dS'   :'AllTTDelay',
                     'dS0'  :'AllTTDelay0',
                     'dS2'  :'AllTTDelay2',
                     'S'    :'S',
                     'sigma':'sigma'}
        _var_dict.update(var_dict)

        if type(_var_dict)==dict:
            if len(list(_var_dict.keys()))<len(dTp_list):
                raise TypeError("The number of keys of var_dict must match the number of signals (dTp) to be saved.")
        else:
            raise TypeError("var_dict must be a dictionary and the number of keys must match the signals (number of dTp) to be saved.")

        for i in range(len(zL)):

            x, y, z  = self.std_dTp(xL[i],yL[i],zL[i])

            q   = x[dep].squeeze()
            t   = y[dep].squeeze()
            dat = z[dep].squeeze()

            datLab = _var_dict[dTp_list[i]]
            #if 'dS0' in dTp_list[i]:
                #datLab = 'AllTTDelayS0'
            #elif 'dS2' in dTp_list[i]:
                #datLab = 'AllTTDelayS2'
            #else:
                #datLab = 'AllTTDelay'

            if i==0:
                D = {'{0}'.format(datLab) : dat.T,
                     'q'                  : q[None,:],
                     'Delays'             : t[:,None],}
            else:
                D.update({datLab:dat.T,})

        directory,fn = os.path.split(path)
        base,ext  = os.path.splitext(fn)

        if not os.path.isdir(directory):
            os.mkdir(directory)

        path = os.path.join(directory,base)

        if os.path.isfile(path) and not ovrWrt:
            raise OSError("{0} exists.\nSet ovrWrt to True if you want to overwrite an existing file.".format(path))
        #elif os.path.isfile(path) and ovrWrt:
            #os.remove(path)

        if version=='hdf5':
            import h5py
            if ovrWrt:
                mode='a'
            else:
                mode='x'

            with h5py.File(path+'.hdf5',mode) as f:
                for key in list(D.keys()):
                    f.create_dataset(key, data=D[key])
        else:
            import hdf5storage as h5s
            if os.path.isfile(path+'.mat') and ovrWrt:
                os.remove(path+'.mat')
            #bork
            h5s.savemat(path, D, appendmat=True, format=version, store_python_metadata=True)
        print("Written a DTU MatLab style file: {0}".format(path))
        return


    def py2SIA(self, dTp=None, path='./', prefix='', postfix='', ext='SIA', file_fmt='%.1f', **kwargs):

        xL,yL,zL,xQuantL,yQuantL,zQuantL,xUnitL,yUnitL,zUnitL,dTp_list=self.loaddTpList(dTp, _nolab=True)

        dep = self.dep

        if not os.path.isdir(path):
            os.mkdir(path)

        for i in range(len(zL)):

            x, y, z  = self.std_dTp(xL[i],yL[i],zL[i])

            for j,dp in enumerate(dep):
                xd, yd, zd = x[j,:,0], y[j,0,:], z[j].T

                #'{0:.2f}'.format(dep[j])
                fn = prefix + file_fmt%dep[j] + postfix + '.' + ext


                shape = zd.shape
                out = np.zeros(np.asarray(shape)+1)
                out[0,1:]  = xd
                out[1:,0]  = yd
                out[1:,1:] = zd

                write_path = os.path.join(path,fn)
                np.savetxt(write_path, out, **kwargs)
                print('Succesfully written: {0}'.format(write_path))

        return


    def py2pyldm(self, dTp=None, path='out.csv', fmt='%.6f', dep=0, ovrWrt=False):

        import scipy.io as sio
        import csv

        xL,yL,zL,xQuantL,yQuantL,zQuantL,xUnitL,yUnitL,zUnitL,dTp_list=self.loaddTpList(dTp, _nolab=True)

        if type(dep)!=int:
            raise TypeError("" )
        # dTp name : 
        #_var_dict = {'dS'   :'dS',
                     #'dS0'  :'dS0',
                     #'dS2'  :'dS2',
                     #'S'    :'S',
                     #'sigma':'sigma'}
        #_var_dict.update(var_dict)

        #if type(_var_dict)==dict:
            #if len(list(_var_dict.keys()))<len(dTp_list):
                #raise TypeError("The number of keys of var_dict must match the number of signals (dTp) to be saved.")
        #else:
            #raise TypeError("var_dict must be a dictionary and the number of keys must match the signals (number of dTp) to be saved.")

        directory,fn = os.path.split(path)
        base,ext  = os.path.splitext(fn)

        if len(directory)==0:
            directory = '.'

        if not os.path.isdir(directory):
            os.mkdir(directory)



        for i in range(len(zL)):

            x, y, z  = self.std_dTp(xL[i],yL[i],zL[i])
            q   = x[dep].squeeze()
            t   = y[dep].squeeze()
            dat = z[dep].squeeze()

            #datLab = _var_dict[dTp_list[i]]


            #if i==0:
                #D = {'{0}'.format(datLab) : dat.T,
                     #'q'                  : q[None,:],
                     #'Delays'             : t[:,None],}
            #else:
                #D.update({datLab:dat.T,})

            fn = os.path.abspath(os.path.join(directory,base))+dTp_list[i]+'.csv'

            if os.path.isfile(fn) and not ovrWrt:
                raise OSError("{0} exists.\nSet ovrWrt to True if you want to overwrite an existing file.".format(fn))

            shape = (y.shape[2]+1,x.shape[1]+1)
            out = np.zeros((shape),dtype=np.float)
            out[1:,0]  = y.squeeze()
            out[0,1:]  = x.squeeze()
            out[1:,1:] = z.squeeze().T

            np.savetxt(fn, out, fmt=fmt, delimiter=',')
            #with open(fn, 'w') as fp:
                #fp = csv.writer(fp, delimiter=',',quoting=csv.QUOTE_MINIMAL)
                ##[fp.writerow(line) for line in out]
                #fp.writerows(out)
            print('Written csv file in pyLDM format: {0}'.format(fn))

        return


    def output(self, filename):
        """
        Dumps difference data to a textfile.
        @param filename: output file.
        @type filename: str
        @return: The dumped data.
        @rtype: numpy.ndarray
        """
        output_array = np.vstack((np.reshape(np.hstack((0,self.t)),(-1,len(self.t)+1)),np.hstack((np.reshape(self.q,(len(self.q),-1)),self.dS))))
        np.savetxt(filename, output_array, fmt='%.3e')
        return output_array

    def scale(self, factor):
        """
        Rescales intensities (absolute and difference) by a factor.
        @param factor: The scaling factor.
        @type factor: float
        """
        self.S *= factor
        self.dS *= factor
        if self.sigma != None:
            self.sigma *= factor

    def padAxis(self, dTp=None, pad = 'edge', xLim=None, yLim=None, ovrWrt=True):
        """
        """
        from scipy.interpolate import interp1d
        
        xL,yL,zL,xQuant,yQuant,zQuant,xUnit,yUnit,zUnit,dTp_list=self.loaddTpList(dTp, _nolab=True)
        
        if type(pad)!=str:
            raise TypeError("pad needs to be a string.")

        
        for i,dat in enumerate(zL):
            
            x,y,dat = self.std_dTp(xL[i],yL[i],dat)
            
            datPad = np.copy(dat)
            
            
            xPad = np.copy(x.squeeze())
            if type(xLim)!=type(None):
                xLim = np.asarray(xLim)
                xIdx, _xLim = self._stdIdxLim(x.squeeze(), xLim, slice=False)
                xLim[xLim==[None]] = _xLim[xLim==[None]]
                if np.min(xLim)<_xLim.min():
                    dx = np.diff(x.squeeze()).mean()
                    _min = np.sort(np.array([x.squeeze().min()-dx, np.min(xLim)]))
                    num = np.abs(np.int(np.diff(_min)/dx))
                    xPad = np.hstack((np.linspace(*_min, num=num), xPad))
                if np.max(xLim)>_xLim.max():
                    dx = np.diff(x.squeeze()).mean()
                    _max = np.sort(np.array([x.squeeze().max()+dx, np.max(xLim)]))
                    num = np.abs(np.int(np.diff(_max)/dx))
                    xPad = np.hstack((xPad,np.linspace(*_max, num=num)))
            xIdx, _xLim = self._stdIdxLim(xPad, [x.squeeze().min(),x.squeeze().max()], slice=True)
            xSlice = slice(*xIdx)
            
            yPad = np.copy(y.squeeze())
            if type(yLim)!=type(None):
                yLim = np.asarray(yLim)
                yIdx, _yLim = self._stdIdyLim(y.squeeze(), yLim, slice=False)
                yLim[yLim==[None]] = _yLim[yLim==[None]]
                if np.min(yLim)<_yLim.min():
                    dy = np.diff(y.squeeze()).mean()
                    _min = np.sort(np.array([y.squeeze().min()-dy, np.min(yLim)]))
                    num = np.abs(np.int(np.diff(_min)/dy))
                    yPad = np.hstack((np.linspace(*_min, num=num), yPad))
                if np.max(yLim)>_yLim.max():
                    dy = np.diff(y.squeeze()).mean()
                    _max = np.sort(np.array([y.squeeze().max()+dy, np.max(yLim)]))
                    num = np.abs(np.int(np.diff(_max)/dy))
                    yPad = np.hstack((yPad,np.linspace(*_max, num=num)))
            yIdx, _yLim = self._stdIdxLim(yPad, [y.squeeze().min(),y.squeeze().max()], slice=True)
            ySlice = slice(*yIdx)
            
            shape = ((dat.shape[0],) + xPad.shape + yPad.shape)
            datPad = np.ones(shape)
            
            if pad == 'edge':
                if np.any(xPad<_xLim.min()):
                    datPad[:,xPad<_xLim.min(),:] = dat[:,0,:][:,None,:]
                if np.any(xPad>_xLim.max()):
                    datPad[:,xPad>_xLim.max(),:] = dat[:,-1,:][:,None,:]
                if np.any(yPad<_yLim.min()):
                    datPad[:,:,yPad<_yLim.min()] = dat[:,:,0][:,:,None]
                if np.any(yPad>_yLim.max()):
                    datPad[:,:,yPad>_yLim.max()] = dat[:,:,-1][:,:,None]
            else:
                raise NotImplementedError
            datPad[:, xSlice, ySlice] = dat
            
            xPad,yPad,datPad = self.std_dTp(xPad, yPad, datPad)
            
            if ovrWrt:
                if i==len(zL)-1:
                    if type(xLim)!=type(None):
                        self.gen_dTp('%s'%(self.getUnit(dTp_list[i])), xPad, like_dTp=(self.getUnit(dTp_list[i])), axis=True, ovrWrt=ovrWrt)
                    if type(yLim)!=type(None):
                        self.gen_dTp('%s'%('t'), yPad, like_dTp='t', axis=True, ovrWrt=ovrWrt)
                self.gen_dTp('%s'%(dTp_list[i]), datPad, like_dTp=dTp_list[i],
                             xAxis='%s'%(self.getUnit(dTp_list[i])),
                             yAxis='%s'%('t'),
                             ovrWrt=ovrWrt)
            else:
                if i==len(zL)-1:
                    if type(xLim)!=type(None):
                        self.gen_dTp('%s_pad'%(self.getUnit(dTp_list[i])), xPad, like_dTp=(self.getUnit(dTp_list[i])), axis=True, ovrWrt=ovrWrt)
                    if type(yLim)!=type(None):
                        self.gen_dTp('%s_pad'%('t'), yPad, like_dTp='t', axis=True, ovrWrt=ovrWrt)            
                self.gen_dTp('%s_pad'%(dTp_list[i]), datPad, like_dTp=dTp_list[i],
                             xAxis= '%s_pad'%(self.getUnit(dTp_list[i])) if type(xLim)!=type(None) else zUnit[i],
                             yAxis= '%s_pad'%('t') if type(yLim)!=type(None) else None,
                             ovrWrt=ovrWrt)
        return

    def resample(self, dTp=None, ext=None, ax='x', ovrWrt=False):
        """
        """
        from scipy.interpolate import interp1d

        xL,yL,zL,xQuant,yQuant,zQuant,xUnit,yUnit,zUnit,dTp_list=self.loaddTpList(dTp, _nolab=True)


        #datList=[]
        for i,dat in enumerate(zL):

            x,y,dat = self.std_dTp(xL[i],yL[i],dat)

            #if i ==0:
            if ax == 'dep':
                axis = -3
                raise NotImplementedError("Cutting dep range not implemented yet!")
            elif ax == 'x':
                axis = -2
                AX = x
                ext = np.atleast_3d(ext) # Will fail in the future when resampling the y axis.
            elif ax == 'y':
                axis = -1
                AX = y
                #bork
                ext = ext[None,None,:]

            if dat.shape[0]!=AX.shape[0] and AX.shape[0]==1:
                AX = np.tile(AX, (dat.shape[0],1,1))
            elif dat.shape[0]!=AX.shape[0]!=1:
                raise ValueError("The 1st dimension (dependency) of the x axis {0} does not match that of the data {1} and is not equal to 1.".format(x.shape[0], dat.shape[0]))

            if dat.shape[0]!=ext.shape[0] and ext.shape[0]==1:
                _ext = np.tile(ext, (dat.shape[0],1,1))
            elif np.ndim(ext)!=1 and dat.shape[0]!=ext.shape[0]!=1:
                raise ValueError("The 1st dimension (dependency) of the ext axis {0} does not match that of the data {1} and is not equal to 1.".format(ext.shape[0], dat.shape[0]))
            else:
                _ext = ext

            AXIS = np.asarray(dat.shape)
            AXIS[axis] = _ext.shape[axis]
            res = np.zeros(AXIS)

            for j,dep in enumerate(dat):
                f = interp1d(AX[j].squeeze(), dep, axis=axis, copy=False, fill_value=0., assume_sorted=True, bounds_error=False)
                res[j] = f(_ext[j].squeeze())
            #bork

            if ovrWrt:
                if i==len(zL)-1:
                    if ax=='x':
                        self.gen_dTp('%s'%(self.getUnit(dTp_list[i])), _ext, like_dTp=(self.getUnit(dTp_list[i])), axis=True, ovrWrt=ovrWrt)
                    if ax=='y':
                        self.gen_dTp('%s'%('t'), _ext, like_dTp='t', axis=True, ovrWrt=ovrWrt)
                self.gen_dTp('%s'%(dTp_list[i]), res, like_dTp=dTp_list[i], xAxis='%s'%(self.getUnit(dTp_list[i])), yAxis='t', ovrWrt=ovrWrt)
            else:
                if ax=='x':
                    self.gen_dTp('{0}_res'.format(xQuant[i]), _ext, 
                                quant='{0}_res'.format(xQuant[i]),
                                unit =xUnit[i], axis=True, ovrWrt=ovrWrt)
                if ax=='y':
                    self.gen_dTp('%s_res'%('t'), _ext, like_dTp='t', axis=True, ovrWrt=ovrWrt)            
                self.gen_dTp('%s_res'%(dTp_list[i]), res, quant=zQuant[i], unit='{0}_res'.format(xQuant[i]),
                             xAxis='{0}_res'.format(xQuant[i]) if ax=='x' else '%s'%(self.getUnit(dTp_list[i])),
                             yAxis='%s_res'%('t') if ax=='y' else 't',
                             ovrWrt=ovrWrt)
        return

    def SVDcleanup(self, dTp=None, SdTp=None, dep=None, Sdep=0, xLim=None, yOff=None, SyPt=None, 
                   comps2use = None, Scomps2use=None, subtractComps=None,reset=False,
                   smooth4SVD=False, smooth4SVD_kws={'filter':('exp2_damp', 0.001)},
                   ovrWrt=False, plot=False):
        """
        Adapted from "Kristoffer Haldrup, Singular value decomposition as a tool for background 
        corrections in time-resolved XFEL scattering data, Phil. Trans. R. Soc. B, 369, 20130336, 2014."
        SdTp needs to be in decreasing order of Target vector weight.
        """

        xL,yL,zL,xQuant,yQuant,zQuant,xUnit,yUnit,zUnit,dTp_list=self.loaddTpList(dTp, _nolab=True)

        if type(SdTp)!=type(None):
            SxL,SyL,SzL,SxQuant,SyQuant,SzQuant,SxUnit,SyUnit,SzUnit,SdTp_list=self.loaddTpList(SdTp, _nolab=True)

            ### Obtain basis "spectra"
            Sx   = np.zeros((len(SzL),) + SxL[0].shape[1:])
            Sy   = np.zeros((len(SzL),) + SyL[0].shape[1:])
            Sdat = np.zeros((len(SzL),) + SzL[0].shape[1:])

            for i in range(len(SzL)):
                Sx[i],Sy[i],Sdat[i] = self.std_dTp(SxL[i],SyL[i],SzL[i])
            ###
        else:
            Sdat = None

        for i,dat in enumerate(zL):
            x,y,dat = self.std_dTp(xL[i],yL[i],dat)

            if not np.all(Sx[0,:,0]==x[0,:,0]):
                raise ValueError("x range of data and solvent have to be the same!")

            if type(SyPt) != type(None) and len(SyPt)==1:
                SyIdx,_ = self._stdIdxLim(Sy[i], SyPt, slice=False)
            elif type(SyPt) == type(None):
                SyIdx   = 0
            else:
                raise ValueError("The value of kwarg SyPt must have a length of 1.")
            cleanL=[]
            for j,dep in enumerate(dat):

                xIdx,xLim = self._stdIdxLim(x[j].squeeze(), xLim, slice=True)
                xSlice = slice(*xIdx)

                if type(yOff)==type(None):
                    yOff = [y[j].min(),0.]

                yIdx,yLim = self._stdIdxLim(y[j].squeeze(), yOff, slice=True)
                ySlice = slice(*yIdx)

                if smooth4SVD:
                    _sx,_sy,_sz = self.std_dTp(x[j][xSlice],y[j][:,ySlice], dep[xSlice,ySlice])
                    dTpSmooth = [(_sx,_sy,_sz),]
                    smooth    = self.smooth_fft(dTp=dTpSmooth, out=True, **smooth4SVD_kws).squeeze()
                    U,s,V = np.linalg.svd(smooth, full_matrices=0, compute_uv=1)
                else:
                    U,s,V = np.linalg.svd(dep[xSlice,ySlice], full_matrices=0, compute_uv=1)

                if type(comps2use)!=type(None):
                    comps2use = np.asarray(comps2use)
                else:
                    comps2use = np.arange(dat.shape[0])

                maskC            = np.zeros_like(s, dtype=bool)
                maskC[comps2use] = True


                if type(Sdat)!=type(None):
                    if type(Scomps2use)!=type(None):
                        Scomps2use = np.asarray(Scomps2use)
                    else:
                        Scomps2use = np.arange(Sdat.shape[0])
                    maskSc             = np.zeros(Sdat.shape[0], dtype=bool)
                    maskSc[Scomps2use] = True
                    coeffM             = np.hstack((U[:,maskC], Sdat[:,xSlice,SyIdx].T[:,maskSc]))
                else:
                    coeffM             = U[:,maskC]

                ScVal, _stdX, rank, _s = np.linalg.lstsq(coeffM,dep[xSlice,:],rcond=None)
                clean = np.copy(dep)

                #bork
                nC                = np.zeros(coeffM.shape[-1], dtype=bool)
                subtractComps     = np.asarray(subtractComps) if type(subtractComps)!=type(None) else np.arange(coeffM.shape[-1])
                nC[subtractComps] = True
                #if type(include)==type(None) or include>=rank or include==0:
                clean[xSlice,:] -= np.dot(coeffM[:,nC], ScVal[nC,:])
                cleanL.append(clean)
                #else:
                    #clean[xSlice,:] -= np.dot(U[:,:include], ScVal[:include,:])

                if plot:
                    from matplotlib import ticker as mplt

                    oU,os,oV = np.linalg.svd(dep, full_matrices=0, compute_uv=1)
                    cU,cs,cV = np.linalg.svd(clean, full_matrices=0, compute_uv=1)
                    fig, ax = plt.subplots(ncols=2, nrows=3, gridspec_kw={'wspace': 0.0,'hspace':0.0})
                    fig.suptitle(r'dTp: {0}'.format(dTp_list[i]))
                    rexAdd = re.compile(r'(\S+)_(\S+)*')
                    if rexAdd.match(xQuant[i]):
                        xq = rexAdd.match(xQuant[i]).groups()[0]
                        xu = xUnit[i]
                    else:
                        xq = xQuant[i]
                        xu = xUnit[i]
                    if rexAdd.match(xQuant[i]):
                        cq = rexAdd.match(self.getQuant(zUnit[i])).groups()[0]
                    else:
                        cq = self.getQuant(zUnit[i])

                    tu = self.getUnit('t')
                    tq = self.getQuant('t')
                    #bork

                    #fig.suptitle(r'SVDcleanup(): {0} dep {0}'.format(cq, j))
                    uinc = np.max(np.abs(np.dot(U[:,:rank],s[:rank])))
                    vinc = np.max(np.abs(np.dot(V.T[:,:rank],s[:rank])))
                    uonc = np.max(np.abs(np.dot(oU[:,:rank],os[:rank])))
                    vonc = np.max(np.abs(np.dot(oV.T[:,:rank],os[:rank])))
                    ucnc = np.max(np.abs(np.dot(cU[:,:rank],cs[:rank])))
                    vcnc = np.max(np.abs(np.dot(cV.T[:,:rank],cs[:rank])))

                    for k in range(nC.size):
                        ax[0,0].axhline(y=uinc*k, ls=':', lw=0.5, color='black')
                        ax[0,0].plot(x[j,xSlice], np.dot(U[:,k],s[k])+uinc*k)

                        ax[0,1].axhline(y=vinc*k, ls=':', lw=0.5, color='black')
                        ax[0,1].plot(y[j,0,ySlice], np.dot(V.T[:,k],s[k])+vinc*k,
                                     label='comp: {0}'.format(k))
                        ax[0,1].legend(frameon=False,loc=0)

                        ax[1,0].axhline(y=uonc*k, ls=':', lw=0.5, color='black')
                        ax[1,0].plot(x[j,:], np.dot(oU[:,k], os[k])+uonc*k)

                        ax[1,1].axhline(y=vonc*k, ls=':', lw=0.5, color='black')
                        ax[1,1].plot(y[j,0,:], np.dot(oV.T[:,k], os[k])+vonc*k)

                        ax[2,0].axhline(y=ucnc*k, ls=':', lw=0.5, color='black')
                        ax[2,0].plot(x[j,:], np.dot(cU[:,k], cs[k])+ucnc*k)

                        ax[2,1].axhline(y=vcnc*k, ls=':', lw=0.5, color='black')
                        ax[2,1].plot(y[j,0,:], np.dot(cV.T[:,k], cs[k])+vcnc*k)


                    xlim0=(x[j,:].min(),x[j,:].max())
                    xlim1=(y[j,0,:].min(), y[j,0,:].max())
                    
                    ax[0,0].set_xlim(*xlim0)
                    ax[0,1].set_xlim(*xlim1)
                    ax[1,0].set_xlim(*xlim0)
                    ax[1,1].set_xlim(*xlim1)
                    ax[2,0].set_xlim(*xlim0)
                    ax[2,1].set_xlim(*xlim1)
                    #lims = [axi.get_ylim() for axi in ax[:,0]]
                    #yl = np.min(lims)
                    #bork
                    ax[0,0].set_ylim(-uinc,uinc*nC.size)
                    ax[1,0].set_ylim(-uonc,uonc*nC.size)
                    ax[2,0].set_ylim(-ucnc,ucnc*nC.size)
                    ax[0,0].text(x[j,xSlice].min(),-uinc, r'SVD raw {0} $t = {1}\ \mathrm{{to}}\ {2}$'.format(dTp_list[i], np.round(yLim.min(),2), np.round(yLim.max(),2)))
                    ax[1,0].text(x[j,xSlice].min(),-uonc, r'SVD raw {0} full $t$ range'.format(dTp_list[i]))
                    ax[2,0].text(x[j,xSlice].min(),-ucnc, r'SVD cleaned {0} full $t$ range'.format(dTp_list[i]))

                    for m in range(ax.shape[0]):
                        ax[m,1].yaxis.set_ticks_position('right')
                        ax[m,1].set_xscale('symlog', linthreshx=1.)
                    for n in range(ax.shape[-1]):
                        [axi.xaxis.set_visible(False) for axi in ax[1:-1,n]]
                    ax[-1,0].set_xlabel(r'${0}\ ({1})$'.format(xq, xu))
                    ax[-1,1].set_xlabel(r'${0}\ log_{{10}}({1})$'.format(tq, tu))

            clean_dat = np.asarray(cleanL)

            _,_,clean_dat = self.std_dTp(x,y,clean_dat)

            if ovrWrt:
                datlab = '{0}'.format(dTp_list[i])
            else:
                datlab = '{0}_clean'.format(dTp_list[i])

            if reset:
                try:
                    delattr(self, datlab)
                except AttributeError:
                    pass
            #if ovrWrt:
                #self.gen_dTp(datlab, clean, like_dTp=dTp_list[i], ovrWrt=ovrWrt)
            #else:
            self.gen_dTp(datlab, clean_dat, like_dTp=dTp_list[i],
                         xAxis=zUnit[i],
                         yAxis=None,
                         ovrWrt=ovrWrt)

        if plot:
            return fig
        else:
            return


    def derivative(self, dTp=None, ax='x', n=1, reset=False, out=False):

        if n==0:
            print("*** Not doing anything: n=0. ***")
            return

        xL,yL,zL,xQuantL,yQuantL,zQuantL,xUnitL,yUnitL,zUnitL,dTp_list=self.loaddTpList(dTp, _nolab=True)

        for i,dat in enumerate(zL):
            x,y,dat = self.std_dTp(xL[i],yL[i],dat)

            if ax == 'dep':
                axis = -3
                raise NotImplementedError("Taking the derivative of dep axis not implemented yet!")
            elif ax == 'x':
                axis = -2
                AX = x
            elif ax == 'y':
                axis = -1
                AX = y

            diff    = np.diff(dat, n=n, axis=axis)

            diff_ax = np.linspace(AX.min(), AX.max(), num=diff.shape[axis])

            if ax == 'x':
                diff_ax,y,diff = self.std_dTp(diff_ax,yL[i],diff)
                if n == 1:
                    axlab = 'd{0}'.format(self.getUnit(dTp_list[i]))
                else:
                    axlab = 'd{0}{1}'.format(n, self.getUnit(dTp_list[i]))
                axunit  = self.getUnit(self.getUnit(dTp_list[i]))
                axquant = axlab
            elif ax == 'y':
                ### this is going to fail when retreiving the y axis. Need to get associations going.
                x,diff_ax,diff = self.std_dTp(xL[i],diff_ax,diff)
                if n == 1:
                    axlab = 'dt'
                else:
                    axlab = 'd{0}t'.format(n)
                axunit  = self.getUnit('t')
                axquant = self.getQuant('t')
            datlab = '{0}_{1}'.format(dTp_list[i], axlab)
            datquant = self.getQuant(dTp_list[i])

            if reset:
                try:
                    delattr(self, axlab)
                    delattr(self, datlab)
                except AttributeError:
                    pass

            #bork
            #bork
            self.gen_dTp(axlab, diff_ax, quant=axquant, unit=axunit, axis=True, ovrWrt=False)
            self.gen_dTp(datlab, diff, quant=datquant, unit=axlab, 
                         xAxis=axlab if ax=='x' else self._asoc[dTp_list[i]]['x'],
                         yAxis=axlab if ax=='y' else self._asoc[dTp_list[i]]['y'],
                         ovrWrt=False)
        return

    def removeHeat(self, dTp, SdTp, via_dTp=None, via_SdTp=None, xLim=None, SyPt=None,
                   subtract_lim=False, ovrWrt=False, reset=False, plot=False, out=False):
        """
        """

        xL,yL,zL,xQuantL,yQuantL,zQuantL,xUnitL,yUnitL,zUnitL,dTp_list=self.loaddTpList(dTp, _nolab=True)
        SxL,SyL,SzL,SxQuant,SyQuant,SzQuant,SxUnit,SyUnit,SzUnit,SdTp_list=self.loaddTpList(SdTp, _nolab=True)
        if via_dTp:
            assert ~(type(via_SdTp)!=type(None) and type(via_dTp)!=type(None))
            xVL,yVL,zVL,xQuantVL,yQuantVL,zQuantVL,xUnitVL,yUnitVL,zUnitVL,dTp_listV=self.loaddTpList(via_dTp, _nolab=True)
            SxVL,SyVL,SzVL,SxQuantV,SyQuantV,SzQuantV,SxUnitV,SyUnitV,SzUnitV,SdTp_listV=self.loaddTpList(via_SdTp, _nolab=True)

        ### Obtain basis "spectra"
        Sx   = np.zeros((len(SzL),) + SxL[0].shape[1:])
        Sy   = np.zeros((len(SzL),) + SyL[0].shape[1:])
        Sdat = np.zeros((len(SzL),) + SzL[0].shape[1:])
        if via_dTp:
            SxV   = np.zeros((len(SzVL),) + SxVL[0].shape[1:])
            SyV   = np.zeros((len(SzVL),) + SyVL[0].shape[1:])
            SdatV = np.zeros((len(SzVL),) + SzVL[0].shape[1:])

        for i in range(len(SzL)):
            Sx[i],Sy[i],Sdat[i]  = self.std_dTp(SxL[i],SyL[i],SzL[i])
        if via_dTp:
            for i in range(len(SzVL)):
                SxV[i],SyV[i],SdatV[i] = self.std_dTp(SxVL[i],SyVL[i],SzVL[i])
        ###
        for i,dat in enumerate(zL):
            x,y,dat = self.std_dTp(xL[i],yL[i],dat)
            if via_dTp:
                xV,yV,datV = self.std_dTp(xVL[i],yVL[i],zVL[i])

            shape = (dat.shape[0], len(Sdat), y.shape[-1])
            ScVal = np.empty(shape)

            for j,dep in enumerate(dat):
                if via_dTp:
                    depV = datV[j]

                xIdx,xLim = self._stdIdxLim(x[j].squeeze(), xLim, slice=True)
                xSlice = slice(*xIdx)
                SxIdx,SxLim = self._stdIdxLim(Sx[0].squeeze(), xLim, slice=True)
                SxSlice = slice(*SxIdx)

                if via_dTp:
                    xVIdx,xVLim = self._stdIdxLim(xV[j].squeeze(), xLim, slice=True)
                    xVSlice = slice(*xVIdx)
                    SxVIdx,SxVLim = self._stdIdxLim(SxV[0].squeeze(), xLim, slice=True)
                    SxVSlice = slice(*SxVIdx)

                if not np.all(Sx[0,SxSlice,0]==x[0,xSlice,0]):
                    raise ValueError("x range of data and solvent have to be the same!")

                if type(SyPt) != type(None) and len(SyPt)==1:
                    SyIdx,_ = self._stdIdxLim(Sy[i], SyPt, slice=False)
                elif type(SyPt) == type(None):
                    SyIdx   = 0
                else:
                    raise ValueError("The value of kwarg SyPt must have a length of 1.")

                if via_dTp:
                    if subtract_lim:
                        ScVal[j], _stdX, rank, _s = np.linalg.lstsq(SdatV[:,SxVSlice,SyIdx].T, depV[xVSlice,:])
                        sub = np.copy(dep)
                        _sub = np.zeros_like(dep)
                        _sub[xSlice,:] = np.dot(Sdat[:,SxSlice,SyIdx].T, ScVal[j])
                        sub -= _sub
                    else:
                        ScVal[j], _stdX, rank, _s = np.linalg.lstsq(SdatV[:,xVSlice,SyIdx].T, depV[xVSlice,:])
                        sub = np.copy(dep)
                        sub -= np.dot(Sdat[:,:,SyIdx].T, ScVal[j])

                else:
                    if subtract_lim:
                        ScVal[j], _stdX, rank, _s = np.linalg.lstsq(Sdat[:,SxSlice,SyIdx].T, dep[xSlice,:])
                        sub = np.copy(dep)
                        _sub = np.zeros_like(dep)
                        _sub[xSlice,:] = np.dot(Sdat[:,SxSlice,SyIdx].T, ScVal[j])
                        sub -= _sub
                    else:
                        ScVal[j], _stdX, rank, _s = np.linalg.lstsq(Sdat[:,xSlice,SyIdx].T, dep[xSlice,:])
                        sub = np.copy(dep)
                        sub -= np.dot(Sdat[:,:,SyIdx].T, ScVal[j])

                if plot:
                    if type(via_dTp)!=None:
                        ncols=3
                    else:
                        ncols=2

                    fig, ax = plt.subplots(ncols=ncols, nrows=1, gridspec_kw={'wspace': 0.0,'hspace':0.0})
                    for k in range(rank):
                        #bork
                        ax[0].plot(x[j,xSlice], Sdat[k,SxSlice,SyIdx].T, label='component {}'.format(k))
                        ax[0].axhline(y=0., ls = ':', color='black')
                        ax[0].set_xlim([x[j,xSlice].min(),x[j,xSlice].max()])
                        ax[0].set_xlabel((r'${}'+r'\ '+r'({})'+r'$').format(xQuantL[i], xUnitL[i]))
                        ax[0].legend(frameon=False, loc=0)
                        #bork
                        if via_dTp:
                            ax[2].plot(SxV[j,SxVSlice], SdatV[k,SxVSlice,SyIdx].T, label='component {}'.format(k))
                            ax[2].axhline(y=0., ls = ':', color='black')
                            ax[2].yaxis.set_ticks_position('right')
                            ax[2].set_xlim([SxV[j,SxVSlice].min(),SxV[j,SxVSlice].max()])
                            ax[2].set_xlabel((r'$d^{{2}}{0}'+r'\ '+r'({1})'+r'$').format(xQuantL[i], xUnitL[i]))
                            ax[2].legend(frameon=False, loc=0)

                        ax[1].plot(y[j,0,:], ScVal[j,k], label='component {}'.format(k))
                        ax[1].set_xscale('symlog', linthreshx=0.1)
                        ax[1].axhline(y=0., ls = ':', color='black')
                        ax[1].set_xlim([y[j,0,:].min(),y[j,0,:].max()])
                        if type(via_dTp)!=None:
                            ax[1].yaxis.set_visible(False)
                        else:
                            ax[1].yaxis.set_label_position('right')
                        ax[1].yaxis.tick_right()
                        ax[1].set_xlabel((r'$\log_{{10}}(\mathrm{{{0}}})'+r'\ '+r'({1})'+r'$').format(yQuantL[i], yUnitL[i]))
                        ax[1].legend(frameon=False, loc=0)
                    fig.tight_layout()

            _,_,sub = self.std_dTp(xL[i],yL[i],sub)

            if reset:
                try:
                    if ovrWrt:
                        delattr(self, '%s'%(dTp_list[i]))
                        delattr(self, '%s_subT'%(dTp_list[i]))
                    else:
                        delattr(self, '%s_sub'%(dTp_list[i]))
                        delattr(self, '%s_subT'%(dTp_list[i]))
                except AttributeError:
                    pass

            if ovrWrt:
                self.gen_dTp('%s_subT'%(dTp_list[i]), ScVal, unit='t', quant='{0}'.format(dTp_list[i]),
                             axis=True, ovrWrt=ovrWrt)
                self.gen_dTp('%s'%(dTp_list[i]), sub, like_dTp=dTp_list[i],
                             xAxis=None,
                             yAxis=self._asoc[dTp_list[i]]['y'],
                             ovrWrt=ovrWrt)
            else:
                ##bork
                self.gen_dTp('%s_subT'%(dTp_list[i]), ScVal, unit='t', quant='{0}'.format(dTp_list[i]),
                             axis=True, ovrWrt=ovrWrt)
                self.gen_dTp('%s_sub'%(dTp_list[i]), sub, like_dTp=dTp_list[i],
                             xAxis=self.getUnit(dTp_list[i]),
                             yAxis=self._asoc[dTp_list[i]]['y'],
                             ovrWrt=ovrWrt)
            if out:
                if i==0:
                    outL = []
                outL += [sub]

        if plot:
            return fig
        elif out:
            return outL
        else:
            return

    def average(self):
        """
        Averages together identical timepoints while accounting for the number of exposures at each. This gives the same result as calling the constructor with average = True, and is sometimes useful if it wasn't.
        """

        # local variables:
        Nt      = len(uniqueList(self.t))
        Nq      = len(self.q)
        Nexp    = self.dS.shape[1]

        # new t, Nrepeats, S and dS:
        t           = np.array(uniqueList(self.t))
        S           = np.zeros((Nq, Nt))
        dS          = np.zeros((Nq, Nt))
        Nrepeats    = np.zeros(Nt)

        for i in range(Nexp):
            timeindex = np.where(self.t[i] == t)[0][0]
            if np.isnan(self.dS[:, i]).any(): continue
            S [:, timeindex]    += self. S[:, i]
            dS[:, timeindex]    += self.dS[:, i]
            Nrepeats[timeindex] += 1

        for i in range(Nt):
            S [:, i] /= Nrepeats[i]
            dS[:, i] /= Nrepeats[i]

        # update the object
        self.t          = t
        self.S          = S
        self.dS         = dS
        self.Nrepeats   = Nrepeats

    def timeSegments(self, times=None, offsets=0.0, qpower=0):
        """
        Returns (optionally offset) dS(q) curves for specified time ranges.
        @param times: Sequence of time range boundaries.
        @type times: list
        @param offsets: Offsets to apply to each successive curve. Can be a single number or a sequence of numbers.
        @type offsets: list, float
        @param qpower: Optionally amplify difference intensities by q^qpower.
        @type qpower: int, float
        @return: A 2d array containing the requested time slices.
        @rtype: numpy.ndarray
        """

        segments = []
        dS = self.dSamplified(qpower=qpower)

        # if explicit time slices are supplied, use them
        if times != None:
            if type(offsets) == float:
                offsets = np.arange(len(times)) * offsets
            for i in range(1, len(times)):
                segments += [ np.mean( dS[:, (self.t>=times[i-1])&(self.t<times[i]) ], axis=1) + offsets[i-1] ]

        # otherwise, use the time vector of this Dataset object (self.t)
        else:
            if (type(offsets) == float) or (type(offsets) == int):
                offsets = np.arange(len(self.t)) * offsets
            for i in range(len(self.t)):
                segments += [ np.mean( dS[:, self.t==self.t[i]], axis=1 ) + offsets[i] ]

        return np.transpose(np.array(segments))

    def qSegments(self, qranges=[], offsets=0.0):
        """
        Returns (optionally offset) dS(t) kinetic traces for specified q ranges.
        @param qranges: List of q-ranges to analyze, where each range is a pair (list, tuple or array) of numbers. The lower bound is inclusive, the upper one is exclusive.
        @type qranges: list
        @param offsets: Offsets to apply to each successive transient. Can be a single number or a sequence of numbers.
        @type offsets: list, float
        @return: A 2d array containing the requested transients.
        @rtype: numpy.ndarray
        """
        if type(offsets) == float:
            offsets = np.arange(len(qranges)) * offsets
        segments = []
        for i in range(len(qranges)):
            q = qranges[i]
            segments += [ np.mean( self.dS[ (self.q>=q[0])&(self.q<q[1]), : ], axis=0) ]
        return np.transpose(np.array(segments))

    def subtractHeat(self, dTp=None, HdTp=None, xLim=None, yLim=None, yPt=None, tnotch=None):
        """
        Subtract the supplied heating data from the object's difference intensities.
        @param heatingdata: Heating data, supplied as an explicit difference intensity array, or as a Dataset object. In the latter case, the last positive timepoint will be used.
        @type heatingdata: Dataset, numpy.ndarray
        @param q: If the heating data is supplied as an explicit difference curve corresponding to a different q grid, then the data is resampled and the original q vector must be supplied.
        @type q: numpy.ndarray
        @param qrange: The q-range over which to scale the heating signal. Defaults to 1.7/A < q < 2.4/A.
        @type qrange: list
        @return: The weight of the heating signal for each timepoint.
        @rtype: numpy.ndarray
        """

        zL,xL,yL,zQuant,xQuant,yQuant,zUnit,xUnit,yUnit,dTp_list = self.loaddTpList(dTp, _nolab=True)
        zH,xH,yH,zQuantH,xQuantH,yQuantH,zUnitH,xUnitH,yUnitH,dTp_listH = heatingdata.loaddTpList(HdTp, _nolab=True)
        
        for i,dat in enumerate(zL):
            
            #if zH[i].shape[0]==1:
                
            for j,dep in enumerate(dep):
                
                x = x[i]
                y = y[i]
                z = z[i]
                
                
                # Default q-range if not supplied:
                xIdx,xLim = self._stdIdxLim()
                
        ####
        if trange != None:
            tmin,tmax = idxpt(self.t,trange)
            trange = [self.t[tmin],self.t[tmax]]
        else:
            trange = [self.t[0],self.t[-1]]
        ####

        # Check that the heating vector is of the right size:
        if self.dS.shape[0] != dSheat.shape[0]:
            print("bad shapes of self.dS and dSheat: ", self.dS.shape, dSheat.shape)
            raise(ValueError)

        # This is bullshit: some byte order nonsense.
        dSheat = np.array(dSheat, dtype='>d')

        # The heating vector must be two-dimensional, so add a nonsense dimension:
        if len(dSheat.shape) == 1:
            dSheat = dSheat.reshape([dSheat.shape[0], 1])

        # Scale and subtract heat:
        # Solve Ax=B in the least-squares sense. Heating scaled to data, so B=data, A=heating.
        qmask = (self.q>=qrange[0]) & (self.q<=qrange[1])
        ####
        if tnotch != None:
            tmask = (self.t>=np.min(trange)) & (self.t<=np.max(trange)) & (self.t>=np.max(tnotch)) | (self.t<=np.min(tnotch))
        else:
            tmask = (self.t>=np.min(trange)) & (self.t<=np.max(trange))
        if not tmask.any():
            raise ValueError('Mask excludes all time points!')
        ####
        x, res, rank, s = np.linalg.lstsq(dSheat[qmask], self.dS[qmask,:][:,tmask])

        tprofile = np.zeros_like(self.t)
        tprofile[tmask]=x

        self.dS -= tprofile * dSheat

        return tprofile

    def expSubtractHeat(self, heatingdata, p0=None, fFlag=None, q=None, qrange=None, trange=None, tnotch=None, noSubtract=False, writeLog=None):
        """
        
        """

        from scipy.optimize import leastsq
        dSheat = heatingdata

        # Resample the heating data to the right q-grid, if necessary
        if q != None:
            tmp = np.copy(dSheat)
            dSheat = resample(tmp, q, self.q)

        # Default q-range if not supplied:
        if qrange == None:
            qrange = [1.7, 2.4]

        if trange != None:
            tmin,tmax = idxpt(self.t,trange)
            trange = [self.t[tmin],self.t[tmax]]
        else:
            trange = [self.t[0],self.t[-1]]

        if p0 != None:
            p0 = np.asarray(p0)
        else:
            p0 = np.array([0.001, 1., 0.0])

        if fFlag != None:
            fFlag = np.asarray(fFlag,dtype=bool)

            if fFlag.shape != p0.shape:
                raise ValueError("Number of parameters do not match the number of fixed parameters: ",p0.shape, pfix.shape)
        else:
            fFlag = np.ones_like(p0,dtyp=bool)
        pfree = p0[fFlag]
        pfixed = p0[~fFlag]

        # Check that the heating vector is of the right size:
        if self.dS.shape[0] != dSheat.shape[0]:
            print("bad shapes of self.dS and dSheat: ", self.dS.shape, dSheat.shape)
            raise(ValueError)

        # This is bullshit: some byte order nonsense.
        dSheat = np.array(dSheat, dtype='>d')

        # The heating vector must be two-dimensional, so add a nonsense dimension:
        if len(dSheat.shape) == 1:
            dSheat = dSheat.reshape([dSheat.shape[0], 1])

        # Scale and subtract heat:
        # Solve Ax=B in the least-squares sense. Heating scaled to data, so B=data, A=heating.
        qmask = (self.q>=qrange[0]) & (self.q<=qrange[1])

        if tnotch != None:
            tmask = (self.t>=np.min(trange)) & (self.t<=np.max(trange)) & (self.t>=np.max(tnotch)) | (self.t<=np.min(tnotch))
        else:
            tmask = (self.t>=np.min(trange)) & (self.t<=np.max(trange))
        if not tmask.any():
            raise ValueError('Mask excludes all time points!')

        out = leastsq(expSubtractHeat_residuals,pfree,args=(pfixed,fFlag,self.t[tmask],self.dS[qmask,:][:,tmask],self.sigma[qmask,:][:,tmask],dSheat[qmask,:]),full_output=1)
        p = p0.copy()
        p[fFlag] = out[0]

        self.calcHeat = eval_exp_2Dltd(p,self.t,dSheat)
        if not noSubtract:
            self.dSorig=self.dS
            self.dS = self.dS-self.calcHeat

        #
        self._unit['dSorig']  = r'q'
        self._quant['dSorig'] = r'\Delta\mathrm{S}'
        self._unit['calcHeat']  = r'q'
        self._quant['calcHeat'] = r'\Delta\mathrm{S}'
        #

        redChi2 = np.sum(np.square(expSubtractHeat_residuals(pfree,pfixed,fFlag,self.t[tmask],self.dS[qmask,:][:,tmask],self.sigma[qmask,:][:,tmask],dSheat[qmask,:])))/len(p)
        psig = cov2sigma(out,redChi2)

        if writeLog:
            if type(writeLog)=='bool':
                filename = self._fitLogName
            else:
                filename = writeLog
            write_log(p,psig,'expSubtractHeat_residuals()',filename)
        return (p,psig,redChi2)


    def extSubtractHeat(self, heatingdata, dTp=None, HdTp=None, p0=None, fFlag=None, x=None, xrange=None, yrange=None, ynotch=None, ovrWrt=False, out=False, writeLog=None):
        """
        Need to update to the loaddTp style handling
        """
        from scipy.optimize import leastsq
        z,x,y,zQuant,xQuant,yQuant,zUnit,xUnit,yUnit,dTp_list = self.loaddTpList(dTp, _nolab=True)
        zH,xH,yH,zQuantH,xQuantH,yQuantH,zUnitH,xUnitH,yUnitH,dTp_listH = heatingdata.loaddTpList(HdTp, _nolab=True)
        output=[]
        
        for i in range(len(z)):
            
            if z[i].shape!=zH[i].shape:
                raise ValueError("Shape of the data %s does not match that of the heat data %s."%(z[i].shape,zH[i].shape))
            
            # Default q-range if not supplied:
            if type(xrange)==type(None):
                xrange=[]
                xrange.append(x[i].min())
                xrange.append(x[i].max())
                
            if yrange != None:
                ymin,ymax = get_lim(y[i],yrange)
                yrange = [y[i][ymin],y[i][ymax]]
            else:
                yrange = [y[i][0],y[i][-1]]
            
            if type(p0)!=type(np.array([])) and type(p0)!=bool and type(p0)!=type(None):
                p0 = np.asarray(p0)
            else:
                print("You need to specify the initial parameters!")
                return None
            
            if fFlag != None:
                fFlag = np.asarray(fFlag,dtype=bool)
            
                if fFlag.shape != p0.shape:
                    raise ValueError("Number of parameters do not match the number of fixed parameters: ",p0.shape, pfix.shape)
            else:
                fFlag = np.ones_like(p0,dtype=bool)
            
            pfree = p0[fFlag]
            pfixed = p0[~fFlag]
            
            xmask = (x>=xrange[0]) & (x<=xrange[1])
            
            if ynotch != None:
                ymask = (y[i]>=np.min(yrange)) & (y[i]<=np.max(yrange)) & (y[i]>=np.max(ynotch)) | (y[i]<=np.min(ynotch))
            else:
                ymask = (y[i]>=np.min(yrange)) & (y[i]<=np.max(yrange))
            if not ymask.any():
                raise ValueError('Mask excludes all time points!')
            
            #print pfree
            out = leastsq(simple_residuals,pfree,args=(z[i][xmask,:][:,ymask],pfixed,fFlag,zH[i][xmask,:][:,ymask]),full_output=1)
            p = p0.copy()
            p[fFlag] = out[0]
            #print p
            redChi2 = np.sum(np.square(simple_residuals(p,z[i][xmask,:][:,ymask],pfixed,fFlag,zH[i][xmask,:][:,ymask])))/len(p)

            try:
                psig = cov2sigma(out,redChi2)
            except ValueError:
                psig = None
                print('Standard deviations cannot be computed!')
            output.append([p,psig,redChi2])

            sub = z[i] - scale_data(p,zH[i])

            if ovrWrt:
                self.gen_dTp('%s'%(dTp_list[i]), sub, like_dTp=dTp_list[i])
            elif not ovrWrt or type(yPt)==float:
                self.gen_dTp('%s_sub'%(dTp_list[i]), sub, like_dTp=dTp_list[i])

            if writeLog:
                if type(writeLog)=='bool':
                    filename = self._fitLogName
                else:
                    filename = writeLog
                write_log(p,psig,'simple_residuals()',filename)
            
        if out:
            return output
        else:
            return None

    def timeEvolution(self, dSbasis, q=None, qrange=None):
        """
        Takes N basis spectra S(q), and returns the corresponding time evolution matrix T(t). So, Data(Nq by Nt) = S(Nq by N) (dot) T(N by Nt), and this function calculates T.
        @param dSbasis: The basis spectra.
        @type dSbasis: numpy.ndarray
        @param q: q vector for the basis spectra, if different from that of the data.
        @type q: numpy.ndarray
        @param qrange: q range to include.
        @type qrange: list
        @return: time evolution, and optionally average time evolution for unique time points, and 95% confidence intervals around these averages.
        @rtype: numpy.ndarray
        """

        # Resample the heating data to the right q-grid, if necessary
        if q != None:
            dSbasis = resample(dSbasis, q, self.q)

        # Default q-range if not supplied:
        if qrange == None:
            qrange = [.0, 10.0]

        # This is bullshit: some byte order nonsense.
        dSbasis = np.array(dSbasis, dtype='>d')

        # Find time evolution as explained above.
        mask = (self.q>=qrange[0]) & (self.q<=qrange[1])

        # linalg.lstsq(A, B) solves Ax=B for x.
        # A = S(q), x = T(t), B = Data
        T, res, rank, s = np.linalg.lstsq(dSbasis[mask], self.dS[mask, :])

        # Work out error bars for the case when there are many data points
        # for the sime timepoint.
        t = np.array(uniqueList(self.t))
        conf = np.zeros((T.shape[0], t.shape[0]))
        mean = np.copy(conf)

        # make a list of lists of lists to contain all the coefficients
        # indices are  [basis spectrum][unique timepoint]
        data = []
        for j in range(T.shape[0]):
            data += [[]]
            for i in range(t.shape[0]):
                data[j] += [[]]

        for j in range(T.shape[0]): # for each basis spectrum
            for i in range(self.t.shape[0]): # for each non-unique timepoint
                timeindex = np.where(self.t[i] == t)[0][0]
                if np.isnan(T[j, i]): continue
                data[j][timeindex] += [T[j, i]]
        # go through the unique timepoints for each basis spectrum and get sigmas:
        for j in range(T.shape[0]):
            for i in range(t.shape[0]):
                mean[j,i], conf[j, i] = error(data[j][i], confidence=.95)

        return T, mean, conf

    def basisSpectra(self, Tevol, qrange=None):
        """
        The reverse of timeEvolution, this method returns basis spectra corresponding to a given time evolution. No statistics are calculated.
        @param Tevol: Time evolution of interest.
        @type Tevol: numpy.ndarray
        @param qrange: q-range to include.
        @type qrange: list
        @return: Basis spectra corresponding to the supplied time evolution.
        @rtype: numpy.ndarray
        """

        # Default q-range if not supplied:
        if qrange == None:
            qrange = [.0, 10.0]

        # Find time evolution as explained above.
        mask = (self.q>=qrange[0]) & (self.q<=qrange[1])

        # linalg.lstsq(A.T, B.T) solves (A.T)x.T = B.T for x.T, that is xA=B for x.
        # x = S(q), A = T(t), B = Data
        S_T, res, rank, s = np.linalg.lstsq(Tevol.T, self.dS[mask, :].T)
        return S_T.T

    def dSamplified(self, dTp=None, xpower = None, out=False, ovrWrt=False, reset=False):
        """
        Amplifies a copy of the dataset's difference intensity vector by q**N, without changing the object.
        @param qpower: the power N.
        @type qpower: int, float
        @return: The amplified difference intensities.
        @rtype: numpy.ndarray
        """
        
        xL,yL,zL,xQuant,yQuant,zQuant,xUnit,yUnit,zUnit,dTp_list=self.loaddTpList(dTp, _nolab=True)
        
        if type(xpower)!=type(None):
            xpower = np.int(xpower)
        else:
            xpower = np.int(1)
        
        if xpower==0:
            print("I'm not going to multiply by 1, thats just a waste of time.")
            return
        
        for i,dat in enumerate(zL):
            x,y,dat = self.std_dTp(xL[i],yL[i],dat) 
            fac = np.power(x ,xpower)
            amp = np.zeros_like(dat)

            amp = dat * fac

            if not out:
                rex = re.compile(r'^{}([0-9]+|)\S+'.format(self.getUnit(dTp_list[i])))
                if rex.match(dTp_list[i]):
                    if len(rex.match(dTp_list[i]).groups()[0])==0:
                        rN = 1 + xpower
                    else:
                        rN = int(rex.match(dTp_list[i]).groups()[0]) + xpower
                else:
                    rN = 0 + xpower
                #bork
                if rN==0:
                    lab = r'{0}'.format(dTp_list[i].lstrip(self.getUnit(dTp_list[i])))
                    qlab = r'{0}'.format(self.getQuant(dTp_list[i]).lstrip(self.getUnit(dTp_list[i])))
                elif rN==1:
                    lab = r'{1}{0}'.format(dTp_list[i].lstrip(self.getUnit(dTp_list[i])), self.getUnit(dTp_list[i]))
                    qlab = r'{1}{0}'.format(self.getQuant(dTp_list[i]).lstrip(self.getUnit(dTp_list[i])), self.getUnit(dTp_list[i]))
                else:
                    lab  = r'{1}{2}{0}'.format(dTp_list[i].lstrip(self.getUnit(dTp_list[i])), self.getUnit(dTp_list[i]),rN)
                    qlab = r'{1}^{2}{0}'.format(self.getQuant(dTp_list[i]).lstrip(self.getUnit(dTp_list[i])), self.getUnit(dTp_list[i]),rN)
                    #bork
                if reset:
                    try:
                        delattr(self, '{0}'.format(lab))
                    except:
                        pass

                self.gen_dTp('{0}'.format(lab), amp, quant=qlab, unit=r'{0}'.format(self.getUnit(dTp_list[i])),
                             xAxis=r'{0}'.format(self.getUnit(dTp_list[i])),
                             yAxis=self._asoc[dTp_list[i]]['y'],
                             ovrWrt=ovrWrt)
            else:
                return z


    def shiftAxis(self, axis=None, dX=None):
        """
        """
        axL  = []
        dTpL = []
        if type(axis)!=type(None) and type(axis)!=bool and isinstance(axis, collections.Iterable):
            if type(axis)==str:
                axis = [axis]
            for ax in axis:
                axL.append(self.getdTp(ax))
                dTpL.append(ax)
        else:
            raise TypeError

        if type(dX)!=type(None):
            dX = np.float(dX)
        else:
            dX = 0.

        for i,ax in enumerate(axL):
            ax += np.ones_like(ax)*dX
            print("Shifted {0} by {1!s}.".format(dTpL[i], dX))

        return


    def rebaseAxis(self, axis=None, factor=1., unit=None):
        axL  = []
        dTpL = []
        if type(axis)!=type(None) and type(axis)!=bool and isinstance(axis, collections.Iterable):
            if type(axis)==str:
                axis = [axis]
            for ax in axis:
                axL.append(self.getdTp(ax))
                dTpL.append(ax)
        else:
            raise TypeError
        if type(factor)!=type(None):
            factor = np.float(factor)
        else:
            factor = 1.

        for i,el in enumerate(axL):
            print("Axis {0} with unit {1}.".format(dTpL[i], self.getUnit(dTpL[i])))
            if type(unit)!=type(None):
                self.setUnit(dTpL[i], unit)
            el *= factor
            print("Scaled {0} by {1!s} now has unit {2}.".format(dTpL[i], factor, self.getUnit(dTpL[i])))

        return


    def rebaseData(self, dTp=None, factor=1., unit=None, quant=None):


        xL,yL,zL,xQuant,yQuant,zQuant,xUnit,yUnit,zUnit,dTp_list=self.loaddTpList(dTp, _nolab=True)

        for i,z in enumerate(zL):
            x,y,z = xL[i],yL[i],zL[i]

            if type(factor)!=type(None):
                factor = np.float(factor)
            else:
                factor = 1.

            print("Dataset {0} with unit {1}.".format(dTp_list[i], self.getUnit(dTp_list[i])))
            if type(unit)!=type(None):
                self.setUnit(dTp_list[i], unit)
            if type(quant)!=type(None):
                self.setQuant(dTp_list[i], quant)
            z *= factor
            print("Scaled {0} by {1!s} now has unit {2} and quantity {3}.".format(dTp_list[i], factor, self.getUnit(dTp_list[i]), self.getQuant(dTp_list[i])))

        return


    def removeTime(self, times):
        """
        Removes a certain timepoint (or sequence of timepoints) from the dataset.
        @param times: The timepoint(s) to strip.
        @type times: float, int, list
        """
        if not hasattr(times, '__iter__'):
            times = [times]
        for t in times:
            mask = (self.t != t)
            self.S  = self.S[:, mask]
            self.dS = self.dS[:, mask]
            self.t  = self.t[mask]
            self.Nrepeats = self.Nrepeats[mask]


    def removePoints(self, axis=None, points=None, indices=None, mask=None,
                     ovrWrt=False, reset=False, out=False):
        """
        Method to remove datapoints from all dTp's associated with a specified axis.
        kwargs:
            axis    (list)               : list of existing x/y axes, ie. 'q', 'q_bin', 't', 't_bin', etc.
            points  (list of lists)      : list of lists of points to be removed from all dTp's associated with 'axis'.
                                           The number of lists must correspond to the length of 'axis'.
            indices (list of list)       : list of a list of indices to be removed from all dTp's associated with 'axis'.
                                           The number of lists must correspond to the length of 'axis'.
            mask    (list of array-like) : list of boolean arrays of points to be removed from all dTp's associated 
                                           with 'axis'. Must have the same shape as axis.
                                           The number of lists must correspond to the length of 'axis'.

        Method will remove all points specified EITHER by points (uses np.searchsorted to get the correct
        points), indices, or mask, prioritized in this order.
        """

        assert type(axis)!=type(None) or type(axis)!=bool, "\'axis\' cannot be {0}".format(type(axis).__name__)
        assert type(points)!=type(None) or type(indices)!=type(None) or type(mask)!=type(None), "Either \'points\', \'indices\', or \'mask\' must be defined."

        if isinstance(points, collections.Iterable) and type(points)!=str:
            points = np.asarray(points)
        elif type(points)!=type(None):
            raise TypeError("\'points\' must be a array-like, not {0}".format(type(points).__name__))

        if isinstance(indices, collections.Iterable) and type(indices)!=str:
            indices = np.asarray(indices)
        elif type(indices)!=type(None):
            raise TypeError("\'indices\' must be a array-like, not {0}".format(type(indices).__name__))

        if isinstance(mask, collections.Iterable) and type(mask)!=str:
            mask = np.asarray(mask)
        elif type(mask)!=type(None):
            raise TypeError("\'mask\' must be a array-like, not {0}".format(type(mask).__name__))

        D={}
        test=[]
        for candidate in axis:
            if self.testdTpObj(candidate, suppress=True):
                dTpL = []
                for key in list(self._asoc.keys()):
                    if self._asoc[key]['x']==candidate:
                        dTpL.append(key)
                        test.append('x')
                    if self._asoc[key]['y']==candidate:
                        dTpL.append(key)
                        test.append('y')
                D.update({candidate : dTpL})
            else:
                axis.remove(candidate)

        for k,key in enumerate(D.keys()):

            ax  = self.getdTp(key)

            if type(points)!=type(None):
                indices = get_lim(ax.ravel(), points)
                points  = ax.ravel()[indices]
            if type(indices)!=type(None):
                mask          = np.zeros_like(ax.ravel(), dtype=bool)
                mask[indices] = True
            if mask.ravel().shape != ax.ravel().shape:
                raise ValueError("mask shape ({0}) does not match {1} shape ({2})".format(mask.ravel().shape, key, ax.ravel().shape))

            mask = mask.reshape(ax.shape).ravel()

            dTp = D[key]
            xL,yL,zL,xQuant,yQuant,zQuant,xUnit,yUnit,zUnit,dTp_list=self.loaddTpList(dTp, _nolab=True)

            for i,z in enumerate(zL):

                #x,y,z    = xL[i],ax,z

                can = [k if self._asoc[dTp_list[i]][k]==key else None for k in list(self._asoc[dTp_list[i]].keys())]
                [can.remove(None) for k in can]
                if can[0]=='y':
                    shape = (z.shape[0],z.shape[1],ax.ravel()[~mask].size)
                    msk   = np.tile(mask, (z.shape[0],z.shape[1],1)) #might need a tweak
                elif can[0]=='x':
                    shape = (z.shape[0],ax.ravel()[~mask].size,z.shape[2])
                    msk = np.tile(mask.reshape(-1,1), (z.shape[0],1,z.shape[2]))
                else:
                    raise TypeError("Noooope")

                zz = z[~msk].reshape(shape)

                if not ovrWrt:
                    lab   = '{0}_prn'.format(dTp_list[i])
                    xAx   = '{0}_prn'.format(self._asoc[dTp_list[i]]['x']) if test[i]=='x' else '{0}'.format(self._asoc[dTp_list[i]]['x'])
                    yAx   = '{0}_prn'.format(self._asoc[dTp_list[i]]['y']) if test[i]=='y' else '{0}'.format(self._asoc[dTp_list[i]]['y'])
                    axlab = '{0}_prn'.format(key)
                else:
                    lab   = '{0}'.format(dTp_list[i])
                    xAx   = '{0}'.format(self._asoc[dTp_list[i]]['x'])
                    yAx   = '{0}'.format(self._asoc[dTp_list[i]]['y'])
                    axlab = '{0}'.format(key)

                if reset:
                    delattr(self, lab)
                #kneut
                if i==0:
                    self.gen_dTp(axlab, self.std_dims(ax.ravel()[~mask.ravel()], ax=test[i]), like_dTp=key, axis=True, ovrWrt=ovrWrt)

                self.gen_dTp(lab, zz, like_dTp=dTp_list[i],
                            xAxis=xAx,
                            yAxis=yAx,
                            ovrWrt=ovrWrt)

        return


    def mergeAxis(self, dTp=None, RdTp=None, new_instance=False, ovrWrt=False, reset=False,
                  out=False):
        """
        Only works with the y axis.
        """

        xL,yL,zL,xQuant,yQuant,zQuant,xUnit,yUnit,zUnit,dTp_list=self.loaddTpList(dTp, _nolab=True)

        RxL,RyL,RzL,RxQuant,RyQuant,RzQuant,RxUnit,RyUnit,RzUnit,RdTp_list=self.loaddTpList(RdTp, _nolab=True)

        if new_instance:
            NI = libwaxs.Dataset(dummy=True)
        else:
            NI = self

        for i,z in enumerate(zL):
            if len(RzL)==1:
                Rx,Ry,Rz = RxL[0],RyL[0],RzL[0]
            elif len(RzL)==len(zL):
                Rx,Ry,Rz = RxL[i],RyL[i],RzL[i]
            else:
                raise ValueError("The number of RdTp needs to match that of dTp or equal to 1.")

            x,y,z = xL[i],yL[i],zL[i]

            if x.shape!=Rx.shape:
                raise ValueError("")
            else:
                if not np.isclose(x,Rx).all():
                    raise ValueError("")
                
            ts = y.squeeze()
            tl = Ry.squeeze()
            mask = ~(~(tl<ts.min())*~(tl>ts.max()))

            tsL  = ts.tolist()
            tlL  = tl[mask].tolist()
            t_u  = np.asarray(tsL + tlL)
            args = np.argsort(t_u)
            t    = t_u[args]
            new  = np.hstack((z.squeeze(), Rz.squeeze()[:,mask]))[:,args]

            mx, my, mz = self.std_dTp(x,t,new)

            if ovrWrt:
                ylab = '%s'%(self._asoc[dTp_list[i]]['y'])
                zlab = '%s'%(dTp_list[i])
            else:
                ylab = '%s_mrg'%(self._asoc[dTp_list[i]]['y'])
                zlab = '%s_mrg'%(dTp_list[i])

            if reset:
                try:
                    delattr(NI, zlab)
                except AttributeError:
                    pass

            if new_instance:
                NI.gen_dTp(self._asoc[dTp_list[i]]['x'], mx, unit = xUnit[i], quant = xQuant[i],
                           axis=True,
                           ovrWrt=ovrWrt)
                #bork

            NI.gen_dTp(ylab, my, unit = yUnit[i], quant = yQuant[i],
                       axis=True,
                       ovrWrt=ovrWrt)

            NI.gen_dTp(zlab, mz, unit = zUnit[i], quant = zQuant[i],
                       xAxis=zUnit[i], yAxis=ylab,
                       ovrWrt=ovrWrt)

        if new_instance:
            return NI
        else:
            return


    def xAdjust(self, dTp=None, RdTp=None, SigmadTp=None, xLim=None, yLim=None, RyPt=None, 
                init_shift=0.99, dep=0, general=False,
                reset=False, ovrWrt=False, out=False, plot=True):
        """
        Method that adjusts the existing q-range to a reference q-range (qRef)
        by means of a least-squares fit to a reference scattering curve (dSRef).
        RdTp is the reference to which the x axis of the dTp is adjusted.
        """
        from scipy.optimize import leastsq
        
        xL,yL,zL,xQuant,yQuant,zQuant,xUnit,yUnit,zUnit,dTp_list=self.loaddTpList(dTp, _nolab=True)
        RxL,RyL,RzL,RxQuant,RyQuant,RzQuant,RxUnit,RyUnit,RzUnit,RdTp_list=self.loaddTpList(RdTp, _nolab=True)
        
        if len(zL)!=1 or len(RzL)!=1:
            raise ValueError("Only one dTp and reference dTp can be given!")
        
        Rx,Ry,Rz = self.std_dTp(RxL[0],RyL[0],RzL[0])
        x, y, z  = self.std_dTp(xL[0],yL[0],zL[0])
        
        if type(SigmadTp)!=type(None):
            SigmaxL,SigmayL,SigmazL,SigmaxQuant,SigmayQuant,SigmazQuant,SigmaxUnit,SigmayUnit,SigmazUnit,SigmadTp_list=self.loaddTpList(SigmadTp, _nolab=True)
            Sigmax, Sigmay, Sigmaz  = self.std_dTp(SigmaxL[0],SigmayL[0],SigmazL[0])
        else:
            Sigmaz = np.ones_like(z)
        
        yIdx,yLim = self._stdIdxLim(y.squeeze(), yLim, slice=True)
        ySlice = slice(*yIdx)
        
        if type(RyPt) != type(None) and len(RyPt)==1:
            RyIdx,_ = self._stdIdxLim(y.squeeze(), RyPt, slice=True)
            RySlice = slice(*RyIdx)
        elif type(RyPt) == type(None):
            RyIdx,RyLim = self._stdIdxLim(Ry[0,0,:], yLim, slice=True)
            RySlice = slice(*RyIdx)
        else:
            raise ValueError("The value of kwarg RyPt must have a length of 1.")
        
        xIdx,xLim = self._stdIdxLim(x[0,:,0], xLim, slice=True)
        xSlice = slice(*xIdx)
        
        RxIdx,RxLim = self._stdIdxLim(Rx[0,:,0], xLim, slice=True)
        RxSlice = slice(*RxIdx)

        dat = np.mean(z[dep,xSlice,ySlice], axis=-1)
        ref = np.mean(Rz[dep,RxSlice,RySlice], axis=-1)

        #Take average over yLim, for better fit!
        ###
        # Initial guess
        A = (np.max(ref)-np.max(ref)/2.)/(np.max(dat)-np.max(dat)/2.)
        stretch = init_shift
        params = [A, stretch]

        out = leastsq(xAdjust_residuals,params,args=(x[0,xSlice,0],dat,ref,Rx[0,RxSlice,0],np.mean(Sigmaz[dep,RxSlice,RySlice], axis=-1)),full_output=1)

        p = out[0]
        
        xcorr = (x*p[1])
        
        redChi2 = np.sum(np.square(xAdjust_residuals(p, x[0,xSlice,0],dat,ref,Rx[0,RxSlice,0],np.mean(Sigmaz[dep,RxSlice,RySlice], axis=-1))))/len(p)
        psig = cov2sigma(out,redChi2)
        
        if plot:
            from matplotlib import ticker as mplt
            fig, ax = plt.subplots(ncols=1, nrows=1, squeeze=True, sharex=True, gridspec_kw={'wspace': 0.0,'hspace':0.0})

            ax.plot(x[0,xSlice,0], np.mean(z[0,xSlice,ySlice], axis=-1), lw=2, color='black', ls='--', label='original')
            ax.plot(Rx[0,RxSlice,0], ref*(1./p[0]), lw=2,color='blue', label='reference')
            ax.plot((x[0,xSlice,0]*p[1]), np.mean(z[0,xSlice,ySlice], axis=-1), color='red', label='adjusted ${}$'.format(xQuant[0]))
            xl = ax.get_xlim()
            yl = np.mean(ax.get_ylim())
            ax.text(np.min(xl),yl,'Amplitude: {0}\n${2}$ scale: {1}'.format(np.round(p,3)[0],np.round(p,3)[1], xQuant[0]), va='top', ha='left')
            ax.legend(loc=0, frameon=False)
            ax.set_xlabel((r'${0}'+r'\ '+r'({1})'+r'$').format(xQuant[0], xUnit[0]))
            ax.set_ylabel((r'${0}'+r'\ '+r'({1})'+r'$').format(zQuant[0], zUnit[0]))
            fig.suptitle('xAdjust(\'{0}\')'.format(dTp_list[0].replace('_','\_')))
            ax.axhline(linestyle=':',color='black')
            ax.xaxis.set_major_formatter(mplt.ScalarFormatter(useOffset=False, useMathText=True))
            ax.xaxis.set_major_locator(mplt.MaxNLocator(nbins=9, prune='lower'))
            ax.xaxis.set_minor_locator(mplt.AutoMinorLocator())
            #scalF = mplt.ScalarFormatter(useOffset=True, useMathText=True)
            #scalF.set_scientific=True
            #ax.yaxis.set_major_formatter(scalF)
            ax.ticklabel_format(style='sci', axis='y', scilimits=(-1,1))
            ax.yaxis.set_major_locator(mplt.MaxNLocator(nbins=9, prune='lower'))
            ax.yaxis.set_minor_locator(mplt.AutoMinorLocator())

        if ovrWrt:
            lab = '%s'%(self.getUnit(dTp_list[0]))
        else:
            lab = '%s_adj'%(self.getUnit(dTp_list[0]))

        if reset:
            try:
                delattr(self, lab)
            except AttributeError:
                pass

        if not general:
            self.gen_dTp(lab, xcorr, like_dTp=dTp_list[0],
                        xAxis=zUnit[0],
                        yAxis=self._asoc[dTp_list[0]]['y'],
                        ovrWrt=ovrWrt)
        else:
            self.rebaseAxis(axis=zUnit[0], factor=p[1], unit=None)

        if out:
            return (p,psig,redChi2)


    def subtractBackGround(self, dTp=None, yLim=None, xLim=None, ovrWrt=False, out=False):
        """
        Self explanatory really. A background is subtracted from the whole dataset.
        If yLim is a range, then the average value of this range is subtracted.
        If xLim is a range, then the average value of this range is subtracted.
        If both are set, the the average value of the plane spanned by the ranges is subtracted.
        If xLim and yLim is are both a single point, the value at the intersection of those points is subtracted.
        If yLim is a single point, the values of the whole x range at that point is subtracted.
        If xLim is a single point, the values of the the first y point at that point is subtracted.
        if neither xLim or yLim is specified, the whole x range at the first y point is subtracted.
        """
        
        xL,yL,zL,xQuant,yQuant,zQuant,xUnit,yUnit,zUnit,dTp_list=self.loaddTpList(dTp, _nolab=True)
        
        for i,dat in enumerate(zL):
            x,y,dat = self.std_dTp(xL[i],yL[i],dat)
            sub = np.zeros_like(dat)
            
            y = y.squeeze()
            x = x.squeeze()
            
            for j,dep in enumerate(dat):
                
                yLim  = np.asarray(yLim)
                yIdx = get_lim(y[j], yLim)
                Yslice = slice(*yIdx)
                
                if type(xLim)==type(None):
                    avg_x = False
                else:
                    avg_x = True
                    xLim  = np.asarray(xLim)
                    xIdx = get_lim(x[j], xLim)
                    Xslice = slice(*xIdx)
                
                if avg_x:
                    avg = np.average(dep[Xslice,Yslice])
                    bkg = np.dot(avg,np.ones_like(x[j]))
                else:
                    avg = np.average(dep[:,Yslice], axis=1)
                    bkg = avg
               
                sub[j] = dep - bkg.reshape(-1,1)

            if not out:
                if ovrWrt:
                    self.gen_dTp('%s'%(dTp_list[i]), sub, like_dTp=dTp_list[i],
                                 xAxis=self._asoc[dTp_list[i]]['x'],
                                 yAxis=self._asoc[dTp_list[i]]['y'],
                                 ovrWrt=ovrWrt)
                else:
                    self.gen_dTp('%s_bkg'%(dTp_list[i]), sub, like_dTp=dTp_list[i],
                                 xAxis=self._asoc[dTp_list[i]]['x'],
                                 yAxis=self._asoc[dTp_list[i]]['y'],
                                 ovrWrt=ovrWrt)
            else:
                return sub
        return


    def getChirp(self, dTp=None, yLim=None, xLim=None, level='auto', dep=0, plot=False,
                 plot_kws={}, deg=4, polyfit_kws={}, plt_lvls=25, lvl_scale=1., linewidths=0.5,
                 write=False, path='./chirp.dat', reset=False, out=False, ovrWrt=False):

        import matplotlib
        import matplotlib.pyplot as plt

        xL,yL,zL,xQuantL,yQuantL,zQuantL,xUnitL,yUnitL,zUnitL,dTp_list=self.loaddTpList(dTp, _nolab=True)

        oCx=[]
        oCy=[]
        oCz=[]

        if dep>len(self.dep):
            raise ValueError("")

        for i,z in enumerate(zL):
            x,y,z = self.std_dTp(xL[i],yL[i],z)

            dat = z[dep]

            yIdx   = find_closest(y[dep].squeeze(), yLim)
            ySlice = slice(*yIdx)

            xIdx   = find_closest(x[dep].squeeze(), xLim)
            xSlice = slice(*xIdx)

            if level=='auto':
                lvl = np.diff((dat[xSlice,ySlice].min(),dat[xSlice,ySlice].max()))/2.
            elif level=='pick':
                lvl = None
            elif type(level)==(float or np.float or int or np.int):
                lvl = np.asarray([level])
            elif isinstance(level, Iterable) and type(level)!=str:
                lvl = np.asarray(level)
            else:
                raise NotImplementedError("level kwarg can take the value \'auto\' or a float/int or a list-like.")

            if type(lvl)!=type(None):
                Cs = []
                #use contour function from matplotlib to determine the points to which we fit the polynomial
                for j,lv in enumerate(lvl):
                    fig,ax = plt.subplots()
                    Cs.append(ax.contour(x[dep,xSlice,0],y[dep,0,ySlice],dat[xSlice,ySlice].T, lv))
                    plt.close(fig)

                paths    = [cs.collections[0].get_paths() for cs in Cs]
                vertices = [p.vertices for p in np.hstack(paths)]

            else:
                fig,ax = plt.subplots()
                #x[dep,xSlice,0],y[dep,0,ySlice],
                xmin,xmax = x[dep,xSlice,0].min(),x[dep,xSlice,0].max()
                ymin,ymax = y[dep,0,ySlice].min(),y[dep,0,ySlice].max()
                #cp = ax.imshow(dat[xSlice,ySlice].T, interpolation='nearest', aspect='auto',
                               #extent=(xmin,xmax,ymax,ymin))
                if False:
                    #old method..
                    import mpldatacursor as mdc
                    cp = ax.contour(x[dep,xSlice,0], y[dep,0,ySlice], dat[xSlice,ySlice].T,
                                    plt_lvls, linewidths=linewidths)

                    annot = mdc.datacursor(cp, display='multiple')
                    plt.show(block=True)

                    _v = np.asarray([[an.xdata,an.ydata] for an in list(annot.annotations.keys())])

                else:
                    mi = dat[xSlice,ySlice].min()
                    ma = dat[xSlice,ySlice].max()
                    dm = ma-mi
                    vmax = dm/lvl_scale
                    vmin = - dm/lvl_scale
                    #cp = ax.contour(x[dep,xSlice,0], y[dep,0,ySlice], dat[xSlice,ySlice].T, plt_lvls, linewidths=linewidths)
                    cf = ax.contourf(x[dep,xSlice,0], y[dep,0,ySlice], dat[xSlice,ySlice].T, plt_lvls,
                                     vmax=vmax, vmin=vmin, ) #cmap='rainbow'
                    _v = np.asarray(plt.ginput(n=50,timeout=0, show_clicks=True, mouse_add=1, mouse_pop=3, mouse_stop=2))
                    #ax.plot(_v[:,0], _v[:,1], marker='+', color='red',pointsize=2.)

                if _v.size==0:
                    raise ValueError("No points selected!!")

                _vmin = _v[:,1].min()
                idx = np.argsort(_v[:,0])
                vertices = [_v[idx,:]]
                lvl = np.array([0.])

                #raise NotImplementedError("Debug")

            pars = []
            chrp = []
            for v in vertices:
                vmin = v[:,0].min()
                par = np.polyfit(v[:,0]-vmin,v[:,1], deg, **polyfit_kws)
                chrp += [np.polyval(par, x[dep]-vmin).squeeze()]
                pars += [par]

            if plot:
                fig,ax = plt.subplots()


                plot_kws.update({'Ax':[ax],
                                 'xLim':xLim,
                                 'yLim':yLim,
                                 'dep':dep,
                                 })

                self.plotCont(dTp=dTp_list[i], **plot_kws)
                [ax.plot(x[dep,xSlice], ch[xSlice], color='red', linewidth=2) for ch in chrp]
                plt.show(block=False)

            lvl    = lvl[None,None,:]
            chirp  = np.asarray(chrp).T[None,:,:]
            x_chrp = x[dep]

            xo,yo,zo = self.std_dTp(x_chrp,lvl,chirp)

            xLab = '{0}_chirp'.format(self._asoc[dTp_list[i]]['x'])
            yLab = '{0}_level'.format(dTp_list[i])
            zLab = '{0}_chirp'.format(dTp_list[i])
            oCx.append(xLab)
            oCy.append(yLab)
            oCz.append(zLab)

            if not out:
                if reset:
                    for el in [xLab, yLab, zLab]:
                        try:
                            delattr(self, el)
                        except AttributeError:
                            pass

                self.gen_dTp(xLab, xo, axis=True, like_dTp=self._asoc[dTp_list[i]]['x'])
                self.gen_dTp(yLab, yo, axis=True, quant=r'level', unit=zUnitL[i])
                self.gen_dTp(zLab, zo, unit=yUnitL[i], quant=yQuantL[i],
                                xAxis=xLab,
                                yAxis=yLab,
                                ovrWrt=ovrWrt)

            if write:
                _path,fn = os.path.split(os.path.abspath(path))
                os.mkdir(_path) if not os.path.isdir(_path) else None
                if os.path.isfile(os.path.join(_path,fn)):
                    base, ext = os.path.splitext(fn)
                    fn = base + '_' + dTp_list[i] + ext
                np.savetxt(os.path.join(_path,fn), np.vstack((xo.squeeze(), zo.squeeze())).T)

        if out:
            return [[getattr(self, oCx[k]), getattr(self, oCy[k]), getattr(self, oCz[k])] for k in range(len(zL))]
        else:
            return


    def correctChirp(self, dTp=None, chirp_dTp=None, level=0, interp_kws={}, resample=True,
                     reset=False, plot=False, plotY=None, plotX=None, plotDep=0, ovrWrt=False, out=False):

        """
        """

        xL,yL,zL,xQuantL,yQuantL,zQuantL,xUnitL,yUnitL,zUnitL,dTp_list=self.loaddTpList(dTp, _nolab=True)
        import scipy as sp

        if type(chirp_dTp)==type(None):
            raise TypeError("Chirp needs to be defined in dTp-style formatting.")
        #if len(chirp_dTp)>1:
            #raise ValueError("Only 1 chirp can be subtracted. \'chirp_dTp\' has length {0}.".format(len(chirp_dTp)))

        xLe,yLe,zLe,xQuantLe,yQuantLe,zQuantLe,xUnitLe,yUnitLe,zUnitLe,dTp_list_e = self.loaddTpList(chirp_dTp, _nolab=True)
        xc,yc,zc = self.std_dTp(xLe[0],yLe[0],zLe[0])

        kws={'axis':-1, 'fill_value':'extrapolate', 
             'kind':'linear', 'assume_sorted':True}
        kws.update(interp_kws)

        oCx=[]
        oCy=[]
        oCz=[]

        for i,z in enumerate(zL):

            x,y,z = self.std_dTp(xL[i],yL[i],z)

            f    = sp.interpolate.interp1d(xc.squeeze(), zc.squeeze(), axis=0, fill_value="extrapolate")
            ch   = f(x)

            cory = np.tile(y, (1,x.shape[1],1)) - ch
            corz = np.zeros_like(cory)

            if resample:
                for j,dep in enumerate(z):

                    g    = [sp.interpolate.interp1d(cory[j,k,:], dep[k,:], **kws) for k in range(x[j,:,0].size)]

                    corz[j,:,:] = [g[k](y[j].squeeze()) for k in range(x[j,:,0].size)]
            else:
                corz = z

            if plot:
                fig,ax = plt.subplots(ncols=2, sharex=True,sharey=True, figsize=(12,6),
                                      gridspec_kw={'wspace':0.0, 'hspace':0.0})
                grid_x,grid_y = np.meshgrid(x[plotDep,:,0],y[plotDep,0,:], indexing='ij')
                ax[0].contour(grid_x,grid_y, z[plotDep], 20, linewidths=0.5, colors='black')
                ax[0].contourf(grid_x,grid_y, z[plotDep], 50, cmap='RdBu')
                ax[1].contour(grid_x,grid_y, corz[plotDep], 20, linewidths=0.5, colors='black')
                ax[1].contourf(grid_x,grid_y, corz[plotDep], 50, cmap='RdBu')

                ax[0].set_ylim(plotY)
                ax[0].set_xlim(plotX)

                plt.show(block=False)

            xo,yo,zo = self.std_dTp(x,y if resample else cory, corz)

            xLab = '{0}'.format(self._asoc[dTp_list[i]]['x'])
            yLab = '{0}{1}'.format(self._asoc[dTp_list[i]]['y'], '' if (resample or ovrWrt) else '_chcor')
            zLab = '{0}{1}'.format(dTp_list[i], '' if ovrWrt else '_chcor')

            oCx.append(xLab)
            oCy.append(yLab)
            oCz.append(zLab)

            if not out:
                if reset:
                    for el in [yLab, zLab]:
                        try:
                            delattr(self, el)
                        except AttributeError:
                            pass

                self.gen_dTp(yLab, yo, axis=True, like_dTp=self._asoc[dTp_list[i]]['y'])

                self.gen_dTp(zLab, zo, like_dTp=dTp_list[i],
                                xAxis=xLab,
                                yAxis=yLab,
                                ovrWrt=ovrWrt)

            #raise NotImplementedError("Work in progress...")

        if out:
            return [[getattr(self, oCx[k]), getattr(self, oCy[k]), getattr(self, oCz[k])] for k in range(len(zL))]
        else:
            return



    def removeChirp(self, dTp=None, yPt=None, xPt=None, test=False, ext_chrp = None, ext_poly=None, 
                    edge_filter='scharr', diff=1, deg=3, p0=None, extrapolate=True, force_fit=False, 
                    save_fit=False, resample=True, force_y_accros_dep=True, ovrWrt=False, out=False):
        """
        DEPRECATED
        Uses the sci-kit image package : Stéfan van der Walt, Johannes L. Schönberger, Juan Nunez-Iglesias, François Boulogne, Joshua D. Warner, Neil Yager, Emmanuelle Gouillart, Tony Yu and the scikit-image contributors. scikit-image: Image processing in Python. PeerJ 2:e453 (2014) http://dx.doi.org/10.7717/peerj.453 
        #Not Uses lmfit: http://dx.doi.org/10.5281/zenodo.11813

        The function removes the dispersion from time-resolved spectroscopic measurements.
        I want to make this automatic... 1st idea is to fit a step function to all frequecies and fit a polynomial through the fitted t0.
        2nd idea, is to use a Sobel edge filter, then fit a polynomial through the max or 1st derivative.
        The coherent artifact should pop out much stronger than the other dynamics.
        Filter types available are: roberts, sobel, scharr, prewitt.
        """

        x,y,z,xQuant,yQuant,zQuant,xUnit,yUnit,zUnit,dTp_list=self.loaddTpList(dTp, _nolab=True)
    
        for i,dat in enumerate(z):

            ### Use std_dims here instead of getPtIdx.
            idxY = self.getPtIdx( y[i], yPt, inc_end=False)
            idxX = self.getPtIdx( x[i], xPt, inc_end=False)

            x,y,dat = self.std_dTp(x[i],y[i],dat)

            #idxX,idxY=[],[]
            #for j in xrange(dat.shape[0]):
                #idxX.append(self._stdIdxLim( x[j], xPt, slice=True)[0])
                #idxY.append(self._stdIdxLim( y[j], yPt, slice=True)[0])
            #idxX = np.asarray(idxX)
            #idxY = np.asarray(idxY)

            
            # first find maximum of the 1st derivative  of the edge within range of dispersion.
            # Make sure of a monotonic increase in the value of the time point of the maximum with increasing wavelength.
            # Fit polynomial to the points
            
            #raise NotImplementedError

            if idxX.shape[1]<2:
                idxX = np.tile(idxX.squeeze(), (dat.shape[0],y.shape[2],1))
            elif idxX.shape[1] != dat.shape[1]:
                raise ValueError("Number of x limits (%s) does not match the number of y ranges (%s)!"%(idxX.shape[1], dat.shape[2]))
            
            if idxY.shape[1]<2:
                idxY = np.tile(idxY.squeeze(), (dat.shape[0],x.shape[1],1))
            elif idxY.shape[1] != dat.shape[2]:
                raise ValueError("Number of y limits (%s) does not match the number of x ranges (%s)!"%(idxY.shape[1], dat.shape[1]))
            
            if type(ext_poly)!=type(None):
                # Generate correct shape here
                
                ext_poly = self.tile_repeat(ext_poly)
                ext_chrp = np.polyval()
            
            if type(ext_chrp)!=type(None):
                # Generate correct shape here
                ext_chrp = self.std_dims(ext_chrp, ax='x')
                if ext_chrp.shape!=x.shape:
                    if ext_chrp.shape[-2]==x[-1].shape and ext_chrp.shape[-1]!=y[-2].shape:
                        ext_chrp = np.transpose(ext_chrp, axes=(0,2,1))
                    else:
                        raise ValueError("")
                else:
                    print("External chirp accepted.")
                ext_chrp = self.tile_repeat(z[i],ext_chrp)
            
            y_maxL = []
            fitL   = []
            covL   = []
            chrpL  = []
            funcL  = []
            for j,dep in enumerate(dat):
                if type(ext_chrp)!=type(None):
                    chrpL.append(ext_chrp[j])
                elif test:
                    import scipy as sp
                    from skimage import filters
                    xSlice = np.asarray([slice(*idxX[j,s]) for s in range(idxX[j].shape[0])])
                    ySlice = np.asarray([slice(*idxY[j,s]) for s in range(idxY[j].shape[0])])
                    edge   = filters.__getattribute__(edge_filter)(dep) # [1:-1] because the edge of the data is always very sharp
                    
                    if not diff:
                        xtr = edge
                    else:
                        xtr = np.diff(edge, axis=-1, n=diff)
                    
                    idx_edge_sorted = np.argsort(np.abs(xtr), axis=-1, kind='quicksort')
                    
                    y_xtp           = np.polyval(fit,x_xtp)
                    #bork
                else:
                    import scipy as sp
                    from skimage import filters
                    xSlice = np.asarray([slice(*idxX[j,s]) for s in range(idxX[j].shape[0])])
                    ySlice = np.asarray([slice(*idxY[j,s]) for s in range(idxY[j].shape[0])])
                    edge   = filters.__getattribute__(edge_filter)(dep) # [1:-1] because the edge of the data is always very sharp
                    
                    if not diff:
                        xtr = edge
                    else:
                        xtr = np.diff(edge, axis=1, n=diff)
                    
                    #if y[j].shape[0]==1:
                        #if y[j].shape[1]==1:
                    y_max = y[j].squeeze()[np.argmax(xtr[xSlice[j],ySlice[j]], axis=-1)+idxY[j,xSlice[j],0].astype(int)][2:-1]
                    ### to get last edge
                    val = y_max.min()
                    new_y_max = np.zeros_like(y_max)
                    new_y_max[0]=y_max[0]
                    for k,el in enumerate(y_max):
                        if el>val:
                            new_y_max[k] = el
                            val = el
                        else:
                            new_y_max[k] = val
                    y_max,mask  = np.unique(new_y_max,return_index=True)
                    x_max       = np.copy(x[:,xSlice[j]].squeeze()[mask])
                    x_org       = np.copy(x[:,xSlice[j]].squeeze())
                    weight      = np.ones_like(x_max)
                    
                    #exchange last x value for a larger point, forces fit to not squiggle where the function plateaus (not very physical behaviour).
                    if force_fit:
                        #sigma[tmp>0.95*tmp.max()] = 100
                        tmp[-1] = tmp[-1]*1.2
                    elif extrapolate:
                        #extend points for a better fit
                        if type(extrapolate)==tuple:
                            fit_num,xtp_fac = extrapolate
                        elif type(extrapolate)==float or type(extrapolate)==int:
                            fit_num  = extrapolate
                            xtp_fac  = fit_num*2
                        else:
                            fit_num  = 4
                            xtp_fac  = 1.25
                            
                        x_fit    = x_max[-fit_num:]
                        y_fit    = y_max[-fit_num:]
                        fit      = np.polyfit(x_fit,y_fit, deg=1)
                        x_xtp    = np.linspace(x_fit[-1], x_org.max()*xtp_fac, num = (9))
                        y_xtp    = np.polyval(fit,x_xtp)
                        
                        x_max    = x_max[:-1].tolist()
                        x_max.extend(x_xtp)
                        #[x_max.append(el) for el in x_xtp]
                        y_max    = y_max[:-1].tolist()
                        y_max.extend(y_xtp)
                        #[y_max.append(el) for el in y_xtp]
                    y_max = np.asarray(y_max)
                    x_max = np.asarray(x_max)
                    
                    # adapting np.polyval for the fit
                    def poly(x,*p0):
                        p0 = list(p0)
                        return (np.polyval(p0[:-1],(x-p0[-1])))
                    # setting initial parameters highest order first, last paramter is shif in x
                    p0      = np.ones((deg + 2))
                    p0[:-1] = 1e-8
                    p0[-1]  = x_max.max()
                    
                    try:
                        fit,cov = sp.optimize.curve_fit(poly, x_max, y_max, p0=p0, sigma=None, maxfev = 120000)
                    except:
                        print("Dataset unfittable!")
                        return None
                    
                    edge_pts = np.vstack((x_max,y_max))
                    y_maxL.append(edge_pts)
                    fitL.append(fit)
                    covL.append(cov)
                    chrpL.append(poly(x,*fit)[0,:,])
                    funcL.append(poly)
            chrp    = np.asarray(chrpL)
            y_chrp  = np.tile(y, tuple((list(dat.shape)[:2]))+(1,))-chrp
            #x_grid  = np.transpose(self.tile_repeat(np.transpose(dat, axes=(0,2,1)),np.transpose(x, axes=(0,2,1))), axes=(0,2,1))
            if resample:
                import scipy.interpolate
                limYnew = np.asarray([np.max(np.min(y_chrp, axis=-1), axis=-1),np.min(np.max(y_chrp, axis=-1), axis=-1)]).T
                if force_y_accros_dep:
                    limYnew = np.tile([np.max(np.min(limYnew, axis=-1), axis=-1),np.min(np.max(limYnew, axis=-1), axis=-1)], (limYnew.shape[0],1)) 
                
                yIdxNew,lim = self._stdIdxLim(y, limYnew, slice=True)
                
                if lim.max()>limYnew.max():
                    yIdxNew[1] -= 1
                y_rec = y[:,:,slice(*yIdxNew)]
                
                z_rec = np.zeros(dat.shape[:2] + y_rec.shape[2:3])
                
                for index in np.ndindex(*dat.shape[:-1]):
                    g = scipy.interpolate.interp1d(y_chrp[index], dat[index], kind='linear', bounds_error=True, assume_sorted=True)
                    z_rec[index] = g(y_rec)
                z_rec = np.asarray(z_rec)
                #bork
            else:
                y_rec = y_chrp
                z_rec = dat
            
            if not out:
                if ovrWrt:
                    self.gen_dTp('t', y_rec.squeeze(), like_dTp='t', axis=True, ovrWrt=ovrWrt)
                    self.gen_dTp('%s'%(dTp_list[i]), z_rec, like_dTp=dTp_list[i],
                         xAxis=zUnit[i],
                         yAxis='t',
                         ovrWrt=ovrWrt)
                else:
                    self.gen_dTp('t_chrp', y_rec.squeeze(), like_dTp='t', axis=True, ovrWrt=ovrWrt)
                    self.gen_dTp('%s_chrp'%(dTp_list[i]), z_rec, like_dTp=dTp_list[i],
                         xAxis=zUnit[i],
                         yAxis='%s_chrp'%(dTp_list[i]),
                         ovrWrt=ovrWrt)
                
                if save_fit:
                    self.gen_dTp('edge', y_maxL, quant='%s'%(yQuant[i]), unit='%s'%(yUnit[i]),
                                 xAxis=zUnit[i],
                                 yAxis='t',
                                 ovrWrt=ovrWrt)
                    self.gen_dTp('chrp_line', chrp, quant='%s'%(yQuant[i]), unit='%s'%(yUnit[i]),
                                 xAxis=zUnit[i],
                                 yAxis='t',
                                 ovrWrt=ovrWrt)
                    self.gen_dTp('chrp_fit', {'fit':fitL,'cov':covL, 'func':funcL}, quant='', unit='', axis=True, ovrWrt=ovrWrt)
                return None
            else:
                return (z_rec,y_rec,y_maxL,chrp,{'fit':fitL,'cov':covL, 'func':funcL})

    def getRdf(self,*args,**kwargs):
        self.reverseSFT(*args,**kwargs)
        return
    
    # Method that calculates radial distribution function from difference scattering
    # by Fourier sine transformation as described in Kim et al., Nature 2015
    # Input: scattering angle q, difference scattering dS and distance vector r
    # Output: rdf is written to self.rdf
    # As a default the instances dS is used for difference scattering
    # Requires equally spaced q values!!
    def reverseSFT(self, dTp=None, r=None, alpha=None, xLim=None, fromZero=False, r2=True,
                   reset=False, out=False, ovrWrt=False, useDeprecated=False, test=False):
        """
        Method that calculates radial distribution function from difference scattering
        by Fourier sine transformation as described in Kim et al., Nature 2015
        Input: scattering angle q, difference scattering dS and distance vector r
        """
        
        xL,yL,zL,xQuant,yQuant,zQuant,xUnit,yUnit,zUnit,dTp_list=self.loaddTpList(dTp, _nolab=True)
        for i, dat in enumerate(zL):
            print('libwaxs')
            xx = xL[i].squeeze()
            yy = yL[i].squeeze()
            #bork
            dxFT = np.mean(np.diff(xx))
            
            xIdx,xLim = self._stdIdxLim(xx, xLim, slice=True)
            xSlice = slice(*xIdx)
            #print(xIdx,xLim,dxFT)
            
            if fromZero:
                xFT = np.linspace(dxFT,np.max(xx[xSlice]),num=np.size(xx[xSlice]),endpoint=True)
                dFT = resample(dat[:,xSlice,:], xx[xSlice], xFT, axis=1)
                dFT[np.isnan(dFT)] = 0.
            elif len(np.unique(np.diff(xx[xSlice])))!=1:
                xFT = np.linspace(np.min(xx[xSlice]),np.max(xx[xSlice]),
                                  num=np.size(xx[xSlice]),endpoint=True) # the np.max(xx[xSlice])/dxFT is wrong!!!!!!
                dFT = resample(dat[:,xSlice,:], xx[xSlice], xFT, axis=1)
                dFT[np.isnan(dFT)] = 0.
            # if grid is already equally spaced
            else:
                xFT = xx[xSlice]
                dFT = dat[:,xSlice,:]

            xFT = self.std_dims(xFT, ax='x')

            if type(r) != type(None):
                r = np.asarray(r)
            else:
                r = np.linspace(2*np.pi/xFT.min(),2*np.pi/xFT.max(),num=xFT.shape[1]).reshape(xFT.shape)
            r = self.std_dims(r, ax='x')

            #print(r.min(),r.max(), np.unique(np.diff(r, axis=1)), r.shape)
            #print("Calculating sine Fourier transform with damping factor %.2f"%alpha)

            # Numerical integration
            if type(alpha) != type(None):
                alpha = float(alpha)
            else:
                alpha = float(0.03) # damping factor as used by Kim et al.


            # Initialize rdf matrix
            if useDeprecated:
                depL = []
                r = r.squeeze()
                dFTT = np.copy(dFT).squeeze().T
                for j,dep in enumerate(dat):
                    exp_term = np.exp(-(xFT**2)*alpha)
                    whole_term = np.zeros(r.shape) # length of r
                    datr = np.zeros(dFTT.shape)
                    #bork
                    print("### Generating RDF for {} ###".format(dTp_list[i]))
                    for t,_ in enumerate(dFTT):
                        for k,rval in enumerate(r):
                            #print t, k, rval
                            #whole_term = xFT * dFTT[t] * np.sin(xFT * rval) * exp_term * dxFT
                            #datr[t][k] = rval / 2 / (np.pi**2) * np.sum(whole_term)
                            whole_term = xFT * dFTT[t] * np.sin(xFT * rval) * exp_term * dxFT
                            datr[t][k] = (1 / (2 * rval * (np.pi**2))) * np.sum(whole_term)
                        print("Calculating sine Fourier transform for timepoint {}\r".format(yy[t])),
                        sys.stdout.flush()
                    print("")
                    print("### Generated RDF for {} ###".format(dTp_list[i]))
                    depL.append(datr.T)
                rdf = np.asarray(depL)
                #rdf  = self.std_dims(rdf)
            elif test:
                #fac    = (1./(2*r*(np.pi**2)))
                expFac = np.exp(-(np.square(xFT))*alpha)
                rdf    = np.zeros((dat.shape[0], r.shape[1], dat.shape[2]))
                for i in range(r.shape[1]):
                    rdf[:,i,:]   = (1./(2*r[:,i,:]*(np.pi**2))) * np.sum(dFT * xFT * np.sin(xFT*r[:,i,:]) * expFac * dxFT, axis=1)
            else:
                #fac    = (1./(2*r*(np.pi**2)))
                expFac = np.exp(-(np.square(xFT))*alpha)
                rdf    = np.zeros((dat.shape[0], r.shape[1], dat.shape[2]))
                for index in np.ndindex(r.shape[:-1]):
                    rdf[index]   = (1./(2*r[index]*(np.pi**2))) * np.sum(dFT * (xFT * np.sin(xFT*r[index]) * expFac * dxFT), axis=1, keepdims=True)
                    #RDF[:, i, :] = ((r[:,i,:]/(2*(np.pi**2))) * np.nansum(q * S * np.sin(q*r[:,i,:]) * expFac * dq, axis=1, keepdims=False))

            _r,_,rdf = self.std_dTp(r[:,::-1,:],yy,rdf[:,::-1,:])
            #bork
            if not out:
                
                rex = re.compile(r'\S+(h|v)$')
                if rex.match(dTp_list[i]):
                    lab = '{0}r{1}'.format(dTp_list[i].rstrip(rex.match(dTp_list[i]).groups()[0]), rex.match(dTp_list[i]).groups()[0])
                else:
                    lab = '{0}r'.format(dTp_list[i])
                if reset:
                    try:
                        delattr(self, lab)
                        delattr(self, 'r')
                    except:
                        pass

                self.gen_dTp('r', _r, quant=r'r', unit=r'\mathring{\mathrm{A}}', axis=True, ovrWrt=ovrWrt)
                self.gen_dTp(lab, rdf, quant=r'\Delta S', unit=r'r',
                         xAxis=r'r',
                         yAxis=self._asoc[dTp_list[i]]['y'],
                         ovrWrt=ovrWrt)

                if r2:
                    self.dSamplified(dTp=lab, xpower=2, reset=reset)
            else:
                return (_r, rdf)
        return


    def forwardSFT(self, dTp=None, r=None, alpha=None, xLim=None, fromZero=False, r2=True,
                   reset=False, out=False, ovrWrt=False, useDeprecated=False):
        """
        
        """
        
        xL,yL,zL,xQuant,yQuant,zQuant,xUnit,yUnit,zUnit,dTp_list=self.loaddTpList(dTp, _nolab=True)
        
        for i, dat in enumerate(zL):
            xx = xL[i].squeeze()
            yy = yL[i].squeeze()
            #bork
            dxFT = np.mean(np.diff(xx))
            
            xIdx,xLim = self._stdIdxLim(xx, xLim, slice=True)
            xSlice = slice(*xIdx)
            
            if fromZero:
                xFT = np.linspace(0,np.max(xx[xSlice]),num=(np.max(xx[xSlice])/dxFT),endpoint=True)
                dFT = resample(dat[:,xSlice,:], xx[xSlice], xFT, axis=1)
                dFT[np.isnan(dFT)] = 0.
            elif len(np.unique(np.diff(xx[xSlice])))!=1:
                xFT = np.linspace(np.min(xx[xSlice]),np.max(xx[xSlice]),num=(np.max(xx[xSlice])/dxFT),endpoint=True) # the np.max(xx[xSlice])/dxFT is wrong!!!!!!
                dFT = resample(dat[:,xSlice,:], xx[xSlice], xFT, axis=1)
                dFT[np.isnan(dFT)] = 0.
            # if grid is already equally spaced
            else:
                xFT = xx[xSlice]
                dFT = dat[:,xSlice,:]
                
            xFT = self.std_dims(xFT, ax='x')

            if type(r) != type(None):
                r = np.asarray(r)
            else:
                r = np.linspace(2*np.pi/xFT.min(),2*np.pi/xFT.max(),num=xFT.shape[1]).reshape(xFT.shape)
            
            r = self.std_dims(r, ax='x')
            #print("Calculating sine Fourier transform with damping factor %.2f"%alpha)

            # Numerical integration
            if type(alpha) != type(None):
                alpha = float(alpha)
            else:
                alpha = float(0.03) # damping factor as used by Kim et al.


            # Initialize rdf matrix
            #if useDeprecated:
                #depL = []
                #r = r.squeeze()
                #dFTT = np.copy(dFT).squeeze().T
                #for j,dep in enumerate(dat):
                    #exp_term = np.exp(-(xFT**2)*alpha)
                    #whole_term = np.zeros(r.shape) # length of r
                    #datr = np.zeros(dFTT.shape)
                    ##bork
                    #print("### Generating RDF for {} ###".format(dTp_list[i]))
                    #for t,_ in enumerate(dFTT):
                        #for k,rval in enumerate(r):
                            ##print t, k, rval
                            ##whole_term = xFT * dFTT[t] * np.sin(xFT * rval) * exp_term * dxFT
                            ##datr[t][k] = rval / 2 / (np.pi**2) * np.sum(whole_term)
                            #whole_term = xFT * dFTT[t] * np.sin(xFT * rval) * exp_term * dxFT
                            #datr[t][k] = (1 / (2 * rval * (np.pi**2))) * np.sum(whole_term)
                        #print("Calculating sine Fourier transform for timepoint {}\r".format(yy[t])),
                        #sys.stdout.flush()
                    #print("")
                    #print("### Generated RDF for {} ###".format(dTp_list[i]))
                    #depL.append(datr.T)
                #rdf = np.asarray(depL)
                ##rdf  = self.std_dims(rdf)
            #else:
            fac    = (1./(2*r*(np.pi**2)))
            expFac = np.exp(-(np.square(xFT))*alpha)
            rdf    = np.zeros((dat.shape[0], r.shape[1], dat.shape[2]))
            for index in np.ndindex(r.shape[:-1]):
                rdf[index]   = (1./(2*r[index]*(np.pi**2))) * np.sum(dFT * (xFT * np.sin(xFT*r[index]) * expFac * dxFT), axis=1, keepdims=True)

            _r,_,rdf = self.std_dTp(r[:,::-1,:],yy,rdf[:,::-1,:])

            if not out:
                rex = re.compile(r'\S+(h|v)$')
                if rex.match(dTp_list[i]):
                    lab = '{0}r{1}'.format(dTp_list[i].rstrip(rex.match(dTp_list[i]).groups()[0]), rex.match(dTp_list[i]).groups()[0])
                else:
                    lab = '{0}r'.format(dTp_list[i])
                if reset:
                    try:
                        delattr(self, lab)
                        delattr(self, 'r')
                    except:
                        pass

                self.gen_dTp('r', _r, quant=r'r', unit=r'\mathring{\mathrm{A}}', axis=True, ovrWrt=ovrWrt)
                self.gen_dTp(lab, rdf, quant=r'\Delta S', unit=r'r',
                         xAxis=r'r',
                         yAxis=None,
                         ovrWrt=ovrWrt)
                if r2:
                    self.dSamplified(dTp=lab, xpower=2, reset=reset)
            else:
                return (_r, rdf)
        return


    def getRdf2(self, dTp=None, r=None, xLim=None, damping=0.173, fromZero=False, raw_out=False, sampling=1.):
        '''
        The 'Sine' Fourrier Transform is described in J. Davidsson et al., Phys. Rev. Lett., 94, 245503, 2005
        The data(q) is pre-multiplied by q*exp(-beta**2 . q**2) and then Fourrier transformed.
        Therefore, the creation of data(q) from rdf(r) is the inverse Fourrier transform, divided by q*exp(-beta**2 . q**2).
        Where q = (4*pi/lambda).sin(theta), and beta = 0.15 Å-1.
        The beta used here is the alpha used by Kim et al., and dampens the high-frequency oscillations of the Fourrier transform.
        This function is not finished!
        '''
        
        import scipy.fftpack as sfft

        if type(self.getdTp(dTp))==type(None):
            print('*** NOT calculating rdf! ***')
            return None
        
        x,y,z,xQuant,yQuant,zQuant,xUnit,yUnit,zUnit,dTp_list=self.loaddTpList(dTp, _nolab=True)
        complements = [['q','r']]
        for i in range(len(z)):
            
            #apply x limits
            if type(xLim)!=type(None):
                xmin,xmax = idxpt(x[i],xLim)
            else:
                xmin,xmax = x[i][0],x[i][-1]

            # TODO implement proper procedure:
            ## - shift data with sfft.shift(dFT, q[0]) !!! not sure about period (whether it is 2*pi or 1)... 
            ##                                         !!! dependes on whether we work in q or q/(2*pi) I guess.
            # - zeropad data: extend range by a factor of 2 and fill with 0s !!! do we need to dampen the transition?
            # - OR interpolate, apparantly computationally less expensive, and we are doing this anyway to get an
            #   equally spaced grid!
            # - FFT the biznitch
            # - Shift inverse back

            ### resampling the x axis for equal spacing
            d  = z[i][xmin:xmax,:]
            xl = x[i][xmin:xmax]
            dx = np.mean(np.diff(x))
            yl = y[i]
            #create equally spaced grid for fft via linear interpolation that starts at 0 and ends at max of given range.
            #if fromZero:
            #else:
                #N = int(round(abs(xl[-1]-xl[0])/dx)*sampling)
                #xFT = np.linspace(xl[0],xl[-1],num=N,endpoint=True)
            N = int(round(abs(xl[-1])/dx)*sampling)
            xFT = np.linspace(xl[0],xl[-1],num=N,endpoint=True)
            dFT = resample(d, xl, xFT)
            dFT[np.isnan(dFT)] = 0.

            #FFT zone
            
            prefreq = np.linspace((1/xl[-1])*2*np.pi,(1/xl[0])*2*np.pi,num=abs(xl[-1]-xl[0])/dx,endpoint=True)
            #zero-pad freq if necessary:
            #if (prefreq!=0).any():
                #dfreq = np.mean(np.diff(prefreq))
                #Nfreq = int(round(abs(prefreq).max())/dfreq)
                #freq  = np.linspace(0,prefreq[-1],num=Nfreq,endpoint=True)
            #else:
            freq = prefreq
            
            
            if type(damping)==int or type(damping)==float:
                prefactor= (xFT*np.exp(-np.power(damping,2)*np.power(xFT,2))).reshape(1,-1)
            else:
                raise TypeError("%s is not an accepted damping factor type!"%(type(damping)))
            
            datComp=np.zeros((freq.size,len(yl)))
            print('Performing FFT for %s.'%(dTp))
            for j,ds in enumerate(np.swapaxes(dFT,1,0)):
                #print 'i: %s; j: %s; xQuant: %s'%(i,j,xQuant[i])
                if xQuant[i]=='q':
                    datComp[:,j] = sfft.idst(ds.reshape(1,-1)*prefactor, n=freq.size, type=1, axis=1)
                elif xQuant[i]=='r':
                    datComp[:,j] = sfft.rfft((ds.reshape(1,-1))*(1./prefactor), axis=1)
            
            cmpl = getsublist(complements, xQuant[i])[getsublist(complements, xQuant[i]).index(xQuant[i])-1]
            
            raw_fft = datComp.copy()
            #bork
            
            if self.testdTpObj(cmpl, suppress=True):
                xI = getattr(self, cmpl)
                if np.unique(np.diff(xI)).size==1:
                    xi = xI
                else:
                    xi = np.linspace(xI[0],xI[-1],num=xI.size,endpoint=True)
                datComp = resample(datComp, freq, xi)
            else:
                xi = freq
            
            #check data
            #if datComp.shape!=dFT.shape:
                #raise TypeError('Shape of FFT result %s does not match input shape %s'%(datComp.shape,dFT.shape))
            #elif xFT.shape!=xi.shape:
                #raise TypeError('Shape of xFT %s does not match complemenatry xi shape %s'%(xFT.shape,xi.shape))
            #elif datComp.shape[0]!=xi.shape[0]:
                #raise TypeError('Shape of FFT result %s does not match complemenatry xi shape %s'%(datComp.shape,xi.shape))
            if raw_out:
                self.gen_dTp(cmpl, freq, quant=r'%s'%(cmpl), unit=r'\mathring{\mathrm{A}}')
                self.gen_dTp('%s%s'%(dTp,cmpl), raw_fft, quant=r'\Delta\mathrm{S}', unit=r'%s'%(cmpl))
            else:
                self.gen_dTp(cmpl, xi, quant=r'%s'%(cmpl), unit=r'\mathring{\mathrm{A}}')
                self.gen_dTp('%s%s'%(dTp,cmpl), datComp, quant=r'\Delta\mathrm{S}', unit=r'%s'%(cmpl))
        #borks
        return None


    def getAnisScat(self, dTp=None, yLim=None, xLim=None, scale=False, fit=False, out=False, ovrWrt=False):
        """
        Calculates anisotropy from vertical and horizontal scattering curves.
        dTp works a little different in this case, option 'dS' collects the
        data for 'dS_v' and 'dS_h', option 'dSr' collects the data for 'dS_vr' and 'dS_hr'.

        **kwargs
            yPt (1darray) : time point to scale vertical and horizontal quadrants to each other.
        """

        xL,yL,zL,xQuant,yQuant,zQuant,xUnit,yUnit,zUnit,dTp_list=self.loaddTpList(dTp, _nolab=True)
        
        for i,dat in enumerate(zL):
            
            x,y,dat = self.std_dTp(xL[i],yL[i],dat)
            
            elL = ['(?=.*%s)'%(el) for el in dTp_list[i]]
            rexv = re.compile(r'(?=.*v$)'+''.join(elL))
            rexh = re.compile(r'(?=.*h$)'+''.join(elL))
            
            for cand in list(self._unit.keys()):
                if rexv.match(cand) and len(cand)==len(elL)+1:
                    datv = self.std_dims(self.getdTp('{}'.format(cand)))
                elif rexh.match(cand):
                    dath = self.std_dims(self.getdTp('{}'.format(cand)))
            
            xIdx,xLim=self._stdIdxLim(x, xLim, slice=True)
            xSlice = slice(*xIdx)
            
            if type(yLim)!=type(None):
                yLim=np.asarray(yLim)
                if yLim.size<2:
                    raise NotImplementedError
                else:
                    yIdx,yLim=self._stdIdxLim(y, yLim, slice=True)
            else:
                yIdx,yLim=self._stdIdxLim(y, None, slice=True)
            
            ySlice = slice(*yIdx)
            
            R = np.zeros_like(dat)
            for j in range(len(dat)):
                if type(scale)!=type(None):
                    scale=np.float(scale)
                    res = [scale,0]
                else:
                    a_h = abs(dath[j,xSlice,ySlice].max()-dath[j,xSlice,ySlice].min())
                    a_v = abs(datv[j,xSlice,ySlice].max()-datv[j,xSlice,ySlice].min())
                    res = [a_v/a_h, 0]
                    
                if fit:
                    fit = np.zeros((y[j,:,ySlice].shape[1],2))
                    for t in range(y[j,:,ySlice].shape[1]):
                        from scipy.stats import linregress
                        f=linregress(dath[j,:,t],datv[j,:,t])
                        fit[t] = np.asarray([f.slope, f.intercept])
                    res = np.mean(fit, axis=1)
                    
                R[j] = datv[j] - dath[j]*res[0]
                
            if not out:
                if self.getUnit(dTp_list[i])!='q':
                    dTp = dTp_list[i].strip(self.getUnit(dTp_list[i]))+'R'+self.getUnit(dTp_list[i])
                else:
                    dTp = dTp_list[i] + 'R'
                #bork
                self.gen_dTp('{0}'.format(dTp), R, like_dTp=dTp_list[i],
                             xAxis=zUnit[i],
                             yAxis=None,
                             ovrWrt=ovrWrt)
            else:
                return (R,res)
                #x, res, rank, s = np.linalg.lstsq(dSheat[xmask], self.dS[xmask,:][:,tmask])

        return

    def genDataFromFunc(self, dTp = None, func = None, func_args = None, yPt=None, normData=False, normFunc=False, out=False, ovrWrt=False):
        '''
        Generates a full dataset from basis vectors.
        @param func: function, where the first argument is the dependence.
        @type  func: function
        @param func_args: contains the arguments to the passed function, except the dependence.
        @type  func_args: tuple
        @param normData: if True, data is normalised to greatest absolute value of the minimum or maximum value of the data.
        @type  normData: bool
        @param normFunc: if True, the numerical result of the function is normalised to greatest absolute value of the minimum or maximum value of the result of the function.
        @type  normFunc: bool
        '''
        
        if not func:
            raise ValueError('### Function not given! ### Not generating any output!')
        elif not func_args:
            raise ValueError('### Function arguments not given! ### Not generating any output!')

        x,y,z,xQuant,yQuant,zQuant,xUnit,yUnit,zUnit,dTp_list=self.loaddTpList(dTp, _nolab=True)

        gen_tot = []

        #shape = (x[i].shape + y[i].shape)
            
        for i,data in enumerate(z):
            if np.ndim(data)<3:
                axes = np.asarray(data.shape).tolist()
                if axes.index(x[i].shape[-1])>0:
                    shape = ((1,) + x[i].shape + y[i].shape)
                    data = np.expand_dims(data, axis=0)
                elif axes.index(x[i].shape[-1])>2:
                    raise ValueError("Given x axis is not the second axis!")
                else:
                    axes.remove(*x[i].shape)
                    shape = ((axes[0],) + x[i].shape + y[i].shape)
                    data = np.expand_dims(data, axis=0)
            elif axes.index(x[i].shape[-1])<2 and np.ndim(data)==3:
                shape = ((axes[0],) + x[i].shape + y[i].shape)
            elif np.ndim(data)>3:
                raise NotImplementedError("Greater than 3-dimensions not implemented!")
            gen = np.zeros(shape)
            
            for j,dep in enumerate(data):
                if dep.ndim<2 or 1 in dep.shape:
                    d = dep
                elif yPt:
                    d = dep[:,get_lim(y[i],yPt)]
                else:
                    d = dep[:,-1]
                
                #if type(extT)!=type(None) or type(extT)!=bool:
                    #x = extT
                #else:
                    #x = self.t
                
                f=func(y[i], *func_args)
                
                if normData:
                    d *= 1./abs(max(d.min(),d.max(), key=abs))
                if normFunc:
                    f *= 1./abs(max(f.min(),f.max(), key=abs))
                
                gen[j] = np.copy(np.dot(d.reshape(-1,1),f.reshape(1,-1)))
            if not out:
                if ovrWrt:
                    self.gen_dTp('%s'%(dTp_list[i]), gen, like_dTp=dTp_list[i])
                    self.gen_dTp('%s'%('t'), y[i], quant=yQuant[i], unit=yUnit[i])
                elif not ovrWrt or type(yPt)==float:
                    self.gen_dTp('%s_gen'%(dTp_list[i]), gen, like_dTp=dTp_list[i])
                    self.gen_dTp('%s_gen'%('t'), y[i], quant=yQuant[i], unit=yUnit[i])
        
        gen_tot.append(gen)
        if out:
            return (y[i],gen_tot)
        else:
            return None


    def genIntSig(self,dTp=None, ax='y', absolute=False, mean=False, Lim=None,
                  out=False, reset=False, ovrWrt=False):
        """
        Generates integrated signal of the datatype, option to generate the absolute too.
        """
        xL,yL,zL,xQuantL,yQuantL,zQuantL,xUnitL,yUnitL,zUnitL,dTp_list=self.loaddTpList(dTp, _nolab=True)
        
        if ax=='y':
            axis   = 2
        elif ax=='x':
            axis   = 1
        
        itg_tot = []
        
        for i,dat in enumerate(zL):
            x,y,z = self.std_dTp(xL[i],yL[i],dat)

            Idx,Lim = self._stdIdxLim(x[0].squeeze(), Lim, slice=True) if ax=='x' else self._stdIdxLim(y[0].squeeze(), Lim, slice=True)
            _Slice = slice(*Idx)
            Slice = [slice(None),slice(None),slice(None)]
            Slice[axis]=_Slice
            dx     = np.unique(np.diff(x.squeeze() if ax=='x' else y.squeeze())).mean()
            option = ''

            if absolute:
                data   = np.abs(dat[Slice])
                option = 'abs'
            else:
                data   = dat[Slice]

            if mean:
                func = np.mean
            else:
                data *= dx
                func = np.sum

            itg = func(data, axis=axis, keepdims=True)

            if ovrWrt:
                datLab  = dTp_list[i]
                datQ    = zQuantL[i]
                datU    = zUnitL[i]

                axLab   = zUnitL[i] if ax=='x' else 't'
                axQ     = xQuantL[i] if ax=='x' else 't'
                axU     = xUnitL[i] if ax=='x' else 't'
            else:
                datLab  = '{0}_{1}{2}{3}'.format(dTp_list[i],option,'I' if not mean else 'M',xQuantL[i] if ax=='x' else 't')
                datQ    = r'{0}{1}{2}{3}'.format('\int' if not mean else '', zQuantL[i], 'd' if not mean else '',zUnitL[i])
                datU    = r'{0}_{1}{2}{3}'.format(zUnitL[i],option,'I' if not mean else 'M',xQuantL[i] if ax=='x' else 't')

                axLab   = r'{0}_{1}{2}{3}'.format(zUnitL[i],option,'I' if not mean else 'M',xQuantL[i] if ax=='x' else 't')
                axQ     = r'{0}_{1}{2}{3}'.format(zUnitL[i],option,'I' if not mean else 'M',xQuantL[i] if ax=='x' else 't')
                axU     = xUnitL[i] if ax=='x' else 't'

            if reset:
                try:
                    delattr(self, datLab)
                    delattr(self, axLab)
                except AttributeError:
                    pass

            self.gen_dTp(axLab, np.zeros((1,1,1)), quant=axQ, unit=axU, axis=True, ovrWrt=ovrWrt)
            self.gen_dTp(datLab, itg, quant=datQ, unit=datU,
                         xAxis=axLab if ax=='x' else None,
                         yAxis=axLab if ax=='y' else None,
                         ovrWrt=ovrWrt)

        itg_tot.append(itg)

        if out:
            return itg_tot
        else:
            return None


    def gridAxis(self, dTp=None, axis=None, num=None, scale='log10', exp_lim=-2,
                 ovrWrt=False, out=False, reset=False):
        """
        num (int) : number of points within each decade.
        """
        assert type(dTp)!=type(None) or type(axis)!=type(None), "Either \'dTp\' or \'axis\' need to be specified."

        if type(axis)!=type(None) and type(dTp)==type(None):
            dTpD, test = self.getAxisDeps(axis)
        else:
            dTpD, test = self.getdTpDeps(dTp, axis=axis)

        for ax in list(dTpD.keys()):

            xL,yL,zL,xQuantL,yQuantL,zQuantL,xUnitL,yUnitL,zUnitL,dTp_list=self.loaddTpList(dTpD[ax], _nolab=True)

            for i,z in enumerate(zL):

                x,y,z = self.std_dTp(xL[i],yL[i],z)

                N = list(range(np.ndim(z)))
 
                if test[i]=='x':
                    AX  = x.squeeze()
                    AXn = 1
                elif test[i]=='y':
                    AX = y.squeeze()
                    AXn = 2
                N.pop(AXn)
                shape = list(z.shape)

                new_AX = genLogScale(AX, num=num, exp_lim=exp_lim)

                AXidx  = get_lim(new_AX, AX)
                AXidx[AXidx==-1] = len(new_AX)-1

                AXidxU     = np.unique(AXidx)
                shape[AXn] = AXidxU.size
                zz = np.empty(shape)

                #kneut
                for j,idx in enumerate(AXidxU):
                    pos         = [slice(None,None,None),slice(None,None,None),slice(None,None,None)]
                    pos[AXn]    = slice(j,j+1,None)

                    posD        = [slice(None,None,None),slice(None,None,None),slice(None,None,None)]
                    posD[AXn]   = AXidx==idx
                    zz[pos[0],pos[1],pos[2]] = np.mean(z[posD[0],posD[1],posD[2]], axis=AXn, keepdims=True)
                    new_AX[idx] = np.mean(AX[AXidx==idx])

                new_AX = new_AX[AXidxU]

                lab    = dTp_list[i] if ovrWrt else '{0}_rm'.format(dTp_list[i])
                axLab  = self._asoc[dTp_list[i]][test[i]] if ovrWrt else '{0}_rm'.format(self._asoc[dTp_list[i]][test[i]])

                if test[i]=='x':
                    new_AX,y,zz = self.std_dTp(new_AX,y,zz)
                elif test[i]=='y':
                    x,new_AX,zz = self.std_dTp(x,new_AX,zz)

                if not out:
                    if reset:
                        try:
                            delattr(self, axLab)
                            delattr(self, lab)
                        except AttributeError:
                            pass

                    self.gen_dTp(axLab, new_AX, like_dTp=self._asoc[dTp_list[i]][test[i]], axis=True, ovrWrt=ovrWrt)
                    self.gen_dTp(lab, zz, like_dTp=dTp_list[i], 
                                xAxis= axLab if test[i]=='x' else self._asoc[dTp_list[i]]['x'],
                                yAxis= axLab if test[i]=='y' else self._asoc[dTp_list[i]]['y'],
                                ovrWrt=ovrWrt)

        return


    def binAxis(self, dTp=None, extY=None, extX=None, binX=1, binY=1, end=False, keepdims=True,
                kind='quadratic', ovrWrt=False, out=False, reset=False, debug=False):
        """
        Ripped from http://stackoverflow.com/questions/14916545/numpy-rebinning-a-2d-array

        Bins the number of specified points in each axis.
        If keepdims is True, then the output and input maxtrix will be of the same shape.
        The datatype will be interpolated to fit the original x and/or y axis shape.
        If False, then a corresponding x and/or y axis will be generated.
        If end is True and the length of the axis to be binned is not a multiple of the bin
        number, the number of points equal to len(axis)%bin_number is subtracted from the end 
        of the axis. If False, len(axis)%bin_number is subtracted from the beginning of the axis.
        """
        import scipy.interpolate as spi

        binX,binY = int(binX), int(binY)
        xL,yL,zL,xQuantL,yQuantL,zQuantL,xUnitL,yUnitL,zUnitL,dTp_list=self.loaddTpList(dTp, _nolab=True)

        for i,dat in enumerate(zL):
            binned = []

            dat = self.std_dims(dat)

            if type(extY)==type(None) and type(extX)==type(None):
                x,y,dat = self.std_dTp(xL[i],yL[i],dat)
            elif type(extY)==type(None) and type(extX)!=type(None):
                x,y,dat = self.std_dTp(extX,yL[i],dat)
            elif type(extY)!=type(None) and type(extX)==type(None):
                x,y,dat = self.std_dTp(xL[i],extY,dat)
            else:
                x,y,dat = self.std_dTp(extX,extY,dat)


            for j,dep in enumerate(dat):
                dx = np.diff(x[j], axis=0)
                if x[j].shape[-1]==1:
                    dxm = dx[:,0].min()
                    _x  = x[j,:,0]
                    _xn = np.arange(_x.min(), _x.max(), dxm)
                    if j==0:
                        datn = np.zeros((self.dep.size,_xn.size,y.shape[-1]))
                        xn = np.zeros((self.dep.size,_xn.size,1))
                    f  = spi.interp1d(_x, dep, axis=0, kind=kind, fill_value="extrapolate", copy=False, assume_sorted=True)
                    datn[j]   = f(_xn)
                    xn[j,:,0] = _xn
                else:
                    raise NotImplementedError("")

            if datn.shape!=dat.shape:
                x,y,dat = self.std_dTp(xn,y,datn)
            #raise NotImplementedError
            #axes = np.asarray(dat.shape).tolist()
            #if np.ndim(dat)<3:
                #if axes.index(x.shape[-1])>0:
                    #shape = ((1,) + x.shape + y.shape)
                    #dat = np.expand_dims(dat, axis=0)
                #elif axes.index(x.shape[-1])>2:
                    #raise ValueError("Given x axis is not the second axis!")
                #else:
                    #axes.remove(*x.shape)
                    #shape = ((axes[0],) + x.shape + y.shape)
                    #dat = np.expand_dims(dat, axis=0)
            #elif axes.index(x.shape[-1])<2 and np.ndim(dat)==3:
                #shape = ((axes[0],) + x.shape + y.shape)
            #elif np.ndim(dat)>3:
                #raise NotImplementedError("Greater than 3-dimensions not implemented!")

            if end and np.asarray(x.shape[1])%binX!=0:
                _x = -int(np.asarray(x.shape[1])%binX)
                _y = -int(np.asarray(y.shape[2])%binY)
                new_x = x[:,:_x if _x!=0 else None,:]
                new_y = y[:,:,:_y if _y!=0 else None]
                dat   = dat[:,:_x if _x!=0 else None,:_y if _y!=0 else None]
            else:
                _x = int(np.asarray(x.shape[1])%binX)
                _y = int(np.asarray(y.shape[2])%binY)
                new_x = x[:,_x:,:]
                new_y = y[:,_y:]
                dat   = dat[:,_x:,_y:]

            xshape = new_x.reshape(new_x.shape[0],-1,binX).shape
            yshape = new_y.reshape(new_y.shape[0],-1,binY).shape
            binned = np.mean(dat.reshape((dat.shape[0],)+xshape[1:]+yshape[1:]), axis=(4,2))
            x_bin  = np.mean(new_x.reshape(xshape), axis=(2))
            y_bin  = np.mean(new_y.reshape(yshape), axis=(2))


            if keepdims:
                z_rec = np.zeros_like(zL[i])

                for j,dep in enumerate(binned):
                    #f = scipy.interpolate.interp2d(x_bin, y_bin, dep, kind=kind, bounds_error=False, fill_value=None)
                    f = spi.RectBivariateSpline(x_bin[j], y_bin[j], dep, kx=3, ky=3, s=0)
                    z_rec[j] = f(x[j].squeeze(),y[j].squeeze())
                _,_,z_rec = self.std_dTp(x,y,z_rec)
            else:
                x_rec = x_bin
                y_rec = y_bin
                z_rec = binned
                x_rec,y_rec,z_rec = self.std_dTp(x_bin,y_bin,binned)
                #bork

            if reset:
                try:
                    delattr(self, '%s_bin'%(dTp_list[i]))
                    if keepdims:
                        delattr(self, '%s_bin'%(zUnitL[i]))
                        delattr(self, '%s_bin'%('t'))
                except AttributeError:
                    pass

            if debug:
                raise Exception("Debugging")
            if not out:
                if ovrWrt:
                    if not keepdims:

                        self.gen_dTp('%s'%(zUnitL[i]), x_rec, like_dTp=zUnitL[i], axis=True, ovrWrt=ovrWrt)

                        self.gen_dTp('%s'%('t'), y_rec, like_dTp='t', axis=True, ovrWrt=ovrWrt) if binY!=1 else None
                        #bork

                    self.gen_dTp('%s'%(dTp_list[i]), z_rec, like_dTp=dTp_list[i], 
                                 xAxis=zUnitL[i],
                                 yAxis='t',
                                 ovrWrt=ovrWrt)
                    #bork
                else:

                    if not keepdims:
                        self.gen_dTp('%s_bin'%(zUnitL[i]), x_rec, quant='{0}_bin'.format(xQuantL[i]), unit=xUnitL[i], axis=True, ovrWrt=ovrWrt) if binX!=1 else None
                        self.gen_dTp('%s_bin'%('t'), y_rec, like_dTp='t', axis=True, ovrWrt=ovrWrt) if binY!=1 else None

                        self.gen_dTp('%s_bin'%(dTp_list[i]), z_rec, quant=self.getQuant(dTp_list[i]),
                                    unit ='{0}_bin'.format(xQuantL[i]) if binX!=1 else xQuantL[i],
                                    xAxis='%s_bin'%(zUnitL[i]) if binX!=1 else xQuantL[i],
                                    yAxis='%s_bin'%('t') if binY!=1 else 't',
                                    ovrWrt=ovrWrt)
                        #bork
                    else:
                        self.gen_dTp('%s_bin'%(dTp_list[i]), z_rec, quant=self.getQuant(dTp_list[i]),
                                    unit ='{0}'.format(xQuantL[i]) if binX!=1 else xQuantL[i],
                                    xAxis='%s'%(zUnitL[i]) if binX!=1 else xQuantL[i],
                                    yAxis='%s'%('t') if binY!=1 else 't',
                                    ovrWrt=ovrWrt)
                        #bork
            else:
                return (x_rec,y_rec,z_rec)

    #def smooth_fft(self, dTp=None, filter=None,fromZero=False, ovrWrt=False, yPt=False, out=False):
        #"""
        #Based off of stackexchange: http://stackoverflow.com/questions/20618804/how-to-smooth-a-curve-in-the-right-way
        #Takes the FFT along the x axis for each y-array (2D array).

        #Kwargs:
                #dTp (str)       : Standard dTp input. See Dataset.loaddTpList for details.
                #filter (list)   : list of tuples in the form (str,float) of which the first element contains the
                                  #filter to be used, and the second element the value to be applied. See libwaxs.fft_filter
                                  #for details on the different implemented filters.
        #Returns:
        #"""


        #import scipy.fftpack as sfft

        #xL,yL,zL,xQuant,yQuant,zQuant,xUnit,yUnit,zUnit,dTp_list = self.loaddTpList(dTp, _nolab=True)
        
        #dep = self.dep
        
        #for i,dat in enumerate(zL):
            
            #depL=[]
            
            #x = xL[i][0].squeeze()
            #y = yL[i][0].squeeze()
            #z = dat[j]
            
            #xx = x
            #dxFT = np.mean(np.diff(xx))

            ##create equally spaced grid for fft via linear interpolation that starts at 0 and ends at max of given range.
            #if fromZero:
                #xFT = np.linspace(0,xx[-1],num=(xx[-1]/dxFT),endpoint=True)
                #dFT = resample(z, xx, xFT)
                #dFT[np.isnan(dFT)] = 0.
            #elif not len(np.unique(np.diff(x))):
                #xFT = np.linspace(xx[0],xx[-1],num=(xx[-1]/dxFT),endpoint=True)
                #dFT = resample(y, xx, xFT)
                #dFT[np.isnan(dFT)] = 0.
            ## if grid is already equally spaced
            #else:
                #xFT = xx
                #dFT = z

            #if dFT.ndim==1:
                #dFT = dFT.reshape(xFT.shape[0],-1)
            
            #N = len(xFT)
            #tmp     = np.zeros_like(dFT)
            #dSmooth = np.zeros_like(dFT)
            #f = sfft.rfftfreq(N, dxFT)
            #print 'Smoothing data with an FT filter.'
            #for k,z in enumerate(dFT):
                #for l,ds in enumerate(np.swapaxes(z,1,0)):
                    #w = sfft.rfft(ds,axis=0)
                    #w2 = w.copy()
                    #if filter != None:
                        #w2,_ = fft_filter(w2,f,filter=filter)
                    #else:
                        #print 'Why did you bother, really?\nSpecify a filter!'

                    #tmp[k,:,l] = sfft.irfft(w2,axis=0)

            ## Back to the original spacing, for compatibility with the rest of the data
                #dSmooth[k] = resample(tmp[k], xFT, xx)
            #dSmooth = np.asarray(dSmooth)
            ##depL.append(dSmooth)
            ##depDsmooth = np.asarray(depL)
            #if ovrWrt==True:
                #self.gen_dTp('%s'%(dTp_list[i]), dSmooth, like_dTp=dTp_list[i], ovrWrt=ovrWrt)
            #if not out:
                #self.gen_dTp('%s_sFT'%(dTp_list[i]), dSmooth, like_dTp=dTp_list[i])
            #else:
                #return depDsmooth

    def smooth_fft(self, dTp=None, filter=None, axis='x', fromZero=False, counter=False,
                   axSegment=None, bufferSegment=3,
                   reset=False, out=False, ovrWrt=False):
        """
        Based off of stackexchange: http://stackoverflow.com/questions/20618804/how-to-smooth-a-curve-in-the-right-way
        Takes the FFT along the x axis for each y-array (2D array).

        Kwargs:
                dTp (str)       : Standard dTp input. See Dataset.loaddTpList for details.
                filter (list)   : list of tuples in the form (str,float) of which the first element contains the
                                  filter to be used, and the second element the value to be applied. See libwaxs.fft_filter
                                  for details on the different implemented filters.
        Returns:
        """
        import scipy.fftpack as sfft
        import copy as cp

        if axis=='x':
            xL,yL,zL,xQuant,yQuant,zQuant,xUnit,yUnit,zUnit,dTp_list=self.loaddTpList(dTp, _nolab=True)
        elif axis=='y':
            xL,yL,zL,xQuant,yQuant,zQuant,xUnit,yUnit,zUnit,dTp_list=self.loaddTpList(dTp, _nolab=True, _transpose=True)

        for i,dat in enumerate(zL):
            x,y,dat=self.std_dTp(xL[i],yL[i],dat)

            xx   = xL[i].squeeze()
            dxFT = np.mean(np.diff(xx))

            if counter:
                # Assumes data is equally spaced
                xFT  = np.arange(xx.size)
                dFT  = dat
                dxFT = 1
            else:
                #create equally spaced grid for fft via linear interpolation that starts at 0 and ends at max of given range.
                if fromZero:
                    xFT = np.linspace(0,np.max(xx),num=int(np.max(xx)/dxFT),endpoint=True)
                    dFT = resample(dat, xx, xFT, axis=1)
                    dFT[np.isnan(dFT)] = 0.
                elif len(np.unique(np.diff(xx)))!=1:
                    xFT = np.linspace(np.min(xx),np.max(xx),num=int(np.max(xx)/dxFT),endpoint=True)
                    dFT = resample(dat, xx, xFT, axis=1)
                    dFT[np.isnan(dFT)] = 0.
                # if grid is already equally spaced
                else:
                    xFT = xx
                    dFT = dat

            if type(axSegment)!=type(None):
                axSegment = np.asarray(axSegment)
                xSi = get_lim(xx if counter else xFT, [axSegment.min(),axSegment.max()])
                xS  = slice(*xSi)
                xBl = [xSi.min()-int(bufferSegment), xSi.max()+int(bufferSegment)]
                xSb = slice(*get_lim(xx if counter else xFT, axSegment))
            else:
                xS  = slice(0,xx.size if counter else xFT.size,None)
                xSb = slice(0,xx.size if counter else xFT.size,None)

            if dFT.ndim==1:
                dFT = dFT.reshape(xFT.shape[0],-1)

            #dFT = np.fft.fftshift()
            N = len(xFT)
            tmp     = cp.deepcopy(dFT)
            dSmooth = np.zeros_like(dat)
            f = sfft.rfftfreq(N, dxFT)
            print('Smoothing data with an FT filter.')
            for k,dat in enumerate(dFT):
                for l,ds in enumerate(np.swapaxes(dat,1,0)):
                    w = sfft.rfft(ds,axis=0)
                    w2 = w.copy()
                    if filter != None:
                        w2,_ = fft_filter(w2,f,filter=filter)
                    else:
                        print('Why did you bother, really?\nSpecify a filter!')

                    tmp[k,xS,l] = sfft.irfft(w2,axis=0)[xS]

            # Back to the original spacing, for compatibility with the rest of the data
                dSmooth[k] = resample(tmp[k], xFT, xx) if not counter else tmp[k]
            _,_,dat=self.std_dTp(x,y,dSmooth)
            dSmooth = np.transpose(np.asarray(dSmooth), (0,2,1)) if axis=='y' else np.asarray(dSmooth)
            
            if not out:
                if ovrWrt:
                    datlab = '{0}'.format(dTp_list[i])
                else:
                    datlab = '{0}_sFT'.format(dTp_list[i])
                if reset:
                    try:
                        delattr(self, datlab)
                    except AttributeError:
                        pass

                self.gen_dTp(datlab, dSmooth, like_dTp=dTp_list[i],
                             xAxis=zUnit[i],
                             yAxis=self._asoc[dTp_list[i]]['y'], # the future is here...
                             ovrWrt=ovrWrt)
            else:
                return dSmooth

    def smooth_ssg(self, dTp=None, wl = 9, delta= 1., plyn=2, axis=1, ovrWrt=False, 
                   reset=False, out=False):
        """
        Smooths data along the specified axis using a Savitzky-Golay filter.

        """
        from scipy import signal

        xL,yL,zL,xQuant,yQuant,zQuant,xUnit,yUnit,zUnit,dTp_list=self.loaddTpList(dTp, _nolab=True)

        for i,dat in enumerate(zL):
            x,y,dat=self.std_dTp(xL[i],yL[i],dat)

            if wl%2==0:
                wl = wl - 1
            dSmooth = np.zeros_like(dat)
            print('Smoothing data with a Savitzky-Golay filter.')

            dSmooth[:] = signal.savgol_filter(dat,int(wl),int(plyn),delta=float(delta),axis=int(axis))

            if ovrWrt:
                datlab = '{0}'.format(dTp_list[i])
            else:
                datlab = '{0}_sSG'.format(dTp_list[i])

            if reset:
                try:
                    delattr(self, datlab)
                except AttributeError:
                    pass


            if not out:
                self.gen_dTp(datlab, dSmooth, like_dTp=dTp_list[i],
                            xAxis=zUnit[i],
                            yAxis=self._asoc[dTp_list[i]]['y'], 
                            ovrWrt=ovrWrt)
            else:
                return dSmooth

    def truncSVD(self, dTp=None, components=1, ovrWrt=False, reset=False, out=False):
        '''
        Function to get the singular values, projection and target vectors of a datatype.
        '''
        xL,yL,zL,xQuantL,yQuantL,zQuantL,xUnitL,yUnitL,zUnitL,dTp_list=self.loaddTpList(dTp, _nolab=True)

        for i in range(len(zL)):

            x, y, z  = self.std_dTp(xL[0],yL[0],zL[0])


            #Utot = np.zeros((z[:,xSlice,0].shape + (c,)))
            #stot = np.zeros((len(z[:,xSlice, ySlice]),)+(y[:,0,ySlice]).shape)
            #Vtot = np.zeros(y[:,0,ySlice].shape+y[:,0,ySlice].shape)
            D = np.zeros_like(z)

            for j,dep in enumerate(z):

                U,s,V = np.linalg.svd(dep, full_matrices=0, compute_uv=1)

                Sr = np.diag(s[:components])
                Ur = U[:,:components]
                Vr = V[:components]

                D[j] = np.dot(np.dot(Ur, Sr), Vr)

            if not out:
                zLab = '{0}_rSVD'.format(dTp_list[i])
            else:
                zLab = '%s'%(dTp_list[i])

            if reset:
                try:
                    delattr(self, zLab)
                except AttributeError:
                    pass

            if not out:
                if ovrWrt==True:
                    self.gen_dTp(zLab, D, like_dTp=dTp_list[i],
                            xAxis=zUnitL[i],
                            yAxis=None,
                            ovrWrt=ovrWrt)
                else:
                    self.gen_dTp(zLab, D, like_dTp=dTp_list[i],
                                 xAxis=zUnitL[i],
                                 yAxis=None,
                                 ovrWrt=ovrWrt)
            else:
                return dSmooth

    def cutRange(self, dTp=None, ax='x', Lim=None, ovrWrt=False, out=False):

        xL,yL,zL,xQuant,yQuant,zQuant,xUnit,yUnit,zUnit,dTp_list=self.loaddTpList(dTp, _nolab=True)

        for i,dat in enumerate(zL):
            x, y, z  = self.std_dTp(xL[0],yL[0],zL[0])
            if ax == 'dep':
                axis = -3
                raise NotImplementedError("Cutting dep range not implemented yet!")
            elif ax == 'x':
                axis = -2
                axSlice, axLim = self._stdIdxLim(x[0].squeeze(), Lim, slice=True)
                cut = dat[:,slice(*axSlice),:]
                cut_axis = x[:,slice(*axSlice),:]
            elif ax == 'y':
                axis = -1
                axSlice, axLim = self._stdIdxLim(y[0].squeeze(), Lim, slice=True)
                cut      = dat[:,:,slice(*axSlice)]
                cut_axis = y[:,:,slice(*axSlice)]

            if not out:
                if ovrWrt==True:
                    if ax == 'dep':
                        raise NotImplementedError("Cutting dep range not implemented yet!")
                    elif ax == 'x':
                        self.gen_dTp('%s'%(zUnit[i]), cut_axis, like_dTp=zUnit[i], axis=True, ovrWrt=ovrWrt)
                    elif ax == 'y':
                        self.gen_dTp('%s'%('t'), cut_axis, like_dTp='t', axis=True, ovrWrt=ovrWrt)
                    self.gen_dTp('%s'%(dTp_list[i]), cut, like_dTp=dTp_list[i],
                                 xAxis=zUnit[i],
                                 yAxis=None,
                                 ovrWrt=ovrWrt)
                else:
                    if ax == 'dep':
                        raise NotImplementedError("Cutting dep range not implemented yet!")
                    elif ax == 'x':
                        self.gen_dTp('%s_cut'%(zUnit[i]), cut_axis, quant='{0}_cut'.format(xQuant[i]), unit=xUnit[i], axis=True, ovrWrt=ovrWrt)
                    elif ax == 'y':
                        self.gen_dTp('%s_cut'%('t'), cut_axis, like_dTp='t', axis=True, ovrWrt=ovrWrt)

                    self.gen_dTp('%s_cut'%(dTp_list[i]), cut, quant=zQuant[i], unit='{0}_cut'.format(xQuant[i]) if ax=='x' else self._asoc[dTp_list[i]]['x'],
                                 xAxis='%s_cut'%(zUnit[i]) if ax=='x' else self._asoc[dTp_list[i]]['x'],
                                 yAxis='%s_cut'%('t') if ax=='y' else self._asoc[dTp_list[i]]['y'],
                                 ovrWrt=ovrWrt)
        return

    def getSVD(self,dTp=None, xLim=None, yLim=None, ViewComp=3, fill_value=0., transpose=False,# cutComponents=None,
               reset=False, out=False, ovrWrt=False, 
               plot=False, weighPlot=True, sLog=False, noCont=False, yLog=True, plotOnly=False, title=False,
               plotLinthreshY=1e0):
        '''
        Function to get the singular values, projection and target vectors of a datatype.
        '''

        xL,yL,zL,xQuantL,yQuantL,zQuantL,xUnitL,yUnitL,zUnitL,dTp_list=self.loaddTpList(dTp, _nolab=True, _transpose=transpose)

        #bork
        UL=[]
        VL=[]
        sL=[]
        for i in range(len(zL)):

            x, y, z  = self.std_dTp(xL[0],yL[0],zL[0])

            for j,dep in enumerate(z):
                xIdx,xLim = self._stdIdxLim(x[j,:,0], xLim, slice=True, debug=False)
                xSlice = slice(*xIdx)

                yIdx,yLim = self._stdIdxLim(y[j,0,:], yLim, slice=True)
                ySlice = slice(*yIdx)

                if j==0:
                    Utot = np.ones((z[:,:,0].shape + (np.min(z[0,:,ySlice].shape),)))*fill_value
                    stot = np.ones((len(z), (np.min(z[0,:,ySlice].shape))))*fill_value
                    Vtot = np.ones((z[:,0,:].shape + (np.min(z[0,:,ySlice].shape),)))*fill_value
                U,s,V = np.linalg.svd(dep[xSlice, ySlice], full_matrices=0, compute_uv=1)

                Utot[j, xSlice] = U

                Vtot[j, ySlice] = V.T

                stot[j] = s

                if plot:
                    if weighPlot:
                        _s = s
                    else:
                        _s = np.ones_like(s)

                    from matplotlib import ticker as mplt
                    matplotlib.rc('text', usetex=True)
                    matplotlib.rc('font', family='serif')
                    fig, ax = plt.subplots(ncols=2, nrows=2, gridspec_kw={'wspace': 0.0,'hspace':0.0})
                    fig.suptitle(r"getSVD() {} dep {}".format(dTp_list[i].replace('_','\_'), j)) if title else None
                    if ViewComp>len(s):
                        ViewComp = len(s)
                    ax[0,0].plot(np.arange(s.size), s, ls='', marker='o',markersize=2, color='b', label='singular values')
                    ax[0,0].plot(np.arange(ViewComp), (s[:ViewComp]), ls='', marker='o',markersize=2, color='orange')
                    #ax[0,0].text(min(ax[0,0].get_xlim()), min(ax[0,0].get_ylim()), " s",
                                 #weight='bold',size=16)
                    ax[0,0].axhline(0, ls=':', color='black')

                    for tl in ax[0,0].get_yticklabels():
                        tl.set_color('b')
                    if sLog:
                        ax[0,0].set_yscale('log')
                        ax[0,0].set_ylim(s.min()-s.min()*0.1, s.max()+s.max()*0.1)

                    tax = ax[0,0].twinx()
                    tax.plot(np.arange(s.size), (100*(s)/np.sum(s)),'-', lw=1, color='red', label='weight \%')
                    tax.plot(np.arange(s.size), (100*np.cumsum(s)/np.sum(s)),':', lw=1, color='red', label='cumulative weight \%')

                    tax.set_ylim(-5, 105)
                    tax.yaxis.tick_right()
                    ax[0,0].set_xlim(-1, s.size+1)
                    for tl in tax.get_yticklabels():
                        tl.set_color('r')
                        tl.set_horizontalalignment('right')
                    tax.yaxis.set_tick_params(pad=-5)
                    handles,labels = tax.get_legend_handles_labels()
                    hdls, lbls = ax[0,0].get_legend_handles_labels()
                    handles.extend(hdls)
                    labels.extend(lbls)
                    ax[0,0].legend(handles, labels, prop={'size':3*(matplotlib.rcParams['font.size']/6)}, loc=0, frameon=False, handlelength=1)

                    #bork
                    for k in range(ViewComp):
                        ax[0,1].plot(x[j,xSlice,0], U[:,k]*_s[k]+np.max(np.abs(np.dot(U[:,:ViewComp],np.diag(_s[:ViewComp]))))*k, label='{}'.format(k))
                        ax[0,1].axhline(np.max(np.abs(np.dot(U[:,:ViewComp],np.diag(_s[:ViewComp]))))*k, ls=':', color='black')

                        ax[1,0].plot(y[j,0,ySlice], V.T[:,k]+np.max(np.abs(V.T[:,:ViewComp]))*k, label='{}'.format(k))
                        ax[1,0].axhline(np.max(np.abs(V.T[:,:ViewComp]))*k, ls=':', color='black')

                    ax[0,1].legend(prop={'size':3*(matplotlib.rcParams['font.size']/6)}, ncol=ViewComp, loc=0, frameon=False, handlelength=1)
                    ax[0,1].set_xlim(xLim)
                    ax[0,1].yaxis.set_label_position('right')
                    ax[0,1].yaxis.tick_right()
                    ax[0,1].text(min(ax[0,1].get_xlim()), min(ax[0,1].get_ylim()), " U",
                                 weight='bold',size=16)

                    ax[1,0].set_xscale('symlog', linthreshx=plotLinthreshY) if yLog else None
                    ax[1,0].set_xlim(yLim)
                    ax[1,0].set_xlabel((r'$\log_{{10}}(\mathrm{{{0}}})'+r'\ '+r'({1})'+r'$').format(yQuantL[i], yUnitL[i]))
                    ax[1,0].text(min(ax[1,0].get_xlim()), min(ax[1,0].get_ylim()), " V",
                                 weight='bold',size=16)

                    if not noCont:
                        zLim = np.array([dep[xSlice, ySlice].min(),dep[xSlice, ySlice].max()])
                        colorlvl = np.linspace(zLim.min(),zLim.max(),num=20,endpoint=True)
                        contours = libwaxs.zeroRange(zLim, 20)
                        newcmap = libwaxs.shiftedColorMap(matplotlib.cm.RdBu_r, midpoint=(1 - zLim.max()/(zLim.max() + abs(zLim.min()))), name='getSVD')
                        ax[1,1].contour(x[j,xSlice,0],y[j,0,ySlice],dep[xSlice, ySlice].T,
                                        levels=contours, vmin=zLim.min(), vmax=zLim.max(),
                                        colors='0', linewidths=0.5)
                        ax[1,1].contourf(x[j,xSlice,0],y[j,0,ySlice],dep[xSlice, ySlice].T,
                                        levels=colorlvl, vmin=zLim.min(), vmax=zLim.max(),
                                        cmap='getSVD')

                        ax[1,1].set_yscale('symlog', linthreshy=plotLinthreshY) if yLog else None
                        ax[1,1].set_xlim(xLim)
                        ax[1,1].set_xlabel((r'${}'+r'\ '+r'({})'+r'$').format(xQuantL[i], xUnitL[i]))
                    ax[1,1].yaxis.set_label_position('right')
                    ax[1,1].yaxis.tick_right()

                    for index in np.ndindex(*ax.shape):
                        #ytL = ax[index].get_yticks()
                        #C,E = det_sci(ytL)
                        #exp = E[E!=0].max()
                        #tick_labs = [r"$%.1f$" % m for m in ytL/10**exp]
                        if index==(1,0) or index==(0,1) or (index==(0,0) and not sLog):
                            ax[index].yaxis.set_major_formatter(mplt.ScalarFormatter(useOffset=False, useMathText=True))
                            ax[index].yaxis.set_major_locator(mplt.MaxNLocator(nbins=9,prune='lower'))
                            ax[index].yaxis.set_minor_locator(mplt.AutoMinorLocator())
                            #ax[index].yaxis.set_ticklabels(tick_labs)
                            ax[index].set_ylabel((r'${0}'+r'\ '+r'({1})'+r'$').format(zQuantL[i], zUnitL[i]))
                        elif index==(1,1):
                            ax[index].set_ylabel((r'$\log_{{10}}(\mathrm{{{0}}})'+r'\ '+r'({1})'+r'$').format(yQuantL[i], yUnitL[i]))
                            
                        if not index==(1,0):
                            ax[index].xaxis.set_major_formatter(mplt.ScalarFormatter(useOffset=False, useMathText=True))
                            ax[index].xaxis.set_minor_locator(mplt.AutoMinorLocator())
                    
                    ax[0,0].xaxis.set_ticklabels([])
                    ax[0,1].xaxis.set_ticklabels([])
                    fig.tight_layout()

            if not out:
                if reset:
                    try:
                        delattr(self,'%s_U'%(dTp_list[i]))
                        delattr(self,'%s_s'%(dTp_list[i]))
                        delattr(self,'%s_V'%(dTp_list[i]))
                        delattr(self,'%s_N'%(dTp_list[i]))
                        delattr(self,'AU_%s'%(dTp_list[i]))
                    except AttributeError:
                        pass
                N = np.arange(stot.ravel().size)
                self.gen_dTp('AU_%s'%(dTp_list[i]), x, like_dTp=zUnitL[i] if not transpose else yUnitL[i], axis=True, ovrWrt=ovrWrt)
                self.gen_dTp('%s_N'%(dTp_list[i]), N, unit=r'', quant=r'', axis=True, ovrWrt=ovrWrt)
                self.gen_dTp('%s_U'%(dTp_list[i]), Utot, quant=self._quant['%s'%(dTp_list[i])], unit='AU_%s'%(dTp_list[i]),
                            xAxis='AU_%s'%(dTp_list[i]) if not transpose else '%s_N'%(dTp_list[i]),
                            yAxis='%s_N'%(dTp_list[i]) if not transpose else 'AU_%s'%(dTp_list[i]),
                            ovrWrt=ovrWrt)
                self.gen_dTp('%s_s'%(dTp_list[i]), stot, quant=self._quant['%s'%(dTp_list[i])], unit='%s_N'%(dTp_list[i]),
                            xAxis='%s_N'%(dTp_list[i]),
                            yAxis='%s_N'%(dTp_list[i]),
                            ovrWrt=ovrWrt)
                self.gen_dTp('%s_V'%(dTp_list[i]), np.transpose(Vtot, (0,2,1)), quant=self._quant['%s'%(dTp_list[i])], unit='t',
                            xAxis='%s_N'%(dTp_list[i]) if not transpose else r't',
                            yAxis=r't' if not transpose else '%s_N'%(dTp_list[i]),
                            ovrWrt=ovrWrt)
            else:
                UL.append(Utot)
                sL.append(stot)
                VL.append(Vtot)

        if plot and out:
            return UL,sL,VL,fig
        elif out and not plot:
            return UL,sL,VL
        elif plot:
            return fig

    def rebuildFromSVD(self, prefix_dTp, components=0, dep=0, reset=False, out=False,
                       ovrWrt=False):
        """
        prefix_dTp (str): 
        """
        cmps = np.asarray([components]).ravel()
        dep  = np.asarray([dep]).ravel()

        if type(prefix_dTp)!=str:
            raise TypeError

        C={}

        for el in ['U','V','s']:
            C[el] = getattr(self, prefix_dTp.rstrip('_')+'_'+el)

        x,y,z,xQuant,yQuant,zQuant,xUnit,yUnit,zUnit,dTp_list = self.loaddTpList(prefix_dTp.rstrip('_'),_nolab=False)

        rec = np.zeros((dep.size, x[0].shape[1], y[0].shape[2]))

        for d in dep:
            rec[d]= np.dot(np.dot(C['U'][d,:,cmps].T,np.diag(C['s'][d, cmps])),C['V'][d,:,cmps])


        if not out:
            zLab = '{0}_rSVD'.format(dTp_list[0])
        else:
            zLab = '%s'%(dTp_list[0])

        if reset:
            try:
                delattr(self, zLab)
            except AttributeError:
                pass

        if not out:
            if ovrWrt==True:
                self.gen_dTp(zLab, rec, like_dTp=dTp_list[0],
                        xAxis=zUnit[0],
                        yAxis=None,
                        ovrWrt=ovrWrt)
            else:
                self.gen_dTp(zLab, rec, like_dTp=dTp_list[0],
                                xAxis=zUnit[0],
                                yAxis=None,
                                ovrWrt=ovrWrt)
        else:
            return rec


    def get2Dcorr(self, dTp=None, dep=None, yPt=None, ovrWrt=False, out=False):
        """
        Method to calculate the synchronous and asynchronous 2D correlation matrices of the data.
        Assumes the y axis is the pertubative axis.
        TODO implement dependency axis is perturbation...
        """
        x,y,z,xQuant,yQuant,zQuant,xUnit,yUnit,zUnit,dTp_list=self.loaddTpList(dTp, _nolab=True)
        
        for i in range(len(z)):
            sync  = []
            async1 = [] # !!! async is a protected variable in python 3.7!!! 
            x_axis_idx = z[i].shape.index(*x[i].shape)
            y_axis_idx = z[i].shape.index(*y[i].shape) # Axis of the pertubation in the Mean Centered matrix
            
            axes = np.arange(np.ndim(z[i]))
            axesT = axes
            axesT[y_axis_idx] = x_axis_idx
            axesT[x_axis_idx] = y_axis_idx
                
            #_,mean = np.meshgrid(y[i], x[i], axis=1)
            for j in range(z[i].shape[axes[~((axes == x_axis_idx)+(axes == y_axis_idx))]]):
                _,mean = np.meshgrid(y[i], np.mean(z[i][j], axis=1))
                MC = z[i][j]-mean
                p = np.tile(np.linspace(1, y[i].shape[-1], num=y[i].shape[-1]).reshape(-1,1), [1,1,y[i].shape[-1]])
                
                HN = (1/np.pi)*(np.transpose(p,axes=axesT) - p)

                sync.append(np.dot(MC, MC.T))
                async1.append((1./y[i].shape[-1])-np.dot(np.dot(MC, HN), MC.T))

            sync  = np.asarray(sync)
            async1 = np.asarray(async1)
            
        if not out:
            self.gen_dTp('%s_sync'%(dTp_list[i]), sync, like_dTp=dTp_list[i],
                         xAxis=zUnit[i],
                         YAxis=zUnit[i],
                         ovrWrt=ovrWrt)
            self.gen_dTp('%s_async'%(dTp_list[i]), async1, like_dTp=dTp_list[i],
                         xAxis=zUnit[i],
                         YAxis=zUnit[i],
                         ovrWrt=ovrWrt)
        else:
            return (sync, async1, MC, HN)
    
    def _contPlot():
        
        return 
    
    def _stdIdxLim(self, A, lim, slice=False, limOnly=False, debug=False):
        """
        Helper function returns indices and values of points in an array.
        #A (sorted 1darray-like)            : 
        lim (1darray-like with 2 elements) : 
        
        slice (bool)                       : If True, any index retreived that comes up as
                                             -1, is set to None.
        """
        A = np.atleast_1d(A.squeeze())


        if type(lim)==type(None):
            lim = np.asarray([A.min(), A.max()])
        elif not isinstance(lim, collections.Iterable) or type(lim)==str:
            raise TypeError("lim has to be an 1darray-like.")
          
        if A.size==1:
            if not slice:
                return [0,-1],A[[0,-1]]
            else:
                return [None,None,None],A[[0,-1]]

        if type(lim[1])==type(None):
            lim[1]=A[-1]
        
        lim = np.asarray(lim).squeeze()
        if lim.size>2:
            raise ValueError("lim has to contain only 2 elements.")
        
        if limOnly:
            return lim

        idx = find_closest(A, lim)
        
        Lim = A[idx]
        
        if slice:
            IDX = idx.astype(object)
            if IDX[1]!=-1:
                IDX[1] += 1
            IDX[IDX==-1] = None
            IDX[IDX>=len(A)] = None
            if A[0]>A[-1]:
                IDX = np.hstack((IDX, -1))
        else:
            IDX = idx
        if debug:
            raise ValueError
        else:
            return (IDX, Lim)
    
    def _linPlt(self, ax, dep=0, dTp=None, zLim=None, zLab=None, yPt=None, cbarFraction=None,
                norm=False, supAx=(False,False,False), invY=False):
        
        from matplotlib import ticker as mplt
        
        xL,yL,zL,xQuant,yQuant,zQuant,xUnit,yUnit,zUnit,dataset_label = self.loaddTpList(dTp, _nolab=False)
        
        if type(dep)!=type(None):
            if isinstance(dep, collections.Iterable) and type(dep)!=type(str):
                if len(dep)>1:
                    raise NotImplementedError
            elif type(dep)!=int:
                dep = int(dep)
        else:
            dep = int(0)
        
        xLim = ax.get_xlim()
        for i,dat in enumerate(zL):
            dat[np.isnan(dat)] = 0.
            x,y,dat = self.std_dTp(xL[i],yL[i],dat)
            if type(yPt)!=type(None):
                if type(yPt)==(float or int):
                    yIdx = get_lim(y[dep,0,:], np.asarray([yPt,]))
                elif type(yPt)==tuple:
                    yIdx = get_lim(y[dep,0,:], np.asarray([yPt]).squeeze())
                else:
                    yIdx,yLim = self._stdIdxLim(y[dep,0,:], yPt, slice=True)
                    yIdx=np.arange(yIdx[0],yIdx[1], dtype=int)
            else:
                yIdx,yLim = np.asarray([-1]), np.asarray(y[:,:,-1].ravel())
            
            xIdx,_ = self._stdIdxLim(x[dep,:,0], xLim, slice=True)
            xSlice=slice(*xIdx)
            ln=[]
            
            ax.axhline(y=0, linestyle=':', color='black')
            for idx in yIdx:
                C,E = det_sci(y[dep,0,idx].squeeze())
                E = np.float(E)
                #label = r'{0} at {1:.0f}$\times$10\textsuperscript{{{2:.0f}}} {3}'.format(dataset_label[i], C,E, yUnit[i])
                label = r'{0}'.format(dataset_label[i])
                
                if norm:
                    nrm  = (dat[dep,xSlice,idx] / np.linalg.norm(dat[dep,xSlice,idx]))
                    zLab = r'$\rm A.U.$'
                else:
                    nrm = dat[dep,xSlice,idx]
                
                ax.plot(x[dep,xSlice],nrm, linewidth=matplotlib.rcParams['lines.linewidth']*2,
                        label=label)
                #bork
        if type(zLim)!=type(None):
            zLim = self._stdIdxLim(dat, zLim, limOnly=True)
            ax.set_ylim(zLim)
        else:
            ax.set_ylim(auto=True)
        
        ax.yaxis.set_major_locator(mplt.MaxNLocator(nbins=5, prune='lower'))
        ax.yaxis.set_major_formatter(mplt.ScalarFormatter(useOffset=False, useMathText=True))
        ax.yaxis.set_minor_locator(mplt.AutoMinorLocator())
        #bork
        ax.yaxis.set_ticks_position('both')
        for axis in ['top','bottom','left','right']:
            ax.spines[axis].set_linewidth(matplotlib.rcParams['axes.linewidth'])
        ytL = ax.get_yticks()
        C,E = det_sci(ytL)
        exp = E[E!=0].max()
        tick_labs = [r"$%.1f$" % x for x in ytL/10**exp]
        ax.yaxis.set_ticklabels(tick_labs)
        if supAx[1] != False:
            ax.get_yaxis().set_ticklabels([])
        else:
            ax.yaxis.set_label_text(zLab+r'$/10^{%s}$'%(int(exp)))
            if invY:
                ax.yaxis.set_label_position('right')
                ax.yaxis.tick_right()
                for label in ax.yaxis.get_ticklabels(): label.set_horizontalalignment('right')
                for tick in ax.yaxis.majorTicks: tick.set_pad(2*matplotlib.rcParams['font.size'])
            else:
                ax.yaxis.set_label_position('left')
                for label in ax.yaxis.get_ticklabels(): label.set_horizontalalignment('right')
        ax.legend(frameon=False,loc=0)
        #bork
        return ax


    def plotCont(self, dTp=None, Ax=None, dep=0, xLim=None, yLim=None, zLim=None, 
                 transpose=False, xLog=False, yLog=False, out=False,
                 linthreshX=0.1, linthreshY=0.1, userPlotParams=None, 
                 clrlvl=50, cntlvl=20, cmap='RdBu_r', resample=None,
                 linPlt=None, linPlt_zLim=None, linPlt_yPt=None, linPlt_SamezLim=False,
                 pltSize=1., pltHeight=6., pltWidth=5., linPltNorm=False, supAx=(False,False,False),cbar='vertical', 
                 cbarFraction=0.15, noCbar=False, Ncbarticks=10, invY=False, Title=None,
                 gridspec_kw={'wspace': 0.025,'hspace':0.025}, *args, **kwargs):
        """
                dTp (str)                : Specifies the data to be plotted.
                dep (list)               : index of dependency to be plotted.
                extY (str or array-like) : 
                xLim                     : 
                yLim                     : 
                transpose                : 
                cntlvl                   : 
                resample (tuple)         : resamples the data to a lower resolution by the given factor. First element 
                                           is for the x axis, the second for the y.
        """
        import matplotlib
        import matplotlib.ticker
        xL,yL,zL,xQuant,yQuant,zQuant,xUnit,yUnit,zUnit,dataset_label = self.loaddTpList(dTp, _transpose=transpose, _nolab=False)
        if transpose:
            yLim,xLim = xLim,yLim
            yLog,xLog = xLog,yLog
            linthreshY,linthreshX = linthreshX,linthreshY

        #xLplt,yLplt,zLplt,xQuantplt,yQuantplt,zQuantplt,xUnitplt,yUnitplt,zUnitplt,dataset_labelplt = self.loaddTpList(linPlt, _nolab=False)

        cPlth = pltHeight

        if type(linPlt)!=type(None):
            ncols = 1
            nrows = 2
            gridspec_kw.update({'height_ratios' : [1,4]})
            h_w   = (pltHeight*(1+(nrows-1)*gridspec_kw['hspace']))/float(pltWidth*(1+cbarFraction+(ncols-1)*gridspec_kw['wspace']))
            cPltw = cPlth*h_w
            lPlth = 0.25*cPlth
            size  = ((cPltw+cPltw*cbarFraction)*pltSize, (cPlth+lPlth)*pltSize)
            gsD   = gridspec_kw
        else:
            ncols = 1
            nrows = 1
            h_w   = (pltHeight/float(pltWidth))
            cPltw = cPlth*h_w
            size  = (cPltw*pltSize,cPlth*pltSize)
            gsD   = None

        #plotParams = {}
        #if userPlotParams:
            #plotParams.update(userPlotParams)
        #elif type(userPlotParams)==type(None):
            #plotParams.update(contPlotParams())

        #matplotlib.rcParams.update(plotParams)
        #matplotlib.rc('text', usetex=True)
        #matplotlib.rc('font', family='serif')

        if type(Ax)==type(None):
            fig, ax = plt.subplots(ncols=ncols, nrows=nrows, squeeze=False, figsize=(size), gridspec_kw=gsD, *args, **kwargs)
            ax = np.asarray([ax], dtype=object).ravel()
        else:
            fig,ax=None,Ax
        
        if type(dep)!=type(None):
            if isinstance(dep, collections.Iterable) and type(dep)!=type(str):
                if len(dep)>1:
                    raise NotImplementedError
            elif type(dep)!=int:
                dep = int(dep)
        else:
            dep = int(0)
        
        for i,dat in enumerate(zL):

            x = xL[i][dep]
            y = yL[i][dep]
            z = dat[dep]

            nanmask = np.isnan(z)
            z[nanmask] = 0

            if type(resample)==type(tuple()) and type(resample)!=type(None):
                import scipy.ndimage as snd
                z = snd.zoom(z,resample,mode='reflect')
                x = snd.zoom(x,(resample[0]),mode='reflect')
                y = snd.zoom(y,(resample[1]),mode='reflect')
            
            
            xIdx,xLim = self._stdIdxLim(x, xLim, slice=True)
            xSlice=slice(*xIdx)
            yIdx,yLim = self._stdIdxLim(y, yLim, slice=True)
            ySlice=slice(*yIdx)
            
            zLim = self._stdIdxLim(z[xSlice,ySlice], zLim, limOnly=True)
            #bork
            grid_x, grid_y = np.meshgrid(x[xSlice,:], y[:,ySlice], copy=False, indexing='ij')

            ax[-1].set_xlim(xLim)
            ax[-1].set_ylim(yLim)
            
            
            #colorlvl = np.linspace(zLim.min(),zLim.max(),num=clrlvl,endpoint=True)
            #contours = np.linspace(zLim.min(),zLim.max(),num=cntlvl,endpoint=True)
            #cbartick = np.linspace(zLim.min(),zLim.max(),num=10,endpoint=True)
            
            colorlvl = np.linspace(zLim.min(),zLim.max(),num=clrlvl,endpoint=True)

            contours = libwaxs.zeroRange(zLim, cntlvl)
            cbartick = libwaxs.zeroRange(zLim, 10)
            
            #cosmetics
            
            ysm = r''
            xsm = r''
            zsm = r''
            
            if yLog:
                ysm = r'\log_{10}'
                ax[-1].set_yscale('symlog',linthreshy=linthreshY)
                #ax[-1].yaxis.set_major_locator(matplotlib.ticker.LogLocator(base=10.0))
                #ax[-1].yaxis.set_major_formatter(matplotlib.ticker.LogFormatterMathtext(base=10.0, labelOnlyBase=True))
                ax[-1].yaxis.set_minor_locator(MinorSymLogLocator(linthreshY))
            else:
                #exp = det_sci(np.diff(yLim))
                #base = 10**exp[1][0]
                #ax[-1].yaxis.set_major_locator(matplotlib.ticker.MultipleLocator(base=base))
                ax[-1].yaxis.set_major_locator(matplotlib.ticker.MaxNLocator(integer=True, prune='lower',
                                                                             #nbins=9,
                                                                             ))
                ax[-1].yaxis.set_major_formatter(matplotlib.ticker.ScalarFormatter(useOffset=False, useMathText=True))
                ax[-1].yaxis.set_minor_locator(matplotlib.ticker.AutoMinorLocator())
            if xLog:
                xsm = r'\log_{10}'
                ax[-1].set_xscale('symlog',linthreshx=linthreshX)
                #ax[-1].xaxis.set_major_locator(matplotlib.ticker.LogFormatterMathtext(base=10.0, labelOnlyBase=True))
                #ax[-1].xaxis.set_major_formatter(matplotlib.ticker.LogLocator(base=10.0))
                ax[-1].xaxis.set_minor_locator(MinorSymLogLocator(linthreshX))
            else:
                #exp = det_sci(np.diff(xLim))
                #base = 10**(exp[1][0]-1)
                #ax[-1].xaxis.set_major_locator(matplotlib.ticker.MultipleLocator(base=base))
                ax[-1].xaxis.set_major_locator(matplotlib.ticker.MaxNLocator(integer=True, prune='upper',
                                                                             #nbins=9,
                                                                             ))
                ax[-1].xaxis.set_major_formatter(matplotlib.ticker.ScalarFormatter(useOffset=False, useMathText=True))
                ax[-1].xaxis.set_minor_locator(matplotlib.ticker.AutoMinorLocator())

            if transpose:
                yls = r'$%s'+r'\ '+r'%s'+r'(%s)'+r'$'
                xls = r'%s'+r' $'+r'%s'+r'(\mathrm{%s})'+r'$'
            else:
                xls = r'$%s'+r'\ '+r'%s'+r'(%s)'+r'$'
                yls = r'%s'+r' $'+r'%s'+r'(\mathrm{%s})'+r'$'

            zls = r'$'+r'%s'+r'%s'+r'(%s)'+r'$'

            rexAdd = re.compile(r'(\S+)_(\S+)*')
            if rexAdd.match('{0}'.format(xQuant[i])):
                xq = rexAdd.match(xQuant[i]).groups()[0]
            else:
                xq = xQuant[i]

            if rexAdd.match('{0}'.format(xQuant[i])):
                cq = rexAdd.match(self.getQuant(zUnit[i])).groups()[0]
            else:
                if type(zUnit[i])!=type(None):
                    cq = self.getQuant(zUnit[i])
                else:
                    cq = 'None'
                
            xlab = xls%(xq,xsm,xUnit[i])
            ylab = yls%(yQuant[i],ysm,yUnit[i])
            cLab = zls%(zQuant[i],zsm,cq)

            #if transpose != False:
                ##grid_x = grid_x.T
                ##grid_y = grid_y.T
                ##z = z.T
                #xLab = ylab
                #yLab = xlab
            #else:
            xLab = xlab
            yLab = ylab

            #print 'x: %s,\ny: %s,\nz: %s,\n'%(xlab, ylab, cLab)

            if supAx[0] != False:
                ax[-1].get_xaxis().set_ticklabels([])
                #pass
            else:
                ax[-1].set_xlabel(xLab)

            if supAx[1] != False:
                ax[-1].get_yaxis().set_ticklabels([])
            else:
                ax[-1].set_ylabel(yLab)

            if invY:
                ax[-1].yaxis.set_label_position('right')
                ax[-1].yaxis.tick_right()
            for axis in ['top','bottom','left','right']:
                ax[-1].spines[axis].set_visible(True)
                ax[-1].spines[axis].set_linewidth(matplotlib.rcParams['axes.linewidth'])

            ax[-1].yaxis.set_ticks_position('both')

            if type(linPlt)!=type(None):
                if type(linPlt_SamezLim)==True:
                    linPlt_zLim = zLim
                self._linPlt(ax[0], dep=dep, zLim=linPlt_zLim, dTp=linPlt, yPt=linPlt_yPt,
                             norm=linPltNorm, zLab=cLab, cbarFraction=cbarFraction,
                             supAx=supAx, invY=invY)

            my_cmap = getattr(matplotlib.cm, cmap)
            newcmap = libwaxs.shiftedColorMap(my_cmap, midpoint=(1 - zLim.max()/(zLim.max() + abs(zLim.min()))), name='shifted')

            CP=ax[-1].contour(grid_x,grid_y,z[xSlice,ySlice],levels=contours,vmin=zLim.min(),vmax=zLim.max(),colors='0', linestyles='solid', linewidths=0.5)
            CP1=ax[-1].contourf(grid_x,grid_y,z[xSlice,ySlice],levels=colorlvl,vmin=zLim.min(),vmax=zLim.max(),cmap='shifted')

            ax[-1].yaxis.set_ticks_position('both')
            for axis in ['top','bottom','left','right']:
                ax[-1].spines[axis].set_linewidth(matplotlib.rcParams['axes.linewidth'])
            #ytL = ax[-1].get_yticks()
            #C,E = det_sci(ytL)
            #exp = E[E!=0].max()
            #tick_labs = [r"$%.1f$" % x for x in ytL/10**exp]
            #ax[-1]data.getAnisScat(dTp=['dS'], yLim=[10000,1000000], xLim=[1.5,5.0], scale=1., ovrWrt=True).yaxis.set_ticklabels(tick_labs)
            #ax[-1].set_ylabel(yLab+r'$/10^{%s}$'%(int(exp)))

            cAx,kw = matplotlib.colorbar.make_axes_gridspec(ax[-1],pad=0, fraction=cbarFraction, orientation=cbar)

            if type(linPlt)!=type(None):
                DAx,_ = matplotlib.colorbar.make_axes_gridspec(ax[0],pad=0, fraction=cbarFraction, orientation=cbar)
                DAx.axis('off')
            
            if supAx[2] or noCbar:
                cAx.axis('off')
            else:
                cb = matplotlib.colorbar.Colorbar(cAx,CP1, drawedges=False)
                cb.outline.set_linewidth(matplotlib.rcParams['axes.linewidth'])
                cb.set_ticks(cbartick.tolist())
                #cb.ax.yaxis.set_major_locator(matplotlib.ticker.MaxNLocator(nbins=5, prune='both'))
                C,E = det_sci(cbartick)
                test = E[E!=0]
                exp = test.max() if test.size!=0 else 0.
                tick_labs = [r"$%.1f$" % x for x in cbartick/10**exp]
                cb.set_ticklabels(tick_labs)
                cb.set_label(cLab+r'$/10^{%s}$'%(int(exp)))
                for label in cb.ax.yaxis.get_ticklabels(): label.set_horizontalalignment('right')
                for tick in cb.ax.yaxis.majorTicks: tick.set_pad(15*matplotlib.rcParams['font.size']*cbarFraction)
                cb.update_ticks()
            #bork
            if Title != None:
                ti=fig.suptitle(r'%s'%(Title))
                pos=ax[-1].get_position()
                ti.set_y(pos.y0+pos.height+0.05*(pos.height))

            if out:
                #(ax,(CP,CP1,cLab))
                return fig
            else:
                return


    def plotContMulti(self, dTp=None, dep=None, xLim=None, yLim=None, zLim=None, xLog=False, yLog=False, transpose=False, userPlotParams=None, Ax=None, clrlvl=50, cntlvl=20., resample=None, supAx=(False,False,False),cbar=False, cbarFraction=0.15, cbarFig = 'vertical', Title=None, plotShape='square', *args, **kwargs):
        """
        xLim            :
        yLim            :
        transpose       :
        cntlvl          :
        resample (tuple): resamples the data to a lower resolution by the given factor. First element is for the x axis, the second for the y.

        @param dTp: A list or tuple containing the datatypes to be plotted.\
        Datatypes of the same instance can be passed as normal: datatype (str).\
        If you want the dataset to have a label, the costruction is a tuple: (datatype (str), label (str)).\
        Data of a different instance of libwaxs.Dataset is passed as a tuple: (instance, datatype(str)).\
        If you want the dataset to have a label, the costruction is: (instance, (datatype(str), label (str))).\
        External data is passed as a tuple: (z (array-like), x (array-like), y (array-like), <label (str)>)\
        Multiple datatypes, libwaxs.Dataset instances, and external datasets.\
        The order of plotting is that of the order in which data is provided.
        @type dTp: list, tuple
        """
        
        xL,yL,zL,xQuant,yQuant,zQuant,xUnit,yUnit,zUnit,dTp_list=self.loaddTpList(dTp, _nolab=True)
                
        for ii,dat in enumerate(zL):
            
            z = self.std
            
            for i in dep:
                if type(resample)==type(tuple()) and type(resample)!=type(None):
                    import scipy.ndimage as snd
                    z[i] = snd.zoom(z[i],resample,mode='reflect')
                    x[i] = snd.zoom(x[i],(resample[0]),mode='reflect')
                    y[i] = snd.zoom(y[i],(resample[1]),mode='reflect')
            
                if plotShape=='square':
                    s = np.square(round(np.sqrt(len(z[i]))))
                    while len(z[i])>s:
                        s+=1
                    shape=(s,s)
                elif plotShape=='row':
                    shape=(1,s)
                elif plotShape=='column':
                    shape=(s,1)
                
                base,ar=10,2
                
                w,h= shape
                width  = w*base
                height = h/ar*base
                gSs=[]

                
                width_ratios  = np.ones(len(z[i])).tolist()
                height_ratios = np.ones(len(z[i])).tolist()
                ncol = 0
                nrow = 0
                if cbarFig == 'vertical':
                    ncol = 1
                    nrow = 0
                    width_ratios.append(cbarFraction)
                    height_ratios.append(1.)
                    
                elif cbarFig == 'horizontal':
                    ncol = 0
                    nrow = 1
                    width_ratios.append(1.)
                    height_ratios.append(cbarFraction)
                
                gs = matplotlib.gridspec.GridSpec(shape[0],shape[1], width_ratios=width_ratios, height_ratios=height_ratios)
                gSs.append(matplotlib.gridspec.GridSpecFromSubplotSpec(1,1,subplot_spec=gs[i]))
                
                if Ax != None:
                    ax = Ax
                else:
                    fig=plt.figure(figsize=(width,height))
                    ax=fig.add_subplot(111)

                # set up z-mask

                #Resize x, y, z, could do this more elegantly with masks...
                
                zLim = np.asarray(zLim)
                xLim = np.asarray(xLim)
                yLim = np.asarray(yLim)
                
                if zLim.ndim<2:
                    zLim = np.tile(zLim, (len(z[i]),1))
                
                if xLim.ndim<2:
                    xLim = np.tile(xLim, (len(x[i]),1))
                
                if yLim.ndim<2:
                    yLim = np.tile(yLim, (len(y[i]),1))
                
                lims=[[],[],[]]
                cnstr=[]
                xstp=[]
                ystp=[]
                xid=[]
                yid=[]
                zma=[]
                
                if xLim[i] != None:
                    lims[0].append(xLim[i].tolist())
                else:
                    lims[0].append([x[i].min(),x[i].max()])

                if yLim[i] != None:
                    lims[1].append(yLim[i].tolist())
                else:
                    lims[1].append([y[i].min(),y[i].max()])
                
                pre_xid=np.zeros(3,dtype=int)
                pre_yid=np.zeros(3,dtype=int)
                if x[i][0]<x[i][-1]:
                    pre_xid[2]=int(1)
                else:
                    pre_xid[2]=int(-1)

                if y[i][0]<y[i][-1]:
                    pre_yid[2]=int(1)
                else:
                    pre_yid[2]=int(-1)
                
                pre_xid[:2]=get_lim(x[i],lims[0][i])
                pre_yid[:2]=get_lim(y[i],lims[1][i])

                xid.append(pre_xid.tolist())
                yid.append(pre_yid.tolist())
                
                preMask = np.zeros(z[i].shape, dtype=bool)
                preMask[slice(*xid[i]),slice(*yid[i])] = True
                cnstr.append(preMask)
                
                if type(zLim[i])==list:
                    zma.append(np.ma.masked_array(z[i], mask=~cnstr[i]))
                    lims[2].append(zLim.tolist())
                elif zLim[i]==True:
                    zma.append(np.ma.masked_array(z[i], mask=~cnstr[i]))
                    labs =  abs(max(zma[i].min(),zma[i].max(), key=abs))
                    lims[2].append([-labs,labs])
                else:
                    zma.append(np.ma.masked_array(z[i], mask=~cnstr[i]))
                    lims[2].append([zma[i].min(),zma[i].max()])
                
                #cosmetics
                plotParams = {}
                if userPlotParams:
                    plotParams.update(userPlotParams)

                matplotlib.rcParams.update(plotParams)
                
                
                
                xls = r'$%s'+r'\ '+r'%s'+r'(%s)'+r'$'
                yls = r'%s'+r' $'+r'%s'+r'(\mathrm{%s})'+r'$'
                zls = r'$'+r'%s'+r'%s'+r'(%s)'+r'$'

                ysm = r'%s'
                xsm = r'%s'
                zsm = r'%s'
                grid_x = []
                grid_y = []
                
                a,b = np.meshgrid(x[i], y[i], copy=False, indexing='ij')
                grid_x.append(a)
                grid_y.append(b)
                
                PlotSpec = {'x':{},
                            'y':{},
                            'z':{}
                            }
                PlotSpec['x']['mtickspace']     = np.diff(lims[0])/10.
                PlotSpec['x']['tickfontsize']   = 10
                PlotSpec['x']['format']         = '%g'
                PlotSpec['y']['mtickspace']     = np.diff(lims[1])/10.
                PlotSpec['y']['tickfontsize']   = 10.
                PlotSpec['y']['format']         = '%.0e'


                contours = np.linspace(lims[i][2][0],lims[i][2][1],num=cntlvl,endpoint=True)
                colorlvl = np.linspace(lims[i][2][0],lims[i][2][1],num=clrlvl,endpoint=True)

                xmajorLocator   = mpl.MultipleLocator(PlotSpec['x']['mtickspace'])
                xmajorFormatter = mpl.FormatStrFormatter(PlotSpec['x']['format'])
                xminorLocator   = mpl.MultipleLocator(PlotSpec['x']['mtickspace']/5.)
                #minorFormatter = mpl.NullFormatter()

                ymajorLocator   = mpl.MultipleLocator(PlotSpec['y']['mtickspace'])
                ymajorFormatter = mpl.FormatStrFormatter(PlotSpec['y']['format'])
                yminorLocator   = mpl.MultipleLocator(PlotSpec['y']['mtickspace']/5.)
                
                
                if yLog != False:
                    ysm%(r'\log_{10}')
                    ax.set_yscale('symlog',linthreshx=0.1)
                    ymajorFormatter = mpl.LogFormatterMathtext(base=10.0, labelOnlyBase=False)
                    ymajorLocator = mpl.LogLocator(base=10.0, subs=[1.0])
                    yminorLocator = mpl.LogLocator(base=10.0, subs=[1/100.0])

                if xLog != False:
                    xsm%(r'\log_{10}')
                    ax.set_xscale('symlog',linthreshx=0.1)
                    xmajorFormatter = mpl.LogFormatterMathtext(base=10.0, labelOnlyBase=False)
                    xmajorLocator = mpl.LogLocator(base=10.0, subs=[1.0])
                    xminorLocator = mpl.LogLocator(base=10.0, subs=[1/10.0])
                ###


                ax.xaxis.set_major_locator(xmajorLocator)
                #ax.xaxis.set_major_formatter(xmajorFormatter)
                ax.xaxis.set_minor_locator(xminorLocator)

                ax.yaxis.set_major_locator(ymajorLocator)
                #ax.yaxis.set_major_formatter(ymajorFormatter)
                ax.yaxis.set_minor_locator(yminorLocator)

                #ax.xaxis.set_minor_formatter(minorFormatter)
                ###


                xlab = xls%(xQuant[i],xsm,xUnit[i])
                ylab = yls%(yQuant[i],ysm,yUnit[i])
                cLab = zls%(zQuant[i],zsm,zUnit[i])
                
                #for i in range(len(z)):
                if transpose != False:
                    grid_x[i] = grid_x[i].T
                    grid_y[i] = grid_y[i].T
                    z[i] = z[i].T
                    zma[i] = zma[i].T
                    xLab = ylab
                    yLab = xlab
                else:
                    xLab = xlab
                    yLab = ylab
                    
                #print 'x: %s,\ny: %s,\nz: %s,\n'%(xlab, ylab, cLab)
            
                if supAx[0] != False:
                    ax.get_xaxis().set_ticklabels([])
                    #pass
                else:
                    ax.set_xlabel(xLab)

                if supAx[1] != False:
                    ax.get_yaxis().set_ticklabels([])
                else:
                    ax.set_ylabel(yLab)
                
                formatter=mpl.ScalarFormatter(useOffset=False, useMathText=True)
                ax.xaxis.set_major_formatter(formatter)
                ax.yaxis.set_major_formatter(formatter)
                
                CP=ax.contour(grid_x[i],grid_y[i],z[i],levels=contours,vmin=lims[2][0],vmax=lims[2][1],hold='on',colors='0', *args, **kwargs)
                CP1=ax.contourf(grid_x[i],grid_y[i],z[i],levels=colorlvl,vmin=lims[2][0],vmax=lims[2][1]) #,cmap=my_cmap

                if supAx[2] != True and cbar!=None:
                    cAx,kw = matplotlib.colorbar.make_axes_gridspec(ax,pad=0, fraction=cbarFraction, orientation=cbar)
                    cb = matplotlib.colorbar.Colorbar(cAx,CP1, format='%0.2e')
                    cb.add_lines(CP)
                    cb.set_label(cLab)

                if Title != None:
                    ti=fig.suptitle(r'%s'%(Title))
                    pos=ax.get_position()
                    ti.set_y(pos.y0+pos.height+0.05*(pos.height))
                    #bork

        if Ax != None:
            figax = ax
        else:
            figax = fig

        figPck=(figax,(CP,CP1,cLab))

        return figPck

    def plot1D(self, dTp=None, axis=None, axis_pts=None, stack=False, dep=0,
               **kwargs):

        if not isinstance(axis , collections.Iterable) or type(dTp)==str:
            axis = [axis]

        if not isinstance(dTp , collections.Iterable) or type(dTp)==str:
            dTp = [dTp]

        D,ordinate = self.getAxisDeps(axis)
        AX   = []
        oAX  = []
        dTpL = []
        ordi = []

        c = 0
        for key in list(D.keys()):
            for j,el in enumerate(D[key]):
                if el in dTp:
                    dTpL.append(el)
                    ordi.append(ordinate[c])
                    #if ordinate[c]=='x':
                        #AX.append(1)
                        #oAX.append(2)
                    #else:
                        #AX.append(2)
                        #oAX.append(1)
                c+=1

        dat  = []
        datx = []
        daty = []
        lab  = []

        for i,el in enumerate(dTpL):

            xL,yL,zL,xQuant,yQuant,zQuant,xUnit,yUnit,zUnit,dataset_label = self.loaddTpList(el, _transpose = True if ordi[i]=='y' else False, _nolab=False)

            x,y,z = self.std_dTp(xL[0],yL[0],zL[0])

            ptIdx = find_closest(y.squeeze(), axis_pts) if type(axis_pts)!=type(None) else np.arange(y.shape[2])
            z_pos = []
            dat.append(z[dep,:,ptIdx].T)
            datx.append(x[dep,:,:])
            daty.append(y[dep,:,ptIdx])


        PdatY = [] #np.asarray(daty, dtype=object)
        PdatX = [] #np.asarray(datx, dtype=object)
        Pdat  = [] #np.asarray(dat,  dtype=object)

        for i in range(len(axis_pts)):
            
            _PdatY = [] #np.asarray(daty, dtype=object)
            _PdatX = [] #np.asarray(datx, dtype=object)
            _Pdat  = [] #np.asarray(dat,  dtype=object)

            for j in range(len(dat)):
                if stack:
                    _Pdat.append(dat[j][:,j])
                    _PdatX.append(datx[j][:,0])
                    _PdatY.append(daty[j][:,0])
                else:
                    Pdat.append(dat[j][:,j])
                    PdatX.append(datx[j][:,0])
                    PdatY.append(daty[j][:,0])

            if stack:
                PdatY.append(np.asarray(_PdatY, dtype=object))
                PdatX.append(np.asarray(_PdatX, dtype=object))
                Pdat.append(np.asarray(_Pdat ,  dtype=object))

        PdatY = np.asarray(PdatY, dtype=object)
        PdatX = np.asarray(PdatX, dtype=object)
        Pdat  = np.asarray(Pdat , dtype=object)

        #quick_plot(**kwargs)
        #poop
        datL = []
        for i in range(len(Pdat)):
            datL.append([PdatX[i], Pdat[i], {}])

        if stack:
            kwargs.update({'style'    : 'WAXSstack',
                           'increment': True})

        quick_plot(datL, **kwargs)

        return


    def plotStack(self, dTp=None, xAx='x', xPt=None, yPt=None, zLim=None, xLim=None, yLim=None,
                  xLog=False, yLog=False, increment=None, transpose=False, userPlotParams=None,
                  Ax=None, Title=None, extXQU=None, extYQU=None, *args, **kwargs):
        '''
        Generates a stacked plot of 1D curves.
        The order of plotting is that of the order in which data is provided.
        @param dTp: Pass data to be plotted, see libwaxs.Dataset.loaddTpList for more details.
        @type dTp: list, tuple
        '''
        from matplotlib import ticker as mplt
        if Ax != None:
            ax = Ax
        else:
            fig=plt.figure()
            ax=fig.add_subplot(111)

        if type(dTp)==str:
            dTp = [dTp]
        
        if type(xAx)==type(str):
            pass
        elif isinstance(xAx, collections.Iterable):
            try:
                xAx = str(xAx[0])
            except:
                print('%s is not a valid input for the kwarg xAx.'%(type(xAx)))
                return None
        else:
            try:
                xAx = str(xAx)
            except:
                print('%s is not a valid input for the kwarg xAx.'%(type(xAx)))
                return None
        
        if xAx == 'x' or (xAx == 'y' and transpose):
            zL,xL,yL,zQuant,xQuant,yQuant,zUnit,xUnit,yUnit,dataset_label = self.loaddTpList(dTp)
        elif xAx == 'y' or (xAx == 'x' and transpose):
            zL,yL,xL,zQuant,yQuant,xQuant,zUnit,yUnit,xUnit,dataset_label = self.loaddTpList(dTp)
            yPt,xPt,yLim,xLim,yLog,xLog = xPt,yPt,xLim,yLim,xLog,yLog
            for i in range(len(z)):
                z[i] = z[i].T
        else:
            print('%s is not a valid axis.'%(xAx))
            xAx='x'
            print('Plotting using default x-axis.')
        
        for i in range(len(z)):
            if not z[i].shape[0]==x[i].shape[0]:
                print("Shape of data x-axis ({1}) does not match that of the x values ({2}) for {0}!".format(dataset_label[i],z[i].shape[0],x[i].shape[0]))
            elif not z[i].shape[1]==y[i].shape[0]:
                print("Shape of data y-axis ({1}) does not match that of the y values ({2}) for {0}!".format(dataset_label[i],z[i].shape[1],y[i].shape[0]))
                if z[i].shape[1]==y[0].shape[0]:
                    print("Shape of data y-axis ({1}) matches that of the the instance y values ({2}) for {0}.".format(dataset_label[i],z[i].shape[1],y[0].shape[0]))
                    y[i]=y[0]
                    pass
                elif z[i].shape[1]==1:
                    print("Creating dummy y axis for {0}.".format(dataset_label[i]))
                    y[i]=np.asarray(yPt)
                    z[i],_ = np.meshgrid(z[i], y[i], copy=False, indexing='ij')
                #print y[i].shape
        
        lims=[[0,0],[0,0],[0,0]]
        for i in range(len(z)):
            if xLim != None:
                lims[0]=xLim
            else:
                lims[0][0],lims[0][1]=x[i].min(),x[i].max()
            
            if yLim != None:
                lims[1]=yLim
            else:
                lims[1][0],lims[1][1]=y[i].min(),y[i].max()
        
        #Resize x, y, z, could do this more elegantly with masks...
        cnstr=[]
        xstp=[]
        ystp=[]
        xid=[]
        yid=[]
        zma=[]
        xslice=[]
        yslice=[]
        for i in range(len(z)):
            pre_xid=np.zeros(3,dtype=int)
            pre_yid=np.zeros(3,dtype=int)
            if x[i][0]<=x[i][-1]:
                pre_xid[2]=int(1)
            else:
                pre_xid[2]=int(-1)

            if y[i][0]<=y[i][-1]:
                pre_yid[2]=int(1)
            else:
                pre_yid[2]=int(-1)
            
            pre_xid[:2] = get_lim(x[i],lims[0])
            pre_yid[:2] = get_lim(y[i],lims[1])
            
            if np.diff(pre_xid[:2]).all()==0:
                pre_xid[:2] = np.array([0,1])
            if np.diff(pre_yid[:2]).all()==0:
                pre_yid[:2] = np.array([0,1])
            
            xid.append(pre_xid.tolist())
            yid.append(pre_yid.tolist())
            
            preMask = np.zeros(z[i].shape, dtype=bool)
            preMask[slice(*xid[i]),slice(*yid[i])] = True
            cnstr.append(preMask)
            
            xslice.append(slice(*xid[i]))
            yslice.append(slice(*yid[i]))
            
            if type(zLim)==list:
                zma.append(np.ma.masked_array(z[i], mask=~cnstr[i]))
                lims[2]=zLim
            elif zLim==True:
                zma.append(np.ma.masked_array(z[i], mask=~cnstr[i]))
                labs =  abs(max(zma[i].min(),zma[i].max(), key=abs))
                
                lims[2][0] = -labs
                lims[2][1] = labs
            else:
                zma.append(np.ma.masked_array(z[i], mask=~cnstr[i]))
                lims[2][0],lims[2][1]=zma[i].min(),zma[i].max()
        
        
        
        plt.rc('text', usetex=True)
        plt.rc('font', family='sanserif')
        plotParams = {}
        if userPlotParams:
            plotParams.update(userPlotParams)

        matplotlib.rcParams.update(plotParams)
        
        formatter=mplt.ScalarFormatter(useOffset=False, useMathText=True)
        ax.xaxis.set_major_formatter(formatter)
        ax.yaxis.set_major_formatter(formatter)
        ax.xaxis.set_minor_formatter(formatter)
        ax.yaxis.set_minor_formatter(formatter)
        
        #ysm = r''
        xsm = r''
        zsm = r''
        
        xls = r'$%s'+r'\ '+r'%s'+r'(%s)'+r'$'
        #yls = r'%s'+r' $'+r'%s'+r'(\mathrm{%s})'+r'$'
        zls = r'$'+r'%s'+r'%s'+r'(%s)'+r'$'
        
        if extXQU:
            xlab = xls%(extXQU[0],xsm,extXQU[1])
        else:
            xlab = xls%(xQuant[0],xsm,xUnit[0])
            
        if extYQU:
            ylab = zls%(extYQU[0],zsm,extYQU[1])
        else:
            ylab = zls%(zQuant[0],zsm,zUnit[0])


        if transpose != False and transpose != None:
            for i in range(len(z)):
                x[i] = x[i].T
                z[i] = z[i].T
            xLab = ylab
            yLab = xlab
        else:
            xLab = xlab
            yLab = ylab
        
        #xPtIdx=[]
        yPtIdx=[]
        
        
        #if type(xPt)==type(None):
            #xPt=[0.]
            #for i in range(len(z)):
                #xPtIdx.append(get_lim(x[i],xPt))
            #inc_lab = ''
        #elif type(xPt)!=type(False) and not isinstance(xPt, collections.Iterable):
            #for i in range(len(z)):
                #xPtIdx.append(get_lim(x[i],np.linspace(0., x[i][xslice[i]].max(), num=10, endpoint=True)))
            #inc_lab = r'%s $\mathrm{%s}$'
        #elif isinstance(xPt, collections.Iterable) or type(xPt)==float or type(xPt)==int:
            #for i in range(len(z)):
                #xPtIdx.append(get_lim(x[i],xPt))
            #inc_lab = r'%s $\mathrm{%s}$'
        #else:
            #print("Specify time point! Type %s is invalid!"%(type(xPt)))
            #return None
        
        if type(yPt)==type(None):
            yPt=[0.]
            for i in range(len(z)):
                yPtIdx.append(get_lim(y[i],yPt))
            inc_lab = ''
        elif type(yPt)==bool and not isinstance(yPt, collections.Iterable):
            for i in range(len(z)):
                yPtIdx.append(get_lim(y[i],np.linspace(0., y[i][yslice[i]].max(), num=10, endpoint=True)))
            inc_lab = r'%s $\mathrm{%s}$'
        elif isinstance(yPt, collections.Iterable) or type(yPt)==float or type(yPt)==int:
            if not isinstance(yPt, collections.Iterable):
                yPt = [yPt]
            for i in range(len(z)):
                yPtIdx.append(get_lim(y[i],yPt))
            inc_lab = r'%s $\mathrm{%s}$'
        else:
            print("Specify time point! Type %s is invalid!"%(type(yPt)))
            return None
        
        inc=0
        if increment==True:
            for i in range(len(z)):
                a = (z[i][xslice[i],yslice[i]].max()-z[i][xslice[i],yslice[i]].min())/2.
                if abs(a)>inc:
                    inc = a
        elif type(increment)==float or type(increment)==int:
            inc = increment
        
        ax.set_xlabel(xLab)
        ax.set_ylabel(yLab)
        
        ax.set_xlim(x[0][xid[0][:2]])
        #ax.set_ylim(z[xid,yid])
        
        if Title != None:
            ti=ax.suptitle(r'%s'%(Title))
            pos=ax.get_position()
            ti.set_y(pos.y0+pos.height+0.05*(pos.height))
        
        color_inc   = ['red', 'LimeGreen', 'blue', 'orange','DarkViolet',  'DarkRed', 'ForestGreen', 'purple']
        color_noinc = ['red', 'LimeGreen', 'blue', 'orange','DarkViolet',  'DarkRed', 'ForestGreen', 'purple']
        no_inclab = r'%s: %s $\mathrm{%s}$'
        for i in range(len(yPt)):
            ax.axhline(y=i*inc,linestyle=':',color='black', *args, **kwargs)
            for j,d in enumerate(z):
                if type(increment)==float or type(increment)==int or increment==True:
                    ax.plot(x[j], d[:,yPtIdx[j][i]]+i*inc,color=color_inc[j], label=r'%s'%(dataset_label[j]), scaley=True, *args, **kwargs)
                    if j==0 and (type(increment)==float or type(increment)==int or increment==True):
                        try:
                            ax.text(x[j][slice(*xid[j])].max()*0.85,inc/5.+(i*inc),inc_lab%(sci_notation(y[0][yPtIdx[0][i]],precision=2,lim=2),yUnit[j]))
                        except TypeError:
                            pass
                elif not increment:
                    ax.plot(x[j], d[:,yPtIdx[j][i]]+i*inc,color=color_noinc[i], label=no_inclab%(dataset_label[j],sci_notation(y[0][yPtIdx[0][i]],precision=2, lim=2),yUnit[j]), scaley=True, *args, **kwargs)
            if i == 0 and (type(increment)==float or type(increment)==int or increment==True):
                #print 'Handles & labels requested for dTp:\'%s\' (level 1)!'%(dataset_label[j]), 'i:%s'%(i), 'j:%s'%(j), 'Increment:\'%s\' type:%s'%(increment, type(increment))
                handles, labels = ax.get_legend_handles_labels()
        if not increment:
            #print 'Handles & labels requested for dTp:\'%s\' (level 2)!'%(dataset_label[j]), 'i:%s'%(i), 'j:%s'%(j), 'Increment:\'%s\' type:%s'%(increment, type(increment))
            handles, labels = ax.get_legend_handles_labels()
            if len(yPt)>=4:
                box = ax.get_position()
                ax.set_position([box.x0, box.y0, box.width * 0.9, box.height])
                ax.legend(handles, labels, ncol=1, frameon=True, loc='center left', bbox_to_anchor=(0.99, 0.5))
            else:    
                ax.legend(handles, labels, ncol=len(z), frameon=False, loc=8)
        else:
            if len(z)>=10:
                box = ax.get_position()
                ax.set_position([box.x0, box.y0, box.width * 0.9, box.height])
                ax.legend(handles, labels, ncol=1, frameon=True, loc='center left', bbox_to_anchor=(0.99, 0.5))
            else:
                ax.legend(handles, labels, frameon=False, loc='lower left')
            
        #bork
        if Ax != None:
            figax = ax
        else:
            figax = fig

        figPck = figax
        #bork
        return figPck

    def std_dims(self, A, ax=None):
        """
        Helper function to get a standardized 3D numpy array of a non-standard array.
        TODO Phase this out! It does not work with x and y axis dependencies!!! 
             The dep axis is lost and the axis is concatenated
        """
        
        A = np.asarray(A)
        if np.ndim(A)<3:
            std_A = A[(tuple(np.newaxis for x in range(3 - np.ndim(A))))]
        elif np.ndim(A)>3:
            raise ValueError("Number of dimensions (%s) of data greater than 3 dimensions is not supported!"%(np.ndim(A)))
        else:
            return A
        
        if ax:
            if ax == 'y' or ax == 2:
                std_A = A.reshape(1,1,-1)
            elif ax == 'x' or ax == 1:
                std_A = A.reshape(1,-1,1)
        
        return std_A

    def std_dTp(self,x,y,z, dTp = None, ovrWrt=False):
        """
        Standardizes a dTp to have the shape: (len(dep), len(x), len(y)).
        Removed write function, create dTp's only with gen_dTp()!
        """
        #x = self.std_dims(x, ax='x')
        #y = self.std_dims(y, ax='y')
        #z = self.std_dims(z)
        x = np.atleast_3d(x.squeeze())
        y = np.atleast_3d(y.squeeze())
        z = self.std_dims(z)
        
        #xShape = x.shape
        #yShape = y.shape
        #zShape = z.shape
        
        if z.shape[:-1] == x.shape[:-1] and z.shape[::2] == y.shape[::2]:
            x,y,z = x,y,z
        elif z.shape[1] == y.shape[1] and z.shape[2] == x.shape[2]:
            x,y,z = np.transpose(x, axes=(0,2,1)),np.transpose(y, axes=(0,2,1)),np.transpose(z, axes=(0,2,1))            
        elif z.shape[2]==x.shape[1] and z.shape[1] == y.shape[2]:
            z = np.transpose(z, axes=(0,2,1))
        elif z.shape[2] != y.shape[2] and z.shape[2] in y.shape:
            y = np.transpose(y, axes=(0,2,1))
        elif z.shape[1] != x.shape[1] and z.shape[1] in x.shape:
            x = np.transpose(x, axes=(0,2,1))
        else:
            raise ValueError("You need to sort this mess out manually!")
        
        #if ovrWrt and not dTp:
            #raise ValueError("libwaxs.datasetClass.std_dTp: If you want to over write a dTp, you must provide the name!")
        #elif ovrWrt:
            #self.gen_dTp('{0}'.format(dTp), z, dTp_like='{0}'.format(dTp),
                         #ovrWrt=ovrWrt)
        
        return (x,y,z)

    ##@staticmethod
    #def getPtIdx(self, A, Pt, axis=-1, inc_end=True):
        #"""
        #Helper function to get points in the suitable format for the libwaxs.datasetClass functions.
        #A       : 1darray in which to search points.
                  #ndarray 1st axis has to be the dependency, the second axis the array in which to search the Pt.
        
        #Pt      : point(s) in the following formats:
                        #3d array:
                            #(n, n, 2):         multiple ranges per dependency
                            #(n, 1, 2):         single range per dependency
                            #(n, n, 1):         multiple points per dependency
                            #(1, 1, 2):         single range for all dependencies
                            #(1, 1, 1):         single point for all dependencies
                        #2d array:
                            #(n, 2):            multiple ranges for all dependencies
                            #(1, 2):            single range for all dependencies
                            #(n, 1):            multiple points for all dependencies
                            #(1, 1):            single point for all dependencies
                        #1d array:
                            #(1):               single point for all dependencies
                            #(2):               single range for all dependencies
                            #(n):               multiple points for all dependencies
                        #bool:
                            #True, False, None: full range for all dependencies
                        #number:
                            #float or int:      single point for all dependencies
        #axis    : axis in which to search the Pt.
        #"""
        
        #if np.ndim(A)>3:
            #raise ValueError("Maximum number of dimensions for data (%s) is 3."%(np.ndim(A)))
        
        #if type(Pt)==bool or type(Pt)==type(None):
            #return self.std_dims([0,None]).astype(object)
        #elif type(Pt)==list or type(Pt)==np.ndarray or type(Pt)==tuple or type(Pt)==int or type(Pt)==float:
            #Pt = self.std_dims(Pt).astype(object)
        #else:
            #raise TypeError("Pt type (%s) is incompatible."%(type(Pt)))

        #A = self.std_dims(A)
        #idx = np.zeros_like(Pt)
        
        #if Pt.shape[0]==A.shape[0]:
            #rAx0 = range(A.shape[0])
        #elif Pt.shape[0]==A.shape[0]:
            #rAx0 = 1
        #else:
            #raise ValueError("Shape of axis 0 of the point list (%s) has to be equal to that of the array (%s) or 1."%(Pt.shape[0],A.shape[0]))
        
        ##try transposing the last two axes, just have to figure out how to determine whether it is necessary.
        #if Pt.shape[1]==A.shape[1] and A.shape[1]!=1:
            #rAxL = A.shape[2]
            #ax = 1
        #elif Pt.shape[1]==A.shape[2] and A.shape[2]!=1:
            #rAxL = A.shape[1]
            #ax = 2
        #else:
            #rAxL = 1
            
        #for i in range(rAx0):
            #for j in range(rAxL):
                #if ax ==1:
                    #idx[i] = np.apply_along_axis(get_lim, -1, A[i,j,:], Pt[i,j]).reshape(Pt[i,j].shape).astype(object)
                #elif ax == 2:
                    #idx[i] = np.apply_along_axis(get_lim, -1, A[i,:,j], Pt[i,j]).reshape(Pt[i,j].shape).astype(object)
        
        #idx[idx==-1] = None
        
        #if inc_end:
            #def _func(x):
                #if x[-1]!=None:
                    #x[-1] += 1
                    #return x
                #else:
                    #return x
            #np.apply_along_axis(_func, -1, idx)
        
        #return idx

    def tile_repeat(self, A, Pt):
        """
        The last axis will be repeated across the others. 
        """
        tile_repeat = np.asarray([1,1,1])
        
        if Pt.shape[0]!=A.shape[0] and Pt.shape[0]==1:
            tile_repeat[0] = A.shape[0]
        elif Pt.shape[0]!=A.shape[0] and Pt.shape[0]!=1:
                raise ValueError("The shape of the 1st dimension of Pt (%s) has to that of A (%s) or 1."%(Pt.shape[0], A.shape[0]))
        
        if Pt.shape[-2]!=A.shape[-2] and Pt.shape[-2]==1:
            tile_repeat[-2] = A.shape[-2]
        elif Pt.shape[-2]!=A.shape[-2] and Pt.shape[-2]!=1:
            raise ValueError("The shape of the 2nd dimension of Pt (%s) has to that of A (%s) or 1."%(Pt.shape[-1], A.shape[-1]))

        return np.tile(Pt, tuple(tile_repeat))

    def getPtIdx(self, A, Pt, axis = -1, one_Pt_per_axis = True, inc_end=True):
        """
        Function returns indeces for use with slice. If Pt is outside the range of A
        a 0 or None is returned depending on whether the Pt falls below min or above 
        max, respectively.
        A   : 1d-array like.
        
        """
        
        if type(Pt)==bool or type(Pt)==type(None):
            return self.std_dims([0,None]).astype(object)
        elif type(Pt)==list or type(Pt)==np.ndarray or type(Pt)==tuple or type(Pt)==int or type(Pt)==float:
            Pt = self.std_dims(Pt).astype(object)
        else:
            raise TypeError("Pt type (%s) is incompatible."%(type(Pt)))
        
        A = self.std_dims(A)
        
        axes        = list(range(len(A.shape)))
        ax_idx      = axes[axis]
        axes.remove(ax_idx)
        axes.append(ax_idx)
        iter_shape  = tuple(axes)
        A = np.transpose(A, axes=iter_shape)
        
        tile_repeat = np.asarray([1,1,1])
        
        if Pt.shape[0]!=A.shape[0] and Pt.shape[0]==1:
            tile_repeat[0] = A.shape[0]
        elif Pt.shape[0]!=A.shape[0] and Pt.shape[0]!=1:
                raise ValueError("The shape of the 1st dimension of Pt (%s) has to that of A (%s) or 1."%(Pt.shape[0], A.shape[0]))
        
        if one_Pt_per_axis:
            if Pt.shape[-2]!=A.shape[-2] and Pt.shape[-2]==1:
                tile_repeat[-2] = A.shape[-2]
            elif Pt.shape[-2]!=A.shape[-2] and Pt.shape[-2]!=1:
                raise ValueError("If one_Pt_per_axis flag is True, then number of Pt (%s) has to equal axis shape (%s) or 1."%(Pt.shape[:-1], A.shape[:-1]))
        Pt = np.tile(Pt, tuple(tile_repeat))
        
        idx = np.zeros_like(Pt)
        
        for index in np.ndindex(*Pt.shape[:-1]):
            idx[index] = get_lim_legacy(A[index], Pt[index]).reshape(Pt[index].shape).astype(object)
        idx[idx==-1] = None
        
        if inc_end:
            def _func(x):
                if x[-1]!=None:
                    x[-1] += 1
                    return x
                else:
                    return x
            np.apply_along_axis(_func, -1, idx)
        
        return idx

    def loaddTpList(self, dTpList, _transpose=False, _nolab=False):
        '''
        @param dTpList: A list or tuple containing the datatypes to be plotted.\
        Datatypes of the same instance can be passed as normal: datatype (str).\
        If you want the dataset to have a label, the costruction is a tuple: (datatype (str), label (str)).\
        Data of a different instance of libwaxs.Dataset is passed as a tuple: (instance, datatype(str)).\
        If you want the dataset to have a label, the costruction is: (instance, (datatype(str), label (str))).\
        External data is passed as a tuple: (x (array-like), y (array-like), z (array-like), datatype (str), <label (str)>)\
        Multiple datatypes, libwaxs.Dataset instances, and external datasets.\
        @type dTpList: list, tuple
        @param _nolab: Internal variable used to flag whether labels are ignored.\
        If false, the label is returned, if true, the dTp name is returned.\
        Used as a security measure in certain functions.\
        @type _nolab: bool
        '''
        
        if type(dTpList)==type(None):
            raise TypeError("dTp not specified")
        
        if type(dTpList)!=list:
            dTpList = [dTpList]
            #return TypeError "Datatypes must be passed as a list!"
        
        intrn = 0
        instn = 0
        extrn = 0
        
        zQuant= []
        zUnit = []
        xQuant= []
        xUnit = []
        yQuant= []
        yUnit = []
        zL = []
        xL = []
        yL = []
        dataset_label=[]
        
        if dTpList != None:
            for pt in dTpList:
                if not self.testdTpObj(pt,suppress=False):
                    raise TypeError('Datatype not valid!')
                elif (type(self.getdTp(pt,suppress=True))!=type(None) or (type(self.getdTp(pt[0],suppress=True))!=type(None))):
                    if type(pt)==str:
                        pt=(pt,None)
                    z = self.getdTp('%s'%(pt[0]))
                    #x = self.getdTp(self.getUnit('%s'%(pt[0])))
                    x = self.getdTp(self._asoc[pt[0]]['x'])
                    #bork
                    y = self.getdTp(self._asoc[pt[0]]['y'])

                    #if self.getUnit('%s'%(pt[0]))=='AU':
                        #y = np.zeros((z.shape[-1]))
                    #else:
                        #y = self.t

                    zQuant.append(self.getQuant('%s'%(pt[0])))
                    zUnit.append(self.getUnit('%s'%(pt[0])))

                    xQuant.append(self.getQuant(self._asoc[pt[0]]['x']))
                    xUnit.append(self.getUnit(self._asoc[pt[0]]['x']))

                    yQuant.append(self.getQuant(self._asoc[pt[0]]['y']))
                    yUnit.append(self.getUnit(self._asoc[pt[0]]['y']))

                    if type(pt[1])!=type(None) and _nolab==False:
                        dataset_label.append(pt[1])
                    else:
                        dataset_label.append(pt[0])
                    intrn +=1
                elif isinstance(pt, collections.Iterable) and isinstance(pt[0], libwaxs.Dataset) and len(pt)>=2:
                    for Pt in pt[1:]:
                        if type(Pt)==str:
                            Pt=(Pt,)
                        z = pt[0].getdTp('%s'%(Pt[0]))
                        #x = pt[0].getdTp(pt[0].getUnit('%s'%(Pt[0])))
                        x = pt[0].getdTp(pt[0]._asoc[Pt[0]]['x'])
                        y = pt[0].getdTp(pt[0]._asoc[Pt[0]]['y'])

                        #if pt[0].getUnit('%s'%(Pt[0]))=='AU':
                            #y = np.zeros((z.shape[-1]))
                        #else:
                            #y = pt[0].t

                        zQuant.append(pt[0].getQuant('%s'%(Pt[0])))
                        zUnit.append(pt[0].getUnit('%s'%(Pt[0])))

                        xQuant.append(pt[0].getQuant(pt[0]._asoc[Pt[0]]['x']))
                        xUnit.append(pt[0].getUnit(pt[0]._asoc[Pt[0]]['x']))

                        yQuant.append(pt[0].getQuant(pt[0]._asoc[Pt[0]]['y']))
                        yUnit.append(pt[0].getUnit(pt[0]._asoc[Pt[0]]['y']))

                        try:
                            if _nolab==False:
                                dataset_label.append(Pt[1])
                            else:
                                dataset_label.append(Pt[0])
                        except:
                            dataset_label.append(Pt[0])
                    instn +=1
                elif isinstance(pt, collections.Iterable) and not isinstance(pt[0], libwaxs.Dataset) and len(pt)>=3:
                    x = pt[0]
                    y = pt[1]
                    z = pt[2]
                    
                    if len(pt)>3:
                        zQuant.append(self.getQuant('%s'%(pt[3])))
                        zUnit.append(self.getUnit('%s'%(pt[3])))
                        xQuant.append(self.getQuant(self._asoc[pt[3]]['x']))
                        xUnit.append(self.getUnit(self._asoc[pt[3]]['x']))
                        yQuant.append(self.getQuant(self._asoc[pt[3]]['y']))
                        yUnit.append(self.getUnit(self._asoc[pt[3]]['y']))

                        try:
                            if _nolab==False:
                                dataset_label.append(pt[4])
                            else:
                                dataset_label.append(pt[3])
                        except:
                            dataset_label.append(pt[3])
                    else:
                        zQuant.append(None)
                        zUnit.append(None)
                        xQuant.append(None)
                        xUnit.append(None)
                        yQuant.append(None)
                        yUnit.append(None)
                    extrn +=1
                else:
                    print('libwaxs.datasetClass.loaddTpList: %s is not an understood datatype format!'%(type(pt)))
                
                x,y,z = self.std_dTp(x,y,z)
                
                if _transpose:
                    x,y,z = np.transpose(y, (0,2,1)),np.transpose(x,(0,2,1)),np.transpose(z,(0,2,1))
                    xUnit,yUnit = yUnit,xUnit
                    xQuant,yQuant = yQuant,xQuant
                    #bork

                zL.append(z)
                xL.append(x)
                yL.append(y)
                
        else:
            raise ValueError("Specifiy the data!")
        
        print("Found %s internal, %s libwaxs.datasetClass instance, and %s external datasets."%(intrn,instn,extrn))
        #bork
        return (xL,yL,zL,xQuant,yQuant,zQuant,xUnit,yUnit,zUnit,dataset_label)


    def getData(self, dTp=None, yPts=None, yLim=None, xPts=None, xLim=None, dPts=None, dLim=None, meta=False):
        '''
        Returns data for selected y points and x range. Also returns the actual y points.
        kwargs:
            yPts (list-like) : y points.
            xLim (list-like) : x range.
        '''

        xL,yL,zL,xQuantL,yQuantL,zQuantL,xUnitL,yUnitL,zUnitL,dTp_list = self.loaddTpList(dTp, _nolab=True)

        #assert yPts and yLim, "Either yPts or yLim kwarg needs to be defined."
        #assert xPts and xLim, "Either xPts or xLim kwarg needs to be defined."

        xLPout,yLPout,dOut,zOut = [],[],[],[]

        for i,z in enumerate(zL):

            x,y,z = self.std_dTp(xL[i],yL[i],z)
            d     = self.dep

            dIdx,dLP = find_closest(d, dPts),0. if type(dPts)!=type(None) else self._stdIdxLim(d, dLim, slice=True)
            dSlice = slice(*dIdx) if type(dPts)==type(None) else None
            dIdx   = np.arange(len(d))[dSlice] if type(dPts)==type(None) else dIdx
            dOut.append(d[dIdx])

            _xLPout,_yLPout = [],[]
            _zOut           = []

            for j in dIdx:

                xIdx,xLP = find_closest(x[j], xPts),0. if type(xPts)!=type(None) else self._stdIdxLim(x[j], xLim, slice=True)
                xSlice = slice(*xIdx) if type(xPts)==type(None) else xIdx

                yIdx,yLP = find_closest(y[j], yPts),0. if type(yPts)!=type(None) else self._stdIdxLim(y[j], yLim, slice=True)
                ySlice = slice(*yIdx) if type(yPts)==type(None) else yIdx

                xo,yo,zo = x[j,xSlice,:],y[j,:,ySlice],z[j,xSlice,ySlice]
                _zOut.append(zo)
                _xLPout.append(xo)
                _yLPout.append(yo)

            zOut.append(_zOut)
            xLPout.append(_xLPout)
            yLPout.append(_yLPout)

        return (xLPout, yLPout, dOut, zOut)


    #def get_y_data(self, dTp=None, xPts=None, yLim=None, heat=False, reverseFT_kws=None):
        #'''
        #Not tested yet!
        #Returns data for selected x points and y range. Also returns the actual x points.
        
        #kwargs:
            #xPts (list-like) : x points.
            #yLim (list-like) : y range.
            #heat (bool)      : if True, returns heat data instead of data.
        #'''

        #if not isinstance(xPts, collections.Iterable):
            #raise TypeError("xPts needs to be list-like.")

        ##if not isinstance(yLim, collections.Iterable):
            ##raise TypeError("yLim needs to be list-like.")

        #yIdx,yLim = fc.stdIdxLim(self.t, yLim, slice=True)
        #ySlice = slice(*yIdx)

        #if type(reverseFT_kws)!=type(None):
            #if type(reverseFT_kws)!=dict:
                #raise TypeError("reverseFT_kws must be None or dict type. Not {0}.".format(type(reverseFT_kws).__name__))
            #else:
                #_dat_r,r = self.reverseFT(_dat, **reverseFT_kws)

                #xIdx,xPts = fc.stdIdxLim(r, xPts)

                #datOut, yOut = _dat_r[xIdx, ySlice], self.t[ySlice]

        #else:

            #xIdx,xPts = fc.stdIdxLim(self.q, xPts)

            #datOut, yOut = _scr_argsdat[xIdx, ySlice], self.t[ySlice]

        #return (datOut, yOut, xPts)


    def getAxisDeps(self, axis):
        D={}
        test=[]
        for candidate in axis:
            if self.testdTpObj(candidate, suppress=True):
                dTpL = []
                for key in list(self._asoc.keys()):
                    if self._asoc[key]['x']==candidate:
                        dTpL.append(key)
                        test.append('x')
                    if self._asoc[key]['y']==candidate:
                        dTpL.append(key)
                        test.append('y')
                D.update({candidate : dTpL})
            else:
                axis.remove(candidate)
        return (D,test)


    def getdTpDeps(self, dTp, axis=None):
        """
        one dTp at a time...
        """
        if type(axis)==type(None):
            raise TypeError("\'axis\' must be specified.")
        #elif axis!='x' or axis!='y':
            #raise TypeError("\'axis\' must be eith \'x\' of \'y\', not {0}.".format(axis))
        
        if not isinstance(dTp, collections.Iterable) and type(dTp)!=str:
            raise TypeError
        elif type(dTp)==str:
            dTp=[dTp]

        D={}
        test=[]
        for el in dTp:
            for ax in list(self._asoc[el].keys()):
                if axis==self._asoc[el][ax]:
                    test.append(ax)
                    D.update({self._asoc[el][ax] : [el],})
        #bork
        return (D,test)


    def getAxis(self, dTp, axis=None):
        if type(axis)==type(None):
            raise TypeError("\'axis\' must be specified.")

        if not isinstance(dTp, collections.Iterable) and type(dTp)!=str:
            raise TypeError
        elif type(dTp)==str:
            dTp=[dTp]

        AX = []

        for el in dTp:
            if str(axis) not in self._asoc[el]:
                raise KeyError("\'{0}\' is not an axis of \'{1}\'.".format(axis, el))
            else:
                AX.append(self.getdTp(self._asoc[el][axis]))

        return AX

    def getUnit(self,dTp,suppress=False):
        '''
        Function which retrieves the appropriate unit from the self._unit dictionary
        associated with the given datatype.
        '''
        #if not self.testdTpObj(dTp, suppress=False):
            #if suppress:
                #return None
            #else:
                #raise AttributeError('dTp %s not part of the instance %s!'%(dTp,self))

        try:
            return self._unit['%s'%(dTp)]
        except:
            if suppress:
                return None
            else:
                raise KeyError('Unit not found for %s !'%(dTp))

    def setUnit(self,dTp,unit,suppress=False):
        '''
        Function which sets the unit in the self._unit dictionary
        associated with the given datatype.
        '''
        if not self.testdTpObj(dTp, suppress=False):
            if suppress:
                return None
            else:
                raise AttributeError('dTp %s not part of the instance %s!'%(dTp,self))

        if type(unit)!=str:
            raise TypeError("unit arg must be a str, not a {0}".format(type(unit).__name__))

        try:
            self._unit['%s'%(dTp)] = unit
        except:
            if suppress:
                return None
            else:
                raise KeyError('Unit not found for %s !'%(dTp))

    def getQuant(self,dTp,suppress=False):
        '''
        Function which retrieves the appropriate quantity from the self._quant dictionary
        associated with the supplied datatype.
        '''
        #if not self.testdTpObj(dTp, suppress=False):
            #if suppress:
                #return None
            #else:
                #raise AttributeError('dTp %s not part of the instance %s!'%(dTp,self))
        
        try:
            return self._quant['%s'%(dTp)]
        except:
            if suppress:
                return None
            else:
                raise KeyError('Quantity not found for %s !'%(dTp))

    def setQuant(self,dTp,quant,suppress=False):
        '''
        Function which sets the quant in the self._quant dictionary
        associated with the given datatype.
        '''
        if not self.testdTpObj(dTp, suppress=False):
            if suppress:
                return None
            else:
                raise AttributeError('dTp %s not part of the instance %s!'%(dTp,self))

        if type(quant)!=str:
            raise TypeError("quant arg must be a str, not a {0}".format(type(quant).__name__))

        try:
            self._quant['%s'%(dTp)] = quant
        except:
            if suppress:
                return None
            else:
                raise KeyError('Unit not found for %s !'%(dTp))

    def getdTp(self,dTp,suppress=False):
        '''
        Function which retrieves the appropriate attribute (generally data) from the instance
        associated with the supplied datatype.
        '''
        try:
            return getattr(self,dTp)
        except:
            if suppress:
                return None
            else:
                raise AttributeError('dTp %s not part of the instance %s!'%(dTp,self))

    def genLab(self, dTp):

        qnt = self.getQuant(dTp)
        unt = self.getUnit(dTp)
        
        lab = r'${0}\ ({1})$'.format(qnt, unt)
        
        return (lab, qnt,unt)

    def gi(self, mode='abridged'):
        '''
        Helper function for when you get lost.
        Prints the dataypes present in the instance, and their assocaited quantities and units.

        Kwargs:
            mode    (str) : can adopt the values 'abridged' (default) and 'all'.

        '''
        if mode == 'all':
            hdr = "{0:35}|{1:10}|{2:20}|{3:20}|{4:30}|{5:30}".format('Attribute:', 'Type:', 'Assoc. x:', 'Assoc. y:', 'Quantity:', 'Unit:')
            print(hdr)
        elif mode == 'abridged':
            hdr = "{0:35}|{1:10}|{2:20}|{3:20}".format('Attribute:', 'Type:', 'Assoc. x:', 'Assoc. y:')
            print(hdr)
        print("".join(['_' for i in range(len(hdr))]))
            
        for att,data in sorted(list(self.__dict__.items()),key=str):
            if hasattr(self,att):
                if mode == 'all':
                    print(r"{0:35}|{1:10}|{2:20}|{3:20}|{4:30}|{5:30}".format(att,type(data).__name__,self._asoc[att]['x'] if att in self._asoc else '',
                                                                              self._asoc[att]['y'] if att in self._asoc else '',
                                                                              self.getQuant(att,suppress=True) if self.getQuant(att,suppress=True) != None else '',
                                                                              self.getUnit(att,suppress=True) if self.getUnit(att,suppress=True)  != None else ''))
                elif mode == 'abridged':
                    if att[0] == '_':
                        continue
                    else:
                        print(r"{0:35}|{1:10}|{2:20}|{3:20}".format(att,type(data).__name__,self._asoc[att]['x'] if att in self._asoc else '',
                                                                    self._asoc[att]['y'] if att in self._asoc else ''))
                #print("Location: {0:20} Revision {1}".format(Location,Revision))
            else:
                continue
        return None


    def testdTpObj(self,Obj, suppress=True):
        '''
        Tests whether a Obj is a valid datatype format.
        Do not supply the container (list ), but the individual tupples, or str if the datatype is from the same instance.
        '''
        if type(Obj)==list:
            if suppress:
                return False
            else:
                raise TypeError("testdTpObj() takes the individual datatype or datatype tuples, not a(n) %s!"%(type(dTp)))
        elif type(Obj)==tuple:
            for i,dTp in enumerate(Obj):
                if type(dTp)==str:
                    if hasattr(self,dTp.strip()) and type(Obj[i+1])==str:
                        return True
                    else:
                        if suppress:
                            return False
                        else:
                            raise TypeError("Invalid dTp type, %s not part of %s!"%(dTp, self))
                
                elif isinstance(dTp, libwaxs.Dataset):
                    test=[]
                    for i,el in enumerate(Obj[1:]):
                        if type(el)==tuple and len(el)==2:
                            if hasattr(dTp,el[0].strip()):
                                test.append(True)
                            else:
                                test.append(False)
                        elif type(el)==str:
                            if hasattr(dTp,el.strip()):
                                test.append(True)
                            else:
                                test.append(False)
                        else:
                            if suppress:
                                return False
                            else:
                                raise TypeError("Invalid dTp type, not str but %s!"%(type(dTp)))
                        if np.asarray(test).all():
                            return True
                        else:
                            if suppress:
                                return False
                            else:
                                raise TypeError("Invalid dTp type, not str but %s!"%(type(el[[j for j, elem in enumerate(test, 1) if not elem]])))

                elif type(dTp)!=str and isinstance(dTp, collections.Iterable) and len(Obj)>=3: # Needs a revision
                        if Obj[2].shape[1:]==(Obj[0].shape[1], Obj[2].shape[2]):
                            return True
                        else:
                            if suppress:
                                return False
                            else:
                                raise TypeError("Shape %s and %s and %s do not match! Not a valid data type"%(Obj[0].shape,Obj[1].shape,Obj[2].shape))

                else:
                    if suppress:
                        return False
                    else:
                        raise TypeError("Invalid dTp type, not str but %s!"%(type(dTp)))

        elif isinstance(Obj, str):
            dTp = Obj.strip()
        
        if hasattr(self,dTp):
            if type(self.getQuant(dTp, suppress=True))!=type(None) and type(self.getUnit(dTp, suppress=True))!=type(None):
                return True
            else:
                return False
        else:
            if suppress:
                return False
            else:
                raise TypeError("%s: Invalid dTp type, not str but %s!"%(dTp, type(dTp)))


    def gen_dTp(self, dTp, z, like_dTp=None, quant=None, unit=None,
                axis=False, xAxis=None, yAxis=None, suppress=False,
                ovrWrt=False):
        '''
        Generates a standard datatype (data, quantity, and unit).
        Can be used to overwrite existing datatypes.
        '''
        if self.testdTpObj(dTp, suppress=True) and not ovrWrt:
            if suppress:
                return None
            else:
                print(UserWarning('*** Datatype %s already exists! ***\n*** Not generating datatype. ***'%(dTp)))
                if np.asarray(getattr(self, '%s'%(dTp))==z).all():
                    return None
                else:
                    raise ValueError('!!! Existing \'%s\' and new datatypes differ !!!\n!!! You need to sort out this mess manually. !!!'%(dTp))

        elif type(like_dTp)!=type(None) and (not self.testdTpObj(dTp, suppress=True) or ovrWrt):
            self._unit[dTp]  = self._unit[like_dTp]
            self._quant[dTp] = self._quant[like_dTp]

            if not axis:
                #assert type(yAxis)==str, "\'yAxis\' must be a {0}, not \'{1]\'.".format(str.__name__, type(yAxis).__name__)
                yAxis = yAxis if type(yAxis)==str else ('t_bin' if 'bin' in dTp else 't')
                xAxis = xAxis if type(xAxis)==str else self._unit[dTp]
                self.buildAsoc(dTp=dTp, dTpX=xAxis, dTpY=yAxis)

            if self.testdTpObj(dTp, suppress=True) and ovrWrt:
                print(UserWarning('Warning: Over writing existing datatype %s.'%(dTp)))

        elif type(quant)!=type(None) and type(unit)!=type(None) and (not self.testdTpObj(dTp, suppress=True) or ovrWrt):
            self._unit[dTp]  = r'%s'%(unit)
            self._quant[dTp] = r'%s'%(quant)

            if not axis:
                #assert type(yAxis)==str, "\'yAxis\' must be a {0}, not \'{1]\'.".format(str.__name__, type(yAxis).__name__)
                yAxis = yAxis if isinstance(yAxis, str) else ('t_bin' if 'bin' in dTp else 't')
                xAxis = xAxis if isinstance(xAxis, str) else self._unit[dTp]
                self.buildAsoc(dTp=dTp, dTpX=xAxis, dTpY=yAxis)

            if self.testdTpObj(dTp, suppress=True) and ovrWrt:
                print(UserWarning('Warning: Over writing existing datatype %s.'%(dTp)))
            
        else:
            if suppress:
                return None
            else:
                raise ValueError('!!! You need to specify either like_dTp or quant and unit for the generation of %s !!!'%(dTp))

        return setattr(self, '%s'%(dTp), z)


    def buildAsoc(self, dTp=None, dTpX=None, dTpY=None, reset=False):

        if type(dTp)==type(None):
            if reset and hasattr(self, '_asoc'):
                delattr(self, '_asoc')
            elif not hasattr(self, '_asoc'):
                self._asoc = {}

            dTpL  = []
            dTpX = []
            dTpY = []
            attributes = list(self.__dict__.keys())
            for candidate in attributes:
                qnt,unt = self.getQuant(candidate, suppress=True),self.getUnit(candidate, suppress=True)
                if type(qnt)!=type(None) and type(unt)!=type(None):
                    if qnt==candidate:
                        dTpX.append(candidate)
                    elif 'delay' in qnt:
                        dTpY.append(candidate)
                    else:
                        dTpL.append(candidate)

        else:
            if reset:
                delattr(self, '_asoc')

            if type(dTp)==str:
                dTpL = [dTp]
            if type(dTpX)==str:
                dTpX=[dTpX]
            if type(dTpY)==str:
                dTpY=[dTpY]

            for el in dTpL:
                assert type(self.getQuant(el, suppress=True))!=type(None), "{0} does not exist.".format(el)
            for ex in dTpX:
                assert type(self.getQuant(ex, suppress=True))!=type(None), "{0} does not exist.".format(ex)
            for ey in dTpY:
                assert type(self.getQuant(ey, suppress=True))!=type(None), "{0} does not exist.".format(ey)

        for i,el in enumerate(dTpL):
            if type(dTp)!=type(None):
                xl = dTpX[i]
                yl = dTpY[i]
            else:
                if 'bin' in el:
                    dTpXl = np.asarray([ex if 'bin' in ex else 'None' for ex in dTpX])
                    dTpYl = np.asarray([ex if 'bin' in ex else 'None' for ex in dTpY])
                else:
                    dTpXl = np.asarray(['None' if 'bin' in ex else ex for ex in dTpX])
                    dTpYl = np.asarray(['None' if 'bin' in ex else ex for ex in dTpY])
                xl = dTpXl[dTpXl!='None'][0]
                yl = dTpYl[dTpYl!='None'][0]

            self._asoc.update({el:{'x':xl,
                                   'y':yl}})

        return

    
    def QonveRt(self,x, value=False):
        """
        Convert between q-space (reciprocal space (Å-1)) and r-space (real space (Å)).
        r = 2.pi/q
        """
        if type(x)==str:
            if hasattr(self, x):
                x = getattr(self, x)
            else:
                raise AttributeError('%s is not an attribute of %s.'%(x,self))
        
        elif isinstance(x, collections.Iterable) and type(x)!=str:
            x = np.asarray(x)
        else:
            raise TypeError('Variable type (%s) passed cannot be converted.'%(type(x)))
        
        xi = np.nan_to_num(x)
        #if np.where(xi==0)[0]
            #if xi[0]>xi[-1]:
                #idx = int(np.where(xi==0)[0])-1
                #xi = xi[:idx]
            #else:
                #idx = int(np.where(xi==0)[0])+1
                #xi = xi[idx:]
        
        #inverse scale remember
        if not value:
            x_conv = np.asarray(np.linspace(2*np.pi/xi.max(), 2*np.pi/xi.min(),num=len(x)).reshape(x.shape))
        else:
            x_conv = 2*np.pi/x

        return x_conv


    @staticmethod
    def plotTimeResolved(title, *args, **kwargs):
        """
        Static method (a method which belongs to the class but isn't bound to an instance)
        which takes a number of Dataset objects, and plots them side by side,
        with the timepoints properly aligned.
        @param title: A title to give the plot
        @type title: str
        @param args: Datasets to plot.
        @type args: Dataset
        @keyword timeSlices: Optionally slice up the runs in the same way, otherwise all timepoints are shown as in normal pump-probe.
        @return: The figure and axes.
        @keyword offset: Offsets between curves.
        @type offset: float, list
        @rtype: matplotlib.figure.Figure, matplotlib.axes.AxesSubplot
        """

        N = len(args) # number of data sets to plot side-by-side

        fig, ax = plt.subplots(nrows=1, ncols=len(args), sharex=True, sharey=True, figsize=[4*N,6])
        # ugly solution to handle the special case of one Dataset object...
        try:
            [a.set_yticks([]) for a in ax]
        except TypeError:
            ax.set_yticks([])
        fig.canvas.set_window_title(title)

        if 'offset' in kwargs:
            offset = kwargs['offset']
        else:
            offset = .01

        # Should we bin timepoints?
        if 'timeSlices' in kwargs:
            import colorsys
            times = kwargs['timeSlices']
            cols = [colorsys.hls_to_rgb(h*1.0/(len(times)-1), .45, .6) for h in range(len(times)-1)]
            for n in range(N):
                segments = args[n].timeSegments(times=times, offsets=offset)
                for i in range(len(times)-1):
                    ax[n].plot(args[n].q, segments[:,i], color=cols[i])

        # Otherwise, each timepoint is plotted, properly aligned between Datasets.
        else:
            # get lists of unique delays, plot offsets and colors
            dtlist = [args[i].t for i in range(N)]
            dt, offsets, colors = delays(*dtlist)

            for n in range(N):
                for i in range(len(args[n].t)):
                    offset_ = offsets[args[n].t[i]]*offset
                    col     = colors[args[n].t[i]]
                    # ugly solution to handle the special case of one Dataset object...s
                    try:
                        tmp = ax[n]
                    except TypeError:
                        tmp = ax
                    tmp.plot(args[n].q, args[n].dS[:,i] + offset_, color=col)
                    tmp.plot(args[n].q, args[n].q*0.0 + offset_, ':', color=col)
                    textparams = {'ha':'right', 'fontsize':6}
                    tmp.text(.98, offset_, nsToString(args[n].t[i]), **textparams)
        return fig, ax


    @staticmethod
    def _axGeom_cont(n, p, sameZscale=False, **kwargs):

        n = int(n)
        p = int(p)

        m = n if n%p==0 else n+(p-1)

        f = 2

        if 'figsize' in kwargs:
            figsize=kwargs['figsize']
        else:
            figsize=(1+p*3.,1.5+f*m//p)

        fig  = plt.figure(figsize=figsize)

        ax    = np.empty((m//p,p), dtype=object)
        o   = 5
        chk = 49
        s   = 1
        gs   = matplotlib.gridspec.GridSpec(o+(chk+s)*(m//p),(chk)*(p)+s*(p-1))

        for i in range(m//p):
            for j in range(p):
                sv = (o+s)+(chk+s)*i
                ev = (o+s)+(chk)*(i+1)+i*s
                sh = (chk+s)*j
                eh = (chk)*(j+1)+j*s

                print('{0}: h {1} {2}; v {3} {4}'.format(i, sv, ev, sh, eh))
                ax[i,j] = plt.subplot(gs[sv:ev, sh:eh])

        if sameZscale:
            cbax    = np.empty((1,),dtype=object)
            cbax[0] = plt.subplot(gs[o-2:o,:])
        else:
            cbax    = np.empty((p,),dtype=object)
            for j in range(p):
                cbax[j] = plt.subplot(gs[o-2:o, sh:eh])

        [removeLines(cbaxi, is_cbar=True) for cbaxi in cbax]

        [set_panel(axi, panel_location='left') for axi in ax[:,0]]
        
        [set_panel(axi, panel_location='right') for axi in ax[:,-1]]

        l = ax.shape[0] - n
        if l!=0:
            removeTickLab(ax.ravel()[-l], axis='x')
            removeLines(ax.ravel()[-l])

        [removeTickLab(axi, axis='x') for axi in ax.ravel()[:-2-l]]
        [removeTickLab(axi, axis='y') for axi in ax[:,1:-1].ravel()]

        [removeTickLab(cbaxi, axis='y') for cbaxi in cbax]

        return (fig, ax, cbax)


    def save(self, filename):
        """
        Save Dataset object to a binary file.
        @param filename: Name of that file
        @type filename: str
        """
        fp = open(filename, 'wb')
        pickle.dump(self, fp)
        fp.close()

    def load(self, filename):
        """
        Load data from a binary file, as saved with save(), into the current Dataset object.
        @param filename: Name of that file
        @type filename: str
        """
        fp = open(filename, 'rb')
        data = pickle.load(fp)
        fp.close()
        # the variables have to be loaded one by one
        for key in list(data.__dict__.keys()):
            exec('self.%s = data.%s'%(key,key))

