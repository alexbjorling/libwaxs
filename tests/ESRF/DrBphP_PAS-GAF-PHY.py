### This script tests the libwaxs module using data from the ESRF.
###

import numpy as np
import matplotlib.pyplot as plt
import libwaxs

datadir = './'
runNumbers = [ 39 ]

### Iteratively create and process a list of Run_PP objects
runList = []
for nr in runNumbers:
    myRun = libwaxs.Run_PP(expID='esrf2013')
    myRun.load(datadir, nr)
    myRun.rejectS(tol='3percent', plot=False)
    myRun.normalize([1.4, 1.7])
    myRun.subtract()
    runList += [myRun]

### Merge all these Run_PP objects into a Dataset object
mydataset  = libwaxs.Dataset(runPpList=runList, average=True)
mydataset2 = libwaxs.Dataset(runPpList=runList, average=False)

### With a Dataset object you can easily make various useful plots, like an offset time slice plot...
plt.figure()
tsegments = mydataset.timeSegments(offsets=.01)
plt.plot(mydataset.q, tsegments)

### ...or a kinetic plot.
plt.figure()
qsegments = mydataset.qSegments(qranges=[[.08,.15],[.15,.25],])
plt.plot(mydataset.t, qsegments)

### ...or one of these multi-Dataset plots, which is a static method of the Dataset class:
fig, ax = libwaxs.Dataset.plotTimeResolved('PAS-GAF-PHY', mydataset, mydataset2, offset=.01)

### ...and you can find time traces of known signals, including error bars:
plt.figure()
import scipy.io
s = scipy.io.loadmat('sls_static_conv.mat')
static = libwaxs.Dataset(q=s['q'], dS=s['dS_CBDPHY_static'])
# using two basis vectors: the static signal and S(q)=const.
bases = np.hstack((static.dS, np.ones(static.dS.shape)))

timeEvol, mean, conf = mydataset2.timeEvolution(bases, q=static.q, qrange=[.1, .5])

plt.plot(np.log10(mydataset2.t*1e-6), timeEvol.T[:,0], 'ob')
plt.errorbar(np.log10(libwaxs.uniqueList(mydataset2.t*1e-6)), mean.T[:,0], yerr=conf.T[:,0], ecolor='r', color='r', linestyle='none', marker='o', elinewidth=2)

plt.show()
