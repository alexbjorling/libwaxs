### This script tests the libwaxs module using data from the SLS.
###

import numpy as np
import matplotlib.pyplot as plt
import libwaxs

datadir = './'
runNumbers = [ 443 ]

### Iteratively create and process a list of Run_RR objects
runList = []
for nr in runNumbers:
    myRun = libwaxs.Run_RR(expID='sls2014')
    myRun.loadmat(datadir, nr, freq=10)
    myRun.rejectS(tol='3percent', plot=False)
    myRun.normalize([1.4, 1.7])
    myRun.subtract()
    runList += [myRun]
    
### Merge all these Run_RR objects into a Dataset object
mydataset = libwaxs.Dataset(runRrList=runList)

### With a Dataset object you can easily make various useful plots, like an offset time slice plot...
plt.figure(1)
tsegments = mydataset.timeSegments(times=[0,1,2,3,4,5,6], offsets=.002)
plt.plot(mydataset.q, tsegments)

### ...or a kinetic plot.
plt.figure(2)
qsegments = mydataset.qSegments(qranges=[[.08,.15],[.15,.25],])
plt.plot(mydataset.t, qsegments)

### Save state data for another script:
segment = np.squeeze(mydataset.timeSegments(times=[1,6]))
#np.savez('Agp1_FL.npz', q = mydataset.q, dS = segment)

plt.show()
