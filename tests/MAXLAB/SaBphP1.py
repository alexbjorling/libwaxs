# This data is treated like pump-probe data, with times -1 and 1 (LED1/LED2).

import matplotlib.pyplot as plt
import libwaxs

# General settings
datadir = './'
outlierrange    = [1.7, 2.0]
outliertol      = '15percent'
normrange       = [1.4, 1.6]

# Here, a function iteratively creates, reads and processes a list of Run_PP objects, then returns a merged Dataset object.
def loadset(runNumbers):
    runList = []
    for nr in runNumbers:
        print "\nLoading scan %u"%nr
        myRun = libwaxs.Run_PP(expID='maxlab2014c')
        myRun.load(datadir, nr)
        myRun.rejectS(qrange=outlierrange, tol=outliertol, plot=False)
        myRun.normalize(qrange=normrange)
        myRun.subtract()
        runList += [myRun]
    return libwaxs.Dataset(runPpList=runList, average=True)

SaP1 =      loadset([18])
SaP1heat =  loadset([17])

# We can easily subtract a heating signal here,
SaP1.subtractHeat(SaP1heat.dS[:,1], qrange=[1.7, 2.1])

# As you can see, there's no heating signal left,
plt.plot(SaP1.q, SaP1.dS)
plt.show()