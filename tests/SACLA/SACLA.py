### This script tests the libwaxs module using data from SACLA.
###

import numpy as np
import matplotlib.pyplot as plt
import libwaxs

datadir = './'
runNumbers = [ 39 ]

### Iteratively create and process a list of Run_PP objects
runList = []
for nr in runNumbers:
    myRun = libwaxs.Run_PP(expID='sacla2015')
    myRun.load(datadir, nr)
    #myRun.rejectS(tol='3percent', plot=False)
    #myRun.normalize([1.4, 1.7])
    myRun.subtract(mode='toggle')
    runList += [myRun]

### Merge all these Run_PP objects into a Dataset object
mydataset  = libwaxs.Dataset(runPpList=runList, average=True)

### With a Dataset object you can easily make various useful plots, like an offset time slice plot...
plt.figure()
tsegments = mydataset.timeSegments(offsets=.01)
plt.plot(mydataset.q, tsegments)

plt.show()
