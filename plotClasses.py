#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import print_function
import numpy as np
import scipy.io
import matplotlib.pyplot as plt
import libwaxs
from helpers import *
import collections
import sys,os,re

import matplotlib
import matplotlib.pyplot as plt

class dataObject(object):
    def __init__(self, z, x=None, y=None):
        if z.shape != x.shape + y.shape:
            raise ValueError("z.shape != x.shape + y.shape")
        else:
            self.x = x
            self.y = y
            self.z = z

        return

    def set_unit(self):

        return

    return

class axisObject(object):
    def __init__(self, ax, unit=None, quantity=None)
    return


class plotBase(object):
    def __init__(self):
        return
    return
    
class contour(plotBase):
    def __init__(self):
        return
    
class stack(plotBase):
    def __init__(self):
        return
