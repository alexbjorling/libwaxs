"""
This is a set of Python classes which aids the reduction and analysis of time-resolved X-ray scattering data.
"""
from .datasetClass import Dataset
from .runClasses import Run_PP, Run_RR, Proc_PP, Spec_Run_PP, Spec_Proc_PP
from .helpers import * # some functions are useful to have in the analysis script
from .RCutils import *